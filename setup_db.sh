#Get latest DB
echo "Fetching database..."
scp -p textdata@80.241.222.199:dump$(date +'%d-%m-%Y').sql dump$(date +'%d-%m-%Y').sql
#Create DB
echo "Populating django_dinx database..."
mysqldump --single-transaction --extended-insert --user=django_user --pass=dinx django_dinx dump$(date +'%d-%m-%Y').sql
echo "Populated django_dinx database..."

