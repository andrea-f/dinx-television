#!/bin/sh
cd ../djangoApp/dinx/
python manage.py build_solr_schema > schema.xml
SOLRPATH=$1
if [ "$SOLRPATH" = "" ]; then
	echo "Copying schema.xml to default solr location..."
	cp schema.xml ../../../apache-solr/apache-solr-4.4.0/example/solr/conf
else
	echo "Path to SOLR: "
    echo $SOLRPATH
    SOLRCONF="$SOLRPATH/example/solr/conf"
    cp schema.xml $SOLRCONF
fi
python manage.py rebuild_index
# if error patch form and indexes
# run again

#Create PIP package
cd django-dinx-television 
python setup.py sdist 
#Install PIP package
cd dist
pip install -r requirements.txt django-dinx-television-0.1.tar.gz
cd ../djangoApp/dinx_config/
cp virtual.conf /etc/apache2/conf.d/


# Create sites-available with dinx configuration running at /dinx
# Symlink sites-available/dinx with sites-enabled
# reload apache

