import fileHandler
import logging
import nerClassification
import os, sys
import fnmatch
dinx = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', '..', '..', '..', 'django', 'dinx', 'db_operations'))
#print dinx
sys.path.append(dinx)
import update

#sys.path.append("/home/legionovainvicta/app/django/dinx")

class GeneratePeople:
    """This class adds PERSON to videos returned from video search."""

    def __init__(self, root, nerOutputFolder, nerParserLocation, results = None, folder = None, matchingFolder = None):
        """Initialization method for GeneratePeople class.

        :param root:
            Holds name of main topic to process, ie Film ITA or Film ENG, string.

        :param nerOutputFolder:
            Folder location where to save parsed NER XML files, string.

        :param nerParserLocation:
            Folder location containing .jar for NER parsing, currently using Stanford NER, string.

        :param results:
            Django DB object or list retrieved from database.

        :param folder:
            Directory location of JSON data to read and match, string.

        :param matchingFolder:
            Directory location of raw data to parse and match, string.

        """
        self.root = root
        self.results = results
        self.nerOutputFolder = nerOutputFolder
        #matchingFolder is location of raw data to parse and match
        self.matchingFolder = matchingFolder
        #where the downloaded data is contained
        self.folder = folder
        #location of ner parser
        self.parserLocation = nerParserLocation
        self.allParsedFolders = []
        self.fileListFolder = ""
        #dates = [event.date], names = [event.name]
        self.dates = []
        self.names = []

    def generator(self, results):
        """Persons location generator engine. Matches JSON event names with XML files parsed from NER to augment available information.

        :param results:
            Django DB object or JSON retrieved from the web with video information from :func:`DINX.run`.

        Return two variables with:
            * **savedItems** -- Total number of saved items after matching, int.
            * **u** -- Total number of matched files, int.

        """
        l = 0
        savedItems = 0
        u = 0
        try:
            self._generateDatesAndNames(results)
            defaultFileListLocation = "%s/filelist.txt" % (self.matchingFolder)
            filesToParse = self._checkAlreadyAvailable(defaultFileListLocation)
            if len(filesToParse) == 0:
                pass
            elif (filesToParse is None):
                defaultFileListLocation = self._getMatchingFileNames()
                l = self._parseFileInFileLists(defaultFileListLocation)
            #elif (len(filesToParse) > 0):
            #    l = self._parseFileInFileLists(defaultFileListLocation)
                 #print "[generator] Parsed %s files in %s" % (l, self.root)
            savedItems, u = self._matchParsedFilesWithEvents(results)
            print "[generator] Saved %s items for %s" % (savedItems, self.root)
        except Exception as e:
            print "[generator] Error: %s" % e
        return savedItems, u

    def _checkAlreadyAvailable(self, defaultFileListLocation):
        """Check which parsed xml exist to avoid duplicating parsing effort.

        :param defaultFileListLocation:
            Holds default directory name containing filelist.txt with list of files to NER parse, string.

        Return list with XML files which have not yet been parsed.
        
        """
        filesInFileList = []
        t = []
        xmlFiles = []
        print "[checkAlreadyAvailable] Filelist location: %s" % defaultFileListLocation
        if os.path.isfile(defaultFileListLocation):
            try:
                filehandler = fileHandler.fileHandler(fileName = defaultFileListLocation)
                filesInFileList = filehandler.readFileByLine()
                if filesInFileList is not False:
                    for file in os.listdir(self.nerOutputFolder):
                        if fnmatch.fnmatch(file, '*.xml'):
                            file = file.decode('ascii','ignore').replace('.txt.xml','')
                            xmlFiles.append(file)

                    for f in filesInFileList:
                        for title in xmlFiles:
                            if f.find(title) != -1:
                                print "[checkAlreadyAvailable] file to parse: %s, xmlFile: %s" % (f, title)
                                t.append(f)
                    for xmlfile in t:
                        if xmlfile in filesInFileList:
                            filesInFileList.remove(xmlfile)
                else:
                    return None                        
            except Exception as e:
                print "[checkAlreadyAvailable] Error in checking: %s" % e
                pass
        else:
            return None
        print "[checkAlreadyAvailable] Total retrieved files %s in %s" % (len(filesInFileList), defaultFileListLocation)
        return filesInFileList

    def _generateDatesAndNames(self, events):
        """Split retrieved events in two lists of names and dates.

        :param events:
            Django DB object or JSON retrieved from the web with video information.

        """
        dates = []
        names = []
        for event in events:
            if event.date.year not in dates:
                dates.append(event.date.year)
            if event.name not in names:
                names.append(event.name)
        self.dates = sorted(dates)
        self.names = names



    def _getMatchingFileNames(self):
        """Save filelist of file names matching event names to parse for NER

        Return string with file path of filelist to be parsed.
        """
        dates = self.dates
        names = self.names
        dates1 =[]
        try:
            filehandler = fileHandler.fileHandler(folders = self.matchingFolder)
            dirsList = filehandler.listDirs()
            #print dirs_list
            print "[getMatchingFileNames] Total directories: %s " % len(dirsList)
        except Exception as e:
            logging.info("[getMatchingFileNames] Couldnt open file %s" % e)
            print e
        dirsList = sorted(dirsList)
        filesInRoot = []
        try:
            for date in sorted(dates): # DATE WHICH WILL BE PARSED FROM RETRIEVED JSON FILES
                for directory in dirsList:
                    if str(date) in directory:
                        print "[getMatchingFileNames] Directory to be parsed: %s %s" % (directory, date)
                        filehandler = fileHandler.fileHandler(folders = directory)
                        fileNames = filehandler.listFilesInDir()
                        filesInRoot.append(fileNames)                        
        except Exception as e:
            logging.info("[getMatchingFileNames] Couldnt read file_names %s" % e)
            print "[getMatchingFileNames] Error in reading file_names %s " % e        
        try:
            fileList = []
            for fileNames in filesInRoot: # EVENTS BY YEAR
                print "[getMatchingFileNames] Matching file names with event names for  %s " % len(fileNames)
                for name in names:
                    for filename in fileNames:
                        filenameEncoded = filename.encode('ascii','ignore')
                        if len(name)>3 and name in filenameEncoded:
                            fileList.append(filename)                            
                print "[getMatchingFileNames] Matched  %s names" % len(fileList)
            c = self.matchingFolder
            try:
                fileListLocation = "%s/filelist.txt" % (c)
                if os.path.isfile(fileListLocation):
                    g = open(fileListLocation, 'a')
                else:
                     g = open(fileListLocation, 'w')
                     g.write('')
            except Exception as e:
                print "[getMatchingFileNames] Error in opening file %s" % e
            for oneFile in fileList:
                oneFile = "%s\n" % (oneFile.encode('utf-8'))
                g.write(oneFile)
            g.close()
            return fileListLocation
        except Exception as e:
            logging.info("[getMatchingFileNames] Error in generating filelist %s" % e)
            print "[getMatchingFileNames] Error in generating filelist %s" % e

    def _parseFileInFileLists(self, fileListLocation):
        """From the generated file list start ner parser and parse events in year.

        :param fileListLocation:
            File path of filelist to be parsed, file list is by year, contains files to parse in a specific year, string.

        Return string with directory location of parsed XML files.

        """
        print "[parseFileInFileLists] parsing selected downloaded file names to identify LOCATION PERSON..."
        outputFolder = ""
        try:
            nerclass = nerClassification.nerClassification(folder = self.nerOutputFolder, filelist = fileListLocation, nerParserLocation = self.parserLocation)
            outputFolder = nerclass.applyNer()                
        except Exception as e:
            print "[parseFileInFileLists] Error in NER extraction: %s" % e
        return outputFolder

    def _matchParsedFilesWithEvents(self,events):
        """ For every parsed folder, ie 1950, match all events in 1950.json.
            From every parsed file extract PERSON and LOCATION to match with an EVENT.

        :param events:
            Django DB object or JSON retrieved from the web with video information, class object.

        Return two variables with:
            * **y** -- Total number of saved items after matching, int.
            * **u** -- Total number of matched files, int.

        """
        y = 0
        n = 0
        u = 0
        #all_names_and_dates contains list of all event names and date and file info for a specific videodata folder: {[event x, event y], 1956, data/film/1956
        #all parsed folder contains each parsed folder location divided by year: for example: parsed_data/film/1945/
        try:
            filehandler = fileHandler.fileHandler(folders = self.nerOutputFolder)
            #file_list contains all file information in parsed folder
            fileList = filehandler.listFilesInDir()
            #name is name of event, names_and_date[0] is list of all events in specific json file, like 1945.json
            for event in events:
                name = event.name
                date = str(event.date.year)
                id = event.id
                for fileName in fileList:
                    fileNameEncoded = fileName.encode('ascii','ignore')
                    #if name in fileNameEncoded and date in fileNameEncoded:
                    if fileNameEncoded.find(name) != -1:
                        filehandler = fileHandler.fileHandler(fileName = fileName)
                        print "[matchParsedFilesWithEvents] extracting from %s " % fileName
                        try:
                            allSentences = filehandler.readXML()
                            allSentences1 = allSentences['all_sentences']
                        except Exception as e:
                            print "allSentences: %s" % e
                        try:
                            eventInfo = self._extractPersonsAndLocations(allSentences1)                            
                        except Exception as e:
                            print "extracpersonadlocation: %s" % e
                        try:
                            # Save matche items using update methods.
                            up = update.Update(root = self.root)
                            n = up.saveItemsByEvent(id,eventInfo)
                            y = y + n
                            u = u + 1
                            print "[matchParsedFilesWithEvents] Saved %s items for %s" % (n, name)
                        except Exception as e:
                            print "[matchParsedFilesWithEvents] Error in reading xml %s" % e
                            pass
        except Exception as e:
            print "[matchParsedFilesWithEvents] Error in matching names with filenames %s" % e
        return y,u

    def _extractPersonsAndLocations(self, all_sentences):
        """Extract person and location tags from input tokens grouped by sentences.
        
        :param all_sentences:
            Contains all_tokens in sentence, whichi in turn contain dict with token word and ner, list.
            
        Return dict with:
            * **locations** -- Holds all locations extracted from all sentences, list.
            * **people** -- Holds all people names extracted from all sentences, list.
            
        """
        eventInfo = {}
        try:        
            people = []
            locations = []
            for sentence in all_sentences:
                person = []
                location = []
                for token in sentence['all_tokens']:
                    #print token
                    if 'PERSON' in token['ner']:
                        #print "%s \n" % token['word']
                        person.append(token['word'])
                    elif 'LOCATION' in token['ner']:
                        location.append(token['word'])
                location = ' '.join(location)
                person = ' '.join(person)
                people.append(person)
                locations.append(location)
            eventInfo = {'locations':locations, 'people':people}        
        except Exception as e:
            print "[extractPersonsAndLocations] Error: %s " % e
        return eventInfo


    def _addToEvent(self, allMatchedInfo):
        """Add extracted information to event.

        :param allMatchedInfo:
            Holds tuple with item information and string with name in event name, list.

        Return int with total number of items added to all events.
        
        """
        f = 0
        try:
            for event in events:
                root = event.trunk.root
                name = event.name
                #date = event.date
                category = event.trunk.category
                for matchedInfo in allMatchedInfo:
                    itemInformation = matchedInfo[1]
                    matchedName = matchedInfo[0]
                    if name in matchedName:
                        for key in itemInformation.keys():
                            if 'people' in key:
                                itemType = 'PERSON'
                            elif 'locations' in key:
                                itemType = 'LOCATION'
                            for itemName in itemInformation[key]:
                                try:
                                    if len(itemName)>0:
                                        item = {'name':itemName, 'ner':itemType, 'category':category, 'root':root}
                                        save = add.SaveEventsInDatabase(root = self.root, itemInput = item)
                                        item = save.addItem()
                                        if self.root in root:
                                            event.items.add(item)
                                            f = f + 1
                                except Exception as e:
                                    logging.info("[addToEvent] Couldnt save items in event %s" % e)
                                    print "[addToEvent] Couldnt save people items in event %s" % e
        except Exception as e:
            print "[addToEvent] Error: %s" % e
        return f

    def _getEventNamesAndDates(self, folder = None):
        """Read input json files and extracts event names to provide additional info using ner.

        :param folder:
            Json files location to be read and subsequently parsed, string.

        Return list with all filelist locations to be parsed with ner
        """
        if folder is None:
            folder = self.folder
        try:
            readdata = readData(folders = self.folder)
            #folders_to_read always returns 1
            matchingFolder = readdata.scanFolderPath()
        except Exception as e:
            print "Error in reading directory %s" % e
        #print matchingFolder
        fileLists = []
        try:
            filehandler = fileHandler.fileHandler(folders = matchingFolder)
            file_list = filehandler.listFilesInDir()
            #print file_list
        except Exception as e:
            print "Error in listing files %s" % e
        try:
            self.all_names_and_dates = []
            #single_file is json file containing parsed events by year. for example 1956.json
            for single_file in file_list:
                filehandler = fileHandler.fileHandler(fileName = single_file)
                #single file containing list of events, in this case movie titles
                list_of_events = filehandler.readJson()
                list_of_events = list_of_events['info']
                names = []
                for event in list_of_events:
                    name = event['name']
                    date = event['date']
                    names.append(name)
                names_and_date = (names, date, single_file)
                self.all_names_and_dates.append(names_and_date)
                #print "NAMES %s" % str(names_and_date)
                file_list_location = self._getMatchingFileNames(names_and_date)
                fileLists.append(file_list_location)
        except Exception as e:
            logging.info(e)
            print "Error in getting json data %s" % e
        return fileLists  
                
class Event:
    def __init__(self, eventName, date):
        self.eventName = eventName
        self.date = date

def main():
    #self.folders = sys.argv[1]
    date = '1902'
    #event = Event(eventName = 'Viaggio nella Luna', date = '1902')
    folder = '/home/legionovainvicta/app/DINXengine/Software/Data/videos/Category:Films_by_year'
    matchingFolderWithPeopleOrLocations = '/home/legionovainvicta/app/DINXengine/Software/Data/downloaded/Category:Films_by_year'
    nerParserLocation = "/home/legionovainvicta/app/DINXengine/Software/Libraries/stanfordnlp"
    gp = generatePeople(category = 'movies',folder = folder, matchingFolder = matchingFolderWithPeopleOrLocations, nerParserLocation = nerParserLocation)
    items = gp.generator()        

if __name__ == "__main__":
    main()