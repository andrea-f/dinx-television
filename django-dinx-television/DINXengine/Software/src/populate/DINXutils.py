#! /usr/bin/python

# To change this template, choose Tools | Templates
# and open the template in the editor.

#Code provided by www.dinx.tv code is under bsd license
__author__="dinx.tv television"
__email__="info@dinx.tv"
__date__ ="$06-Mar-2013 22:18:13$"

class UtilitiesTools(object):
    """This class contains some common functions to manipulate string.

    """
    def __init__(self):
        """Does nothing"""

    def getDateFromName(self, entity):
        """Extracts four digits to match date in name.

        :param entity:
            Words to extract date as four digits from :class:`GenerateRedditData`, string.

        Return int with year information extracted from entity.
        """
        import re
        year = []
        try:
            #year = re.findall(r"\D(\d{4})\D", entity)
            year = re.findall(r"(\d{4})", entity)
            #print "name: %s year: %s" % (entity, len(year))            
        except Exception as e:
            pass
        try:
            return year[0]
        except Exception as e:
            return year

    def getVideoId(self, url):
        """Get video id.

        :param url:
            URL encoded information with fully qualified Youtube URL from :class:`GenerateRedditData`, string.

        Return string with newly formatted url for playback.

        """
        url = url.split('v=')
        videoId = url[1]
        print videoId
        url = "http://www.youtube.com/v/%s?version=3&f=videos" % videoId
        return url

if __name__ == "__main__":
    name = "whatever (1934)"
    url = "http://www.youtube.com/watch?v=9iBuPLpSSf0"
    ut = UtilitiesTools()
    year = ut.getDateFromName(name)
    url = ut.getVideoId(url)
    print exists
