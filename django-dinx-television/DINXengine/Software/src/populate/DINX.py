import sys
#import csv
import os
import logging
import ConfigParser
import logging
#import csv
import time
import imp
import json
import re
import traceback
import pprint
import DINXGenerateTextFromWWW
import fileHandler
import nerClassification
import generateEvents
#import generatePeople
import getReddit

class DINX(object):
   """Main DINX Singleton class."""
   
   # Private class variables   

   __inputResource = ""
   #__inputResource = "file:///home/legionovainvicta/app/populate/populate/paths.json"
   __inputType = ".json"

   __outputResource = ""
   __outputType = ".json"
   
   __dinxEnrichmentFile = ""
   
   # Variables related to config properties
   __dinxProperties = None
   __dinxPropertyMainSectionHeader = "DINX Properties"
   __requiredProperties = ['inputresource','enrichmentfile','outputresource'] # All property keys are automatically lowercased when read in

   __supportedInputTypes = ['.csv', '.json']
   __supportedOutputTypes = ['.json']
   __inputObject = {}
   # Information used by Transform file
   __processedInput = None  # List of all objects referenced by input
   #__enrichedOutput = None  # Output list of processed objects
   __instance = None
   
   def __init__(self, pathOption = 0):
      """Init function - does nothing.
      
      :param pathOption:
          Contains pointer to paths.json input file to fetch text data and video from, int.
      
      """
      self.data = []
      self.__processedInput = []
      self.pathOption = pathOption
      
   
   @staticmethod
   def getInstance():
      """Static function for creating instance of DINX singleton."""
      try:
         if DINX.__instance == None:
            DINX.__instance =  DINX()
            print "true"
      except:
         pass
      finally:
         DINX.__instanceCreationLock.release()         
      return DINX.__instance

   def readProperties(self,propertyFileLocation):
      """Reads DINX properties file, expanding env vars as it goes.
      
      :param propertyFileLocation:
        Location of DINX properties files, string.
      """
      logging.info("Reading properties " + propertyFileLocation)
      self.__dinxProperties = ConfigParser.ConfigParser()
      self.__dinxProperties.readfp(open(propertyFileLocation))
      properties = self.__dinxProperties.items(self.__dinxPropertyMainSectionHeader)
      # List all properties
      definedPropertyKeys = []
      for key, value in properties:
         definedPropertyKeys.append(key)
         #Ensure $DINXHOME reference gets expanded
         value = os.path.expandvars(value)
         self.__dinxProperties.set(self.__dinxPropertyMainSectionHeader,key,value)
         logging.info("{DINX}[readProperties] Property: " + key + " " + value)
      # Set class variables based on input properties
      self.__inputResource = self.__dinxProperties.get(self.__dinxPropertyMainSectionHeader,'inputResource')
      self.__nerParserLocation = self.__dinxProperties.get(self.__dinxPropertyMainSectionHeader,'nerParserLocation')
      self.__gDataLocation = self.__dinxProperties.get(self.__dinxPropertyMainSectionHeader,'gDataLocation')
      self.__downloadedDataLocation = self.__dinxProperties.get(self.__dinxPropertyMainSectionHeader,'downloadedDataLocation')
      self.__parsedDataLocation = self.__dinxProperties.get(self.__dinxPropertyMainSectionHeader,'parsedDataLocation')
      self.__videoDataLocation = self.__dinxProperties.get(self.__dinxPropertyMainSectionHeader,'videoDataLocation')
      self.__inputType = os.path.splitext(self.__inputResource)[1]
        
   def log(self, logFileLocation, logLevel = 'INFO'):
      """Sets up logging

      :param logFileLocation:
          Specifies location where to write log file, string.

      :param logLevel:
          Specifies level of log either: INFO, DEBUG, ERROR, string.
      """
      import logging
      logging.basicConfig(filename=logFileLocation, filemode='w',level=logging.INFO)
   
   def _readInputFile(self):
      """Reads input file, delegates to relavent function depending on protocal type and input file type."""
      file = None
      # Error checking first
      if self.__inputType not in self.__supportedInputTypes:
         logging.error("%s is not a currently supported extension [%s] - unable to reading input" % (self.__inputType, self.__supportedInputTypes))
         sys.exit(2)
      if ".json" in self.__inputResource:
         file = self._readLocalInputFile()
      else:
         logging.error("[readInputFile] %s is an unsupported output resource protocol type" % self.__inputResource)
         sys.exit(2)   
      if self.__inputType == ".json":
         self._processJSONInputFile(file)
      if self.__inputType == ".csv":
         self._processCSVInputFile(file)

   def _readLocalInputFile(self):
      """Reads file from local file system, returns file pointer. Does basic error checking
      as it goes. References class variables: __inputType, __supportedInputTypes.
      
      Return FilePointer object (or none if file creation was unsuccessful).
      """
      file = None
      self.__inputResource = self.__inputResource.replace("file://", "")

      try:
         file = open(self.__inputResource, "r")
         logging.info("[readLocalInputFile] Successfully opened input file [%s] for reading" % (self.__inputResource))
      except Exception, msg:
         logging.error("[%s] %s" % (self.__inputResource, msg))
         sys.exit(2)
      return file

   def _processJSONInputFile(self, file):
      """Processes json input file. Explicitly ignores non ascii characters. References class variable: __inputResource.
      
      :param file: File pointer to be read from, file object.      
      """
      if file != None:
         logging.info("Decoding input JSON file [%s]" % self.__inputResource)
         self.__processedInput = json.load(file)["Paths"]
         file.close()

   def _getOrCreateFolderReferences(self, dinxLocation):
      """Execute discovery of data.
      
      :param dinxLocation:
          Holds reference to dinx home path, string.
          
      Return tuple with 3 elements containing:
          * **fullyQualifiedRootFolderLocation** -- Contains full directory of dinx home folder.
          * **category** -- Category of requested input.
          * **self.onepath** -- Wikipedia page name to retrieve and create folders by.
      """
      self.onepath = self._buildPathsFromJSONfile()
      category = self.onepath['Category']
      logging.info(category)
      self.category = category
      self.is_category = 0
      self.language = self.onepath['Language']
      fullyQualifiedRootFolderLocation = ""
      filehandler = fileHandler.fileHandler(folders = self.onepath['Root'], dinxLocation = dinxLocation)
      filehandler.saveFolders()
      fullyQualifiedRootFolderLocation = dinxLocation['downloadedData']
      fullyQualifiedRootFolderLocation = "%s/%s" % (fullyQualifiedRootFolderLocation, self.onepath['Root'])
      folder_category_path = (fullyQualifiedRootFolderLocation, category, self.onepath,)
      return folder_category_path    
         
    
   def _buildPathsFromJSONfile(self, pathOption = None):
       """Builds path(folder structure) for query, if element is contained in JSON data then it is added in path,

       :param pathOption:
           Number of element in paths.json to analyze, int.

       Return dict with input keywords from paths.json.
       """
       if pathOption is None:
           pathOption = self.pathOption
       try:
           logging.info("Category: %s" % self.__processedInput[0]['Category'])
       except Exception as e:
           logging.info("error in printing category %s" % e)	
       return self.__processedInput[int(pathOption)]
    
 


   def run(self, inputOption, results = None, searchOption = None):
      """Runs DINX engine. Performs the following actions:
     
         1. Process input URL
         2. Load transform file
         3. Execute transform function for each line of input
         4. Add each transformed line to output collection
         5. Write output collection to file
         6. Exit
      
      :param inputOption:
          Number of element in paths.json to analyze, int.

      :param results:
          Django DB object or list retrieved from database.

      :param searchOption:
          Holds information on what operation to perform on inputOption, string.
          Options are:
          * **textdata** -- Retrieve textual information from wikipedia, saved in line separated sentences in folder names from paths.json
          * **ner** -- Apply Named Entity Recognition to downloaded text data. Save results in XML format.
          * **videositems** -- Retrieve videos associated with each inputted item, add to DB.
          * **videos** -- Retrieve videos for entities extracted during NER specified by input option.
          * **people** -- Match people names in parsed files with corresponding video, to increment information on specific video.
          * **items** -- Extract items, ie most common words, from videos using LCS and Lev. distance.
          * **reddit** -- Get videos from reddit fullyoutubemovies channel and save in DB.

      Return:
         None
      """
      # Process input file
      startProcessingTime = time.clock()      
      logging.info("{DINX}[run] Processing input %s" % self.__inputResource)
      #load json file with initial value
      self._readInputFile();
      #Only --nodate supported for now
      self.searchOption = searchOption
      self.inputOption = inputOption
      try:
         # Dynamically import transform file and invoke transform function
         logging.info("{ DINX } [run] Example data input object:\n%s" % pprint.pformat(self.__processedInput[0]))
         startProcessingTime = time.time()
         logging.info("{ DINX } [run] Running DINX")
         self.dinxLocation = dinxLocation = {"gDataLocation":self.__gDataLocation, "downloadedData":self.__downloadedDataLocation, "parsedData":self.__parsedDataLocation, "videoData":self.__videoDataLocation}
         #build search query - generateTextFromWWW.py
         r = self._getOrCreateFolderReferences(dinxLocation)         
         r = list(r)
         inputCategory = r[2]
         self.relatedWords = relatedWords =  inputCategory["RelatedWords"].split(',')
         self.eventsFolder = eventsFolder = r[0].replace('videos','downloaded')
         self.params = params = {"duration": inputCategory['Duration'], "minimum_score": inputCategory['Score']}
         self.category = category = inputCategory["Category"]
         self.root = root = inputCategory['Root']
         logging.info("{ DINX } [run] Building %s index..." % inputOption)
         if "textdata" in self.inputOption:
             try:
                 txt = DINXGenerateTextFromWWW.GenerateTextFromWWW(jsonInputFile = self.__processedInput[0], dinxLocation = dinxLocation)
                 links = txt.walkPath()
                 print "{ DINX } [run]  Total links : %s" % links
                 
             except Exception as e:
                 logging.info("{DINX}[textdata] Error in generating text: %s" % e)
                 print "{DINX}[textdata] Error in generating text: %s" % e

         elif "ner" in self.inputOption:
             nerclassification = nerClassification.nerClassification(folder = r[0], category = r[1], path = r[2], nerParserLocation = self.__nerParserLocation)
             nerclassification.setHomeDir()
             logging.info("number of events: %s" % len(all_years))
             logging.info(all_years[0])

         elif "videositems" in self.inputOption:
             self._searchItems(results)

         elif "videos" in self.inputOption:
             logging.info("[DINX] Getting videos...")
             nerclassification = nerClassification.WordsToEvents(inputToProcess = r)
             allYears = nerclassification.decideParsingScheme()
             generateevents = generateEvents.GenerateEvents(
                 years = allYears,
                 category = category,
                 dinxLocation = dinxLocation,
                 relatedWords = relatedWords,
                 root = root,
                 eventsFolder = eventsFolder,
                 params = params
             )
             events = generateevents.createItemsFromEvents()
             print "[DINX] Total events: %s" % len(events)
             generateevents.generateVideoData(events)

         elif "people" in self.inputOption:
             gp = generatePeople.generatePeople(category = r[1], folder = r[0], nerParserLocation = self.__nerParserLocation)
             items = gp.generator()

         elif "items" in self.inputOption:
             gi.generateItems()
             itemsFromVideoInfo = gi.groupByWord()

         elif "reddit" in self.inputOption:
             redditdata = getReddit.getReddit()
         
         logging.info("{ DINX } [run] Finished processing input in %dms" % ((time.time() - startProcessingTime) * 1000))
      except Exception as msg:
         print "{ DINX } [run] Error in DINX.py %s" % msg
         logging.error(msg)
         exc_type, exc_value, exc_traceback = sys.exc_info()
         error = "{ DINX } [run] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
         logging.error(error)
         sys.exit(2)

   def _searchItems(self, results):
        """Searches for videos starting from already present db data.
        Data used for searching more videos is item name(location or person)

        :param results:
          Django DB object or list retrieved from database.
        """
        allYears = []
        allItems = []
        allDates = []
        for event in results:
            date = event.date.year
            itemsObject = event.items.all()
            items = []
            for itemObj in itemsObject:
                if (itemObj.name not in allItems) and (len(itemObj.name)>3):
                    items.append(itemObj.name)
            if date in allDates:
                for year in allYears:
                    if date == int(year['year']):
                        year['events_in_file'] = year['events_in_file'] + items
            else:
                itemsByYear = {'year': str(date), 'events_in_file':items,'helper_words': self.relatedWords}
                allYears.append(itemsByYear)
                allDates.append(date)
        generateevents = generateEvents.GenerateEvents(
            years = allYears,
            category = self.category,
            dinxLocation = self.dinxLocation,
            relatedWords = self.relatedWords,
            root = self.root,
            eventsFolder = self.eventsFolder,
            params = self.params,
            option = self.searchOption
        )
        events = generateevents.generateEventByItem()
        print "[DINX] Total events: %s" % len(events)
        generateevents.generateVideoData(events)

