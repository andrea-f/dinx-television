import json
from jsonrpce import ServerProxy, JsonRpc20, TransportTcpIp
from pprint import pprint


class StanfordNLP:
    def __init__(self):
        self.server = ServerProxy(JsonRpc20(),
                                  TransportTcpIp(addr=("127.0.0.1", 3456)))    
    def parse(self, text):
        return json.loads(self.server.parse(text))

#nlp = StanfordNLP()
#result = nlp.parse("Hello world!  It is so beautiful. Rome Lazio")
#sents = result['sentences']
#for sent in sents:
#    for word in sent['words']:
#        if len(word[1]['NamedEntityTag'])>1:
#            pprint(word[0])
#            pprint(word[1]['NamedEntityTag'])
#from nltk.tree import Tree
#tree = Tree.parse(result['sentences'][2]['parsetree'])
#pprint(tree)
