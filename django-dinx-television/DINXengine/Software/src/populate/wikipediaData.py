# coding: utf-8
import DinxHTTPUtils
import fileHandler
import re
import simplejson
from urllib import urlencode
import locale
import logging

class wikipediaData:
        """Get content from Wikipedia"""

        def __init__(self, depth = None, query=None, rawData = None, language = None):
            """Initializes wikipediaData class.
            
            :param depth:
                Specifies how many links to explore from found pages, int.
                
            :param query:
                Specifies which page to retrieve from Wikipedia, string.
            
            :param rawData:
                Holds HTML with all page informatino, string.
            
            :param language:
                Which language, either it or eng to use when fetching data from Wikipedia, string.
                
            """
            #allUrlTextfromPage = list of all dictionaries of links and text in XML page
            self.query = query
            self.rawData = rawData  
            self.depth = depth
            self.language = language
        
        def downloadWikipediaDirect(self, wikiResponseFile = '/home/legionovainvicta/app/bitbucket/dinx-television/django-dinx-television/DINXengine/Software/src/Data/indexes/wikipediaQueries.txt', query = "Serie A", wikiQueryFile = '/home/legionovainvicta/app/DINXengine/Software/src/populate/wikipediaQueries.txt'):
            """Download Wikipedia Page from URL.

            :param wikiQueryFile:
                Which file to use to read all Wikipedia queries to run, string.

            :param query:
                Specifies which page to retrieve from Wikipedia, string.

            :param wikiResponseFile:
                Specifies where to save every query after it has been run.

            Return string with page fetched from Wikipedia.
            
            """
            query = self.query
            dinxhttputils = DinxHTTPUtils.DinxHTTPUtils()
            try:
                f = open(wikiQueryFile, 'a')
                fh = fileHandler.fileHandler(fileName = wikiQueryFile)
                outlist = fh.readFileByLine()
            except:
                outlist = []
            if query not in outlist:
                querySave = "%s\n" % str(query)
                print "eee: " + querySave
                try:
                    f.write(querySave)
                except:
                    pass
                query1 = ""
                if "Categor" in query:
                    query1 = urlencode({'action':'query','list':'categorymembers','cmlimit':500, 'format':'json', 'cmtitle':query})
                    logging.info("[downloadWikipediaDirect] Processing a category...")
                else:
                    query1 = urlencode({'action':'query','rvprop':'content','prop':'revisions','format':'json','titles': query})
            urlRoot = "http://%s.wikipedia.org/w/api.php?%s" % (self.language,query1)
            result = dinxhttputils.executeUrlService(url=urlRoot)
            try:
                f = open(wikiResponseFile, 'w')
                f.write(str(result))
            except:
                pass
            return result

        def exploreWikipediaJSON(self, rawData = "page from wikipedia with HTML stuff", uniqueLinks = []):
            """Use JSON for information retrieval and Wikipedia API.
            Uses path on Wikipedia data returned in json format.
            Run query on API, then find links according to path and year.
            Save resulting category index follow links in order, save followed pages.

            :param rawData:
                Holds HTML with all page informatino, string.

            :param uniqueLinks:
                Holds reference to all unique text items/links found in returned Wikipedia page, list.

            Return list with unique links cleaned from strange characters found in downloaded Wikipedia page.
            
            """
            rawData = self.rawData
            allLinks = []
            outlist = []
            try: #get to content of json feed
                dataList = rawData['query']['categorymembers']
                out = []
                for item in dataList:
                    if item['title'] not in out:
                        out.append(item['title'])
                        allLinks.append(item['title'])     
            except Exception as e:
                logging.info("[exploreWikipediaJSON] Processing pages...")
                try:
                    dataList = dict(rawData['query']['pages'])
                except Exception as e:
                    logging.info("[exploreWikipediaJSON] No pages found: %s" % rawData['query'])
            try:
                for item in dataList:
                    logging.info("[exploreWikipediaJSON] Type of item: %s" % item)
                    try:
                        pageData = dataList[item]['revisions'][0]['*'] #pageData is returned wikipedia article inside json feed.
                        
                        logging.info("[exploreWikipediaJSON] pageData: %s" % pageData)
                        links = self.getSpecificData(pageData)
                        for item in links:
                            for char in item:
                                if char in "[?!/;]":
                                    item = item.replace(char,'')                                    
                                    if item not in allLinks:
                                        print item
                                        allLinks.append(item)
                    except Exception as e:
                        print "[exploreWikipediaJSON] Adding category : %s" % item['title'] 
                        allLinks.append(item['title'])
                        pass
            except Exception as e:
                print "[exploreWikipediaJSON] Unable to fetch page %s" % e
                pass  
            for x in allLinks:
                if x not in outlist:
                    outlist.append(x)
            for y in outlist:
                locale.getdefaultlocale() 
                y = y.encode('utf-8')
                y = y.replace(r'\xe2\x80\x93','-')
                y = y.replace('-','–')
                #STRANGE CHARACTER –
                uniqueLinks.append(y)
            return uniqueLinks

        def getSpecificData(self, text, regEx = r'\[[^|\]]*'):
            """Extracts all information in pattern from text.

            :param regEx:
                Has reg expression to filter data by, string.
                * **Links** -- r'\[[^|\]]*'
                * **Sentences** -- 

            :param text:
                Data to be filtered by pattern, string.

            Return list with all occurrences of info extracted from text.
            """
            links = []
            pattern = re.compile(regEx)
            links = re.findall(pattern, text)
            return links



def main():
    #use sysarg to set folder name
    try: 
        inputQuery = sys.argv[1]
    except Exception as e:
        print "Please insert input query in the form of Category:Films_by_year OR 'Serie A'" 
    wikidata = wikipediaData(query = inputQuery)
    ### result is json response from wikipedia
    result = wikidata.downloadWikipediaDirect()
    
if __name__ == "__main__":
    main()        
        

