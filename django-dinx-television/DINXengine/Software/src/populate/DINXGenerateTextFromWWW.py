import wikipediaData
import logging
import fileHandler
import os
import nerClassification
import shlex

class GenerateTextFromWWW:
    """Build query for content search on CP"""

    def __init__(self, jsonInputFile, dinxLocation):
        """Init function, initialises all instance based variables.

        :param jsonInputFile:
            Holds keys containing information on what to search, dict.

        :param dinxLocation:
            Holds information on where to save different files based on input and in which folders, dict.

        """
        self.jsonInputFile = jsonInputFile
        self.language = jsonInputFile['Language']
        self.root = jsonInputFile['Root']
        self.category = jsonInputFile['Category']
        self.allPathsInJsonFile = []
        self.folderPath = "%s/%s" % (dinxLocation['downloadedData'], self.root)
        self.allLinks = []
        self.allYears = []
        self.files_to_save=[]
        self.matchingYears = []
        self.dinxLocation = dinxLocation
        self.depth = 0
        self.isCategory = False
        
    def walkPath(self, singlePath = None):
        """Remove item from path list and use item as step.
        Convention, root of path is always first item in json.

        :param singlePath:
            Holds keys defining first item in input JSON file from :func:`DINX.run`, dict.

        Return int with total number of links found.
        """
        if singlePath is None:
            singlePath = self.jsonInputFile
        outlist = []
        if "Categ" in self.root:
            self.isCategory = True
        if len(singlePath) > 0:
            self.depth = self.depth + 1
            allLinks = self._step(singlePath['Root'])
            self._matchYears()
            self._exploreYears()
            if len(allLinks)==1:
                logging.info("[walkPath] Only one link on page %s" % len(allLinks))
                #Handle redirects
                allLinks = self._step(singlePath['Root'])
            try:
                self.depth = self.depth + 1
                for link in allLinks:
                    self.allLinks = self._step(link)
                    logging.info("[walkPath] Didn't find second step of path in links, checking years in root links...")
                    try:
                        outlist = self._matchYears()
                    except Exception as e:
                        logging.info("[walkPath] Error in matching years: %s" % e)
                    try:
                        self._exploreYears(yearList = outlist)
                    except Exception as e:
                        logging.info("[walkPath] Error in exploring years: %s" % e)
            except Exception as e:
                logging.info("[walkPath] Error in step: %s" % e)
	    logging.info("[walkPath] All links found: %s" % len(self.allLinks))
            return len(self.allLinks)
                   
            
    def _step(self,q):
        """Take first item in the path list and send it to be retrieved from wikipedia.
        Holds what to send to wikipedia, can be page, initially is Root.

        :param q:
            Which wikipedia page to retrieve, string.

        Return if success list with links retrieved from page.

        Return if exception boolean False.
        """
        print "[step] query: %s" % q
        #get page
        try:            
            logging.info("[step] query: %s depth: %s" % (q, self.depth))
            wikipediadata = wikipediaData.wikipediaData(query = q, language = self.language)
            page = wikipediadata.downloadWikipediaDirect()
            #get all links in page
            wikipediadata = wikipediaData.wikipediaData(rawData = page)
            allLinks = wikipediadata.exploreWikipediaJSON()
            return allLinks
        except Exception as e:
            logging.info("[step] Error: %s" % e)
            print "[step] Error: %s" % e
            return False

    def _matchYears(self):
        """Check years in links, create new links send back new links.

        Return list with links of each found year.
        """
        #allUrlTextfromPage = list of dictionaries with text and link
        matchingYears = []
        #check only first item in path inputted
        textlink = self.root
        for link in self.allLinks: 
            linkclean = link.decode('ascii','ignore')
            year=1900
            while (year<=2014):
                if (textlink in linkclean) and (str(year) in linkclean):
                    if "http" in link:
                        self.allLinks.remove(link)
                    else:
                        matchingYears.append(link)
                elif (str(year) in link[:4]) or (str(year) in link[:-4]) or (str(year)[:2] in link[:-2]):
                    matchingYears.append(link)
                year = year + 1        
        outlist = []
        for y in matchingYears:
            if y not in outlist:
                outlist.append(y)
        self.matchingYears = outlist
        #return all links which contain year
        return outlist
            

    def _exploreYears(self, yearList = None):
        """Explore extracted year links from downloaded Wikipedia article.

        :param yearList:
            Contains links to each of the years found, list.

        Adds each yearLink dict with year, links in year, parent link from which this year derived to self.allYears.
        """
        outlist = []
        outlist1 = []
        linksInYear = {}
        for year in self.matchingYears:
            try:
                wikipediadata = wikipediaData.wikipediaData(query = year, language = self.language)
                page = wikipediadata.downloadWikipediaDirect()
                wikipediadata = wikipediaData.wikipediaData(rawData = page, language = self.language)
                linksFromPage = wikipediadata.exploreWikipediaJSON()
                linksFromPage = filter(lambda item: item[:4] != "http", linksFromPage)
                if (len(linksFromPage) == 1):
                    if linksFromPage not in outlist:
                        outlist.append(linksFromPage)
                        self.matchingYears.remove(year)
                        link = linksFromPage[0]
                        linksFromPage = self._step(link)
            except Exception as e:
                logging.info("[exploreYears] Error in getting links: %s" % e)
            #here linksfrompage is a list of movie titles per year, get page from title
            try:
                if self.isCategory is True:
                    if linksFromPage not in outlist:
                        outlist.append(linksFromPage)
                        for link in linksFromPage:
                            linksFromPage = self._step(link)
                            linksInYear = {"year":year,"linksFromPage":linksFromPage, 'generatorLink':link}
                            #logging.info("[exploreYears] Original: %s links in page %s" % (link,len(linksFromPage)))
                            #list containing dictionaries of year and all links in that year
                            self.item = linksInYear
                            self._fileInFoldersPath()
                            self.allYears.append(linksInYear)
                else:
                   if linksFromPage not in outlist1:
                       outlist1.append(linksFromPage)
                       for link in linksFromPage:
                           linksInYear = {"year":year,"linksFromPage":linksFromPage, 'generatorLink':link}
                           self.item = linksInYear
                           self._fileInFoldersPath()
                           self.allYears.append(linksInYear)
            except Exception as e:
                logging.info("[exploreYears] Error in checking category: %s" % e)


        
    def _fileInFoldersPath(self, folder = None, item = None):
        """From the costructed folder path(with root and possibly subcategory) save files in latest branch,
        end of path singleFile = item in allYears, the year of event.

        Saving single page and NOT links is not currently supported.

        :param folder:
            Folder name where to save files in, string.

        :param item:
            Cointains information with year, links in year, parent link from which this year derived, dict.

        """
        if folder is None:
            folder = self.folderPath
        if item is None:
            item = self.item
        try:
            self.allInfo = []
            filePathLinksContent = {}
            self.filesToSave = []
            try:
                filePath = "%s/%s" % (folder.encode("utf-8"), item["year"])
            except Exception as e:
                filePath = folder.encode("utf-8")
            logging.info("[fileInFoldersPath] Saving to: %s" % filePath)
            self.filesToSave.append(filePath)
            fn = self._sanitize()
            #filePathLinksContent = is a dictionary containing the path to the file, the link from wikipedia and the content of that link
            filePathLinksContent = {"folder":folder.encode("utf-8"),"filePath":fn,"first_level":item["year"],"third_level":item["linksFromPage"], "second_level":item['generatorLink'], "cat":self.category}
            self.allInfo.append(filePathLinksContent)
            filehandler = fileHandler.fileHandler(allInfo = self.allInfo, dinxLocation = self.dinxLocation)
            filehandler.saveFiles()
            self.allInfo.remove(filePathLinksContent)
        except Exception as e:
            logging.info("[fileInFoldersPath] Error: %s" % e)
            
    def _sanitize(self):
        """Remove noise from links, check for specific patterns, remove patterns from text.

        Return string with normalized filename.
        """
        for item in self.files_to_save:
            item = item.decode('utf-8')
            item = item.encode('latin-1', 'replace')
            item = item.replace('?','-')
        return item

