__author__     = "James Grafton"
__copyright__  = "Copyright 2011, Startup Intelligence Ltd"
__credits__    = [""]
__license__    = "Startup Intelligence, All Rights Reserved"
__version__    = "0.1"
__maintainer__ = "James Grafton"
__email__      = "jrgrafton@gmail.com"
__status__     = "Development"

import logging
import re
import traceback

class DinxGeneralUtils():
   """Utility class used to group all general utility functions together"""
   
   def __init__(self):
      """Init function, initialises all instance based variables.

      """
      self.data = []
   
   @staticmethod   
   def getFormattedException(exceptionMsg):
      top = traceback.extract_stack()[-3]
      return traceback.format_exc(limit=3)
      #return ", ".join([type(exceptionMsg).__name__, os.path.basename(top[0]), str(top[1])])
   
   @staticmethod
   def capitalizeExtended(inputString):
      """Extended version of Pythons built in string capitalise() function which will also create capitals
      after full stops.
      
      :param inputString:
          Words to be capitalized, string.
      
      Return correctly captilized version of input string.

      """
      try:
         inputStringComponents = inputString.split(".")
         inputStringComponents = [item.capitalize() for item in inputStringComponents]
         return "".join(inputStringComponents)
      
      except Exception, msg:
         logging.info("Unable to capitalise [%s] because of [%s]" % (inputString, msg))
   
   @staticmethod
   def convertToAscii(data):
      """Convienience function that converts input data to ascii

      
      :param data:
          Data to be processed, string.
       
      Return ascii converted string.
      """
      if isinstance(data, basestring):
         return ''.join([x for x in data if ord(x) < 128])
      else:
         return data

   @staticmethod
   def removeExtraSpaces(data):
      """Take an input string data removes all extra spaces.

      :param data:
          Input to be processed, string.
      
      Return string resulting string with all additional spaces removed
      """
      p = re.compile(r'\s+')
      return p.sub(' ', data)
