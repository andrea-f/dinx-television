import os, sys
lib = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'ConfigLibrary'))
sys.path.append(lib)
libStanford = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..','ConfigLibrary', 'stanford-corenlp-python'))
sys.path.append(libStanford)
import fileHandler
import generateVideos
import generateEvents
import DINXSearchVideos
import re
from pprint import pprint
import time
import traceback
import hashlib
import generateScore

class TextAnalysis():
    """This class parses any web resource.
    Extracts links, sentences and NER information from text passed to it.
    """
    #dinxHome = os.path.realpath(os.path.dirname(sys.argv[0]))    
    videosLocation = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Data','videos'))
    sys.path.append(videosLocation)
    dinxHome = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', '..','..','..','..','..','..'))
    sys.path.append(dinxHome)


    def __init__(self, location = "/home/legionovainvicta/app/bitbucket/dinx-television/django-dinx-television/DINXengine/Software/src/Data/text", category = "history", root = "Riots", title = "List_of_riots", text = "Some.text to parse!", extract = "all", dateRoot = 0):
        """Initializes TextAnalysis class.

        :param text:
            A bunch of words to extract meaning from, string.

        :param extract:
            What to extract from the input text, default is all, string.
            Available options:
                * **all** -- Extracts links, NER and sentences.
                * **links** -- Extracts only links from text.
                * **NER** -- Extracts only Named Entities from text.
                * **sentences** -- Extracts only sentences from text.

        :param root:
            Which parent owns the text to be analysed, string.
        
        :param title:
            Non verbose root name, string.
        
        :param category:
            Category of text to be analysed, string.

        :param location:
            Where to save json file with page analysed information, string.

        """
        self.text = text
        self.extract = extract
        self.date = 0
        self.dateRoot = dateRoot
        self.root = root
        self.title = title
        self.category = category
        self.location = location
        self.allQueriesForRoot = []

    def extractInformation(self):
        """Processes text to extract all information from it.
        **1** -- Sanitize text removing refs and stripping tags.
        **2** -- Divide text into sentences.
        **3** -- Extract links from text.
        **4** -- Extract Ner from text.

        Return dict with:
            * **text** -- The inputted text split in string sentences, list.
            * **links** -- All the links as strings found, list.
            * **nerList** -- Dicts with ner words and ner type per sentence, list.
            * **date** -- Date either inputted or extracted from text, int.
        """
        root = self.root
        title = self.title
        category = self.category
        date = self.date
        sanitizedText = self.sanitize(self.text)
        wordCount = self.mostCommonWordsInPage(sanitizedText)
        sents = self.getSentences(sanitizedText)        
        nerList = self.extractNerInformation(sents)
        documentInformation = {
        "text": sents,
        "nerList": nerList,
        "date": dateRoot,
        "root": root,
        "title": title,
        "category": category,
        "wordCount": wordCount
        }
        return documentInformation

    def stringifyNerSents(self, nerSents = [], type = "all"):
        """Applies NLP to retrieved ner list.

        :param nerSents:
            Holds all dicts containing entities information, list.

        :param type:
            Default is all: links, ner, relevant words, can be set only to specific type of ner, string.

        Return string with all ner analysed words.
        """
        
        nerPage = self.getNerWords(type = type, nerSents = nerSents)
        for sent in nerSents:
            if "all" in type:
                for link in sent['links']:
                    nerPage.append(link)
                for rel in sent['relevantWords']:
                    nerPage.append(rel)
        nerPage = ' '.join(nerPage)
        return nerPage

    def reuniteWords(self, nerType = "all", subset = ['enrico'], mainlist = ["enrico berlinguer"] ):
        """Rejoin after processing name and last name. Save specific MCE file.
        
        :param subset:
            Holds most important words by ner or all, list.
        
        :param mainlist:
            Holds all words, list.

        :param nerType:
            NER categorization of words being processed, is part of output filename, string.
        
        Return list with reunited most important words.
        """
        mostImportantWords = []
        for word in subset:
            for w in mainlist:
                wl = w.lower()
                if (word in wl) and (len(word) != len(wl)) and (w not in mostImportantWords):
                    mostImportantWords.append(w)
                    exists = self.saveProcessedVideo(root = self.root, id = w, saveKey = "mce_"+nerType, save = True)
        return mostImportantWords

    def getNerWords(self, type = "all", nerSents = []):
        """Gets all or specific subset of ner types.

        :param type:
            Ner category to return, default all, string.

        :param nerSents:
            Ner analysed page of information, list.

        Return list with all or subset of ner entities.
        """
        nerWords = []
        for sent in nerSents:
            for ner in sent['nerList']:
                if "all" in type:
                    if 'date' not in ner['type'].lower():
                        nerWords.append(ner['word'])
                else:
                    if type.lower() in ner['type'].lower():
                        nerWords.append(ner['word'])
        return nerWords

    def _getWords(self, html):
        """Strips all HTML and other tags from page.

        :param html:
            Text to remove html tags from, string.

        Return list with clean words.
        """
        try:
            txt2 = str(html.encode('utf-8'))
        except:
            txt2 = str(html)            
        txt = re.compile(r'<[^>]+>').sub('', txt2)
        words = re.compile(r'[^A-Z^a-z]+').split(txt)
        return [word.lower() for word in words if word != '']

    def mostCommonWordsInPage(self,root = "", minFreq = 0.1, maxFreq = 1.0, page = "This is a massive amount of text"):
        """Extracts most common words in page.

        :param page:
            Set of text to extract most common words from, string.

        :param root:
            Main category/root of page, string.

        :param minFreq:
            Minimum frequency of word to be considered, float.

        :param maxFreq:
            Maximum frequency of word to be considered, float.

        Return list with most common words(of frequency between 10% and 50% and their frequency on the page.
        """
        wordCount = {}
        words = self._getWords(page)
        #print "[mostCommonWordsInPage] Total words: " + str(len(words))
        for word in words:
            if (len(word) > 3) and (("US" or "USA" or "UK") not in word):
                wordCount.setdefault(word,0)
                wordCount[word]+= 1
        wordList = []
        for w in sorted(wordCount, key=wordCount.get):
            frac = (float(wordCount[w])/len(words)) * 100
            #print w, wordCount[w], frac
            if frac>minFreq and frac<maxFreq: wordList.append(w)
        if len(root)>0:
            r = root.replace('-', ' ').lower()
            wordList.append(r)
        return wordList

    def sanitize(self, text, regEx = r'<ref(.*?)</ref>'):
        """Removes <ref> tags from text and other spurious characters.

        :param text:
            Document to clean up and sanitize, string.

        :param regEx:
            What regular expression to use when selecting things to remove from text, string.

        Return string without refs and other noisy characters.
        """
        purify = ['http','<','version','>']
        import re
        text = re.sub('\{[^)]*\}', '', text)
        pattern = re.compile(regEx, re.DOTALL)
        allRefs = re.findall(pattern, text)
        for ref in allRefs:            
            text = text.replace(ref,'').replace('<ref','').replace('</ref>','')
        return text

    def getSentences(self, text = "Some text. To split into sentences."):
        """Divide text into sentences.

        :param text:
            Blob of text to split into sentences, string.

        Return list with sentences.
        """
        import nltk.data        
        tokenizer=nltk.data.load('tokenizers/punkt/english.pickle')
        #text = nltk.corpus.gutenberg.raw('chesterton-thursday.txt')
        sents = tokenizer.tokenize(text)
        clearSents = []
        nonEmptyClearSents = []
        for sent in sents:
            s1 = sent.split('\\n')
            clearSents = clearSents + s1
        for s in clearSents:
            pattern = re.compile(r'\{[^)]*\}', re.DOTALL)
            allCit = re.findall(pattern, text)
            for cit in allCit:
                s.replace(cit,'')
            if len(s) != 0:
                nonEmptyClearSents.append(s)
        return nonEmptyClearSents

    def extractNerInformation(self, tool = "calais", sentences = ["This is a sentence about London", "This is another sentence about Rome"], fns = ""):
        """Extracts named entities from list of sentences using Open Calais.

        :param fns:
            File name of sentences NER analysed retrieved from video search, string.

        :param sentences:
            Strings with text to be NER analysed, list.

        :param tool:
            Which tool to use when performing NER. Default is Open Calais, string.

        Return list of dicts with:
            * **word** -- Ner tagged word, string.
            * **type** -- recognized entity associated with word, string.
        """        
        nerSents = []
        self.dateRoot = self.getDateInSentence(self.title)
        a = 0        
        for sent in sentences:
            resultStanford = resultCalais = {}
            hash = self.hashSentence(sent = sent)
            exists = self.doubleCheck(root = self.root,indexFileName = "hashIndex.txt", hash = hash, save = False)
            #print "\n ================================================== \n"
            print "[extractNerInformation] " + sent[:100] + "\n[extractNerInformation] Exists? " + str(exists) + "\n"
            date = self.matchCorrectDate(sent = sent, sentences = sentences)
            if exists is False:
                calais = self._calaisObject()
                stanford = self._stanfordNerObject()
                #get latest date
                if date == 0:
                    date = self.getDateInSentence(sent)
                if (date != 0) and (date < 2014) and (date > 1800):
                    self.date = date
                if ("|" not in sent) and ("{" not in sent) and ("}" not in sent) and ("=" not in sent):
                    for i in range(2):
                        try:
                            resultCalais = calais.analyze(sent)
                            break
                        except Exception as e:
                            print "\n[extractNerInformation] Error in calling calais web service: %s" % e
                            time.sleep(4)
                    for c in range(3):
                        try:
                            resultStanford = stanford.parse(sent)
                            break
                        except Exception as e:
                            print "\n[extractNerInformation] Error in calling stanford ner: %s" % e
                            resultStanford = {}
                            time.sleep(10)
                    try:
                        links = self.getLinks(sent)
                    except Exception as e:
                        print "\n[extractNerInformation] Error in extracting links from sentence: %s" % e
                    try:
                        relevantWords = self.getLinks(sent, regEx = "'(.*)'")
                    except Exception as e:
                        print "\n[extractNerInformation] Error in extracting relevantWords from sentence: %s" % e
                    try:
                        fullSent = self.fillSentence(hash = hash, relevantWords = relevantWords, resultCalais = resultCalais, resultStanford = resultStanford, sent = sent, links = links, date = self.date)
                        #print "\n[extractNerInformation] Analysis:"
                        #pprint(fullSent)
                        if (len(fullSent['links']) != 0) or (len(fullSent['nerList']) != 0) or (len(fullSent['relevantWords']) != 0):
                            # Save analysed sentence
                            #loc1 = self.location + "/" + fns + ".json"
                            jsonObject, locSaved = self.saveAnalysedData(sents = [fullSent], location = self.location,title = self.title, category = self.category, root = self.root, fns = fns)
                            nerSents.append(fullSent)
                    except Exception as e:
                        print "\n[extractNerInformation] Error in filling sentences %s" % e
            a += 1
            exists = self.doubleCheck(root = self.root, indexFileName = "hashIndex.txt",hash = hash, save = True)
        return nerSents


    def matchCorrectDate(self, maxAvgDistance = 11, sent = "a sentence with possibly an year such as 1988", sentences = [], pastLookups = 80):
        """Assign correct date to event, based on previous sentences.

        :param sentences:
            Contains sentences to for which date has to be extracted from, list.

        :param sent:
            Text to associate date to, string.

        :param pastLookups:
            How many previous dates to consider before choosing one, int.

        :param maxAvgDistance:
            Max offset from average of the previous dates allowed to choose a date, int.
            
        Return int with date for event.
        """
        prevDates = []
        avg1 = 0
        currentSent = 0
        dateAnalysis = self.location.replace('text','indexes')
        dateAnalysis = dateAnalysis + "/" + "dates_" + self.root + ".txt"
        sent = re.sub('\{[^)]*\}', '', sent)
        #current sentence number
        #self.writeOut(fileName = dateAnalysis, text = "\n================\nSent: " + sent + "\n")
        for currentSent in range(0, len(sentences)-1):
            if sent in sentences[currentSent]:
                break
        date = self.getDateInSentence(sent = sent)
        if date == 0:
            if currentSent > pastLookups:
                ran = pastLookups
            else:
                ran = currentSent
            for i in range(ran):
                prev = self.getDateInSentence(sent = sentences[currentSent - i])
                if prev != 0 and prev > 1850 and prev < 2014:
                    prevDates.append(prev)
                    #self.writeOut(fileName = dateAnalysis, text = str(len(prevDates)) + " "+ str(prev) + "\n")
            for h in range(pastLookups):
                if len(sentences) > (currentSent + h):
                    next = self.getDateInSentence(sent = sentences[currentSent + h])
                    if next != 0 and next > 1850 and next < 2014:
                        #self.writeOut(fileName = dateAnalysis, text = str(len(prevDates))+ " "+ str(next) + "\n")
                        prevDates.append(next)            
            if len(prevDates) != 0:
                avg1 = sum(prevDates)/len(prevDates)
                for date1 in prevDates:
                    if abs(date1 - avg1) <= maxAvgDistance:
                        date = date1
                        break
                #print "average: " + str(avg1)
                #self.writeOut(fileName = dateAnalysis, text = "average: " + str(avg1) + "\n")
        #self.writeOut(fileName = dateAnalysis, text = "Selected Date: " + str(date) + "\n")
        #if avg1 > 0:
        #    self.writeOut(fileName = dateAnalysis, text = "Avg dist: " + str(abs(date - avg1)) + "\n")
        return date

    def writeOut(self, text, fileName):
        """Writes information to a file.

        :param text:
            What to write, string.

        :param fileName:
            Which file to write information to, string.
        """
        with open(fileName, 'a') as content:
            content.write(text)
        content.close()

    def getDateInSentence(self,  sent = "This is a sentence with a date such as 1988."):
        """Extract date from a sentence.

        :param sent:
            Text to extract date from, string.

        Return int with four digits representing date in sentence.
        """
        date = self.getLinks(text = sent, regEx = "\d{4}")
        if (len(date) >= 1) and int(date[0]) < 2014 and int(date[0]) > 1800:
            return int(date[0])
        else:
            return 0


    def _calaisObject(self, API_KEY = "pxt78qt53pcuxa2hyrv7dmu3"):
        """Initializes Open Calais web service.

        :param API_KEY:
            Open Calais registered API key, string.

        Return Calais authenticated object.
        """
        from calais import Calais
        return Calais(API_KEY, submitter="dinx television")

    def _stanfordNerObject(self):
        """Initializes RPC with Stanford NLP server.

        Return Stanford NLP object.
        """
        import client        
        return client.StanfordNLP()

    def fillSentence(self, hash = None, resultCalais = None, resultStanford = None, sent = None, links = [], date = 1988, relevantWords = "Italics or quoted words in sentences"):
        """Creates list of sentences including original sentence, links in sent and entities in sent.

        :param resultCalais:
            Calais response after analysing sent, dict.

        :param resultStanford:
            Stanford NER response after analysing sent, dict.

        :param sent:
            Original sentence, string.

        :param relevantWords:
            Words extracted from text using specific regex, list.

        :param hash:
            Hex digit with sent hash for example 144653930895353261282233826065192032313L, int.

        Return dict with sentence information.
        """
        nerList = []
        outlist = []
        try:
            if hasattr(resultCalais, "entities"):
                for item in resultCalais.entities:
                    if item['name'] not in outlist:
                        outlist.append(item['name'])
                        splitted = item['name'].split()
                        for s in splitted:
                            outlist.append(s)
                        nerList.append({"word": item['name'], "type": item['_type']})
        except Exception as e:
            print "[fillSentences] Error in adding calais entities result to nerList: %s" % e
        try:
            if hasattr(resultCalais, "topics"):
                for topic in resultCalais.topics:
                    if topic['categoryName'] not in outlist:
                        outlist.append(topic['categoryName'])
                        nerList.append({"word": topic['categoryName'], "type": "TOPIC"})
        except Exception as e:
            print "[fillSentences] Error in adding calais topics result to nerList: %s" % e
        try:
            sents = resultStanford['sentences']
            for s in sents:
                for word in s['words']:
                    #print word[0]
                    if len(word[1]['NamedEntityTag'])>1:
                        if word[0] not in outlist:
                            nerList.append({"word": word[0], "type": word[1]['NamedEntityTag']})
                            outlist.append(word[0])
        except Exception as e:
            print "[fillSentences] Error in adding stanford result to nerList: %s" % e        
        sentFull = {
        "sent": sent,
        "nerList": nerList,
        "date": date,
        "links": links,
        "relevantWords": relevantWords,
        "hash": hash
        }
        return sentFull

    def getLinks(self, text = "this is a [[text]] with [[links]]", regEx = r'\[[^|\]]*'):
            """Extracts all information in pattern from text.

            :param regEx:
                Has reg expression to filter data by, string.
                * **Links** -- r'\[[^|\]]*'                

            :param text:
                Data to be filtered by pattern, string.

            Return list with all occurrences of info extracted from text.
            """
            links = []
            allLinks = []
            pattern = re.compile(regEx)
            links = re.findall(pattern, text)
            for item in links:
                for char in item:
                    if char in "'[?!/;]":
                        item = item.replace(char,'')
                if item not in allLinks:
                    allLinks.append(item)
            return allLinks

    def saveAnalysedData(self,fns = "", mcw = [], mce = [], mct = [], location = None, root = None, title = None, category = None, dateRoot = 0, sents = [{"sent":"Roma Lazio", "nerList":[{"word":"Lazio", "type":"ORGANIZATION"}]}]):
        """Saves analysed information in JSON format for later retrieval.

        :param fns:
            File name of sentences NER analysed retrieved from video search, string.

        :param root:
            Which parent owns the text to be analysed, string.

        :param title:
            Non verbose root name, string.

        :param category:
            Category of text to be analysed, string.

        :param dateRoot:
            Specific date for root, optional, int.

        :param mcw:
            Most Common Words in page, list.

        :param mce:
            Most common entities extracted from page, list.

        :param mct:
            Most common topics extracted from page, list.

        :param sents:
            Holds text data in dicts, parsed and original, list.
            Dict has:
                * **sent** -- Holds original sentence not parsed, string.
                * **nerList** -- Holds dicts with word and word type, list.
                * **date** -- Date found in sentence or from previous sentence, int.
                * **links** -- Hyperlinks found in sentence, list.

        :param location:
            Where to save the json file, string.

        Return saved json object.
        """
        if location is None:
            location = self.location
        if root is None:
            root = self.root
        if title is None:
            title = self.title
        if category is None:
            category = self.category
        if dateRoot == 0:
            dateRoot = self.dateRoot
        if dateRoot != 0:
            jsonFile = location + "/" + title + "/" + dateRoot + ".json"
        else:
            jsonFile  = location + "/" + title
            if len(fns)>0:
                jsonFile += "_"+fns+".json"
            else:
                jsonFile += ".json"
        jsonObject = {
        "category": category,
        "root": root,
        "title": title,
        "dateRoot": dateRoot,
        "sents": sents,
        "mostCommonWords": mcw,
        "mostCommonEntities": mce,
        "mostCommonTopics": mct
        }
        #print "[saveAnalysedData] len sents: "+ str(jsonObject['sents'])
        try:
            hash = jsonObject['sents'][0]["hash"]
            if root is None:
                root = jsonObject['root']

            exists = self.doubleCheck(root = root, hash = hash, save = False)
            if exists is False:
                fh = fileHandler.fileHandler()
                fh.saveJSON(fileName = jsonFile, data = jsonObject)
                #print "[saveAnalysedData] Saved file: %s" % jsonFile
            else:
                #print "[saveAnalysedData] Sentence exists."
                pass
        except Exception as e:
            print "[saveAnalysedData] Error in TextAnalysis, couldn't save JSON: %s" % e
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = "Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            print error
            sys.exit(2)
        return jsonObject, jsonFile

    def hashSentence(self, sent = "Any sentence would do here!", hashLength = 16):
        """Generates MD5 hash for sentence, so only unique sentences are saved.

        :param sent:
            Text to generate hash from, string.

        :param hashLength:
            Max length of generated hash, int.

        Return hex int with sent hash.
        """
        hash = int(hashlib.md5(sent).hexdigest(), hashLength)
        return hash

    def doubleCheck(self,root = None, save = True, indexFileName = "hashIndex.txt", indexesLocation = "indexes", hash = '144653930895353261282233826065192032313L'):
        """Checks that hash is unique and not a sentence already analysed.

        :param indexesLocation:
            Only folder name where indexes are contained, replaces 'text' in location, string.

        :param root:
            Main parent of information being saved, string.

        :param hash:
            Hash to check in DB if it exists or not, int.

        :param exists:
            Wether a hash exists or not, boolean.

        :param indexFileName:
            Name of the index file to save hash to, string.

        :param save:
            Wether to save the sentence's hash or not, bool.

        Return boolean wether hash exists or not.
        """
        exists = False
        f = self.location.replace('text', indexesLocation)
        if root is not None:
            f += "/" + root
        if os.path.exists(f) != 1:
            os.makedirs(f)
        loc = f +"/"+ indexFileName
        hashFile = open(loc, 'a')
        fh = fileHandler.fileHandler()
        hashList = fh.readFileByLine(fileName = loc)
        try:
            objData = hash.encode('utf-8')
        except:
            objData = hash
        if str(objData) in hashList:
            exists = True
        else:
            if save is True:
                hashLine = u"%s\n" % (hash)
                hashFile.write(hashLine.encode('utf-8'))
        hashFile.close()
        return exists

    def createItemObject(self, item = {}, root = "Cold War", category = "history", date = 1988, normalized = None, hash = None):
        """Instantiate :class:`Item` objects.

        :param item:
            Holds item information such as word and ner type, dict.

        :param root:
            What is parent of item, string.

        :param category:
            How to categorize item, string.

        :param date:
            4 digit year of item, int.

        :param
        """
        i = Item()
        i.nameOfEntity = item['word']
        i.ner = item['type']
        i.root = root
        i.dates = i.dates + [date]
        i.helperWords = []
        i.category = category
        i.hashList = i.hashList + [hash]
        try:
            i.normalized = normalized
        except:
            pass
        return i

    def createVideoObject(self, entry = {}, event = {}, q = "Query which got the video"):
        """Instantiate :class:`Video` objects.

        :param entry:
            YouTube video information, object.

        :param event:
            It is :class:`Event` instance, object.

        :param q:
            Query which got the video, string.

        Return :class:`Video` object.
        """
        video = Video(
            title = entry.media.title.text,
            category = entry.media.category[0],
            query = q,
            url = entry.GetSwfUrl(),
            duration = entry.media.duration.seconds,
            eventName = event.eventName,
            date = event.date,
            image = entry.media.thumbnail[1].url,
            root = event.root,            
            categoryOfEvent = event.category,
            uploadDate = entry.published.text,
            uploader = entry.author[0].name.text,
            helperWords = event.helperWords,
            eventHash = event.hash,
            items = event.items_name
        )
        try:
            video.viewcount = entry.statistics.view_count
        except:
            pass
        try:
            video.description = entry.media.description.text           
        except:
            pass
        try:
            video.keywords= entry.media.keywords.text
        except:
            pass
        return video

    def createEventMetaData(self, duration = 0, mcw= [], mce = [], are = [], score = 100, dinxLocation = {}, relatedWords = [], params = {}, gDataLocation = "/Libraries/gdata"):
        """Generate event metadata information.
        
        :param dinxLocation:
            Holds information regarding default directory locations for generated JSON videodata, dict.
    
        :param relatedWords:
            Contains words related or unrelated to root and hence video, comma separated, used to improve scoring, string.

        :param are:
            All root entities strings, list.

        :param mcw:
            Most common words for root, list.
        
        :param mce:
            Most common entities for root, list.

        :param params:
            Holds key information regarding, for example, minimum duration, dict.
        
        Return dict with metadata information.
        """
        dinxLocation = {
            'gDataLocation': self.dinxHome + gDataLocation,
            'videoData': self.videosLocation
        }
        params = {
            "duration": duration,
            "minimum_score": score,
            "relatedWords": relatedWords,
            "mcw": mcw,
            "mce": mce,
            "are": are
        }
        metaData = {
            "dinxLocation": dinxLocation,
            "params": params            
        }
        return metaData

    def runBroadQuery(self, queriesDist = 4, similarQ = {"q1":"1980 Bologna Years of Lead","r1":0, "q2":"1980 Bologna", "r2":30}, root = ""):
        """Run similar queries or not based on results.

        :param queriesDist:
            Max distance between broad and narrow query, int.

        :param root:
            Main parent of event being searched and part of narrow query, string.

        :param similarQ:
            Holds previous query and results info and current one as well for comparison, dict.

        Return bool if there is point in analysing broader query results or not.
        """
        runBroad = True
        q1 = similarQ['q1']
        q2 = similarQ['q2']
        print q1
        print q2
        if root in q1 and q1.replace(root,'').strip() in q2:
            r1 = similarQ['r1']
            r2 = similarQ['r2']
            if abs(r1 - r2)<queriesDist:
                print abs(r1-r2), queriesDist
                runBroad = False
        return runBroad



    def searchEvent(self,specificDate = None, queries = None, event = None, relatedWords = [], metaData = {}, maxVideosPerEvent = 4, mcw = [], mce = [], are = [] ):
        """Searches an event in video databases.

        :param maxVideosPerEvent:
            Max number of video to associate to an event, int.
        
        :param specificDate:
            Limit queries only to a single date, int.

        :param event:
            It is an instance of :class:`Event`, object.

        :param queries:
            Which queries to run, if None, queries are generated from event, list.

        :param metaData:
            Holds search parameters such as minimum duration, score, relatedWords, dict.

        Return string with file name created.
        """
        fn = ""
        processed = False
        tempVideos = []
        vids = DINXSearchVideos.SearchVideos(event = event)
        if queries is None:
            queries = self.createEventQueries(event = event, cwr = metaData['params']['relatedWords'])
        if len(metaData) == 0:
            if len(relatedWords) == 0:
                metaData = self.createEventMetaData()
            else:
                metaData = self.createEventMetaData(relatedWords = relatedWords)
        c = 0
        prevRes = []
        for q in queries:            
            videosForQuery = 0
            queryProcessed = self.saveProcessedVideo(
                    root = event.root,
                    id=q,
                    save = False,
                    saveKey = "searched_queries_name"
                )
            if queryProcessed is True:
                feed = None
            else:
                if specificDate is not None:
                    if specificDate == event.date:
                        feed = vids.runYouTubeQuery(query = q)
                    else:
                        feed = None
                else:
                    feed = vids.runYouTubeQuery(query = q)
                
            if feed is not None:
                res = len(feed.entry)
                prevRes.append((q,res))
                vidQ = q+","+str(res)
                processed = self.saveProcessedVideo(
                    root = event.root,
                    id=vidQ,
                    save = True,
                    saveKey = "searched_queries"
                )
                if len(queries) >= 2 and len(prevRes)>=2:
                    similarQ = {
                        "q1": prevRes[c-1][0],
                        "q2": q,
                        "r1": prevRes[c-1][1],
                        "r2": res
                    }
                    print "[searchEvent] "+str(similarQ)
                    runBroad = self.runBroadQuery(similarQ = similarQ, root = event.root)
                else:
                    runBroad = True
                print "[searchEvent] Run broad query: " + str(runBroad)
                if runBroad == True and feed != 0:
                    for entry in feed.entry:
                        url = entry.GetSwfUrl()
                        if url is None:
                            processed = True
                        else:
                            idP = url.split('/v/')[1]
                            id = idP.split('?')[0]
                            processed = self.saveProcessedVideo(root = event.root, id = id, save = False, saveKey = "processed_videos")
                        if processed is False:
                            video = self.createVideoObject(entry = entry, event = event, q = q)
                            if video.description is not None:
                                if video.description.endswith('...') and len(feed.entry)<15:
                                    #related_feed = yt_service.GetYouTubeRelatedVideoFeed(video_id='abc123')
                                    try:
                                        if len(id)==11:
                                            entryLong = vids.getVideo(id=id)
                                            video.description = entryLong.media.description.text
                                    except: pass
                            #scorevideo = generateScore.ScoreVideo(video = video, params = metaData['params'])

                            try:
                                #videoStat = scorevideo.getScore()
                                #video.score = videoStat['score']
                                #video.lev = videoStat['lev']
                                video.score = 0
                            except Exception as e:
                                print "[searchEvent] Error in scoring video: %s" % e
                                exc_type, exc_value, exc_traceback = sys.exc_info()
                                error = "Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
                                print error
                                sys.exit(2)
                            #print "[searchEvent] %s, Score: %s" % (video.title, video.score)
                            svn = generateScore.ScoreVideoNer(video = video, params = metaData['params'])
                            scMcw = svn.videoInMostCommonWords()
                            video.score += scMcw
                            exists = self.saveProcessedVideo(root = video.root, id = id, saveKey = "processed_videos", save = True)
                            print "[searchEvent] scoreLEV:" + str(video.score)
                            while (video.score > metaData['params']['minimum_score']):
                                #svn2 = generateScore.ScoreVideoNer(video = video, params = metaData['params'])
                                #print mcwAvg
                                #sc, videoEnts = svn2.extractNerData()
                                #video.score += sc
                                #print "[searchEvent] NER score:" + str(sc)
                                sc=0
                                exists = self.saveProcessedVideo(root = video.root, id = id, saveKey = "positive_videos", save = True)
                                hashVideoId = str(event.hash)+","+str(id)
                                tempVideos.append(video)
                                if video.score >= metaData['params']['minimum_score']:
                                    fn = self.saveQueryResult(video = video, q = q, nerScore = sc, queryNum = c)
                                    processed = self.saveProcessedVideo(root = video.root, id = hashVideoId, save = True, saveKey = "map_hash_id")
                                    exists = self.saveProcessedVideo(root = video.root, id = id, saveKey = "added_videos", save = True)
                                    print "[searchEvent] ++ POSITIVE ("  + str(video.date) + ") -- " + video.title + " SCORE: " + str(video.score) + " Query: " + str(q)
                                break
                    c += 1
            if len(tempVideos) >= maxVideosPerEvent:
                for v in range(maxVideosPerEvent):
                    event.videos.append(tempVideos[v])
            elif len(tempVideos) < maxVideosPerEvent:
                event.videos = tempVideos
            try:
                if len(event.videos) > 0:
                    ga = generateEvents.GenerateEvents()
                    fileLoc = self.location.replace('text','videos')
                    fl = fileLoc+"/json/"+self.root+"/videodata"
                    if os.path.exists(fl) != 1:
                        os.makedirs(fl)
                    ga.saveVideoEvents(evs = [event], dinxLocation = {"videodata": fl})
            except Exception as e:
                print "[searchEvent] Error in saving event w/ videos to DB: %s" % e
                exc_type, exc_value, exc_traceback = sys.exc_info()
                error = "Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
                print error
                sys.exit(2)
            scorevideo = 0
        return fn

    def saveProcessedVideo(self, root = "", id = id, saveKey = "positive_videos", save = True):
        """Saves retrieved information to avoid duplication and for statistical purposes.

        :param root:
            Holds video main root information, string.

        :param id:
            YouTube Video id to be checked and in case, saved, string.

        :param saveKey:
            File name to write to, string.

        :param save:
            Whether to save video information or just check them, bool.

        Return boolean true if video saved correctly.
        """
        exists = self.doubleCheck(save = save, indexFileName = saveKey+".txt", root = root, indexesLocation = "indexes", hash = id)
        return exists


    def createEventObject(self,sentences = [], s = {}, root = None, category = None):
        """Instantiate :class:`Event` objects.

        :param root:
            Parent of s, string.

        :param sentences:
            Hold text of all sentences without ner for correct date extraction, list.

        :param category:
            Type of root, string.
            
        :param s:
            Sentence to generate event from, dict.
        """
        if root is None:
            root = self.root
        if category is None:
            category = self.category
        completeInfo = []
        itemsList = []
        itemsName = []
        event = Event()
        event.date = self.matchCorrectDate(sent= s['sent'], sentences = sentences)
        if str(event.date) not in s['sent']:
            event.eventName = str(event.date) + " " + s['sent']
        else:
            event.eventName = s['sent']
        event.category = category
        event.root = root
        try:
            event.hash = s['hash']
        except:
            event.hash = self.hashSentence(sent = s['sent'])
        try:
            norm = self._normalizeDate(nerList=s['nerList'], date = s['date'])
            #print "norm1: "+str(norm)
            if norm != 0 and len(norm)>=8:
                event.normalized = norm
        except Exception as e:
            print "[createEventObject] Error in normalizing date: %s" % e
            #print "[saveAnalysedData] Error in TextAnalysis, couldn't save JSON: %s" % e
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = "Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            print error
            sys.exit(2)
        
        hasLink = False
        for link in s['links']:
            for ner in s['nerList']:
                if link in ner['word']:
                    hasLink = True
                if hasLink is False:
                    completeInfo.append({'word': link, 'type': 'LINK'})
        for rel in s['relevantWords']:
            completeInfo.append({'word': rel, 'type': 'RELEVANT'})
        completeInfo = completeInfo + s['nerList']
        for item in completeInfo:
            if (item['word'] not in itemsName) and (len(item['type'])>2) :
                itemsName.append(item['word'])
                i = self.createItemObject(
                    item = item,
                    root = event.root,
                    category = event.category,
                    date = event.date,
                    normalized = event.normalized,
                    hash = event.hash
                )
                itemsList.append(i)
        event.items_in_event = itemsList
        event.items_name = itemsName
        return event

    def createEventQueries(self, event, cwr = [], relatedTypes = ['link','relevant'], entityTypes = ['person','misc','organization', 'team'], locTypes = ['naturalfeature','country','state','region','city', 'location']):
        """Generates a list of queries based on event.

        :param event:
            From :class:`Event`, object.

        :param locTypes:
            All possible location ner types, list.

        Return list of queries associated to event.
        """
        queries = []
        items = event.items_in_event
        fullItems = []
        queriesRelRoot = []
        for g in range(0, len(items)-1):
            if (items[g].ner.lower() in relatedTypes):
                if event.date != 0:
                    queriesRelRoot.append(str(event.date) + " "+ items[g].nameOfEntity + " " + event.root)
            if (items[g].ner.lower() in entityTypes) or (items[g].ner.lower() in relatedTypes):
                tot = items[g].nameOfEntity + " " + items[g+1].nameOfEntity
                if tot in event.eventName:
                    fullItems.append(tot)
                else:
                   fullItems.append(items[g].nameOfEntity)
        for item in fullItems:
            if event.date < 2014 and event.date > 1800:
                queries.append(str(event.date)+ " " + item + " " + self.root)
                if len(item.split()) >=2:
                    queries.append(str(event.date)+ " " + item)
        if event.date != 0 and event.date > 1800:
            queries.append(str(event.date)+ " " + event.root)            
        queries = queries + queriesRelRoot
        outlist = []
        for g in queries:
            exists = self.saveProcessedVideo(root = event.root, id = g, saveKey = "queries", save = False)
            if g not in outlist and exists is False:
                outlist.append(g)
        return outlist

    def createRootDateEvent(self, root = "",sentences = [], initialDate = 1800, finalDate = 2014, category = ""):
        """Creates date queries by root.

        :param root:
            Parent of queries, string.

        :param initialDate:
            Starting date for dateQueries, int.

        :param category:
            Parent of root, string.

        :param sentences:
            Plain text sentences used for reference, list.

        :param finalDate:
            Last date for dateQueries, int.

        Return list with date queries.
        """
        queries = []
        s = {
            "category": self.category,
            "root": root,
            "title": self.title,
            "date": initialDate,
            "nerList": [
                {"type": "root", "word": root}
            ],
            "relevantWords": [],
            "links": []
        }
        while initialDate <= finalDate:
            s['date'] = initialDate
            s['sent'] = str(initialDate)+" "+root
            event = self.createEventObject(sentences=sentences, s = s, root = root, category = category)
            #queries.append(str(initialDate)+" "+root)
            queries.append(event)
            if initialDate is not None:
                initialDate += 1
        return queries

    def createRootQueries(self,date = 0, cwr = [], root = "", typeCwr = ""):
        """Set of queries from most common words by root.

        :param cwr:
            Common words in root, list.

        :param typeCwr:
            Type of most common words, entities or links or page, string.

        :param root:
            Parent of related common words, string.

        :param date:
            Four digit symbolyzing date to add,int.

        Return list with queries for root.
        """
        queries = []
        if len(cwr) > 0:
            for word in cwr:
                q = root + " " + word
                if date != 0:
                    q += " " + str(date)
                if q not in queries:
                    queries.append(q)
        return queries
    
    def _normalizeDate(self, nerList = [], date = 1988):
        """Normalize date from multi date entities.

        :param nerList:
            Entities in sentence, list.

        :param date:
            Date of sentence or date root, int.

        Return string with date as 29101988.
        """
        repl = ['s','st','rd','th','nt','nd',',','the','thi']
        year = 0
        fd = []
        dayStr = 0
        normalized = "0"
        try:
            for item in nerList:
                t = item['type']
                w = item['word']
                if t == 'DATE':
                    for r in repl:
                        if w.lower().endswith(r):
                            w = w.replace(r,'')
                    fd.append(w)
        except Exception as e:
            print "[normalizeDate] Error in creating norm date: %s"%e
        try:
            fl = ' '.join(fd).split()
            newDate = []
            for dt in fl:
                if dt.isdigit():
                    dt = int(dt)
                    if dt<=9:
                        dayStr = '0'+str(dt)
                    elif dt <= 31:
                        dayStr = str(dt)
                    else:
                        dayStr = ""
                    if len(dayStr) > 0:
                        newDate.append(dayStr)
                else:
                    monthInt = self._convertMonthsToInt(dt)
                    if monthInt !=0:
                        if monthInt <= 9:
                            monthStr = "0"+str(monthInt)
                        else:
                            monthStr = str(monthInt)
                    else:
                        monthStr = ""
                    if len(monthStr) > 0:
                        newDate.append(monthStr)
            if len(newDate) == 1:
                normalized ='-'.join(newDate)+ "-"+ str(date)
            else:
                normalized = str(date)            
        except Exception as e:
            print "[normalizeDate] Error: %s"%e
            #print "[saveAnalysedData] Error in TextAnalysis, couldn't save JSON: %s" % e
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = "Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            print error
            sys.exit(2)
        return normalized

    def _convertMonthsToInt(self, month = "March"):
        """Convert a month name to an int.

        :param month:
            Text with month name, string.

        Return int with month number.
        """
        months = {
        "January": 1,
        "February": 2,
        "March": 3,
        "April": 4,
        "May": 5,
        "June": 6,
        "July": 7,
        "August": 8,
        "September": 9,
        "October": 10,
        "November": 11,
        "December": 12
        }
        try:
            if months[month] <= 12:
                m = months[month]
            else:
                m = 0
        except:
            m = 0
        return m

    def saveQueryResult(self, video = {}, q = "", nerScore = 0, queryNum = 0):
        """Save query result in html.

        :param nerScore:
            Video analysed ner score, int.

        :param queryNum:
            Order number of query from event, int.

        :param video:
            Instance of :class:`Video`, object.

        :param q:
            Query searched to get this video, string.

        Return HTML file location where video information was saved.
        """
        try:
            tit = video.title.decode('utf-8')
        except:
            tit = video.title
        try:
            name = video.eventName.decode('utf-8')
        except:
            name = video.eventName
        try:
            html = u"""
                <table>
                    <tr>
                        <td>
                            <img src = %s>
                        </td>
                        <td>
                            Query: <b>%s</b> ::%s<br>Title: <b>%s</b><br>Event name: %s<br>Lev distance: %s
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Score: %s
                        </td>
                    </tr>
                    <tr>
                        <td>
                            NER Score: %s
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Duration: %s
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Uploader: %s
                        </td>
                    </tr>
                    <tr>
                        
                        URL: <a href = %s>%s</a>
                        
                    </tr>
                    """ % (
                        video.image,
                        q,
                        queryNum,
                        tit,
                        name,
                        video.lev,
                        video.score,
                        nerScore,
                        video.duration,
                        video.uploader.decode('utf-8'),
                        video.url,
                        video.url
                    )
        except Exception as e:
            print "[saveQueryResult] Error in saving video in HTML: %s" % e
        c = "%s/html/%s/%s" % (self.videosLocation, video.root,time.strftime("%d_%m_%Y"))
        if os.path.exists(c) != 1:
            os.makedirs(c)
        fn = "%s/%s.html" % (c,video.date)
        #print "[saveQueryResult] save to html file: %s" % fn
        filehandler = fileHandler.fileHandler(result = html, fileName = fn)
        filehandler.saveHTML()
        return fn

class Event(object):
    """This class describes an event."""

    def __init__(self, hash = None, eventName = None, itemsInEvent = [], videos = [], category = None, date = None, location = None, items_name = [], helperWords = None, normalized = None):
        """Initializes Event object.

        :param itemsInEvent:
            List of :class:`Item` objects, object.
        """
        self.eventName = eventName
        self.items_in_event = itemsInEvent
        self.items_name = items_name
        self.videos = videos
        self.category = category
        self.date = date
        self.location = location
        self.helperWords = helperWords
        self.normalized = normalized
        self.hash = hash

class Item(object):
    """This class creates an object item of an entity with location, name of Entity, ner, nnp, year, helper words, language, category."""

    def __init__(self, hashList = [], root = None, nameOfEntity = None, ner = None, dates = [], helperWords = [], category = None, normalized = None, **itemhash):
        """Initializes Item class.
        """
        self.nameOfEntity = nameOfEntity
        self.ner = ner        
        self.dates = dates
        self.helperWords = helperWords
        self.category = category
        self.normalized = normalized
        self.root = root
        self.hashList = hashList

class Video(object):
    """This class creates an object containing a url query score and items."""

    def __init__(self,
        lev = None,
        eventHash = None,
        uploader = None,
        url = None,
        query = None,
        normalized = None,
        score = None,
        items = [],
        date = None,
        category = None,
        title = None,
        duration = None,
        image = None,
        eventName = None,
        description = None,
        viewcount = None,
        keywords = None,
        categoryOfEvent = None,
        uploadDate = None,
        helperWords = None,
        root = None,
        **videohash):
        self.eventHash = eventHash
        self.url = url
        self.query = query
        self.score = score
        self.items = items
        self.date = date
        self.category = category
        self.description = description
        self.eventName = eventName
        self.title = title
        self.duration = duration
        self.image = image
        self.viewcount = viewcount
        self.keywords = keywords
        self.categoryOfEvent = categoryOfEvent
        self.uploadDate = uploadDate
        self.helperWords = helperWords
        self.root = root
        self.uploader = uploader
        self.normalized = normalized        
        self.lev = lev



class DownloadTextData():

    def __init__(self, query = "Operating Systems", location = "/where/to/save/downloaded/files",language = "en"):
        self.query = query
        self.language = language
        self.location = location

    def getWikipediaPage(self, query = ""):
        """Downloads Wikipedia page corresponding to query.

        :param query:
            Which page to retrieve from Wikipedia, string.

        Return string with location of downloaded page.
        """
        wd = wikipediaData.wikipediaData(query = query, language = language)
        page = wd.downloadWikipediaDirect()
        #Comment out to avoid saving file
        fileName = "sampleData/"+query.replace(' ','_')+".json"
        f = open(fileName,'a')
        dt = json.dumps(page)
        f.write(dt)
        f.close()
        return fileName

