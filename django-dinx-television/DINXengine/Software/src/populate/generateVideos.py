# coding: utf-8
import sys
import logging
import fileHandler
import generateScore
import os
import traceback
import operator
from DINXutils import UtilitiesTools
class Video(object):
    """This class creates an object containing a url query score and items."""
    
    def __init__(self, uploader = None, url = None, query = None, normalized = None, score = None, items = None, date = None, category = None, title = None, duration = None, thumbnails = None, eventName = None, description = None, viewcount = None, keywords = None, category_of_event = None, upload_date = None, relatedWords = None, root = None, **videohash):
        self.url = url
        self.query = query
        self.score = score
        self.items = items
        self.date = date
        self.category = category
        self.description = description
        self.eventName = eventName
        self.title = title
        self.duration = duration
        self.thumbnails = thumbnails
        self.viewcount = viewcount
        self.keywords = keywords
        self.category_of_event = category_of_event
        self.upload_date = upload_date
        self.relatedWords = relatedWords
        self.root = root
        self.normalized = normalized
        self.uploader = uploader
        
        

class GetVideos():
    """Retrieve videos from youtube.
    """
    import operator

    def __init__(self, event, cat, dinxLocation, relatedWords, root, params):
        """Initializes GetVideos class.

        :param event:
            Holds information about event to search, Event object.

        :param cat:
            Category of events to search, string.

        :param dinxLocation:
            Holds information regarding default directory locations for generated JSON videodata, dict.

        :param relatedWords:
            Contains words related or unrelated to root and hence video, comma separated, used to improve scoring, string.

        :param root:
            Word identifying main category of events and hence videos, string.

        :param params:
            Holds key information regarding, for example, minimum duration, dict.
            
        """
        self.event = event
        self.scoreParameters = params
        self.score = 0
        self.root = root
        self.allVideosInEvent = []
        self.all_scores = []
        self.category_of_event = cat
        self.dinxLocation = dinxLocation
        self.relatedWords = relatedWords
        sys.path.append(self.dinxLocation['gDataLocation'])
    
    def _formDate(self):
        """Formats the date in event to improve query result."""
        if len(self.event.date) > 4:
            self.event.date = self.event.date.replace('-','|')
        else:
            pass

    def _getDateFromTitle(self, title):
        """Get date from title using utilities tools.

        :param title:
            Holds video title information, string.

        Return int with retrieved date if any, otherwise None.
        
        """
        ut = UtilitiesTools()
        date = ut.getDateFromName(title)
        return date


    def _formQuery(self, categoryOfEvent = None, queriesForEvent = [], relatedQueries = []):
        """This function creates a matrix of queries to increase the chance of finding a valuable video.
        Entities in query are matched with date and rest of entities attached in a separate list so can be used in scoring.

        :param categoryOfEvent:
            Specifies which category, either history, movies or sport, the event belongs to, string.

        :param queriesForEvent:
            Holds set of tuples(query, related entites) which will be performed by youtube API, list.

        :param relatedQueries:
            Holds set of related queries to main query for history category, list.

        Return list with all generated queries for event.

        """
        if categoryOfEvent is None:
            categoryOfEvent = self.category_of_event
        if "history" in categoryOfEvent:
            #Make a list of multiple queries for one event
            if len(self.event.items_name) > 1:
                outlist = []
                #items_name is list of locations and persons in event not misc
                for item in self.event.items_name:
                    if len(item) > 3:
                        query = "%s %s" % (self.event.date, item)
                        if item not in outlist:
                            outlist.append(item)                        
                        try:
                            relatedQueries = self._addToQuery(query)
                        except:
                            logging.info("{GetVideos} [getVideos] Error in scoring video %s" % e)
                            exc_type, exc_value, exc_traceback = sys.exc_info()
                            error = "{GetVideos} [getVideos] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
                            logging.error(error)
                        day = self.event.normalized[-2:]
                        month = self.event.normalized[-4:-2]
                        query2 = u"%s/%s/%s %s" % (day, month, self.event.date, item)
                        queriesForEvent.append(query2)
            queriesForEvent = queriesForEvent + relatedQueries            
        else:
            if  "sports" in categoryOfEvent:
                query = u"%s %s|%s" % (
                    self.event.eventName,
                    self.event.date,
                    self.event.date[2:4]
                    )
            elif "movies" in categoryOfEvent:
                evName = self.event.eventName.replace('film','').replace('(','').replace(')','').replace('\'',' ').replace('  ', ' ').decode('utf8').replace(self.event.date,'')
                query = u"%s" % (evName)
                queriesForEvent.append(query)
                query = u"%s %s" % (evName,self.event.date)
                queriesForEvent.append(query)
            try:
                if self.video.date > "2005":
                    query = u"%s %s" % (query, self.root)
                    queriesForEvent.append(query)
            except Exception as e:
                logging.info("[formQuery] Error in updating query with root: %s" % e)
            relatedQueries = self._addToQuery(query)
            queriesForEvent = queriesForEvent + relatedQueries
        return queriesForEvent

    def runQuery(self, categoryOfEvent = None):
        """Create youtube search string and run it.

        :param categoryOfEvent:
            Specifies which category, either history, movies or sport, the event belongs to, string.

        Return list with retrieved Video class objects.
        
        """
        import gdata.youtube
        import gdata.youtube.service
        if categoryOfEvent is None:
            categoryOfEvent = self.category_of_event
        ############ YOUTUBE QUERY PARAMETERS ################# 
        yt_service = gdata.youtube.service.YouTubeService()
        yt_service.ssl = False
        #query.orderby = 'viewCount'
        queryyt = gdata.youtube.service.YouTubeVideoQuery()
        queryyt.racy = 'include'
        queryyt.orderby = 'relevance'
        #queryyt.safeSearch = 'none'
        queryyt.max_results=50
        #yt_service.GetYouTubeRelatedVideoFeed(video_id='abc123')
        #######################################################         
        self._formDate()
        if "sports" in categoryOfEvent:
            queriesForEvent = self._formQuery()
            for query in queriesForEvent:
                self.query = query
                print "[runQuery] Query to be searched: " + query
                queryyt.vq = query
                self.feed = yt_service.YouTubeQuery(queryyt)
                self.allVideosInEvent = self.allVideosInEvent + self._getVideos(q = query)
            return self.allVideosInEvent
        elif "history" in categoryOfEvent:
            queriesForEvent = self._formQuery()
            for query in queriesForEvent:
                #queries for event is a list with tuple with (query, related entities)
                self.query = query
                print "[runQuery] Query to be searched: " + query
                try:
                    query = query
                except:
                    query = query[0]                
                try:
                    logging.info("{GetVideos} [runQuery] Query: %s" % query)
                except:
                    pass
                try:
                    queryyt.vq = query.rstrip('s')
                    self.feed = yt_service.YouTubeQuery(queryyt)
                except:
                    queryyt.vq = query.rstrip('s').encode('utf-8')
                    self.feed = yt_service.YouTubeQuery(queryyt)                
                self.allVideosInEvent = self.allVideosInEvent + self._getVideos(q = query)
            return self.allVideosInEvent
        elif "movies" in categoryOfEvent:
            queriesForEvent = self._formQuery()
            for query in queriesForEvent:
                self.query = query
                print "[runQuery] Query to be searched: " + query
                queryyt.vq = query
                self.feed = yt_service.YouTubeQuery(queryyt)
                self.allVideosInEvent = self.allVideosInEvent + self._getVideos(q = query)
            return self.allVideosInEvent

    def _addToQuery(self,query):
        """Add to total queries if searchwith parameter in paths.json.

        :param query:
            Holds query information to be augmented with searchwith option in related words, string.

        Return list of queries with augmented initial query and searchwith term.
        
        """
        queriesForEvent = []
        for word in self.relatedWords:
            if "searchwith-" in word:
                optList = word.split('-')
                wordToQuery = optList[1]
                query1 = u"%s %s" % (query,wordToQuery)
                queriesForEvent.append(query1)
        return queriesForEvent

    def _getVideos(self, q = None, categoryOfEvent = None):
        """Get videos and score each returned one.

        :param q:
            Query to be run through YouTube API, string.

        :param categoryOfEvent:
            Specifies which category, either history, movies or sport, the event belongs to, string.

        Return list with retrieved Video class objects.
        
        """
        if categoryOfEvent is None:
            categoryOfEvent = self.category_of_event
        for entry in self.feed.entry:
            self.title = entry.media.title.text
            if "--nodate" in self.relatedWords:
                date = self._getDateFromTitle(self.title)
                if len(date) >= 4:
                    self.event.date = date                
            self.category = entry.media.category[0]
            self.keywords = entry.media.keywords.text
            self.flashurl = entry.GetSwfUrl()
            self.duration = entry.media.duration.seconds
            self.thumbnails = entry.media.thumbnail
            self.upload_date = entry.published.text
            try:
                self.description = entry.media.description.text.decode('utf-8','ignore')
            except Exception as e:
                self.description = entry.media.description.text 
            if entry.statistics:
                self.viewcount = entry.statistics.view_count
            else:
                self.viewcount = 0
            try:
                video = Video(title = self.title,
                query = q,
                category = self.category,
                keywords = self.keywords,
                url = self.flashurl,
                duration = self.duration,
                description = self.description,
                eventName = self.event.eventName,
                items = self.event.items_name,
                date = self.event.date,
                thumbnails = self.thumbnails[1].url,
                viewcount = self.viewcount,
                category_of_event = self.category_of_event,
                upload_date = self.upload_date,
                relatedWords = self.relatedWords,
                root = self.root,
                normalized = self.event.normalized)
            except Exception as e:
                try:
                    video = Video(title = self.title,
                    query = q,
                    category = self.category,
                    keywords = self.keywords,
                    url = self.flashurl,
                    duration = self.duration,
                    description = self.description,
                    eventName = self.event.eventName,
                    items = self.event.items_name,
                    date = self.event.date,
                    thumbnails = self.thumbnails[1].url,
                    viewcount = self.viewcount,
                    category_of_event = self.category_of_event,
                    upload_date = self.upload_date,
                    relatedWords = self.relatedWords,
                    root = self.root)
                except:
                    try:
                        logging.info("{GetVideos} [getVideos] Full video couldnt be created because %s" % e)
                        video = Video(title = self.title,
                        category = self.category,
                        query = q,
                        keywords = self.keywords,
                        url = self.flashurl,
                        duration = self.duration,
                        description = self.description,
                        eventName = self.event.eventName,
                        date = self.event.date,
                        thumbnails = self.thumbnails[1].url,
                        root = self.root,
                        viewcount = self.viewcount,
                        category_of_event = self.category_of_event,
                        upload_date = self.upload_date,
                        relatedWords = self.relatedWords)
                    except Exception as e:
                        pass
                    pass
            ############## VIDEO SCORE ##############            
            scorevideo = generateScore.ScoreVideo(video = video, params = self.scoreParameters)
            try:
                self.score = scorevideo.getScore()
            except Exception as e:
                logging.info("{GetVideos} [getVideos] Error in scoring video %s" % e)
                exc_type, exc_value, exc_traceback = sys.exc_info()
                error = "Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
                logging.error(error)
            video.score = self.score
            try:
                logging.info(u"{GetVideos} [getVideos]EVENT: %s TITLE: %s SCORE: %s QUERY: %s" % (self.event.eventName, video.title, video.score, q))
            except:
                pass
            outlist = []
            if (video.score>int(self.scoreParameters['minimum_score'])) and (video.url not in outlist):
                outlist.append(video.url)
                if "movies" in categoryOfEvent:
                    try:
                        logging.info("{GetVideos} [getVideos] %s is a movie" % video.title.decode('utf-8', 'ignore'))
                    except:
                        pass
                video_and_score = (video, video.score)
                self.all_scores.append(video_and_score)
                try:
                    logging.info("{GetVideos} [getVideos] title: %s, score: %s\n" % (video.title.decode('utf-8', 'ignore'), video.score))
                except:
                    pass
                self.saveQueryResult()            
            sorted_x = sorted(self.all_scores, key=operator.itemgetter(1), reverse=True)            
            s = 0            
            if len(sorted_x)>0:
                bestVideos = []
                while s <= 3:
                    try:
                        bestVideos.append(sorted_x[s][0])
                    except Exception as e:
                        pass
                    s = s + 1 
                for item in best_videos:
                    self.allVideosInEvent.append(item)
        return self.allVideosInEvent

    def saveQueryResult(self):
        """Save query result in html."""
        try:
            html = u'<table><tr><td><img src = %s></td><td><b>%s<br>%s</b></td></tr><tr><td>%s</td><td><b>Score: %s</b></td><tr><td>Duration: %s</td></tr>' % (self.thumbnails[1].url, self.query.encode('utf-8'), self.title, self.flashurl, self.score, self.duration)
        except:
            html = "<table><tr><td><img src = %s></td><td><b>%s<br>%s</b></td></tr><tr><td>%s</td><td><b>Score: %s</b></td><tr><td>Duration: %s</td></tr>" % (self.thumbnails[1].url, self.query.encode('ascii','ignore'), self.title.encode('ascii','ignore'), self.flashurl, self.score, self.duration)
        c = "%s/%s" % (self.dinxLocation['videoData'], self.root)
        fn = "%s/html/%s.html" % (c,self.event.date)
        logging.info("[saveQueryResult] save to html file: %s" % fn)
        filehandler = fileHandler.fileHandler(result = html, fileName = fn)
        filehandler.saveHTML()
