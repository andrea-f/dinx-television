#! /usr/bin/python
# coding: utf-8
# To change this template, choose Tools | Templates
# and open the template in the editor.

#Code provided by www.dinx.tv code is under bsd license
__author__="dinx.tv television"
__email__="info@dinx.tv"
__date__ ="$07-Mar-2013 21:36:18$"

import logging
import sys
import DINXGenerateItemFromText
import traceback
import Levenshtein
from DINXutils import UtilitiesTools

class ScoringTools(object):
    """Different video scoring techniques."""

    def __init__(self, video, params, score):
        """Initializes variables in ScoringTools class.

        :param video:
            Video class object with returned video information from :class:`ScoreVideo`, object.

        :param params:
            Holds information with minimum duration from :class:`ScoreVideo`, dict.

        :param score:
            Holds number of points gathered by the current video from :class:`ScoreVideo`, int.
            
        """
        self.video = video
        self.score = score
        self.isRequestedDuration = 0
        self.videoHasEventTitle = 0
        self.scoreParameters = params
        self.relatedWords = params['relatedWords']
        self.title = video.title

    def checkLevenshteinDistance(self, videoText = "", rootText = ""):
        """Checks levenshtein distance between event, entities and video.

        :param videoText:
            Input text to calculate lev. from, string.

        Return float with levenshtein distance.
        """
        lev = Levenshtein.distance(rootText,videoText)
        return lev

    def checkRelatedWords(self):
        """Check if related words are in video.

        Return int of unrelated words and int of relate words from paths.json found in video.
        
        """
        from pprint import pprint
        rel = self.relatedWords
        hasUnRelatedWord = 0
        hasRelatedWord = 0
        for word in rel:
            word = word.decode('utf-8')
            title = self.video.title.decode('utf-8')
            desc = self.video.description.decode('utf-8')
            if word in title.lower():
                hasRelatedWord = hasRelatedWord + 1
            if desc:
                desc = desc.encode('utf-8')
                if word in desc.decode('utf-8').lower():
                    hasRelatedWord = hasRelatedWord + 1
            if "unrelated-" in word:
                w = word.replace('unrelated-', '')
                if w.lower() in title.lower():
                    hasUnRelatedWord = hasUnRelatedWord + 1
        #logging.info("hasUN: %s, hasREL: %s" % (hasUnRelatedWord, hasRelatedWord))
        return hasUnRelatedWord, hasRelatedWord


    def checkEventNameInTitle(self, score):
        """Check if event name is contained in video title.

        :param score:
            Holds number of points gathered by the current video from :class:`ScoreVideo`, int.

        Return int with video score, add points if event name in title and viceversa.        
        """
        hasTitle = 0
        entity = self.video.eventName
        entity = entity.replace('film','').replace('(','').replace(')','').replace('\'',' ').replace('  ', ' ')#encode('utf-8').
        try:
            ut = UtilitiesTools()
            year = ut.getDateFromName(self.video.title)
        except Exception as e:
            logging.info("[checkEventNameInTitle] Error removing in title: %s" % e)
        if " " in entity:
            new = entity.split(" ")
            for n in new:
                if len(n) <= 2:
                    entity.replace(n,'')
                    logging.info(entity)
        videoTitle = self.video.title.replace('\'', ' ')
        querylen = self.video.query.split(' ')
        easc = entity.decode('utf-8')#.decode('ascii','ignore')
        vasc = videoTitle.decode('utf-8')#.decode('ascii','ignore')
        if (easc.lower() in vasc.lower()):
            if len(querylen) > 4:
                score = score + 400
            else:
                score = score + 200
            hasTitle = hasTitle + 1
            try:
                logging.info("[checkEventNameInTitle] has title %s" % self.video.title)
            except:
                pass
        if self.video.description:
            dasc = self.video.description#.decode('utf-8')
            if (easc in dasc) and (easc not in vasc):
                score = score + 300
                hasTitle = hasTitle + 1
        self.videoHasEventTitle = hasTitle
        if hasTitle == 0:
            score = score - 600
        return score

    def checkVideoDuration(self, score):
        """Checks if video duration meets requested duration.

        :param score:
            Holds number of points gathered by the current video from :class:`ScoreVideo`, int.

        Return int with updated video score.
        
        """
        isRequestedDuration = 0
        if int(self.video.duration) > int(self.scoreParameters['duration']):
            try:
                logging.info("[checkVideoDuration] %s is longer than %s" % (self.video.title, self.scoreParameters['duration']))
            except:
                pass
            score = score + 400
            isRequestedDuration = isRequestedDuration + 1
        self.isRequestedDuration = isRequestedDuration
        return score

    def dateInVideo(self, title = None, year = None):
        """Check if there is date in title and award points accordingly.

        :param title:
            Video title information from :class:`ScoreVideo`, string.

        :param year:
            Holds information of year to check in video from :class:`ScoreVideo`, string.

        Return int with updated video score.
        """
        if title is None:
            title = self.title
        if year is None:
            year = self.video.date
        year = str(year)
        score = self.score
        #### IF YEAR HAS SAME UPLOAD DATE
        if int(year) == int(str(self.video.upload_date)[:4]):
            score = score + 100
            logging.info("Video uploaded before event. Video date %s , upload date %s" % (year, self.video.upload_date))
        try:
            description = self.video.description
        except:
            description = ""
        try:
            keywords = self.video.keywords
        except:
            keywords = ""
        ########################## Check if date as it is is contained in title
        hasDate = 0
        year4 = year
        if len(year) >= 4:
            year4 = year[:4]

        if (year4 in title):
            score=score+400
            #logging.info("date match in title")
            hasDate = hasDate + 1
        else:
            score=score-200
        ######################### Check if last two digits of date and the year after are contained in the title
        try:
            #dEnd = last two digits from date
            dEnd = int(year)[6:]
            #logging.info("Date has more than 6 digits")
            dEnd_next = dEnd + 1
            if (year[6:] in title) and (str(dEnd_next) in title) and (year not in title):
                score = score + 200
                hasDate = hasDate + 1
            else:
                score = score - 40
        except Exception as e:
            pass
        ####################### Check if last two digits from year are contained in title
        #yEnd = last two digits from year
        yEnd = int(year[2:4])
        try:
            if (int(str(yEnd)[0:1]) == 0):
                yEnd_next = "0%s" % (int(str(yEnd)[1:2])+1)
            else:
                yEnd_next = yEnd+ 1
        except Exception as e:
            logging.info("error %s" % e)
        twoDigits = 0
        if (str(year[2:4]) in title) and (str(yEnd_next) in title) and (str(year4) not in title):
            score = score + 300
            hasDate = hasDate + 1
            twoDigits = 1
        ###################### Check if date is contained in keywords
        if keywords:
            if year4 in keywords.lower():
                score=score+10
        ###################### Check if date is in description
        if description:
            if year4 in description:
                score = score + 400
                hasDate = hasDate + 1
                #logging.info("Date is in description scorenow %s" % score)
            else:
                if (year4 not in title) and (twoDigits == 0):
                    score = score - 200
            if (year[2:4] in description):
                score=score+60
        ###################### if video has date pass otherwise take points off.
        if hasDate>0:
            #logging.info("%s has date %s" % (title,year))
            pass
        else:
            #logging.info("%s doesnt have date %s" % (title,year))
            score = score - 500
        self.score = score
        return score

    def refine(self, title = None, score = None, year = None, description = None):
        """Take into account helper words and other date matches in video information.

        :param title:
            Video title information from :class:`ScoreVideo`, string.

        :param score:
            Video score information from :class:`ScoreVideo`, int.

        :param description:
            Holds video description information from :class:`ScoreVideo`, string.

        :param year:
            Holds information of year to check in video from :class:`ScoreVideo`, string.


        Return int with updated video score.        
        """
        if title is None:
            title = self.video.title
        if score is None:
            score = self.score
        if year is None:
            year = self.video.date
        if description is None:
            description = self.video.description
        yearOther = 1900
        y = int(year) + 1
        x = int(year) -1
        try:
            while (yearOther<=2012):
                if (year[2:4] in title.lower()) and str(yearOther) in title.lower():
                    score = score - 200
                if int(year) != int(yearOther) and y != int(yearOther):
                    if str(yearOther) in self.title.lower():
                        score = score - 600
                    elif x in self.title.lower():
                        score = score - 500
                    if description:
                        if str(yearOther) in description:
                           score = score - 60
                yearOther = int(yearOther) + 1
        except Exception as e:
            logging.info("{DinxScoringTools}[refine] Error in refining score %s" % e)
        return score

    def compareDates(self):
        """This function compares the upload date with the date of the event and video.

        Return int with updated video score.        
        """
        score = self.score
        if self.video.date > self.video.upload_date:
            if "--nodate" in self.relatedWords:
                pass
            elif str(self.video.date) in str(self.video.upload_date):
                score = score + 100
            else:
                score = score - 200
        self.score = score
        return score


    def itemInVideo(self, title = None, score = None, description = None):
        """Check if entity/item is contained in video.

        :param title:
            Video title information from :class:`ScoreVideo`, string.

        :param score:
            Video score information from :class:`ScoreVideo`, int.

        :param description:
            Holds video description information from :class:`ScoreVideo`, string.

        Return int with updated video score.
        """
        if title is None:
            title = self.video.title.decode('utf-8')
        if score is None:
            score = self.score
        if description is None:
            description = self.video.description.decode('utf-8')
        try:
            keywords = self.video.keywords.decode('utf-8')
        except:
            keywords = ""
        num_of_entities = 0
        videoItems = self.video.items
        try:
            for entity in self.video.items:
                if entity.lower() in title.lower():
                    if "history" in self.video.categoryOfEvent:
                        score = score + 300
                        if (str(self.video.date) in title) or (str(self.video.date) in description):
                            score = score + 2000
                    else:
                        score=score+300
                    num_of_entities = num_of_entities + 1
                else:
                    if "history" in self.video.categoryOfEvent:
                        score = score - 300
                    else:
                        score = score - 200
                if keywords:
                    for key in keywords:
                        if str(entity.lower()) in key.lower():
                            score=score+5
                if description:
                    if (entity.lower() in description.lower()) and (entity.lower() not in title.lower()):
                        score = score + 200
                        num_of_entities = num_of_entities + 1
            if (len(self.video.items) >= 2):
                if num_of_entities == 2:
                    score = score + 50
                    pass
                else:
                    if "history" in self.video.categoryOfEvent:
                        score = score - 100
                    else:
                        score = score - 300
                if (num_of_entities >= 3):
                    pass
                    score = score + 50
                else:
                    if "history" in self.video.categoryOfEvent:
                        score = score - 200
                    else:
                        score = score - 300
            if num_of_entities == 0:
                score = score - 800
            if (videoItems >= 2) and (num_of_entities <=1):
                if "sports" in self.video.categoryOfEvent:
                    score = score - 500
        except Exception as e:
            logging.info("[entityInVideo] Error in scoring entity: %s" % e)
            print "[entityInVideo] Error in scoring entity: %s" % e
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = "Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            print error
            sys.exit(2)
        return score

if __name__ == "__main__":
    pass
