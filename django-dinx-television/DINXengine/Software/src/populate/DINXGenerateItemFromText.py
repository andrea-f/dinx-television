import sys
import fileHandler
import os
import traceback
# l is string of full folder location of ConfigLibrary folder conatining paths.json
l = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'ConfigLibrary'))
sys.path.append(l)
import dinxLcs

class GenerateItemFromText(object):
    def __init__(self, root):
        """This class takes a list of results and extracts the most similar words in these results.

        :param root:
            Word identifying subcategory of results being passed in, string.

        """
        self.root = root
        self.distance = 4

    def createItemsFromVideoData(self, results):
        """Generate a list of video Event items.
        Break up the title and creating item from most common word in titles with the same root.

        :param results:
            Django DB object or list retrieved from database, from :func:`Update.updateDatabase`.

        Return list with unique words extracted from results.

        """
        #result is a video object belonging to root
        outlist = []
        uniqueList = []
        fuzzyList = []
        sys.stderr.write("[createItemsFromVideoData] Retrieved %s videos to generate items for %s" % (len(results), self.root))
        sys.stderr.flush()
        
        try:
            results = results.order_by('-title')
            sys.stderr.write("[createItemsFromVideoData] Total results: %s" % len(results))
        except Exception as e:
            results = results.order_by('-name')
            sys.stderr.write("[createItemsFromVideoData] Error in ordering list: %s" % e)
            sys.stderr.flush()
        count = 0
        try:
            while count <= self.distance:
                try:
                    if count == 0:
                        outlist = self._reduceList(results = results)
                        print "\n[createItemsFromVideoData] Iteration %s/%s elements created: %s" % (count, self.distance, len(outlist))
                    elif count < 5:
                        outlist = self._reduceList(fullList = outlist)
                        print "\n[createItemsFromVideoData] Iteration %s/%s elements created: %s" % (count, self.distance, len(outlist))
                    #print count, self.distance
                    self._saveInfo(outlist, count)
                except Exception as e:
                    sys.stderr.write("[createItemsFromVideoData] Error in applying levenshtein to list: %s" % e)
                    sys.stderr.flush()
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    error = "Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
                    print "[updateDatabase] Error detail: %s" % error
                    outlist = []
                count = count + 1
        except Exception as e:
            sys.stderr.write("[createItemsFromVideoData] Error in applying levenshtein to list: %s" % e)
            sys.stderr.flush()
        try:
            print "[createItemsFromVideoData] Getting most common words in list of %s items...." % len(outlist)
            mostCommonWords = self._getMostCommonWords(outlist)
            for word in mostCommonWords:
                #print word
                if word in outlist:
                    #not sorted
                    fuzzyList.append(word)
        except Exception as e:
            mostCommonWords = ""
            print "[createItemsFromVideoData] Error in getMostCommonWOrds: %s" % e
        fuzzyList = sorted(fuzzyList)
        for w in fuzzyList:
            replacedChars = ["!","(",".","?",":","[","/","]","avi","hd","jas"]
            for char in replacedChars:
                w = w.replace(char,'')
            if (w not in uniqueList) and (len(w) > 0) and (not w.isdigit()):
                uniqueList.append(w)
        self._saveInfo(uniqueList, 'lcs')        
        noDuplicates = []
        for b in range(0, (len(uniqueList) - 1)):
            try:
                if uniqueList[b] in uniqueList[b+1]:
                    uniqueList[b+1] = uniqueList[b]
            except:
                pass
        uniqueList = sorted(uniqueList)
        for u in uniqueList:
            if u not in noDuplicates:
                if len(u) > 2:
                    noDuplicates.append(u)
        for x in noDuplicates:
            print "[createItemsFromVideoData] unique item: %s " % (x)
        self._saveInfo(noDuplicates, 'noDuplicates')
        #outlist.append({'commonWords': commonWords, )
        print "[createItemsFromVideoData]%s unique words" % (len(mostCommonWords))
        print "[createItemsFromVideoData] total videos in %s are %s" % (self.root, results.count())
        return noDuplicates

    def getMovieInformation(self):
        """Get movie information from IMDB.
        """
        parameters = {'limit': limit}
        #parameters= defaults.copy()
        parameters.update(kwargs)
        url = r'http://www.reddit.com/r/{sr}/{top}.json'.format(sr=sr, top=sorting)
        r = client.get(url,params=parameters)
        print '[getMovieInformation] ssent URL is', r.url

    def calculateStringsSimilarity(self, str1, str2):
        """Calculate levenshtein distance between string 1 and string 2.

        :param str1:
            Reference text to be used for lev calculation, string.

        :param str2:
            Comparison text for lev calculation, string.

        Return float with levenshtein distance between str1 and str2.
        """
        try:
            wordList1 = self._normalizeString(str1)
            wordList2 = self._normalizeString(str2)
        except Exception as e:
            print "[getClosestWord] Error normalizeString: %s" % e
            pass
        
        lev = self._levenshtein(wordList1, wordList2)
        return lev

    def _getClosestWord(self, title1,title2):
        """Calculate levenshtein distance of title1 title2.

        :param title1:
            Current words to compare to string title2, string.

        :param title2:
            Words used to compare with title1, string.

        Return list if there are common words between title1 and title2.

        Return string with title1 normalized.

        Return empty string if lev distance between title1 and title2 is less than set distance.
        
        """
        lev = self.calculateStringsSimilarity(str1 = title1, str2 = title2)
        wordList1 = self._normalizeString(title1)
        wordString1 = ' '.join(wordList1) #, wordList2
        commonWord = ''
        if lev <= self.distance:
            commonWords = []
            for word in wordList1:
                if word in wordList2 and str(word) not in commonWords:
                    commonWords.append(str(word))
                    commonWord = ' '.join(commonWords)
            if len(commonWord) > 0:
                if commonWord in wordString1:
                    #print "\n[getClosestWord] commonWord: %s" % commonWord
                    return commonWord
                else:
                    return wordString1
            else:
                return wordString1
        else:
            return ""



    def _getMostCommonWords(self, wordsList):
        """Find longest common pattern in list.

        :param wordsList:
            Data to operate LCS on.

        Return list with unique most common string patterns.
        
        """
        outlist = []
        #print "[createItemsFromVideoData] %s %% processing completed...%s/%s ADDING: %s" % (percent, a, len(results), commonWord)
        try:
            for a in range(0, (len(wordsList) -1)):
                word1 = wordsList[a]
                word2 = wordsList[a+1]
                lcsString = self._getLcs(word1,word2)
                if len(lcsString) > 3 and lcsString not in outlist:
                    lcsList = self._normalizeString(lcsString)
                    lcsString = ' '.join(lcsList)
                    outlist.append(lcsString)
            return outlist
        except Exception as e:
            print "[getMostCommonWords] Error: %s" % e

    def _getLcs(self, title1, title2):
        """Longest common subsequence string checker.

        :param title1:
            Reference words to compute LCS on with title2, string.

        :param title2:
            Other words to compare with title1, string.

        Return string with longest common subsequence, empty string if none.
        
        """
        lcs = dinxLcs.LCS(title1, title2)
        assert len(lcs) == dinxLcs.LCSLength(title1, title2)
        #print 'Length of Longest common subsequence: %d' %(len(lcs),)
        lcsNoSpace = lcs.replace(' ', '')
        if len(lcsNoSpace) >= 3:
            #print 'Longest common subsequence: %s' % (lcs,)
            return lcs
        else:
            return ""

    def _reduceList(self, results = None, fullList = None):
        """Calculates levenshetein distances between element a and a + 1 of results, append a to list if lev < 4.

        :param results:
            Django DB object or list retrieved from database.

        :param fullList:
            Contains related words by root for example, to calculate lev dist, list.

        Return list with most common words found in results or fullList.

        """
        outlist = []
        if results is None:
            el = fullList
        elif fullList is None:
            el = results
        for a in range(0, len(el)-1):
            if results is None:
                try:
                    title1 = el[a]
                    title2 = el[a + 1]
                except:
                    print "[reduceList] Error in assigning [a+1] only: " % (e)
                #print title1,title2
            elif fullList is None:
                try:
                    title1 = el[a].title #.encode('ascii','ignore')
                    title2 = el[a+1].title
                except Exception as e:
                    title1 = el[a].name #.encode('ascii','ignore')
                    title2 = el[a+1].name
                    print "[reduceList] Error in assigning title %s assigning by EVENT NAME" % (e)
            try:
                if title1 != title2:
                    try:
                        commonWord = self._getClosestWord(title1, title2)
                        if (len(commonWord) > 0) and (commonWord not in outlist) and (commonWord is not None):
                            commonWord = str(commonWord)
                            outlist.append(commonWord)
                            #print "[createItemsFromVideoData] %s %% processing completed...%s/%s ADDING: %s" % (percent, a, len(results), commonWord)
                    except Exception as e:
                        print "[reduceList] Error in getClosestWord: %s" % (e)
            except Exception as e:
                print "[reduceList] Error in lev calculation" % (e)
        outlist = sorted(outlist)
        return outlist

    def _levenshtein(self, seq1, seq2):
        """Calculates levenshtein distance between two word sequences.

        :param seq1:
            Reference sequence of words, list.

        :param seq2:
            Word sequence to compare with seq1, list.

        Return int with levenshtein distance between seq1 and seq2.
        """
        oneago = None
        thisrow = range(1, len(seq2) + 1) + [0]
        for x in xrange(len(seq1)):
            twoago, oneago, thisrow = oneago, thisrow, [0] * len(seq2) + [x + 1]
            for y in xrange(len(seq2)):
                delcost = oneago[y] + 1
                addcost = thisrow[y - 1] + 1
                subcost = oneago[y - 1] + (seq1[x] != seq2[y])
                thisrow[y] = min(delcost, addcost, subcost)
        return float(thisrow[len(seq2) - 1])

    def _normalizeString(self, text, minimumWordLength = 3):
        """Breaks string into words by whitespace and lowers all words.

        :param text:
            Words to normalize, string.

        :param minimumWordLength:
            Minimum word length to analyse, int.

        Return list with all normalized and lowered words.        
        """
        wordsInTitle = text.split(' ')
        small = []
        for w in wordsInTitle:
            if len(w) > minimumWordLength:
                small.append(w.lower())
        return small

    def _saveInfo(self, outlist, count = None):
        """Save HTML information of retrieved list.

        :param outlist:
            Words to save in iteration items HTML file, list.

        :param count:
            Holds reference to what is being saved, string.
        """
        retr = ""
        f = ""
        fn = "/home/legionovainvicta/app/DINXengine/Software/src/Data/commonItems/%s_iteration_items.html" % count
        for item in outlist:
            f = "<b> Word: %s </b><br>" % item
            retr = "%s %s" % (retr,f)
        fh = fileHandler.fileHandler(fileName = fn, result = retr)
        fh.saveHTML()
        print "[saveInfo] Saving HTML with %s items to %s" % (len(outlist), fn)