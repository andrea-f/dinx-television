import os,sys
import logging
import fileHandler
import traceback
from collections import defaultdict
import shlex
import DINX

class nerClassification(object):
    """Perform named entity recognition on submitted text with Stanford NLP."""

    def __init__(self, folder = None, path = None, filelist = None, category = None, filename = None, date = None, nerParserLocation = None):
        """Initialize class variables.

        :param filename:
            Holds file location and directory to be parsed, string.

        :param nerParserLocation:
            Location of directory containing NER parser java package, string.

        :param folder:
            Holds location directory containing text files to parse, string.

        :param filelist:
            Holds location of filelist containing location and names of file to parse, string.

        :param category:
            Category of files to parse for more accurate parsing, string.

        :param date:
            Date of filelist to be read for parsing contained files, int.

        :param path:
            Helper words extracted from full location name, string.
            
        """
        self.directory = nerParserLocation        
        self.folder = folder
        self.filelist = filelist
        self.category = category
        self.year = '1111-11'
        self.allitems = [] 
        self.all_years = []
        self.path = path
        self.filename = filename
        self.singlefile = ""
        self.date_of_filelist = date
        

    def _setHomeDir(self, directory = None):
        """Set NLP directory as working directory.

        :param directory:
            Location of directory containing NER parser java package, string.
            
        """
        if directory is None:
            directory = self.directory
        try:
            os.chdir(directory)
        except Exception as e:
            print "[setHomeDir] Error in setting dir in nerClassification.py setHomeDir : %s" % e
            pass
        s = "dir"
        os.system(s)

    def applyNer(self, filelist = None):
        """Apply Stanford NER to a set of files in a folder.

        :param filelist:
            Holds location of filelist containing location and names of file to parse, string.

        Return output folder location where files have been saved after parsing.

        """
        if filelist is None:
            filelist = self.filelist
        self._setHomeDir()
        if len(filelist):
            pass            
        else:
            filelist = "%s/filelist_second.txt" % self.folder
        self.output = self.folder
        self.output = self.output.replace("downloaded","parsed")
        try:
            s = "java -cp stanford-corenlp-2012-04-09.jar:stanford-corenlp-2012-04-09-models.jar:xom.jar:joda-time.jar -Xmx1024m edu.stanford.nlp.pipeline.StanfordCoreNLP -annotators tokenize,ssplit,pos,lemma,ner,regexner -filelist \"%s\" -outputDirectory \"%s\"" % (self.filelist, self.output)             
        except Exception as e:
            print "[applyNer] Error: %s" % e
            logging.info("[applyNer]Error in NER %s" % e)
        print "[applyNer] output folder %s" % self.output
        print "[applyNer] nlp engine location: %s" % self.directory
        self.output = "%s/" % self.output
        self.directory = "%s/" % self.directory
        if os.listdir(self.output):
            os.system(s)
            print "[applyNer] output folder %s" % self.output
        return self.output

class WordsToEvents(object):

    def __init__(self, inputToProcess = None, singlefile = None):
        """Assign input to specific variable, token extraction.

        :param inputToProcess:
            Contains information regarding the files to be parsed and dict with their location and type, list.

        :param singlefile:
            Holds file location and directory to be parsed, string.

        """
        try:
            jsonInput = inputToProcess[2]
            self.folder = inputToProcess[0]
            self.category = jsonInput["Category"]
            self.categoryType = jsonInput["CategoryType"]
            self.override = jsonInput["Override"]
            self.path = jsonInput["RelatedWords"].split(',')
            self.allYears = []
            self.allSentencesInAllFiles = []
            self.allSentences = []
        except Exception as e:
            print "[WordsToEvents] Initialization error: %s \n" % e
            pass
        self.singlefile = singlefile

    def _xmltodictNer(self, allSentences = [], nerEventsInFolder = []):
        """Extract information from parsed XML files. List of dicts containing NER,POS,WORD of files.

        :param allSentences:
            Set as empty, will hold all sentences extracted from XML files, list.

        :param nerEventsInFolder:
            Set as empty will hold all extracted Named Entity Recognition found in folder containing XML files, list.

        Return if sports in category:
            * **nerEventsInFolder** -- List of extracted NER events from input folder.

        Else return:
            * **allSentences** -- List of extracted sentences from XML files.
            
        """
        import re
        self.output = self.folder.replace("downloaded","parsed") 
        logging.info("{WordsToEvents} [xmltodictNer] Reading parsed XML files from: %s" % self.output)
        filehandler = fileHandler.fileHandler(folders = self.output)
        filesInXML = filehandler.listFilesInDir()        
        try:
            for singlefile in filesInXML:
                filehandler = fileHandler.fileHandler(fileName = singlefile)
                nerTokensInFile = filehandler.readXML()
                self.singlefile = singlefile
                date = self.getDate()
                print "{WordsToEvents} [xmltodictNer] date of file: %s" % date
                self.year = date
                if "yes" in self.override:                    
                    videodataFileExists = False
                    pass
                else:
                    videodataFileExists = self._checkIfVideoDataFileExists(folder = self.output, date = date)
                if videodataFileExists is False:
                    try:
                        allSentences = nerTokensInFile['all_sentences']
                        sentencesYear = (allSentences, date)
                        self.allSentencesInAllFiles.append(sentencesYear)
                    except Exception as e:
                        logging.info("{WordsToEvents} [xmltodictNer] Error in XML parsing %s" % e)
                        print "{WordsToEvents} [xmltodictNer] Error in XML parsing %s" % e
                        pass
                    #get information from file name
                    fnNoending = singlefile[:-8]
                    i = re.split('/',fnNoending)
                    v = (len(i)-1)
                    i = i[v].split()
                    try:
                        eventsInFile = {'year': date, 'ner_events': nerTokensInFile['ner_events_in_folder']}
                        nerEventsInFolder.append(eventsInFile)
                    except Exception as e:
                        logging.info("{WordsToEvents} [xmltodictNer] Couldnt extract year from file because: %s" % e)
        except Exception as e:
            logging.info("{WordsToEvents} [xmltodictNer] Error in retrieving files %s" % e)
        logging.info("{WordsToEvents} [xmltodictNer] Finished parsing all XML files")
        if 'sports' in self.category:
            return nerEventsInFolder
        else:
            return allSentences

    def _checkIfVideoDataFileExists(self, folder, date):
        """Check if videodata file exists and avoid redownloading.

        :param folder:
            Holds information about directory location of JSON files in videodata folder, string.

        :param date:
            Holds date information of file to be saved, will be name of written JSON file in videodata folder, int.

        Return boolean:
            * **True** -- If file exists.
            * **False** -- If file doesn't exist.

        """
        try:
            f = folder.replace("downloaded","videos").replace('parsed', 'videos')
            f = "%s/videodata/%s.json" % (f, date)
            filehandler = fileHandler.fileHandler(fileName = f)
            fileExists = filehandler.checkIfFileExists()
            return fileExists
        except Exception as e:
            logging.info("{WordsToEvents} [checkIfVideoDataFileExists] Error: %s" % e)

    
    def decideParsingScheme(self, category = None, categoryType = None):
        """Choose automatically most suited categorization of events.
        Either: Sport, Movies, Music, Misc.

        :param category:
            Holds information regarding category of data to parse, string.

        :param categoryType:
            Specifies type of subcategory of data, string.

        Return list with dics of date of file events in file and helper words.

        """
        if category is None:
            category = self.category
        if categoryType is None:
            categoryType = self.categoryType
        if 'sports' in category and "team" in categoryType:
            nerEventsInFolder = self._xmltodictNer()
            for itemList in nerEventsInFolder:
                try:
                    self.itemList = itemList
                    teamNames = self._getTeams()
                except Exception as e:
                    logging.info("{WordsToEvents} [decideParsingScheme] Error in getting teams %s, decideParsingScheme" % e)
                try:    
                    self.content = self._selectTeamName(teamNames)
                except Exception as e:
                    logging.info("{WordsToEvents} [decideParsingScheme] Error in selecting team name %s" % e)
                try:
                    self.fullsent = self._groupById()                    
                except Exception as e:
                    logging.info("{WordsToEvents} [decideParsingScheme] Error in grouping by id %s" % e)
                try:
                    self._enrichFullSentencesSports()
                except Exception as e:
                    logging.info("{WordsToEvents} [decideParsingScheme] Error in enriching full sentences %s" % e)
                try:    
                    fullsentclean = self._unify(self.fullsent)                    
                except Exception, e:
                    logging.info("{WordsToEvents} [decideParsingScheme] Couldnt unify because: %s" % e)
                self.allYears.append(fullsentclean)
        elif 'movies' in category:
            l = self.folder
            filehandler = fileHandler.fileHandler(folders = l)
            files = filehandler.listFilesInDir()
            for f in files:
                filehandler = fileHandler.fileHandler(fileName = f)
                # Lines in file is []
                linesInFile = filehandler.readFileByLine()
                self.singlefile = f
                date = self.getDate()
                fileInfo = {'year': date, 'events_in_file':linesInFile,'helper_words':self.path}
                self.all_years.append(fileInfo)
        elif 'history' in category:
            self._xmltodictNer()
            for allSentences in self.allSentencesInAllFiles:
                try:
                    self.allSentences = allSentences[0]
                    listOfWordsByYear = self._extractSimilarWords()
                    sentencesByYear = self._removeUnwantedWords(listOfWordsByYear)
                    fileInfo = {
                        'year': allSentences[1],
                        'events_in_file':sentencesByYear,
                        'helper_words': self.path
                    }
                    self.allYears.append(fileInfo)
                    logging.info("{WordsToEvents} [decideParsingScheme] Number of years: %s" % len(self.allYears))
                except Exception as e:
                    logging.info("{WordsToEvents} [decideParsingScheme] Error in parsing history, extractSimilarWords %s" % e)
        else:
            self._xmltodictNer()
        return self.allYears
                
############################## HISTORY ############################################################
        
    def _removeUnwantedWords(self, sentences):
        """This function takes in a list of sentences, each sentence is divided in words.

        :param sentences:
            Contains dict with word information like name, ner, type, list.

        Return list with normalized words.
        
        """
        for sentence in sentences:
            for word in sentence:
                if "|" in word['word']:                    
                    sentence.remove(word)
        return sentences

    def _extractSimilarWords(self, allSentences = None):
        """This function selects similar words based on ner analysis, sentence id and token number.
        Check ner analysis and proximity.

        :param allSentences:
            Contains all tokens, subdivided then in dict with word and ner and sentence_id, split by sentence, list.

        Return list with sentences, each sentence is made up of only NER tokens.

        """
        if allSentences is None:
            allSentences = self.allSentences
        self.normalizedDates = []
        entities_in_sentence = []
        nerItemsInSentence = []
        try:
            for sentence in allSentences:
                tokensInSentence = sentence['all_tokens']
                entities = []
                for a in range(0,(len(tokensInSentence)-1)):
                    token = tokensInSentence[a]
                    if ('ORGANIZATION' in token['ner']) or ('LOCATION' in token['ner']) or ('PERSON' in token['ner']) or ('MISC' in token['ner']):
                        entities.append(token)
                    elif 'DATE' in token['ner']:
                        datesAndSentID = (token['normalizedNer'], token['sentence_id'])
                        self.normalizedDates.append(datesAndSentID)
                    else:
                        token['word'] = '|'                        
                        entities.append(token)
                for a in range(0,(len(entities)-1)):
                    try:
                        token = entities[a]
                        try:
                            previousToken = entities[a-1]
                        except Exception as e:
                            logging.info("beginning of file")
                            previousToken = token
                        if '|' in previousToken['word'] :
                            for b in range(0,4):
                                if entities[a+b]['word'] not in '|':
                                    entities[a]['word'] = "%s %s" % (entities[a]['word'], entities[a+b]['word'])                                                                   
                    except Exception as e:
                        pass
                for entity in entities:
                    if "|" in entity['word']:
                        entities.remove(entity)
                for entity in entities:
                        outlist = []
                        if (' ' in entity['word']) and ('|' not in entity['word']):
                            splitted = entity['word'].split(' ')
                            for split in splitted:
                                if split not in outlist:
                                    outlist.append(split)
                            entity['word'] = ' '.join(outlist)                            
                        for normdate in self.normalizedDates:
                            if normdate[1] in entity['sentence_id'] and (len(normdate[0])>4) and normdate[0].isdigit():
                                entity['normalized'] = normdate[0]                                
                        entity['year'] = self.year
                        entity['category'] = self.category
                        entity['helper_words'] = self.path   
                outlist_final = []
                all_entities = []                
                for entity in entities:
                    if entity['word'] not in outlist_final and (len(entity['word'])>2) and('|' not in entity['word']):
                        outlist_final.append(entity['word'])
                        all_entities.append(entity)
                all_entities_final = []
                one_more_round = []
                for one in all_entities:
                    if one['word'] not in one_more_round:
                        one_more_round.append(one['word'])
                        all_entities_final.append(one)
                entities_in_sentence.append(all_entities_final)
            for entities in entities_in_sentence:
                a = 0
                for entity in entities[:]:
                    entities1 = entities
                    for another in entities1:
                        if entity['word'] in another['word']:
                            try:
                                entities.remove(entities[a])
                            except:
                                pass
                            another['word'] = entity['word']
                    a = a + 1
                try:
                    nerItemsInSentence.append(entities1)
                except:
                    nerItemsInSentence.append(entities)                
        except Exception as e:
            print "{WordsToEvents} [extractSimilarWords] Error in extractsimilarwords %s, %s" % (e, self.year)
            logging.info("{WordsToEvents} [extractSimilarWords] Error in extractsimilarwords %s, %s" % (e, self.year))
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = "{WordsToEvents} [extractSimilarWords] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            logging.error(error)
        logging.info("{WordsToEvents} [extractSimilarWords] Total ner items in sentence: %s" % len(nerItemsInSentence))
        return nerItemsInSentence           
                             
##################################### SPORTS ######################################################
    
    def _getTeams(self, itemList = None):
        """Process events categorized as sports.

        :param itemList:
            Contains ner events to extract team names from, dict.

        Return list with unique set of team names.
        """
        if itemList is None:
            itemList = self.itemList
        try:
            try:
                nerlist = itemList['ner_events']
                for item in nerlist:
                    if ('ORGANIZATION' in item['ner']) or ('LOCATION' in item['ner']) or ('PERSON' in item['ner']):
                        if len(item['word']) <= 4 or len(item['word']) <= 6:
                            item['word'] = item['word'].replace('.','')
                        if len(item['word']) <= 2 or len(item['word']) <= 3:
                            nerlist.remove(item)
                self.year = str(itemList['year'])
                allItems = self._unify(nerlist)
            except Exception as e:
                logging.info("Error in getTeams %s" % e)  
        except Exception as e:
            print e
            pass
        return allItems
            
    def _selectTeamName(self, allItems):
        """Join names of list real + madrid, make dictionary with team name year ner and sentence id.

        :param allItems:
            Contains dics with word and ner extracted from items, list.

        Return list with dics of team names and ner.
        
        """
        teams = []
        for x in allItems:
            if ('ORGANIZATION' in x['ner']) or ('LOCATION' in x['ner']):
                teams.append(x)    
        return teams

    def _enrichFullSentencesSports(self, fullSentence = None):
        """Create dictionary with all information grouped by the same sentence id.

        :param fullSentence:
            Contains dicts with word, year, category and ner for sport category, list.

        """
        if fullSentence is None:
            fullSentence = self.fullsent
        for sent in fullSentence:
            word = ' '.join(sent['word'])
            sent['word'] = word
            sent['year'] = self.year
            sent['category'] = self.category            
            for a in range(len(self.content)):
                if sent['sentence_id'] in self.content[a]['sentence_id']:     
                    sent['ner'] =  self.content[a]['ner']
                    if 'LOCATION' in sent['ner']:
                        sent['location'] = sent['word']
                    else:
                        sent['location'] = "NoLoc"
            sent['pos'] = "NoNNP"
     
    def _removeFolderPathInWords(self, fullSentClean):
        """Remove folder words in analysed word.

        :param fullSentClean:
            Contains dicts with word to remove unwanted characters from, list.

        Return list with dicts of words without unwanted characters.
        """
        #check if word is contanined in directory path, if so add it to a new list
        fp = self.path
        print "fp: %s" % fp
        for p in fp:
            if " " in p:
                new = p.split(" ")
                fp[0] = p
                fp.remove(p)
                for n in new:
                    fp.append(n) 
        for y in fullSentClean:
            y['word'] = y['word'].replace('#', '')
            for fitem in fp:
                 if len(fitem) > 3:
                     y['word'] = y['word'].replace(fitem,"").rstrip().lstrip()
            y['helper_words'] = fp   
        return fullSentClean
    
###########################################################################################
    
    def _unify(self,raw):
        """Create unique list from input.

        :param raw:
            Contains dicts with word, helperwords, list.

        Return list with unique words, removing duplicates.
        
        """
        outlist = []
        outword = []
        for sent in raw:
            sent['word'].replace('#', '')
            sent['helper_words'] = self.path
            if sent['word'] not in outword:
                outlist.append(sent)
                outword.append(sent['word'])
        return outlist        

    def _groupById(self, content = None):
        """self.content is a list of dictionaries.

        :param content:
            Contains dicts with word and sentence_id to group words with same sentence id and assign sentence id of first word, list.

        Return list with dictionaries of complete word and corresponding sentence id.
        
        """
        if content is None:
            content = self.content
        input1=[]
        #self.content is a list of dictionaries        
        for a in range(len(content)):
            try:
                tuple_id_word = (content[a]['word'], content[a]['sentence_id'])
                input1.append(tuple_id_word)
            except Exception, e:
                content.remove(content[a])
                logging.info("[groupById] Unexpected error:%s" % e)        
        res = defaultdict(list)
        for v, k in input1: res[k].append(v)
        fullsent = [{'sentence_id':k, 'word':v} for k,v in res.items()]
        self.content = content
        return fullsent
    
    def prepareItems(self):
        """Inspect items in all years.

        Return list with all extracted information, list of dictionaries.
        
        """
        return self.allYears


def main():
    #use sysarg to set folder name
    self.folders = sys.argv[1]
    nerclassification = nerClassification()
    nerclassification.setHomeDir()
    #nerclassification.applyNer()
    
        

if __name__ == "__main__":
    main()
