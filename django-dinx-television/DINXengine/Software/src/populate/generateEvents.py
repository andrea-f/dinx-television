import logging
import generateVideos
import fileHandler
import os
import sys
import traceback
import time
dinx = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', '..', '..', '..', 'djangoApp', 'dinx', 'db_operations'))
sys.path.append(dinx)
from add import SaveEventsInDatabase

class Item(object):
    """This class creates an object item of an entity with location, name of Entity, ner, nnp, year, helper words, language, category."""
    
    def __init__(self, nameOfEntity = None, ner = None, nnp = None, dateOfEntity = None, helperWords = None, category = None, sentence_id = None, normalized = None, **itemhash):
        """Initializes Item class.
        """
        self.nameOfEntity = nameOfEntity
        self.ner = ner
        self.nnp = nnp
        self.dateOfEntity = dateOfEntity
        self.helperWords = helperWords
        self.category = category
        self.sentence_id = sentence_id
        self.normalized = normalized

class CreateItem(object):
    """Create an Item object.
    """

    def __init__(self, entity):
        """Initializes CreateItem class.

        :param entity:
            Contains information about item to create, dict.

        """
        self.entity = entity

    def create(self):
        """Creates Item object.

        Return created Item object.
        """
        try:
            item = Item(self.entity['word'],self.entity['ner'],self.entity['pos'],self.entity['year'],self.entity['helper_words'],self.entity['category'], self.entity['sentence_id'], self.entity['normalized'])
        except Exception as e:
            item = Item(self.entity['word'],self.entity['ner'],self.entity['pos'],self.entity['year'],self.entity['helper_words'],self.entity['category'], self.entity['sentence_id'])
            #logging.info(item)
            pass
        return item
        

class CreateItems():
    """Create items including ner tags and words."""

    def __init__(self, entities):
        """Initializes CreateItems class.

        :param entities:
            Items in one year. Contains dicts with item information per year, list.

        """
        self.entities = entities
        
    def createMultiple(self):
        """Create a list of items.

        Return list of created item objects.
        
        """
        items = []
        outlist = []
        logging.info("{CreateItems} [createMultiple] Creating multiple items...")
        if type(self.entities) is list :
            pass
        elif type(self.entities) is dict:
            self.entities = self.entities['events_in_file']            
        try:
            for entities_sub in self.entities:
                for a in range(0,len(entities_sub)-1):
                    entity = entities_sub[a]
                    try:
                        if (entity['word'] not in outlist) and ("|" not in entity['word']) and (entity['word'] not in entities_sub[a-1]['word']):
                            try:
                                createitem = CreateItem(entity = entity)
                                oneItem = createitem.create()
                            except Exception as e:
                                logging.info("{CreateItems} [createMultiple] Problem in item: %s" % e)
                                raise
                            outlist.append(entity['word'])
                            items.append(oneItem)
                    except Exception as e:
                        logging.info("{CreateItems} [createMultiple] Entity parsing error: %s" % e)
                        exc_type, exc_value, exc_traceback = sys.exc_info()
                        error = "{CreateItems} [createMultiple] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
                        logging.error(error)                        
            return items
        except Exception as e:
            try:
                for entity in self.entities:
                    if (entity['word'] not in outlist) and ("|" not in entity['word']):
                        createitem = CreateItem(entity = entity)                        
                        try:
                            oneItem = createitem.create()
                        except Exception as e:
                            logging.info("Problem in item: %s" % e)
                            raise                        
                        outlist.append(entity['word'])
                        items.append(oneItem)
                return items
            except Exception as e:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                error = "{CreateItems} [createMultiple] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
                logging.error(error)
            

class CreateVideos():
    """This class retrieves a video based on an event."""
    
    def __init__(self, dinxLocation = None, events = None, cat = None, yr = None, eventsFolder = None, relatedWords = None, root = None, params = None):
        """Initializes CreateVideos class."""
        self.events = events
        self.eventsFolder = eventsFolder
        self.cat = cat
        self.yr = yr
        self.dinxLocation = dinxLocation
        self.root = root
        self.relatedWords = relatedWords
        self.scoreParameters = params
    
    def checkFile(self, date, folder = None):
        """Check if the year has already been retrived from wikipedia in the form of existance of a json file of that year.

        :param date:
            Contains information with digits of a specific year in a root, string.

        :param folder:
            Directory where to check for JSON file of specific year, if None defaults to dinxLocation downloaded data, string.

        Return boolean:

            * **True** -- File exists.
            * **False** -- File doesn't exist.

        """
        fileexist = False
        try:
            if folder is None:
                c = "%s/%s" % (self.dinxLocation['downloadedData'], self.root)
            else:
                c = "%s/%s" % (self.dinxLocation['videoData'].replace('downloaded','videos'), self.root)
        except Exception as e:
            c = os.getcwd()
        print "[checkFile] date: %s" % date
        if '-' in date:
            date = date.replace('-',' ')
        else:
            pass
        fn = "%s/videodata/%s.json" % (c,date)
        if os.path.isfile(fn): 
            try:
                open(fn)
                fileexist = True                
            except IOError as e:
                logging.info("[checkFile] file %s exists" % fn)
                pass
        else:
           fileexist = False
        return fileexist
    
    def searchVideos(self, events = []):
        """This function creates a string query to be passed to video search engine per year.

        :param events:
            Holds objects information for events in one year, list.

        """
        if len(events) == 0:
            events = self.events
        percent = 0
        i = 0
        try:
            for event in self.events:
                try:
                    if self.yr.isdigit():
                        if event.date != self.yr:
                            event.date = self.yr
                except:
                    pass
                self.date = event.date
                try:
                    fileExist = self.fileExist
                except:
                    fileExist = False
                logging.info("[searchVideos] %s is a file %s" % (self.date,fileExist))
                if fileExist is False:
                    try:                        
                        vids = generateVideos.GetVideos(event = event, cat = self.cat, dinxLocation = self.dinxLocation, relatedWords = self.relatedWords, root = self.root, params = self.scoreParameters)
                        videos = vids.runQuery()                        
                        event.videos = videos
                    except Exception as e:
                        logging.info("[searchVideos] Error in runquery because %s" % e)
                        exc_type, exc_value, exc_traceback = sys.exc_info()
                        error = "[searchVideos] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
                        logging.error(error)
                    i += 1
                    percent = 100 * (float(i)/float(len(self.events)))
                    try:
                        print "[searchVideos] %s %% ... %s/%s event: %s %s total videos: %s" % (percent, i, len(self.events), event.eventName.decode('utf-8'), event.date, len(event.videos))
                    except:
                        pass
            try:                        
                if fileExist is False:
                    teg = GenerateEvents(dinxLocation = self.dinxLocation, evs = self.events, root = self.root)            
                    teg.saveVideoEvents()
            except Exception as e:
                logging.info("[searchVideos] Problem in saving events %s" % e)
                pass  
        except Exception as e:
            logging.info("[searchVideos] error in searchvideos because %s" % e)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = "Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            logging.error(error)
            pass
            
            
class Event(object):
    """This class describes an event."""
    
    def __init__(self, eventName = None, items_in_event = None, videos = None, category = None, date = None, location = None, items_name = None, helperWords = None, normalized = None, **eventhash):
        """Initializes Event object."""
        self.eventName = eventName
        self.items_in_event = items_in_event
        self.items_name = items_name 
        self.videos = videos
        self.category = category
        self.date = date
        self.location = location
        self.helperWords = helperWords
        self.normalized = normalized
        

class CreateEvents():
    """Combine items into events."""
    
    def __init__(self,items = None, videos = None):
        """Initializes CreateEvents class.

        :param items:
            Holds Item class objects connected to an event per year, list.

        :param videos:
            Holds Video class objects connected to an event per year, list.

        """
        self.items = items
        self.videos = videos

    
    def createEventsFromMultiEntitySentences(self, items = None):
        """Use sentences to create events per year.

        :param items:
            Holds Item class objects connected to an event per year, list.

        Return list with Event objects created.

        """
        if items is None:
            items = self.items
        events = [] 
        outlist = []
        try:
            for item in items:
                if item.sentence_id not in outlist:
                    outlist.append(item.sentence_id)                    
            for sentence in outlist:
                event = Event()                
                entities_in_event = []
                event.items_in_event = []
                event.items_name = [] 
                for item in items:
                    if (int(item.sentence_id) == int(sentence)) and (item.nameOfEntity not in entities_in_event):
                        entities_in_event.append(item.nameOfEntity)
                        if "LOCATION" in item.ner or "PERSON" in item.ner:
                            event.items_name.append(item.nameOfEntity)
                        event.items_in_event.append(item)
                        event.category = item.category
                        event.date = item.dateOfEntity
                        event.root = item.helperWords[0]
                        try:
                            event.normalized = item.normalized                            
                        except Exception as e:
                            logging.info("{CreateEvents} [createEventsFromMultiEntitySentences] No normalization because %s" % e)
                outlist = []
                entities_in_event = ' '.join(entities_in_event)
                entities_in_event = entities_in_event.split(' ')
                for entity in entities_in_event:
                    if entity not in outlist and len(entity)>2:
                        outlist.append(entity)
                event.eventName = ' '.join(outlist) 
                events.append(event)
        except Exception as e:
                logging.info("{CreateEvents} [createEventsFromMultiEntitySentences] Error in createEventsHistory: %s" % e)
        return events       
 
    def combineItems(self, items = None):
        """Add items to events per year for sports category.

        :param items:
            Holds Item class objects connected to an event per year, list.

        Return list of :class:`Event` objects.
        """
        if items is None:
            items = self.items
        events = []
        try:
            for currentItem in self.items:
                if len(self.items)>0:
                    for item in self.items:
                        if item.nameOfEntity not in currentItem.nameOfEntity:
                            #remove empty item
                            event = Event()
                            currentItem.dateOfEntity = currentItem.dateOfEntity.replace("-"," ")
                            if currentItem.nameOfEntity:
                                event.eventName = "%s %s" % (item.nameOfEntity,currentItem.nameOfEntity)
                                event.items_in_event = []
                                event.items_name = [] 
                                event.items_in_event.append(item)
                                event.items_name.append(item.nameOfEntity)
                                event.items_name.append(currentItem.nameOfEntity)
                                event.items_in_event.append(currentItem)
                                event.category = item.category
                                event.date = item.dateOfEntity 
                                try:
                                    event.helperWords = item.helperWords
                                    #ASSUMING ROOT IS ELEMENT 0 OF INPUT ARRAY
                                    event.root = item.helperWords[0]
                                except Exception as e:
                                    logging.info("[combineItems] helper word problem: %s" % e)
                            events.append(event)
                else:
                    event = Event()
                    event.eventName = currentItem.nameOfEntity
                    event.items_in_event = currentItem
                    event.category = currentItem.category
                    event.date = currentItem.dateOfEntity
                    event.helperWords = currentItem.helperWords                    
                    try:
                        event.helperWords = currentItem.helperWords
                        #ASSUMING ROOT IS ELEMENT 0 OF INPUT ARRAY
                        event.root = currentItem.helperWords[0]
                    except Exception as e:
                        logging.info("[combineItems] helper word problem: %s" % e)
                    events.append(event)    
                    logging.info("[combineItems] defining one item in event")
        except Exception as e:
            logging.info("[combineItems] Problem in combining items: %s" % e)
        return events

class GenerateEvents(object):
    """Test objects creation. Create a list of Items by year, then creates a list of events by year using those links.
    Expects a list of events."""
    
    def __init__(self, years = None, evs = None, option = None, category = None, dinxLocation = None, eventsFolder = None, relatedWords = None, root = None, params = None):
        """Initializes GenerateEvents class.
        
        :param years:
            Contains dicts with all years/files from a root directory, list.
            
        :param evs:
            Contains Event objects with videos and items to be saved, list.

        :param option:
            Sets video search options to avoid using date in CP search query if option is set, currently supported is --nodate, string.

        :param category:
            Parent category of root to use when saving and searching events, string.

        :param dinxLocation:
            Holds key information with full directory path of downloadedData, parsedData and videoData, dict.

        :param eventsFolder:
            Directory name containing saved JSON events location, string.

        :param relatedWords:
            Holds all information relating to a given root including searching and scoring options, comma separated, string.

        :param root:
            Holds information defining the root under which videos and events are being searched and created, string.

        :param params:
            Holds score parameters information, such as minimum duration, dict.

        """
        self.years = years #allYears from DINX
        self.evs = evs
        self.events = []
        self.category = category
        self.dinxLocation = dinxLocation
        self.eventsFolder = eventsFolder
        self.relatedWords = relatedWords
        self.root = root
        self.scoreParameters = params
        self.option = option
        self.sleepTime = 300
        self.yearsPerBatch = 5
    
    def createItemsFromEvents(self, years = None):
        """Generate Item and Event objects for each dict year from all years.

        :param years:
            All links by year, year is a list of all links in a year with associated ner information, list.

        Return list of all years processed in root, each year is a list of events for that year.
        
        """
        if years is None:
            years = self.years
        events = []
        if 'sports' in self.category:
            events = []
            logging.info("{GenerateEvents} [createItemsFromEvents] Creating SPORT items...\n")
            try:
                for year in years:
                    eventsInYear = []
                    try:
                        createitems = CreateItems(entities = year)
                        self.allItems = createitems.createMultiple()                        
                    except Exception as e:
                        print "{GenerateEvents} [createItemsFromEvents] Error in createmultiple: %s\n " % e
                    eventsInYear = self._createSportEvents()
                    events.append(eventsInYear)
            except Exception as e:
                logging.info("{GenerateEvents} [createItemsFromEvent] Error in item creation: %s\n" % e)
        elif 'history' in self.category:
            logging.info("{GenerateEvents} [createItemsFromEvents] Creating history items...for %s year\n" % len(years))
            events = []
            for year in years:
                logging.info("{GenerateEvents} [createItemsFromEvents] Creating history items...for %s\n" % year['year'])
                try:
                    createitems = CreateItems(entities = year)
                    try:                        
                        self.allItems = createitems.createMultiple()
                    except Exception as e:
                        logging.info("{GenerateEvents} [createItemsFromEvents] error in createMultiple %s\n" % e)
                    try:            
                        createevents = CreateEvents(items = self.allItems)
                        eventsPerYear = createevents.createEventsFromMultiEntitySentences()
                    except Exception as e:
                        logging.info("{GenerateEvents} [createItemsFromEvents] Error in creating multiple sentences %s\n" % e)
                    self.year = year['year']
                    events.append((eventsPerYear, year['year']))
                    logging.info("{GenerateEvents} [createItemsFromEvents] Creating history items...for %s\n" % year['year'])
                except Exception as e:
                    logging.info("{GenerateEvents} [createItemsFromEvents] Error in %s in item creation: %s\n" % (self.category, e) )
        elif 'movies' in self.category:            
            events = []
            logging.info("{GenerateEvents} [createItemsFromEvents] Creating events from links in category for %s years\n" % len(years))
            try:
                for year in years:
                    if year['year'] is not None:
                        try:
                            cv = CreateVideos(dinxLocation = self.dinxLocation, root = self.root)
                            fileExist = cv.checkFile(date = year['year'], folder = 1)
                        except Exception as e:
                            fileExist = False
                        self.fileexist = fileExist
                        for key,value in year.iteritems():
                            try:
                                print "{GenerateEvents} [createItemsFromEvents] %s %s with %s\n " % (year['year'],key, len(value))
                            except:
                                pass
                        if fileExist is False:
                            eventsInYear = []
                            if len(year['events_in_file'])>0:
                                for link in year['events_in_file']:                                    
                                    event = Event()
                                    event.date = year['year']
                                    try:
                                        event.eventName = link.decode('utf-8').replace('film','').replace('(','').replace(')','').replace(year['year'],'')
                                    except:
                                        event.eventName = u'%s' % link
                                    event.category = self.category
                                    event.helper_words = year['helper_words']
                                    event.root = year['helper_words'][0]
                                    eventsInYear.append(event)
                                logging.info("{GenerateEvents} [createItemsFromEvents] Events num in %s %s" % (year['year'], len(eventsInYear)))
                                events.append(eventsInYear)
            except Exception as e:
                print "{GenerateEvents} [createItemsFromEvent] Error in movies: %s" % e
                exc_type, exc_value, exc_traceback = sys.exc_info()
                error = "Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
                logging.error(error)
                sys.exit(2)
        self.events = events
        return events
            
    def generateEventByItem(self, years = None):
        """Create events in years by lines in file where filename is year.

        :param years:
            All links by year, year is a list of all links in a year, list.

        Return list of all years processed in root, each year is a list of events for that year.
        
        """
        if years is None:
            years = self.years
        events = []
        try:
            for year in years:
                if year['year'] is not None:
                    eventsInYear = []
                    if len(year['events_in_file'])>0:
                        for link in year['events_in_file']:
                            event = Event()
                            event.date = year['year']
                            try:
                                event.eventName = link.decode('utf-8').replace('film','').replace('(','').replace(')','').replace(year['year'],'')
                            except:
                                event.eventName = u'%s' % link
                            event.category = self.category
                            if self.option is not None:
                                event.helper_words = year['helper_words'] + [self.option]
                            event.root = year['helper_words'][0]
                            #ideally generate ner by category for now person
                            iteminfo = {'name':link, 'ner':'person', 'helper_words':['generated'],'category':self.category}
                            event.items_in_event = []
                            event.items_in_event.append(iteminfo)
                            eventsInYear.append(event)
                            logging.info("{GenerateEvents}[createItemsFromEvents] Events num in %s %s" % (year['year'], len(eventsInYear)))
                        events.append(eventsInYear)
        except Exception as e:
            print "{GenerateEvents} [createItemsFromEvent] Error in movies: %s" % e
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = "{GenerateEvents} [createItemsFromEvent] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            logging.error(error)
            sys.exit(2)
        self.events = events
        return events

    def _createSportEvents(self, allItems = None):
        """Create event from combining multiple items.

        :param allItems:
            Contains Item objects relating to a specific year in sports, list.

        Return list with all events name generated for a specific year in sports.

        """
        if allItems is None:
            allItems = self.allItems
        logging.info("{GenerateEvents} [createSportEvents] Creating events...")
        try:
            ####check category if different than sports dont combine
            createevents = CreateEvents(items = allItems)
            events = createevents.combineItems()
            return events
        except Exception as e:
            logging.info("{GenerateEvents} [createSportEvents] didnt create events because: %s \n %s" % (e,str(allItems)[:100]))            
            
    def generateVideoData(self, events):
        """Retrieve videos from cp related to events.

        :param events:
            Holds objects information for events in all years, list.
        
        """
        try:
            logging.info("{GenerateEvents} [generateVideoData] Creating videos for %s events" % len(events))
        except Exception as e:
            print "{GenerateEvents} [generateVideoData] error in creating videos events %s" % e
        try:
            if "sports" in self.category:
                for events_in_year in events:
                    self.year = events_in_year[0].date
                    createvideos = CreateVideos(
                        yr = self.year,
                        dinxLocation = self.dinxLocation,
                        events = events_in_year,
                        cat = self.category,
                        eventsFolder = self.eventsFolder,
                        relatedWords = self.relatedWords,
                        root = self.root,
                        params = self.scoreParameters
                    )
                    createvideos.searchVideos()
            elif "movies" in self.category:
                processedYears = 0
                for events_in_year in events:
                    logging.info("{GenerateEvents} [generateVideoData] Date to be parsed: %s , out of %s years" % (events_in_year[0].date,len(events)))                    
                    #sleep for sleepTime after yearsPerBatch number of years processed, to avoid throtteling by different web APIs.
                    if processedYears == self.yearsPerBatch:
                        time.sleep(self.sleepTime)
                        processedYears = 0
                        logging.info("{GenerateEvents} [generateVideoData] Sleeping.... processedYears batch count: %s" % (processedYears))
                        print "{GenerateEvents} [generateVideoData] Sleeping.... processedYears batch count: %s" % (processedYears)
                    try:
                        self.year = events_in_year[0].date
                        #logging.info(self.year)
                        if self.year is not None:
                            createvideos = CreateVideos(dinxLocation = self.dinxLocation,
                            events = events_in_year,
                            cat = self.category,
                            relatedWords = self.relatedWords,
                            root = self.root,
                            yr = self.year,
                            params = self.scoreParameters)
                            createvideos.searchVideos() 
                    except Exception as e:
                        logging.info("{GenerateEvents} [generateVideoData] movies error in generatevideodata: %s " % e)
                    processedYears = processedYears + 1
            elif "history" in self.category:
                logging.info("{GenerateEvents} [generateVideoData] Generating videos for: %s years" % len(events))
                for eventsInYear in events:
                    self.year = eventsInYear[1]
                    logging.info("{GenerateEvents} [generateVideoData] Generating HISTORY videos for: %s " % self.year)
                    try:
                        createvideos = CreateVideos(dinxLocation = self.dinxLocation,
                            events = eventsInYear[0],
                            cat = self.category,
                            yr = self.year,
                            relatedWords = self.relatedWords,
                            params = self.scoreParameters,
                            root = self.root)
                    except:
                        createvideos = CreateVideos(dinxLocation = self.dinxLocation,
                            events = eventsInYear[0],
                            cat = self.category,
                            yr = self.year,
                            relatedWords = self.relatedWords,
                            params = self.scoreParameters,
                            root = self.root)
                    createvideos.searchVideos()
        except Exception as e:
            logging.info("{GenerateEvents} [generateVideoData] Problem in creating videos: %s" % e)
        
    def saveVideoEvents(self, evs = None, root = None, dinxLocation = None):
        """Save events in json by year.

        :param evs:
            Contains Event objects with videos and items to be saved, list.

        :param root:
            Holds information defining the root under which videos and events are being searched and created, string.

        :param dinxLocation:
            Holds key information with full directory path of downloadedData, parsedData and videoData, dict.
            
        """
        if evs is None:
            evs = self.evs
        if root is None:
            root = self.root
        if dinxLocation is None:
            dinxLocation = self.dinxLocation
        try:
            c = "%s/%s" % (dinxLocation['videoData'], root)
        except Exception as e:
            c = os.getcwd()
        evsJSON = []        
        outlist = []
        for event in evs:
            fn = event.root + "_videos.json"
            itms = []
            vids = []
            try:
                for video in event.videos:
                    if video.url not in outlist:                        
                        videoinfo = {
                            'url':video.url,
                            'score':video.score,
                            'description':video.description,
                            'title':video.title,
                            'duration':video.duration,
                            'thumbnail':video.image,
                            'viewcount':video.viewcount,
                            'keywords':video.keywords,
                            'root': video.root,
                            #'category': video.category.text,
                            'uploader': video.uploader,
                            'eventHash': video.eventHash,
                            'query': video.query,
                            'date': video.date,
                            'eventName': video.eventName,
                            'helperWords': video.helperWords,
                            'categoryOfEvent': video.categoryOfEvent,
                            'items': video.items,
                            'uploadDate': video.uploadDate
                        }
                        outlist.append(video.url)   
                        vids.append(videoinfo)
                try:
                     #do the same for items as for videos
                    for item in event.items_in_event:                        
                        try:
                            iteminfo = {
                                'name':item.nameOfEntity,
                                'ner':item.ner,
                                'helper_words':[],
                                'category':item.category,
                                'root': item.root,
                                'dates': item.dates,
                                'helperWords': item.helperWords
                            }
                        except:
                            iteminfo = {'name':item['name'], 'ner':item['ner'], 'helper_words':[],'category':item['category']}
                        itms.append(iteminfo)                         
                except Exception as e:
                    itms = []
                    pass
                if len(vids)>0:
                    try:
                        currentDate = event.date
                        root1 = event.root
                        eventJSON = {
                            'name':event.eventName,
                            'date':event.date,
                            'videos':vids,
                            'items':itms,
                            'category':event.category,
                            'helper_words':event.helperWords,
                            'root':event.root,
                            'hash': event.hash,
                            'items_name': event.items_name
                        }
                        try:
                            eventJSON['normalized'] = event.normalized
                        except:
                            pass
                    except Exception as e:
                        print "{GenerateEvents} [saveVideoEvents] Error in generating json"
                    evsJSON.append(eventJSON)
            except Exception as e:
                logging.info("{GenerateEvents} [saveVideoEvents] Error in creating videodata file: \n %s %s" % (fn, e))
                print "{GenerateEvents} [saveVideoEvents]  Error in creating videodata file: \n %s %s" % (fn, e)
        if len(evsJSON)>0:
            ev = {"info":evsJSON}
            try:
                try:
                    #saveindb = SaveEventsInDatabase(list_of_events = evsJSON, root = video.root, date = video.date)
                    #savedVideos = saveindb.saveEvents()
                    #logging.info("{GenerateEvents} [saveVideoEvents] SaveInDb %s for %s in %s" % (savedVideos, root1, currentDate))
                    #print "{GenerateEvents} [saveVideoEvents] SavedInDb %s for %s in %s" % (savedVideos, root1, currentDate)
                    pass
                except Exception as e:
                    logging.info("[saveVideoEvents] Error in calling database: %s" % e)
                    print "[saveVideoEvents] Error in calling database: %s" % e                    
                try:
                    fn2 = dinxLocation['videodata']+"/"+fn
                    filehandler = fileHandler.fileHandler(result = ev, fileName = fn,  dinxLocation = dinxLocation)
                    filehandler.saveJSON(keyToSave = "info",data = ev,fileName = fn2)
                    assert os.path.isfile(fn2)
                    print "[saveVideoEvents] Saved file: %s" % fn2

                except Exception as e:
                    logging.info("[saveVideoEvents] Error in saving file: %s" % e)
                    print "[saveVideoEvents] Error in saving file: %s" % e
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    error = "Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
                    print error
                    sys.exit(2)
            except Exception as e:
                logging.info("{GenerateEvents} [saveVideoEvents] Error in calling filehandler: %s" % e )
                print "{GenerateEvents} [saveVideoEvents] Error in calling filehandler: %s" % e
                exc_type, exc_value, exc_traceback = sys.exc_info()
                error = "Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
                print error
                sys.exit(2)
        else:
            print "{GenerateEvents} [saveVideoEvents] no events in json file"

def main():
    #use sysarg to set folder name
    pass
    #self.folders = sys.argv[1]       

if __name__ == "__main__":
    main()
