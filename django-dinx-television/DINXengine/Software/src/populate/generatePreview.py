import fileHandler
import os
import logging

class GeneratePreview:
    """This class generates html preview files by videos by year - chain videos and years together"""
    
    def __init__(self, events):
        self.events = events
    
    def addToChannel(self):
        """Add input video to stream"""
        array = "var video_array = new Array(); var event_array = new Array();"
        videoitem = ""
        videoarray = []
        i = 0
        outlistv = []
        for event in self.events:
            for video in event.videos:
                if video.url not in outlistv:
                    vev = (video.url,event.eventName.encode('ascii','ignore'))
                    outlistv.append(vev)
                else:
                    event.videos.remove(video)
        eventarray = []
        for vid in outlistv:
            videoitem = "video_array[%s]=\'%s\';\n" % (i, vid[0])
            eventitem = "event_array[%s]=\'%s\';\n" % (i, vid[1])
            eventarray.append(eventitem)
            videoarray.append(videoitem)
            i += 1
        
        evs = " ".join(eventarray)        
        videos = " ".join(videoarray)
        #logging.info(videos)
        playercontainer= """
                        <script type="text/javascript" language="javascript">
	                        function onYouTubePlayerReady(playerid) {
                            ytp = document.getElementById('myytplayer');
                            ytp.addEventListener("onStateChange", "onytplayerStateChange")
                            ytp.setVolume(100);
    
                        };
	                    </script>
        
                        """
        
        
        
        
        headel = array + videos + evs
        playercontrol = """
                    <script type = "text/javascript">
                    var i = 0;
                    var next = "next video";
                    var m = false
                    function pause() { if (ytp) {ytp.pause(); console.log("pause() Fired!");}} 
                    function play() { if (ytp) {ytp.play(); console.log("play() Fired!"); document.getElementById('name').innerHTML=window.event_array[i];}}
                    function start() { if (ytp) {ytp.loadVideoByUrl(window.video_array[i]); document.getElementById('name').innerHTML=window.event_array[i];}}
                    function mute() { if (ytp) { if(window.m==false){ytp.mute();window.m=true} else {ytp.unMute();}}}
					
					function nextVid() {if(ytp){i=i+1; if (window.video_array[i] != window.video_array[i+1]){ytp.loadVideoByUrl(window.video_array[i]);} console.log("next video Fired! i is "+i+" and video url is "+window.video_array[i]); document.getElementById('name').innerHTML=window.event_array[i];}}


                    function onytplayerStateChange(newState) {if (newState == 0){ if (window.video_array[i] != window.video_array[i+1]){ i = i + 1;next = window.video_array[i]; ytp.loadVideoByUrl(next); document.getElementById('name').innerHTML=window.event_array[i]}}}
                           
                  
                    </script>    
                    
        
        """
        
        body = """
                <div id="pl" align="center"><object id="ytapiplayer"></object></div>
                    
                    <div id = "controls">
                    <a href="javascript:void(0);" onclick="start();">Play</a>-
                    <a href="javascript:void(0);" onclick="pause();">Pause</a>-
                    <a href="javascript:void(0);" onclick="mute();">Mute</a>-
                    <a href="javascript:void(0);" onclick="nextVid();">Next Video</a> :::: 
                    <div id="name"></div>
                   </div>
                 
                  <script type="text/javascript">
                  var atts = { id: "myytplayer" };
                  var params = { allowScriptAccess: "always" };
                  swfobject.embedSWF('http://www.youtube.com/apiplayer?enablejsapi=1',"ytapiplayer", "800", "600", "8", null, null, params, atts);
                  
                  function loadVideo(playerUrl) {   
                    swfobject.embedSWF(playerUrl + "&enablejsapi=1&playerapiid=ytplayer&version=3&autoplay=1",
                       "ytapiplayer", "425", "356", "8", null, null, params, atts);
                    
                    }
                  </script>
                    
                
        
                """
        html = "<html><head><title>%s</title><script type='text/javascript' src='http://swfobject.googlecode.com/svn/trunk/swfobject/swfobject.js'></script><script type='text/javascript'>%s</script>%s</head><body bgcolor='black' text='white'>%s%s</body></html>" % (self.events[0].date, headel, playercontainer, playercontrol,body)
        c = os.getcwd()
        
        fn = "%s/channels/%s.html" % (c,self.events[0].date)
        filehandler = fileHandler.fileHandler(fileName = fn, result = html)
        filehandler.saveHTML()
