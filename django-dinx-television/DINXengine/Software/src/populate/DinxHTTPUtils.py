from urllib import urlencode
import urllib3
import json
#import logging
import re
#import htmlentitydefs
import socket
import simplejson
import json

from DinxGeneralUtils import DinxGeneralUtils
#from SIKeyValueStorage import *

class DinxHTTPUtils():
   """Utility class used to group all common HTTP functionality together.
   """
   
   # HTTP general config options
   socketDefaultTimeout = 5     # Socket timeout in secs
   maxConnectionsPerHost = 100 # Max connections per host - useful to prevent flooding
   defaultHeaders = {"User-Agent" : "Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11", 
                     "Connection" : "close"} # Sent with all requests!

   def __init__(self):
      """Init function, initialises all instance based variables.
      
      Keyword arguments:
         None
      
      Return:
         None
      """
      self.data = []
   


   def executeUrlService(self, connectionPool=None, method='GET', url='', fields=None, headers=defaultHeaders):
      """Returns content for url using connection pool when possible for optimal performance.
      
      :param connectionPool:
          Connection pool to use to fetch this URL.
      :param method:
          GET or POST.
        
      :param url:
          URL to fetch.
      :param fields:
          URL fields to use with request (only valid with GET request).

      :param headers:
          Headers to use with request.
         
      Return result of request.
      
      """

      if url == '':
         return None
      
      result = None
      connection = None
      #key = url + str(fields) + str(headers)
      
      try:         
         #fetch url
         if connectionPool == None:
            connectionPool = urllib3.connection_from_url(url)
         if method == 'GET':
            #logging.info("Executing [%s] on URL:[%s] Fields:[%s] Headers:[%s]" % (method, url, fields, headers))
            connection = connectionPool.get_url(url, fields, headers)

            
         elif method == 'POST':
            #logging.info("Executing [%s] on URL:[%s] Fields:[%s] Headers:[%s]" % (method, url, fields, headers))
  
            body = urlencode(fields or {})
            connection = connectionPool.post_url(url, fields=fields, headers=headers,encode_multipart=False)

         else:
            #logging.error("[%s] Unknown URL method [%s]" % (url, method))
            print "Unknown URL"

            
         if connection:
            headers = connection.getheaders()
            #print headers
            result = connection.data
            if 'content-type' in headers:
                if 'text/' in headers['content-type']:
                    result = DinxGeneralUtils.convertToAscii(connection.data)
                elif 'application/json' in headers['content-type']:
                    result = DinxGeneralUtils.convertToAscii(connection.data)
                    result = json.loads(result)
                    
                else:
                    print "something else" 
                 #logging.error("[%s] content-type currently not accepted" % (headers['content-type']))
           #         print "headers: %s" % headers
                  
            #else:
               #logging.error("Content-type not in return headers [%s]" % (headers))
             #  print "headers: %s" % headers
            #self.keyValStorage.set(key, result)
      except urllib3.HTTPError, msg:
         print "error %s %s" % (url, msg)
         #logging.error("[%s] URL open returned HTTP error %s" % (url, msg))
    
      return result
   

   @staticmethod
   def removeLeadingProtocol(data):
      """Convienience function that removes 'http[s][www.]' from front of URL.

      :param data:
          Data to be processed, string.
       
      Return string representing cleaned data.
      """
      p = re.compile(r'^http[s]{0,1}://(www.){0,1}')
      return p.sub('', data)
