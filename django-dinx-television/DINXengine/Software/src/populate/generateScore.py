import logging
import re
from DINXutils import UtilitiesTools
from DINXScoringTools import ScoringTools
import DINXTextAnalysis
from operator import itemgetter
from pprint import pprint

class ScoreVideo(object):
    """Score video class."""

    def __init__(self, video, params):
        """Initializes score video class.

        :param score:
            Holds video accumulated points, int.
            
        :param video:
            Video class object with video information.

        :param params:
            Holds key information regarding, for example, minimum duration, dict.
            
        """
        self.video = video
        self.scoreParameters = params
        self.relatedWords = params['relatedWords']
        self.score = 0
        self.st = ScoringTools(video = self.video, params = self.scoreParameters, score = self.score)
        
    
    def getScore(self, categoryOfEvent = None, score = 0):
        """Determine scoring system based on category.

        :param categoryOfEvent:
            Holds information regarding type of category of video for correct scoring, string.

        :param score:
            How many points have been assigned to video, int.
        Return int with updated video score after scoring.
        
        """
        #print categoryOfEvent
        if categoryOfEvent is None:
            categoryOfEvent = self.video.categoryOfEvent.lower()
        if "sports" in categoryOfEvent:
            score = self._getScoreSports()            
        elif "movies" in categoryOfEvent:
            score = self._getScoreMovies()            
        elif "history" in categoryOfEvent:
            score = self._getScoreHistory()
        videoStat = {
            "score": score,
            "lev": self.st.checkLevenshteinDistance()
        }
        return videoStat

    def _getScoreHistory(self):
        """Updates video's score if video is in history category.

        Return int with updated video score after scoring.
        
        """
        score = 0
        if self.video.description:
            pass
        else:
            self.video.description = ""
        #### CHECK DATE IN TITLE
        if (str(self.video.date) in self.video.title) or (str(self.video.date) in self.video.description):
            score = score + 1500
            try:
                day = self.video.normalized[-2:]
                month = self.video.normalized[-6:-4]
                #print "{ScoreVideo} [getScoreHistory] %s/%s/%s TITLE: %s" % (day, month, self.video.date, self.video.title)
                if (((day in self.video.title) and (month in self.video.title)) or ((day in self.video.description) and (month in self.video.description))):
                    score = score + 100
            except Exception as e:
                #print "{ScoreVideo} [getScoreHistory] No normalized date: %s" % e
                pass
        else:
            score = score - 5000            
        yearOther = 1900
        try:
            while (yearOther<=2012):
                if (str(yearOther) in self.video.title.lower()) and (yearOther != int(self.video.date)):
                    score = score - 800
                yearOther = yearOther + 1
           #logging.info("Durationvideo %s" % self.video.duration)
        except Exception as e:
            logging.info("{ScoreVideo} [getScoreHistory] yearother error: %s"%e)
        self.score = score
        score = self.st.itemInVideo()
        hasUN, hasREL = self.st.checkRelatedWords()
        if hasREL > 0:
            if len(self.video.description) > 160:
                score += (50 * hasREL)
            else:
                score += (150 * hasREL)
            #print "[getScoreHistory] has related words: " + str(hasREL)
        if hasUN > 0:
            score = score - 200
        #lev = self.st.checkLevenshteinDistance()
        return score        
    
    def _getScoreMovies(self):
        """Calculate score of movie video.

        Return int with updated video score after scoring.
        
        """
        score = 0
        self.score = score
        if self.video.description:
            pass
        else:
            self.video.description = ""
        # CHECK DATE IN TITLE
        if (str(self.video.date) in self.video.title) or (str(self.video.date) in self.video.description):
            score = score + 200
        yearOther = 1900
        try:
            while (yearOther<=2014):
                if (str(yearOther) in self.video.title.lower()) and (str(yearOther) not in str(self.video.date)):
                    if "--nodate" in self.relatedWords:
                        pass
                    else:
                        score = score - 300
                yearOther = yearOther + 1
        except Exception as e:
            logging.info("{ScoreVideo} [getScoreMovies] yearother error: %s"%e)
        logging.info("{ScoreVideo} [getScoreMovies] score for %s after checking dates: %s" % (self.video.title, score))
        # CHECK EVENT NAME IN TITLE
        score = self.st.checkEventNameInTitle(score)
        if self.st.videoHasEventTitle > 0:
            pass
        else:
            score = score - 600 
        logging.info("{ScoreVideo} [getScoreMovies] score for event %s after checkEventNameInTitle: %s" % (self.video.eventName, score))
        # CHECK DURATION IN TITLE
        score = self.st.checkVideoDuration(score)
        if (self.st.videoHasEventTitle > 0) and (self.st.isRequestedDuration > 0):
            pass
        else:
            score = score - 600            
        logging.info("{ScoreVideo} [getScoreMovies] score after checkVideoDuration: %s" % score)
        # CHECK IF RELATED WORDS ARE CONTAINED IN VIDEO
        hasUN, hasREL = self.st.checkRelatedWords()
        if (self.st.videoHasEventTitle > 0) and (self.st.isRequestedDuration > 0) and (hasREL > 0):
            score = score + 200
        else:
            pass
        if hasREL > 0:
            score = score + (150 * hasREL)
        if hasUN > 0:
            score = score - 1500
        logging.info("{ScoreVideo} [getScoreMovies] score after checkRelatedWords: %s" % score)
        return score

    def _getScoreSports(self):
        """Calculate score of video.

        Return int with updated video score after scoring.

        """
        score = 0
        #add event name in title
        try:
            score = self.st.checkEventNameInTitle()
        except:
            pass
        try:
            score = self.st.dateInVideo()
        except Exception as e:
            logging.info("{ScoreVideo} [dateInVideo] ScoringTools Score error: %s" % e)
        try:
            score = self.st.itemInVideo()
        except Exception as e:
            logging.info("{ScoreVideo} [itemInVideo] ScoringTools Score error: %s" % e)
        try:
            score = self.st.compareDates()
        except Exception as e:
            logging.info("{ScoreVideo} [dateInVideo] ScoringTools Score error: %s" % e)
        try:
            hasUN, hasREL = self.st.checkRelatedWords()
            if hasUN > 0:
                score = score - 900
            if hasREL > 0:
                score = score + (150 * hasREL)
        except Exception as e:
            logging.info("{ScoreVideo} [getScoreSports] ScoringTools Score error: %s" % e)
            raise            
        return score

class ScoreVideoNer():
    """This class extracts ner from video information."""

    def __init__(self, video, params = {}, score = 0):
        self.video = video
        if len(params) == 0:
            ta = DINXTextAnalysis.TextAnalysis()
            metaData = ta.createEventMetaData()
            params = metaData['params']
        else:
            self.params = params
        self.score = score
        self.videoText = "all video information in a string."
        self.st = ScoringTools(video = self.video, params = self.params, score = self.score)



    def extractNerData(self, sentences = [], score = 0, query = ""):
        """Extracts ner information from video.

        :param score:
            Current score of video, if any, int.
            
        :param sentences:
            Holds plain text to be analysed divide in sentences, list.

        :param query:
            Video search engine query, string.

        Return video score with analysed sentences.
        """
        from pprint import pprint
        videoText = ""
        if self.score != 0:
            score = self.score
        ta = DINXTextAnalysis.TextAnalysis(title = self.video.root, root = self.video.root)
        videoText = self.convertVideoToString()
        scr = self.rootInVideo(date = ta.getDateInSentence(query),root = self.video.root, query = query, videoText = videoText)
        st = ta.sanitize(videoText.strip())
        sents = ta.getSentences(text = st)
        nerSents = ta.extractNerInformation(sentences = sents, fns = "youtube_queries")
        #print nerSents
        t = self.video.title.replace(',', ' ')
        titleList = t.split()
        for tit in titleList:
            levTitleRoot = self.st.checkLevenshteinDistance(
                videoText = self.video.root.lower(),
                rootText = tit.lower()
            )
            if levTitleRoot <= 2:
                break
        for sent in nerSents:
            for nerItem in sent['nerList']:
                try:
                    ni = nerItem['word'].decode('utf-8')
                except:
                    try:
                        ni = nerItem['word'].encode('utf-8')
                    except:
                        ni = nerItem['word']

                for sentItem in self.video.items:
                    try:
                        si = sentItem.decode('utf-8')
                    except:
                        try:
                            si = sentItem.encode('utf-8')
                        except:
                            si = sentItem
                    lev = self.st.checkLevenshteinDistance(
                        videoText = str(ni).lower(),
                        rootText = str(si).lower()
                    )
                    if lev <= 2:
                        score += 150
                    else:
                        if levTitleRoot > 2:
                            score -= 50
                        else:
                            score -= 10
                    if "TOPIC" in nerItem['type'] and lev <= 2:
                        score +=100               
        score += scr
        return score, nerSents

    def rootInVideo(self, date= 0, root = "", videoText = "", query = ""):
        """Checks whether root is in video if query is date+root.

        :param root:
            Main parent of video, string.

        :param videoText:
            Stringified video information, string.

        :param date:
            Date of query, int.

        :param query:
            Video search engine query, string.

        Return int with score.
        """
        score = 0
        if root in query:
            if len(query.replace(root, '').split())==1 and date != 0:
                if root.replace('-',' ') in videoText:
                    score += 150
                else:
                    score -= 200
                if date in videoText:
                    score += 150
                else:
                    score -= 500
        return score

    def videoInMostCommonWords(self, videoText = "", mcw = [], are= []):
        """Calculates Levenshtein distance between video and most common words.

        :param videoText:
            Holds one dimension :class:`DINXTextAnalysis.Video` object, string.

        :param are:
            All root entities strings, list.

        :param mcw:
            Most common words for root, list.

        Return float with Levenshtein distance between :class:`DINXTextAnalysis.Video` instance and most common words.
        """
        score = 0
        if len(mcw)==0:
            mcw = self.params['are']
        if len(videoText) == 0:
            videoText = self.convertVideoToString()
        videoText = videoText.replace(',',' ')
        date = self.video.date
        ta = DINXTextAnalysis.TextAnalysis(title = self.video.root, root = self.video.root)
        dt = ta.getDateInSentence(sent = videoText)
        dtLev = self.st.checkLevenshteinDistance(
            videoText = str(dt),
            rootText = str(date)
        )
        videoList = videoText.split()#.append(self.video.root)
        levList = []
        levTuples = []
        purify = ['http','<','version','>']
        totPerfect = 0
        # videoListPurified
        vlp = []
        for m in videoList:
            vi = m.replace('&amp', ' ').replace('label=','')
            for p in purify:
                if (len(vi)>2):
                    if p in vi:
                        pass
                    else:
                        vlp.append(vi)
        uniqueWordsInVideo = []
        perfectMatch = []
        #print "ffefrefrff " + str(mcw)
        for v in vlp:
            avgLenDist = []
            if v not in uniqueWordsInVideo:
                for w in mcw:
                    lev = self.st.checkLevenshteinDistance(
                        videoText = v.lower(),
                        rootText = w.lower()
                    )
                    if lev <= 2:
                        if v not in perfectMatch:
                            perfectMatch.append(v)
                        totPerfect += 1
                    avgLenDist.append(lev)                    
                uniqueWordsInVideo.append(v)
                totLevWord = (sum(avgLenDist)/len(avgLenDist))
                levList.append(totLevWord)
                levTuples.append((v,totLevWord))
        mcwAvg = sum(levList)/len(levList)
        levTuples = sorted(levTuples, key=itemgetter(1))
        pprint(levTuples)
        top3 = ""
        for a in range(3):
            word = str(levTuples[a][0])
            top3 +=" " + word
            for p in perfectMatch:
                lev = self.st.checkLevenshteinDistance(
                    videoText = word.lower(),
                    rootText = p.lower()
                )
                if lev <=3:
                    score += 100
        print "[videoInMostCommonWords] Top 3 words: " + str(top3)
        print "[videoInMostCommonWords] Perfect matches: " + str(perfectMatch)
        print "[videoInMostCommonWords] MCW Average lev distance: " + str(mcwAvg)
        score += (totPerfect*100) #- (mcwAvg*30)
        if totPerfect >= 2:
            if dt == 0:
                score += 77
            if dtLev <= 1:
                score +=400
            else:
                score -= 200
        print "[videoInMostCommonWords] Score: " + str(score)
        return score
    
    def convertVideoToString(self):
        """Converts video information in one dimension.

        Return string with video information.
        """
        videoText = ""
        entryTypes = [
            self.video.title,
            self.video.description,
            self.video.uploader,
            self.video.category,
            self.video.keywords
        ]
        entries = [ (e) for e in entryTypes if "None" not in str(e)]
        for entry in entries:
            videoText+= " " + str(entry)
        ta = DINXTextAnalysis.TextAnalysis(title = self.video.root, root = self.video.root)
        st = ta.sanitize(videoText.strip())
        return st
