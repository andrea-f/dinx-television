class SearchVideos():
    """This class searches videos based on an event."""


    def __init__(self, event = None):
        """Does nothing for now.
        
        :param event:
            From :class:`Event`, object.            
        """
        self.event = event

    def _setUpYTApi(self):
        """Sets up YouTube API client.

        Return YouTube API object.
        """
        import gdata.youtube
        import gdata.youtube.service
        ytService = gdata.youtube.service.YouTubeService()
        ytService.ssl = False
        return ytService

    def _setUpYTQuery(self, query = ""):
        """Sets up YouTube Query object.

        :param query:
            Which query to run on youtube, string.

        Return YouTube Query object.
        """
        import gdata.youtube
        import gdata.youtube.service
        #query.orderby = 'viewCount'
        queryyt = gdata.youtube.service.YouTubeVideoQuery()
        queryyt.racy = 'include'
        queryyt.orderby = 'relevance'
        #queryyt.safeSearch = 'none'
        queryyt.max_results=50
        queryyt.vq = query
        return queryyt

    def runYouTubeQuery(self, query):
        """Retrieves video feed based on query build on YouTube service.

        :param query:
            YouTube Query class item, object.

        Return list with video feed.
        """
        ytService = self._setUpYTApi()
        queryObject = self._setUpYTQuery(query = query)
        try:
            feed = ytService.YouTubeQuery(queryObject)
            print "[runYouTubeQuery] " + query + " RESULTS: " + str(len(feed.entry))
        except:
            feed = None
        return feed

    def getVideo(self, id = "2NxVMcf6TFc"):
        """Get YouTube video by id.
        
        :param id:
           11-chars matching specific YouTube video ID, string.
 
        Return :class:`gdata.youtube` object with video entry.
        """
        ytService = self._setUpYTApi()
        entry = ytService.GetYouTubeVideoEntry(video_id = id)
        return entry