#!/usr/bin/env python

import sys
import traceback
import os
import getopt
import ConfigParser
import logging
#import stacktracer

# Append the location of the main.py script to the path
dinxHome = os.path.realpath(os.path.dirname(sys.argv[0]))
sys.path.append(dinxHome)
os.environ["DINXHOME"] = dinxHome

# Which allows us to import the DINX class from the local DINX.py script
from populate import DINX
import populate

__usage__ = "\nUsage:\n\n main.py <--props propsfile> [--log logfile] [--cached] \n\nNote: $DINXHOME variable in \
property file assumes the location of the main.py script\n"

def main():
   """Main entry point for History of Sports Television program. Performs actions the in following order:
   
      1. Parses command line options
      2. Creates new HOST instance
      3. Sets up logging on HOST instance
      4. Loads properties into HOST instance
      5. Calls run() on HOST instance
      6. Exit

      Keyword arguments:
      None

   Return:
   void
   """
   #stacktracer.start_trace("trace.html",interval=5,auto=True)
   
   propertyFileLocation = ""
   logFileLocation = None
   logLevel = None
   opts = None
   useCache = False
   inputOption = None
   
   # parse command line options
   try:
      opts, args = getopt.getopt(sys.argv[1:], "h", ["help","inputindex=", "generate=", "props=","logfile=","cached"])
   except getopt.error, msg:
      print "Error parsing command line args: %s" % msg
      print "For help use --help or -h"

   if opts == None or len(opts) == 0:
      print __usage__
      sys.exit(2)
   
   # process options
   for o, a in opts:
      if o in ("-h", "--help"):
         print __usage__
         sys.exit(0)
         
      elif o == "--props":
         propertyFileLocation = a

      elif o == "--logfile":
         logFileLocation = a
      
      elif o == "--logLevel":
         logLevel = a

      elif o == "--cached":
         useCache = True

      elif o == "--generate":
          inputOption = a
      elif o == "--inputindex":
          pathOption = a
   # process arguments
   #for arg in args:
   #Ignore all other command line arguments for now
   
   if propertyFileLocation == "":
      print __usage__
      sys.exit(2)
   
   if not os.path.isfile(propertyFileLocation):
      print "Error: " + propertyFileLocation + " is not a file"
      sys.exit(2)
   
   # Create new DINX object
   try:
       dinx = DINX.DINX(pathOption = pathOption)
   except:
       dinx = DINX.DINX()
   
   
   # Setup logging
   try:
      if logLevel:
         dinx.log(logFileLocation, logLevel)
      else:
         dinx.log(logFileLocation)
   except logging.error, msg:
      print "Error setting up logging: %s" % (msg)
      sys.exit(2)
      
   #Set up DINX properties
   try:
      logging.info("Property file location(main.py): %s" % propertyFileLocation)
      dinx.readProperties(propertyFileLocation)
      
   except ConfigParser.Error, msg:
      print "Error reading property file: %s" % (msg)
      sys.exit(2)

   # Run engine
   try:
      try:
          #print "input option: %s" % inputOption
          dinx.run(inputOption = inputOption)
      except Exception as e:
          print "Error in main run method %s " % e
          #dinx.run(inputOption = "fullstack")
   except:
      exc_type, exc_value, exc_traceback = sys.exc_info()
      error = "Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
      logging.error(error)
      sys.exit(2)
   
   # Possibly do some cleanup here
   #stacktracer.stop_trace()
   sys.exit(0);
   
if __name__ == "__main__":
   main()
