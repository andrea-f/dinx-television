# coding: utf-8
import unittest
from mock import MagicMock
import os,sys
import json
lib = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'populate'))
sys.path.append(lib)
import wikipediaData
import DINXGenerateTextFromWWW
import DINXTextAnalysis
import DINXSearchVideos
import generateScore
import DINXGenerateItemFromText
from operator import itemgetter
from pprint import pprint

class testDINXGenerateTextFromWWW(unittest.TestCase):
    def mockDinxLocation(self):
        """Mock location directories for Dinx functioning.

        Return dict with dir information.
        """
        dinxLocation = {
            "gDataLocation":self.__gDataLocation,
            "downloadedData":self.__downloadedDataLocation,
            "parsedData":self.__parsedDataLocation,
            "videoData":self.__videoDataLocation
        }
        return dinxLocation

    def mockJsonInputFile(self):
        """Mocks required json input file variable.

        Return dict with input information:
            * **Root** -- List_of_riots, string.
            * **RootName** -- Verbose name for root, string.
            * **Duration** -- Minimum duration for a video in this category, int.
            * **Score** -- Minimum score for video in this category to be accepted in DB, int.
            * **Description** -- Description about root, currently always empty, string.
            * **RelatedWords** -- Set of words to either include, search with root, and exclude from search, comma separated, string.
            * **Category** -- Parent of root, string.
            * **CategoryType** -- Subcategory of root, string.
            * **Override** -- Save again from zero if year has already been processed, string.
            * **Language** -- Language to use when retrieving information from wikipedia, string.
            
        """
        jif = {
            "Root":"List_of_riots",
            "RootName":"Riots",
            "Duration":"30",
            "Score":"40",
            "Description":"",
            "RelatedWords":"Riots,unrelated-home for sale,unrelated-home for sale,unrelated-homes,unrelated-sale,unrelated-homes,unrelated-home,unrelated-apartment,unrelated-house,luce,report,istituto luce,incom,cinegiornale,news,breaking news,unrelated-home,unrelated-property,unrelated-for sale,unrelated-house,white house,white house",
            "Category":"history",
            "CategoryType":"riots",
            "Override": "no",
            "Language":"en"
        }
        return jif

    def testWalkPath(self):
        """Test wikipedia download and link extraction cycle."""
        dl = self.mockDinxLocation()
        jif = self.mockJsonInputFile()
        wp = DINXGenerateTextFromWWW.GenerateTextFromWWW(jif, dl)
        self.assertEqual(1 + 2, 3, "1 + 2 not equal to 3")

    def testStep(self):
        """Test getting one page from wikipedia.

        Return var:
            * **allLinks** -- All links found in page, list.
            * **False** -- False if there was error in getting page and links, boolean.
        """

    def testSanitize(self):
        """Test text conversion to latin1 and some char adjustment.
        """

class MockData():
    """This class mocks different data objects and types used in Dinx."""

    def __init__(self, query = "", location = ""):
        self.query = query
        if len(query) == 0:
            #self.query = "Timeline_of_events_in_the_Cold_War"
            #self.query = "Years_of_lead(Italy)"
            #self.query = "Coca-Cola"
            #self.query = "Timeline_of_operating_systems"
            #self.query = "Derby_della_Capitale"
            self.query = "List_of_riots"
            #self.query = "Italian Communist Party"
        else:
            self.query = query
        self.language = 'en'
        self.sampleSentence = "Something even more random to get a hash from"
        self.category = "history"
        self.initialDate = None
        self.root = "Riots"
        #self.root = "Years of Lead"
        self.specificDate = None
        #self.root = "Cold War"
        #self.root = "Italian Communist Party"
        #self.root = "Coca-Cola"
        #self.root = "Operating Systems"
        self.analyseVideos = False
        self.savedVideoSents = self.root + "_" + "youtube_queries"
        self.score = 0
        if len(location) == 0:
            self.location = "/home/legionovainvicta/app/bitbucket/dinx-television/django-dinx-television/DINXengine/Software/src/Data/text"
        else:
            self.location = location

    def mockEvents(self):
        """Mocks Event class."""
        items = []
        items_name = []
        sent = {
            "category": "history",
            "nerList": [
                {"word": "Rome", "type": "City"},
                {"word": "investigative journalist", "type": "Position"},
                {"word": "Mino Pecorelli", "type": "Person"},
                {"word": "20", "type": "DATE"},
                {"word": "March", "type": "DATE"}
            ],
            "links": ["Mino Pecorelli", "Rome"],
            "relevantWords": [],
            "root": "Years of Lead",
            "date": 1978,
            "normalized": 20031978,
            "hash": 52871277519190178461302899563807507515,
            "sent": "On 20 March, investigative journalist [[Mino Pecorelli]] was gunned down in his car in [[Rome]]."
        }
        sent1 = {
            "category": "history",
            "nerList": [
                    {"word": "Germany", "type": "Country"},
                    {"word": "February", "type": "DATE"},
                    {"word": "8", "type": "DATE"},
                    {"word": "Yalta", "type": "ORGANIZATION"},
                    {"word": "Conference", "type": "ORGANIZATION"}
            ],
            "links": ["Yalta Conference"],
            "relevantWords": [],
            "date": 1945,
            "root": "Cold War",
            "normalized": "08021945",
            "hash": 231834222555305362115481361936522411400,
            "sent": "* February 8: The [[Yalta Conference]] occurs, deciding the post-war status of Germany."
        }
        event = DINXTextAnalysis.Event()
        event.eventName = str(sent['date']) + " " +sent['sent']
        event.category = sent['category']
        event.date = str(sent['date'])
        event.hash = sent['hash']
        event.normalized = sent['normalized']
        event.root = sent['root']
        event.helperWords = sent['relevantWords']
        for item in sent['nerList']:
            i = DINXTextAnalysis.Item()
            i.nameOfEntity = item['word']
            i.ner = item['type']
            i.category = event.category
            i.root = event.root
            i.hashList = [event.hash]
            i.dates = [event.date]
            items_name.append(item['word'])
            items.append(i)
        event.items_name = items_name
        event.items_in_event = items
        return event

    def loadData(self, fileName = ""):
        """Loads data to be searched.

        :param fileName:
            Add to query path name, string.

        Return json formatted dict with file info.
        """
        fn = ""
        if len(fileName)>0:
            fn = fileName
        else:
            fn = self.query
        with open(self.location+"/"+fn+".json",'r') as jsonFile:
            content = jsonFile.read()
        jsonObject = json.loads(content)
        return jsonObject
    
    def initData(self):
        """Mock data initialization."""
        initDict = {
            "query": self.query,
            "language": self.language,
            "sampleSentence": self.sampleSentence,
            "category": self.category,
            "root": self.root,
            "score": self.score,
            "location": self.location,
            "initialDate": self.initialDate,
            "specificDate": self.specificDate,
            "savedVideoSents": self.savedVideoSents,
            "analyseVideos": self.analyseVideos
        }
        return initDict

class testWikipediaData(unittest.TestCase):    

    def setUp(self):
        md = MockData()
        init = md.initData()
        self.query = init['query']
        self.language = init['language']

    def tearDown(self):
        #self.query.dispose()
        self.query = None

    def testDownloadWikipediaDirect(self):
        """Test getting one page with wikipedia API.

        Return retrieved page from wikpedia.
        """
        query = self.query
        language = self.language
        wd = wikipediaData.wikipediaData(query = query, language = language)
        page = wd.downloadWikipediaDirect()
        #Comment out to avoid saving file
        fileName = "sampleData/"+query.replace(' ','_')+".json"
        f = open(fileName,'a')
        dt = json.dumps(page)
        f.write(dt)
        pageInfo = page['query']['pages']
        for pageId in pageInfo:
            pg = pageInfo[pageId]
        assert isinstance(page, dict)
        print pg['title'], query
        self.assertEqual(pg['title'], query, "Retrieved page is not equal to requested query.")
        assert os.path.isfile(fileName)

    def testExploreWikipediaJSON(self):
        """Tests parsing data from Wikipedia.
        """
        pathToData = "sampleData/"+self.query.replace(' ','_')+".json"
        with open(pathToData, 'r') as content_file:
            pageRaw = content_file.read()
        page = json.loads(pageRaw)
        wd = wikipediaData.wikipediaData(rawData = page)
        links = wd.exploreWikipediaJSON()
        assert isinstance(links,list)

    def testGetSpecificData(self):
        """Test reg expression on text.
        """
        pathToData = "sampleData/"+self.query.replace(' ','_')+".json"
        with open(pathToData, 'r') as content_file:
            page = content_file.read().replace('[[','').replace(']]','').replace('-',' ').replace(',',' ').replace('\'',' ').replace('\\u2013','')
        wd = wikipediaData.wikipediaData(rawData = page)
        #links = wd.getSpecificData(page,regEx = r'(\d{4}[\s\w\\]+)(?!\\n\*)')
        links = wd.getSpecificData(page,regEx = r'(\\n\\n[\s\w\\]+)(?!\\n\*)')
        #links = wd.getSpecificData(page,regEx = r'(?!\\n\*)([\s\w\\]+)(?!\\n\*)')
        for link in links:
            print "\nLINK: " + link + "\n"
        assert isinstance(links, list)
        assert isinstance(links[0], string)

class testTextAnalysis(unittest.TestCase):

    def setUp(self):
        md = MockData()
        init = md.initData()
        self.query = init['query']
        self.language = init['language']
        self.sampleSentence = init['sampleSentence']
        self.category = init['category']
        self.root = init['root']
        self.score = init['score']
        self.location = init['location']
        self.initialDate = init['initialDate']
        self.specificDate = init['specificDate']
        self.analyseVideos = init['analyseVideos']
        self.svs = init['savedVideoSents']
        if self.analyseVideos is True:
            pathToData = self.location + "/" + self.svs + ".json"
        else:
            pathToData = "sampleData/"+self.query.replace(' ','_')+".json"
        #title = pathToData.split('/')[1].split('.')[0]
        if os.path.isfile(pathToData):
            pass
        else:
            dta = DINXTextAnalysis.DownloadTextData(query = self.query, language = self.language)
            pathToData = dta.getWikipediaPage()
        with open(pathToData, 'r') as content_file:
            self.page = content_file.read()

    def tearDown(self):
        self.query = None

    def testSanitize(self):
        """Tests sanitize method of TextAnalysis to remove unwanted characters"""
        ta = DINXTextAnalysis.TextAnalysis()
        st = ta.sanitize(self.page)
        errors = 0
        if ("<ref>" or "</ref>") in st:
            errors = errors + 1
        self.assertEqual(errors,0,'[testSanitize] Not all references to ref were removed.')

    def testGetSentences(self):
        """Tests get sentences method to split text into sentences."""
        # Not strictly necessary for testing but gives
        # clearer results
        ta = DINXTextAnalysis.TextAnalysis()
        st = ta.sanitize(self.page)
        sents = ta.getSentences(text = st)
        sents = sorted(sents)
        assert isinstance(sents, list)

    def testExtractNerInformation(self):
        """Tests named entity recognition extraction from text using open calais.
        """        
        ta = DINXTextAnalysis.TextAnalysis(title = self.query)
        st = ta.sanitize(self.page)
        sents = ta.getSentences(text = st)
        # To avoid relying on other methods
        # uncomment the following line to do a simple split. The results are much poorer.
        #s1 = sent.split('\\n')
        nerSents = ta.extractNerInformation(sentences = sents)
        assert isinstance(nerSents, list)
        assert isinstance(nerSents[0], dict)
        #self.asserEqual(nerSents[0]['title'], title, "Mismatch in titles.")

    def mockNerSentences(self):
        """Mocks list of ner sentences to be saved."""
        nerSents = [{
            "date": 0,
            "nerList": [
                {"type": "City", "word": "Berlin"},
                {"type": "Country", "word": "Germany"},
                {"type": "TOPIC", "word": "Politics"},
                {"type": "TOPIC", "word": "Environment"},
                {"type": "TOPIC", "word": "Disaster_Accident"},
                {"type": "TOPIC", "word": "Sports"}
            ],
            "links": ["Yalta Conference"],
            "sent": "Before the agreement to divide Germany into four zones ([[Yalta Conference]]), the four nations also decide to split Germany's capital, Berlin, into four zones as well.\n",
            "relevantWords": []
        }]
        return nerSents

    def testStringifyNerSents(self):
        """Test nerSents conversion to string."""
        #Use live data
        md = MockData(query = self.query, location = self.location)
        jsonObject = md.loadData()
        nerSents = jsonObject['sents']
        #Use test data
        #nerSents = self.mockNerSentences()
        ta = DINXTextAnalysis.TextAnalysis()
        nerPage = ta.stringifyNerSents(nerSents = nerSents)
        assert isinstance(nerPage, unicode)

    def testSaveAnalysedData(self):
        """Test json object saving."""
        #nerSents = self.mockNerSentences()
        ta = DINXTextAnalysis.TextAnalysis(location = self.location,title = self.query, category = self.category, root = self.root)
        st = ta.sanitize(self.page)
        sents = ta.getSentences(text = st)
        nerSents = ta.extractNerInformation(sentences = sents)
        wordList = ta.mostCommonWordsInPage(page = nerSents)
        #print self.query, wordList
        jsonObject, jsonFile = ta.saveAnalysedData(sents = nerSents, mce = wordList)
        assert isinstance(jsonObject, dict)
        assert isinstance(jsonObject['sents'], list)
        assert os.path.isfile(jsonFile)

    def testHashSentence(self):
        """Tests whether hash is created from sentence"""
        ta = DINXTextAnalysis.TextAnalysis()
        hash = ta.hashSentence(sent = self.sampleSentence, hashLength = 16)
        print "[testHashSentence] Sentence: '" + self.sampleSentence + "'"
        print "[testHashSentence] Hash: " + str(hash) + "\n"
        assert isinstance(hash, long)

    def testDoubleCheck(self):
        """Tests whether checking for duplicates works."""
        ta = DINXTextAnalysis.TextAnalysis()
        #This...
        hash = ta.hashSentence(sent = self.sampleSentence, hashLength = 16)
        #Or uncomment this to test purely
        #hash = '109254163844465769304174429373126707564'
        exists = ta.doubleCheck(hash = hash)
        print "[testDoubleCheck] Hash " + str(hash) + " exists? " + str(exists)
        assert isinstance(exists,bool)

    def mockSentDict(self):
        """Mocks analysed sentence structure."""
        s = {
        'category': "History",
        'root': "Cold War",
        'title': "Timeline_of_Cold_War",
        'date': 1951,
        'hash': 141917584918823460588742794315220714012L,
        'links': ['Julius and Ethel Rosenberg', 'Cold War'],
        'nerList': [
            {'type': 'Person', 'word': 'Julius Rosenberg'},
            {'type': 'Person', 'word': 'Ethel Rosenberg'},
            {'type': 'TOPIC', 'word': 'War_Conflict'},
            {'type': 'TOPIC', 'word': 'Law_Crime'},
            {'type': 'TOPIC', 'word': 'Politics'},
            {'type': u'DATE', 'word': u'March'},
            {'type': u'DATE', 'word': u'29'},
            {'type': u'MISC', 'word': u'World'},
            {'type': u'MISC', 'word': u'War'},
            {'type': u'MISC', 'word': u'II'}
        ],
        'relevantWords': [],
        'sent': '* March 29:  [[Julius and Ethel Rosenberg]] are convicted of espionage for their role in passing atomic secrets to the Soviets during and after World War II.'}
        return s


    def testCreateEventObject(self):
        """Tests whether :class:`Event` object is created correctly."""
        s = self.mockSentDict()
        ta = DINXTextAnalysis.TextAnalysis()
        event = ta.createEventObject(s=s)
        assert isinstance(event.date, int)
        assert isinstance(event.items_in_event, list)
        assert isinstance(event, DINXTextAnalysis.Event)
        assert isinstance(event.items_in_event[0], DINXTextAnalysis.Item)

    def testCreateEventMetaData(self):
        """"Tests creation of event metadata information.
        Such as:
            * dinxLocation
            * relatedWords
            * params(minimum score, duration)
        """
        ta = DINXTextAnalysis.TextAnalysis()
        metaData = ta.createEventMetaData()
        assert isinstance(metaData, dict)
        assert isinstance(metaData['params']['duration'], int)
        assert isinstance(metaData['params']['minimum_score'], int)
        assert isinstance(metaData['dinxLocation'], dict)
        assert isinstance(metaData['relatedWords'], list)
        self.assertEqual(os.path.exists(metaData['dinxLocation']['gDataLocation']), 1, "YouTube API files not found.")
        self.assertEqual(os.path.exists(metaData['dinxLocation']['videoData']), 1, "Videodata folder does not exist.")

    

    def testSearchEvent(self):
        """Gets videos related to an event."""
        import gdata
        type = "all"
        ta = DINXTextAnalysis.TextAnalysis()
        mockedEvent = MockData.mockEvents()
        md = MockData(query = self.query, location = self.location)
        jsonObject = md.loadData()
        nerPage = ta.stringifyNerSents(nerSents = jsonObject['sents'], type = type)
        mce = ta.mostCommonWordsInPage(page = nerPage)
        pprint(sorted(mce))
        fn = ta.searchEvent(relatedWords = mce, event = mockedEvent, queries = ['1978 Mino Pecorelli'])
        #print "FILENAME: %s" % fn
        assert os.path.isfile(fn)

    def testMostCommonWordsInPage(self):
        """Extracts most common words in page, of frequency between 10% and 50%."""
        nerTypes = [
            {"name": "TOPIC", "minFreq": 2.0, "maxFreq":80},
            {"name": "person", "minFreq": 0.2, "maxFreq":80},
            {"name":"all", "minFreq": 0.7, "maxFreq":10},
            {"name": "link", "minFreq": 0.1, "maxFreq":80},
            {"name": "page", "minFreq": 0.010, "maxFreq":900}
        ]
        ta = DINXTextAnalysis.TextAnalysis(root = self.root)
        md = MockData(query = self.query, location = self.location)
        if self.analyseVideos is True:
            jsonObject = md.loadData(fileName = self.root+"_youtube_queries")
        else:
            jsonObject = md.loadData()
        for nerType in nerTypes:
            nerPage = ta.stringifyNerSents(nerSents = jsonObject['sents'], type = nerType['name'])
            if "page" in nerType['name']:
                nerWords = ta.mostCommonWordsInPage(page = self.page, minFreq = nerType['minFreq'], maxFreq=nerType['maxFreq'])
                wordList = ta.reuniteWords(subset = nerWords, mainlist = nerWords, nerType = nerType['name'])
            else:
                mce = ta.mostCommonWordsInPage(page = nerPage, minFreq = nerType['minFreq'], maxFreq=nerType['maxFreq'])
                nerWords = ta.getNerWords(type = nerType['name'], nerSents = jsonObject['sents'])
                wordList = ta.reuniteWords(subset = mce, mainlist = nerWords, nerType = nerType['name'])
            print "\n[testMostCommonWordsInPage] " + self.root + "\n[testMostCommonWordsInPage] MC "+nerType['name']+ ": "
            pprint(wordList)
            print "[testMostCommonWordsInPage] Wordlist: "+str(len(wordList))
            assert isinstance(wordList, list)

    def testCompleteTextAnalysis(self):
        """Test getting videos from events."""
        fileNameList = []
        type1 = ["all","person", "topic"]
        ta = DINXTextAnalysis.TextAnalysis(
            location = self.location,
            title = self.query,
            category = self.category,
            root = self.root
        )
        st = ta.sanitize(self.page)
        sents = ta.getSentences(text = st)
        md = MockData(query = self.query, location = self.location)
        jsonObject = md.loadData()
        topWords = {}
        print self.root
        for t in type1:
            nerPage = ta.stringifyNerSents(nerSents = jsonObject['sents'], type = t)
            if "topic" in t:
                mce = ta.mostCommonWordsInPage(
                    page = nerPage,
                    root = self.root,
                    maxFreq = 80
                )
                mce = mce[-5:]
            else:
                mce = ta.mostCommonWordsInPage(
                    page = nerPage,
                    root = self.root
                )
            topWords[t] = mce
            pprint(mce)
        metaData = ta.createEventMetaData(
            relatedWords = mce,
            mcw = topWords['topic'],
            mce = topWords['person'],
            are = topWords['all'],
            score = self.score
        )
        dateRootEvents = ta.createRootDateEvent(
            category = self.category,
            root = self.root,
            initialDate = self.initialDate,
            sentences = sents
        )
        totalSearchList = []
        for qevent in dateRootEvents:
            queries1 = ta.createEventQueries(qevent)
            totalSearchList.append((qevent,queries1))        
        for sent in jsonObject['sents']:
            event = ta.createEventObject(s=sent,sentences = sents)
            queries = ta.createEventQueries(event)
            totalSearchList.append((event,queries))
        tsl =  sorted(totalSearchList,key=itemgetter(1), reverse = False)
        outlist1 = []
        for w in tsl:
            for qu in w[1]:
                if qu not in outlist1:
                    exists = ta.saveProcessedVideo(root = self.root, id = qu, saveKey = "all_queries", save = True)
                    outlist1.append(qu)
        outlist2 = []
        for eventQuery in tsl:
            if eventQuery[1] not in outlist2:
                fn = ta.searchEvent(specificDate = self.specificDate,queries = eventQuery[1], event = eventQuery[0], metaData = metaData)
                fileNameList.append(fn)
        for createdFile in fileNameList:
            if len(createdFile)>0:
                assert os.path.isfile(createdFile)

    def testMatchCorrectDate(self):
        """Test matching correct date to sentences."""
        ta = DINXTextAnalysis.TextAnalysis(title = self.query, root = self.root)
        st = ta.sanitize(self.page)
        sents = ta.getSentences(text = st)
        for sent in sents:
            print "=====================\n[testMatchCorrectDate] Sent: " + sent
            date = ta.matchCorrectDate(sent = sent, sentences = sents)
            print "[testMatchCorrectDate] Date: " + str(date) + "\n================"
            assert isinstance(date, int)
        
    def testCreateEventQueries(self):
        """Tests creation of queries from event."""
        ta = DINXTextAnalysis.TextAnalysis(location = self.location,title = self.query, category = self.category, root = self.root)
        st = ta.sanitize(self.page)
        sents = ta.getSentences(text = st)
        md = MockData(query = self.query, location = self.location)
        jsonObject = md.loadData()
        fn2 = self.location.replace('text','indexes')
        fnComplete = fn2 + "/"+self.root+"_queries_Testing.txt"
        content = open(fnComplete,'a')
        for sent in jsonObject['sents']:
            event = ta.createEventObject(s=sent,sentences = sents)
            print event.eventName
            content.write("\n=="+event.eventName.encode('utf-8')+ "==\n")
            queries = ta.createEventQueries(event)
            for query in queries:
                content.write(str(query.encode('utf-8'))+"\n")
        assert isinstance(queries,list)
        assert os.path.isfile(fnComplete)

    

class testSearchVideos(unittest.TestCase):

    def setUp(self):
        md = MockEvent()
        init = md.initData()
        self.query = init['query']
        
    def testRunYouTubeQuery(self):
        """Tests getting a feed from YouTube"""
        sv = DINXSearchVideos.SearchVideos()
        feed = sv.runYouTubeQuery(query = self.query)
        print "[runYouTubeQuery] " + str(feed.entry[0].media.title.text) + "\n"
        print feed.entry[0]



class testScoring(unittest.TestCase):
    
    def setUp(self):
        """Sets up scoring"""
        md = MockData()
        init = md.initData()
        self.root = init['root']
        self.str1 = "1945 Before the agreement to divide Germany into four zones ([[Yalta Conference]]), the four nations also decide to split Germany's capital, Berlin, into four zones as well."
        self.str2 = "EXERTION Berlin being rebuilt, Germany 1945"

    def testLevenshteinDistance(self):
        """Test levenshtein distance between 2 strings."""
        gift = DINXGenerateItemFromText.GenerateItemFromText(self.root)
        lev = gift.calculateStringsSimilarity(self.str1, self.str2)
        print "[testLevenshteinDistance] Str1: " + str(self.str1) + "\nStr2: " + str(self.str2) + "\nLev: " + str(lev)
        assert isinstance(lev, int)

    def testExtractNerData(self):
        """Tests generating score from ner in video."""
        import gdata
        type1 = "all"
        ta = DINXTextAnalysis.TextAnalysis()
        md = MockData()
        mockedEvent = md.mockEvents()
        print type(mockedEvent)
        print type(mockedEvent.items_in_event[0])
        jsonObject = md.loadData()
        nerPage = ta.stringifyNerSents(nerSents = jsonObject['sents'], type = type1)
        mce = ta.mostCommonWordsInPage(page = nerPage)
        metaData = ta.createEventMetaData(relatedWords = mce)
        vids = DINXSearchVideos.SearchVideos(event = mockedEvent)
        queries = ta.createEventQueries(mockedEvent)
        for q in queries:
            feed = vids.runYouTubeQuery(query = q)
            for entry in feed.entry:
                video = ta.createVideoObject(entry = entry, event = mockedEvent, q = q)
                svn = generateScore.ScoreVideoNer(video = video, params = metaData['params'])
                sc, videoEnts = svn.extractNerData()
            print """
            [testExtractNerData] Title: %s\n
            [testExtractNerData] Entities: %s\n
            [testExtractNerData] Query: %s\n
            [testExtractNerData] Event: %s\n
            [testExtractNerData] Score: %s\n


            """ % (
                str(video.title.encode('utf-8')),
                str(videoEnts),
                str(q),
                str(video.eventName),
                sc
            )
            assert isinstance(sc,int)



if __name__ == '__main__':
    suite = unittest.TestSuite()
    #suite.addTest(testWikipediaData('testDownloadWikipediaDirect'))
    #suite.addTest(testWikipediaData('testExploreWikipediaJSON'))
    #suite.addTest(testWikipediaData('testGetSpecificData'))
    #suite.addTest(testTextAnalysis('testSanitize'))
    #suite.addTest(testTextAnalysis('testGetSentences'))
    #suite.addTest(testTextAnalysis('testExtractNerInformation'))
    #suite.addTest(testTextAnalysis('testSaveAnalysedData'))
    #suite.addTest(testTextAnalysis('testHashSentence'))
    #suite.addTest(testTextAnalysis('testDoubleCheck'))
    #suite.addTest(testTextAnalysis('testCreateEventObject'))
    #suite.addTest(testTextAnalysis('testCreateEventMetaData'))
    #suite.addTest(testTextAnalysis('testSearchEvent'))
    #suite.addTest(testTextAnalysis('testMostCommonWordsInPage'))
    suite.addTest(testTextAnalysis('testCompleteTextAnalysis'))
    #suite.addTest(testTextAnalysis('testStringifyNerSents'))
    #suite.addTest(testScoring('testLevenshteinDistance'))
    #suite.addTest(testTextAnalysis('testMatchCorrectDate'))
    #suite.addTest(testTextAnalysis('testCreateEventQueries'))
    #suite.addTest(testScoring('testExtractNerData'))
    #unittest.main()
    unittest.TextTestRunner().run(suite)
    