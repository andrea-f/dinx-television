import os
from setuptools import setup

README = open(os.path.join(os.path.dirname(__file__), 'README.rst')).read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

try:
    vers = '0.1.%s' % os.environ.get('BUILD_NUMBER')
except Exception as e:
    print "No environment variable: %s" % e
    vers = '0.1.0'


setup(
    name='django-dinx-television',
    version=vers,
    packages=['djangoApp','DINXengine'],
    include_package_data=True,
    license='BSD License',  # example license found in LICENSE
    description='TV channel generator',
    long_description=README,
    url='http://www.dinx.tv/',
    author='DINX LTD',
    author_email='info@dinx.tv',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License', # example license
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
