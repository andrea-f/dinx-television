from django.conf.urls.defaults import *
from dinx.dinx_app.views import xhr, about, api, cd, watchHome, watch, videos, videosHome, customSearch, faq
from django.conf import settings
#from registration import activate,register
# Uncomment the next two lines to enable the admin:
from django.contrib import admin,auth
from django.contrib.auth import views
from django.views.generic.simple import direct_to_template
admin.autodiscover()



urlpatterns = patterns('',
    # Example:
    # (r'^dinx/', include('dinx.foo.urls')),
    # Uncomment the admin/doc line below to enable admin documentation:
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^crossdomain.xml$','flashpolicies.views.simple',
        {'domains': ['localhost','localhost:8000', 'dinx.tv','127.0.0.1:8000','127.0.0.1']}
    ),
    # Uncomment the next line to enable the admin:
    (r'^admin/', include(admin.site.urls)),
    (r'^accounts/', include('registration.backends.default.urls')),    
    (r'^about/', about),
    (r'^api/(?P<channelRequested>[\w|\W]+)/$',api),
    (r'^watch/(?P<channelRequested>[\w|\W]+)/$',watch),
    (r'^videos/(?P<channelRequested>[\w|\W]+)/$',videos),
    #(r'^search/$',search),
    ('^watch/$', watchHome),
    (r'^videos/$',videosHome),
    ('^$', videosHome),
    (r'^faq/', faq),
    #('^hello/$', hello),
    #(r'^(\d{4})/([a-z])/$',getEvents),
    (r'^trova/', customSearch),
    (r'^cerca/', include('haystack.urls')),
    #(r'^(?P<year>\d{4})/(?P<item>[\w|\W]+)/$',yearItem),
    #(r'^(?P<year>\d{4})/$',defaultYear),
    ('^ajax/$', xhr),
    #(r'^([\w|\W]+)/([\w|\W]+)/',twoStepUrl),
    #(r'^([\w|\W]+)/',checkWord),
    #(r'^(?P<year>\d{4})/(?P<root>[\w|\W]+)/(?P<item>[\w|\W]+)/$',item_in_root_in_year),
    #(r'^((?P<root>[\w|\W]+)/?P<year>\d{4})/(?P<item>[\w|\W]+)/$',item_in_root_in_year),
    #(r'^watch/$((?P<root>[\w|\W]+)/(?P<item>[\w|\W]+)/?P<year>\d{4})/$',api),
    
    
    #(r'^(?P<year>\d{4})/(?P<month>[a-z]{3})/$','month_display'),

    
)

#urlpatterns += patterns(
#        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
#    )

urlpatterns += patterns('',
                       url(r'^login/$',
                           auth.views.login,
                           {'template_name': 'registration/login.html'},
                           name='auth_login'),
                       url(r'^logout/$',
                           auth.views.logout,
                           {'template_name': 'registration/logout.html'},
                           name='auth_logout'),
                       url(r'^password/change/$',
                           auth.views.password_change,
                            {'template_name': 'registration/password_change_form.html'},
                           name='auth_password_change'),
                       url(r'^password/change/done/$',
                           auth.views.password_change_done,
                            {'template_name': 'registration/password_change_done.html'},
                           name='auth_password_change_done'),
                       url(r'^password/reset/$',
                           auth.views.password_reset,
                            {'template_name': 'registration/password_reset_form.html'},
                           name='auth_password_reset'),
                       url(r'^password/reset/confirm/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
                           auth.views.password_reset_confirm,
                            {'template_name': 'registration/password_reset_confirm.html'},
                           name='auth_password_reset_confirm'),
                       url(r'^password/reset/complete/$',
                           auth.views.password_reset_complete,
                            {'template_name': 'registration/password_reset_complete.html'},
                           name='auth_password_reset_complete'),
                       url(r'^password/reset/done/$',
                           auth.views.password_reset_done,
                            {'template_name': 'registration/password_reset_done.html'},
                           name='auth_password_reset_done'),
)



