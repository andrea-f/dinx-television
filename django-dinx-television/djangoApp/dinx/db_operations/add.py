# Add information to models
import logging
import traceback
import sys
import os
dinx = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..'))
sys.path.append(dinx)

class ReadData(object):
    def __init__(self, folders = None, root = None, item = {}):
        self.custom_root = root
        self.item = item
        self.folders = folders
    
    def scanFolderPath(self, folder = None):
        """Get all videodata folder references in path.

        :param folder:
            Which folder to read from, string.

        Return string with the full path of the folder named 'videodata' containing json to add to db

        >>> print "scanFolderPath matching folder %s " % scanFolderPath(folder='/some/folder/containing/videodata/')
        '/some/folder/containing/videodata/'

        Return "" if videodata is not found.
        """
        if folder is None:
            folder = self.folders
        rootdir = folder
        folders_to_read = []
        matchedFolder = ""
        #print os.walk(rootdir)
        for root, subFolders, files in os.walk(rootdir):            
            print "sub: " % subFolders
            for folder in subFolders:
                print "%s has subdirectory %s" % (root, folder)
                if "videodata" in folder:
                    #print "%s has videodata subdirectory %s" % (root, folder)
                    folder = os.path.join(root,folder)
                    matchedFolder = folder
                    folders_to_read.append(folder)
        self.matching = folders_to_read
        print "scanFolderPath matching folder %s " % matchedFolder
        return matchedFolder

    def loadJSON(self, libPath = '../../../DINXengine/Software/src/populate'):
        """Read serialized JSON, start from parsed data folder then read path and files from each videodata folder. 
        Calls nerClassification.WordsToEvents and SaveEventsInDatabase.saveEvents.

        :param libPath:
            The name containing the dinxEngine python files, string.
        """
        import nerClassification
        import fileHandler
        libPathAbs = os.path.abspath(libPath)
        sys.path.append(libPathAbs)
        totalVideos = 0
        for matchingDir in self.matching:
            filehandler = fileHandler.fileHandler(folders = matchingDir)
            fileList = filehandler.listFilesInDir()
            for singleFile in fileList:
                print single_file
                getdate = nerClassification.WordsToEvents(singlefile = singleFile)
                date = getdate.getDate()
                filehandler = fileHandler.fileHandler(fileName = singleFile)
                listOfEvents = filehandler.readJson()
                #print "list: %s " % list_of_events
                saveindb = SaveEventsInDatabase(listOfEvents = listOfEvents['info'],root = self.custom_root, date = date)
                savedVideos = saveindb.saveEvents()
                totalVideos = totalVideos + savedVideos
        print "[loadJSON] Total saved videos: %s" % totalVideos

class SaveEventsInDatabase:
    def __init__(self, list_of_events = None,root = None, itemInput = None, date = None ):
        self.list_of_events = list_of_events
        self.custom_root = root
        self.itemInput = itemInput
        self.date = date
    
    def saveEvents(self, listOfEvents = None):
        """Save in database all events contained in a file.

        :param listOfEvents:
            List of event objects to save in DB.

        Return int with total saved videos in database.

        """
        if listOfEvents is None:
            listOfEvents = self.list_of_events
        self.outlist = []
        savedVideos = 0
        for event in listOfEvents:
             self.event = event
             self.eventCat = event['category'] 
             try:
                 self.saved_videos = []
                 for video in event['videos']:
                    if video:
                        self.video = video
                        try:
                            v, created = self.addVideo()
                            if created is True:
                                self.saved_videos.append(v)
                                savedVideos += 1
                        except Exception as e:
                            print "[saveEvents] Error in adding video %s" % e
                            #exc_type, exc_value, exc_traceback = sys.exc_info()
                            #error = "[saveEvents] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
                            #print error
                            #logging.error(error)
                            #sys.exit(2)
                 self.saved_items = []    
                 if len(event['items']) > 0:
                     for item in event['items']:
                         self.item = item
                         i = self.addItem()
                         self.saved_items.append(i)                 
                 self.addEvent()
             except Exception as e:
                 print "[saveEvents] error in adding videos %s" % e
        return savedVideos
                                
    def addVideo(self, video = None):
        """Add video to db

        :param video: Django video DB object containing title, description, url, thumb url ...

        Return two variables with:
            * **v** -- Created video object in database, dict.
            * **created** -- True if video is created, false if video is already present, bool.
        """
        from dinx_app.models import Video
        if video is None:
            video = self.video
        try:
            t = video['title'].encode('utf-8')
        except:
            t = video['title']
        #try:
        #    date = self.formatNormalizedDate(self.event['normalized'])
        #except:
        #    date = "%s-01-01" % self.event['date']
        t=t[:199]
        self.title_name = t
        if video['description']:
             d = video['description'].encode('utf-8')
        else:
             d = video['description']
        if video['keywords']:
            k = video['keywords'].encode('utf-8') 
        else:
            k = video['keywords']
        if video['url']:
            v,created = Video.objects.get_or_create(
                url = video['url'],
                title = t,
                description = d,
                thumbnail = video['thumbnail'],
                duration = video['duration'],
                viewcount = video['viewcount'],
                keywords = k,
                score = video['score'],
                root = video['root']
            )
            print "[addVideo] Title video added to DB: "+video['title']
            return v,created       
        
    def _videoExists(url):
        """Check if video url exists in database
           returns true or false

        :param url:
            Url to be searched in database if video exists, string.

        Return boolean with True or false if video exists or not.
        """
        from dinx_app.models import Video
        result = Video.objects.get(url=url)
        exists = False;
        if url in result.url:
            exists = True
        return exists
    
    def addItem(self, itemInput = None):
        """Add item to db.

        :param itemInput:
            Dict with ner, category, occurrences, name of item, read from class instance.

        Return dict with django query set object containing saved item information.
        """
        from dinx_app.models import Item
        if itemInput is None:
            itemInput = self.itemInput
        if itemInput is not None:
            item = itemInput
            root = item['root']
        else:
            item = self.item
            root = self.event['root']
        try:
            n = item['name'].encode('utf-8')
        except:
            n = item['name']
        if len(root) > 0:
            i, created = Item.objects.get_or_create(name = n, ner = item['ner'], category = item['category'], root = root, defaults={'occurrences': 1})
            return i
        else:
            print "[addItem] No root: |%s|" % root

    def formatNormalizedDate(self, date):
        """Format normalized date so it can be correctly saved in db."""
        try:
            norm = date.replace(' ','0')
            print "[formatNormalizedDate] date %s" % norm
            year = norm[0:4]
            month = norm[4:6]
            day = norm[6:8]
            if len(day)==2:
                dt1 = "%s-%s-%s" % (year, month, day)
                print "[formatNormalizedDate] date full %s" % dt1
            else:
                dt1 = "%s-%s-01" % (year, month)
                print "[formatNormalizedDate] date no day %s" % dt1
            return dt1
        except Exception as e:
            print "[formatNormalizedDate] Error: %s" % e

    def addEvent(self, event = None):
        """Adds an event in db

        :param event:
            Event date, category etc, dict.
        """
        from dinx_app.models import Event, Trunk
        if event is None:
            event = self.event
        root = self.event['root']
        print "[addEvent] date of event: "+str(self.event['normalized'])
        try:
            dt = self.formatNormalizedDate(self.event['normalized'])
        except:
            dt = "%s-01-01" % event['date']            
        r, created = Trunk.objects.get_or_create(category = event['category'], root = root, date = dt)
        n = event['name'].encode('utf-8')
        e, created = Event.objects.get_or_create(name = n[:252], date = dt,defaults={'occurrences': 1})
        if created is False:
            e.occurrences += 1
        e.trunk.add(event['root'])
        for vid in self.saved_videos:
            if vid:
                e.videos.add(vid)
        for itm in self.saved_items:
            e.items.add(itm)

def main():
    try:
        getdata = ReadData(folders = sys.argv[1], root = sys.argv[2])
    except Exception as e:
        getdata = ReadData(folders = sys.argv[1])
    matching = getdata.scanFolderPath()
    getdata.loadJSON()
        

if __name__ == "__main__":
    main()

