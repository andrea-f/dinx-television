#!/usr/bin/env python
# coding: utf-8
import time
import os,sys
import traceback
import requests
import json
l = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..'))
lib = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..','..','..','DINXengine/Software/src/'))
os.environ["DINXHOME"] = lib
DATALOCATION = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..','..','..','DINXengine/Software/src/Data/downloaded/'))
NERPARSERLOCATION = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..','..','..','DINXengine/Software/src/Libraries/stanfordnlp/'))
NEROUTPUTFOLDER = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..','..','..','DINXengine/Software/src/Data/parsed/'))
#nerParserLocation = "/home/legionovainvicta/app/DINXengine/Software/Libraries/stanfordnlp"
#print lib
pop = "%s/populate" % lib
sys.path.append(pop)
import fileHandler
import DINXGenerateItemFromText
import DINXGenerateItemFromNer
from getReddit import GenerateRedditData
from DINX import DINX
sys.path.append(lib)
t = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', '..'))
sys.path.append(t)
os.environ['DJANGO_SETTINGS_MODULE'] = 'dinx.settings'
import remove
sys.path.append(l)
from dinx_app.models import Video, Event
import add
from django.db.models import Q


class Update:
    """This class takes care of retrieving records from db, split in ner, category, year, name"""

    def __init__(self, option = None,folder = None, root = None, param = None, wordBinding = None):
        self.option = option
        self.folder = folder
        self.results = []
        self.root = root
        self.param = param
        self.wordBinding = wordBinding


    def updateDatabase(self, option = None):
        """Update database by deleting unvailable, remove duplicates
        Possible options:
            --occurrences: Count total number of items in a [root]
            --imdb: try to associate name of a [root] with imdb info
            --neritems: generate items by parsing downloaded data by event name = file name, parse filename for person location orgs
            --clearitems: delete all items belonging to a [root]
            --available: check which videos were removed and delete from db by [root]
            --mostcommon: extract common items from [[parameter]] = 'video' title or 'event' name or 'item'
            --rename: rename word with another word, syntax: python update.py --rename [root/video/item] originalWord-replacementWord
            --getitemsfromvideos: match item names in title or description of videos
            --removebyword: removes all references and videos containing [root] <- root can also be an arbitray word
            --social: get all video links from social networks
            --searchitems: generate videos from each item by root
            --removeroot: remove all references and videos to [root]

        :param option:
            Options to update database by, string.

        """
        if option is None:
            option = self.option
        startProcessingTime = time.clock()
        try:
            if len(self.root) != 0:
                results = Event.objects.filter(trunk__root__icontains=self.root)
                print "[updateDatabase] %s for %s" % (option, self.root)
            else:
                results = Event.objects.all()
        except:
            results = Event.objects.all()
            pass
        if "--occurrences" in option:
            results = self._selectDbTable('item')
            occurrences = self._checkOccurrances(results)
        elif ("--help" in option) or ("-h" in option) or ("help" in option):
            print """
            **** UPDATES DINX TELEVISION DATABASE ****

            Command is:

            user@example.com$: python update.py [option] [root] [[ parameter ]]

            Possible options:
            --occurrences: Count total number of items in a [root]
            --imdb: try to associate name of a [root] with imdb info
            --neritems: generate items by parsing downloaded data by event name = file name, parse filename for person location orgs
            --clearitems: delete all items belonging to a [root]
            --available: check which videos were removed and delete from db by [root]
            --mostcommon: extract common items from [[parameter]] = 'video' title or 'event' name or 'item'
            --rename: rename word with another word, syntax: python update.py --rename [root/video/item] originalWord-replacementWord
            --getitemsfromvideos: match item names in title or description of videos
            --removebyword: removes all references and videos containing [root] <- root can also be an arbitray word
            --social: get all video links from social networks
            --searchitems: generate videos from each item by root
            --removeroot: remove all references and videos to [root]
            """
        elif "--additemstoevents" in option:
           print  """Adds items to events from text data or webdata"""

        elif "--relateditems" in option:
            print """Show roosvelt hitler mussolini when watching churchill"""

        elif "--searchitems" in option:
            results = self._selectDbTable('event')
            #film ita is 3
            dinxengine = DINX(pathOption = 3)
            logFileLocation = "%s/logItems.txt" % lib
            propertyFileLocation = "%s/ConfigLibrary/host.properties" % lib
            dinxengine.log(logFileLocation)
            dinxengine.readProperties(propertyFileLocation)
            if self.param is not None:
                dinxengine.run(inputOption = 'videositems', results = results, option = self.param)
            else:
                dinxengine.run(inputOption = 'videositems', results = results)
            print "[updateDatabase] %s for %s" % (option, self.root)
            #send items information to generateVideos
            #create videos locally
            #run generate videos from main.py
            #match item as event name and associate root information
            #search and save videos
        elif "--social" in option:
            #reddit
            grd = GenerateRedditData()
            saved = grd.getRedditData()
            print "[updateDatabase] Saved %s from social" % saved
        elif "--imdb" in option:
            results = self._selectDbTable('event')
            for event in results:
                self.getMovieInformation(event)
            results = self._selectDbTable('item')
            occurrences = self._checkOccurrances(results)
        elif "--neritems" in option:
            #generate items by parsing downloaded data by event name = file name, parse filename for person location orgs
            results = self._selectDbTable('event')
            totalItems = self._generateItemsFromNer(results)
            print "[updateDatabase] Total creted items %s in %s" % (totalItems, self.root)
        elif "--clearitems" in option:
            results = self._selectDbTable('item')
            totalDeleted = self._clearItems(results)
            print "[updateDatabase] Total deleted items %s in %s" % (totalDeleted, self.root)
        elif "--available" in option:
            results = self._selectDbTable('video')
            totalRemoved = self._checkRemovedVideos(results)
            print "[updateDatabase] Total removed videos %s from %s" % (totalRemoved, self.root)
        elif "--removebyword" in option:
            totalRemoved = self._removeByWord()
            print "[updateDatabase] Total removed videos %s from %s" % (totalRemoved, self.root)
        elif "--removeroot" in option:
            totalRemoved = self._removeRoot()
            print "[updateDatabase] Total removed videos %s from %s" % (totalRemoved, self.root)
        elif "--rename" in option:
            #rename root in trunk, video, item table
            # RENAME word[0] TO word[1]
            words = self.param.split('-')
            removeTables = ['trunk','video','item']
            renameConfig = {
                "word1": words[0],
                "word2": words[1]
            }
            try:
                print "[updateDatabase] Renaming %s %s with %s" % (self.root, words[0], words[1])
                totalRenamed = 0
                if "root" in self.root:
                    for table in removeTables:
                        totalRenamed = self._rename(self.root,renameConfig) + totalRenamed
                elif self.root in removeTables:
                    totalRenamed = self._rename(self.root,renameConfig)
                print "[updateDatabase] Total renamed videos %s from %s" % (totalRenamed, self.root)
            except Exception as e:
                print """

                Error: %s

                Possible solution:

                [updateDatabase] Pleae provide a table to operate rename on...\n

                Command: python update.py --rename [parameter] word-wordReplaceWith

                Example: python update.py --rename root 'Film ENG-Movies'
                Replaces all root recursively from Film ENG to Movies
                """ % e
        elif "--getitemsfromvideos" in option:
            #match already present items in videos title or description
            results = self._selectDbTable('item')
            #totalMatched = total matched items for x videos
            matched = self._getItemsFromVideos(results)
            sys.stderr.write("[update] getItemsFromVideos. Saved %s items for %s videos "% (matched['items'],matched['videos']))
            sys.stderr.flush()

        elif "--mostcommon" in option:
            #extract common words and save into items from video or event title
            try:
                if self.param is None:
                    results = self._selectDbTable('video')
                elif "event" in self.param:
                    results = self._selectDbTable('event')
                elif "item" in self.param:
                    results = self._selectDbTable('item')
            except Exception as e:
                print "[updateDatabase] Error in createitems: %s" % e

            try:
                cm = DINXGenerateItemFromText.GenerateItemFromText(root = self.root)
                commonWords = cm.createItemsFromVideoData(results)
                savedItems = self._saveNewItems(results, commonWords)
            except Exception as e:
                print "[updateDatabase] Error in extracting items: %s" % (e)
                exc_type, exc_value, exc_traceback = sys.exc_info()
                error = "Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
                print "[updateDatabase] Error detail: %s" % error

        print "Finished %s in %d minutes" % (option, (((time.time() - startProcessingTime) / 60000000)))


    def _getItemsFromVideos(self, results):
        """For every item in root find connected videos by scanning video title and description

        :param results:
            list of Django db objects from Item table.

        Return dictionary with:
            * **videos** -- number of total saved videos, int
            * **items** -- number of total saved items, int
        """
        #from item point of view
        totalSavedItems = 0
        totalSavedVideos = 0
        for item in results:
            if len(item.name) > 3:
                lastName = item.name.split(' ')
                try:
                    del lastName[0]
                    lastName = ' '.join(lastName)
                except:
                    lastName = item.name
                #print lastName
                if len(lastName) == 0:
                    lastName = item.name
                #print lastName

                matchingVideos = Video.objects.filter(root=self.root).filter(Q(title__icontains = item.name) | Q(description__icontains=item.name) | Q(description__icontains=lastName)| Q(title__icontains=lastName)).distinct()
                for video in matchingVideos:
                    try:
                        i = Event.objects.get(id = video.event_set.all()[0].id).items.add(item)
                    except:
                        pass
                    totalSavedItems = totalSavedItems + 1
                    totalSavedVideos = totalSavedVideos + 1
                    try:
                        sys.stderr.write("[getItemsFromVideos] %s in %s \n" % (item.name,video.title))
                        sys.stderr.flush()
                    except:
                        pass
        matched = {
            "videos":totalSavedVideos,
            "items": totalSavedItems
        }
        return matched


    def _clearItems(self, results):
        """Remove items by root

        :param results:
            list of Django db objects from Item table to be deleted.

        Return int with count of deleted objects.

        Otherwise return None.

        """
        try:
            deleted = results.delete()
            print "[clearItems] deleted %s in %s" % (deleted, self.root)
            return deleted.count()
        except:
            print "[clearItems] deleted %s in %s" % (deleted, self.root)

    def saveItemsByEvent(self, id, itemInformation):
        """Get event name and save items for event.

        :param id:
            Django id of event to retrieve from db, int.

        :param itemInformation:
            dict sets wether the item to be saved is LOCATION or PERSON.

        Return int with total items saved.

        """
        event = Event.objects.get(id = id)
        f = 0
        category = event.trunk.all()[0].category
        root = event.trunk.all()[0].root
        for key in itemInformation.keys():
            if 'people' in key:
                itemType = 'PERSON'
            elif 'locations' in key:
                itemType = 'LOCATION'
            for itemName in itemInformation[key]:
                try:
                    if len(itemName)>0:
                        item = {'name':itemName, 'ner':itemType, 'category':category, 'root':root}
                        save = add.SaveEventsInDatabase(root = self.root, itemInput = item)
                        item = save.addItem()
                        if self.root in root:
                            event.items.add(item)
                            f = f + 1
                except Exception as e:
                    #logging.info("[addToEvent] Couldnt save items in event %s" % e)
                    print "[saveItemsByEvent] Couldnt save people items in event %s" % e
        return f

    def _saveNewItems(self, results, commonWords):
        """Associate video results by title and event name to generated commonWords

        :param results:
            list of Django db objects from Event or Item, matching most common words in videos/items title, name, desc etc...

        :param commonWords:
            list of strings containing most common words extracted using LCS method and Lev. distance.

        Return int with total number of items saved in database.
        """
        from django.db.models import Q

        allItems = []
        a = 1
        g = []

        for t in commonWords:
            replacedChars = ["\"","!","(",".","?",":","[","/","]"] #,"avi","hd","jas"
            for char in replacedChars:
                t = t.replace(char,'')
            if len(t) > 0 and t not in g and (t.isdigit() is False):
                g.append(t)
        commonWords = sorted(g)
        try:
            for word in commonWords:
                try:
                    #percent = 100 * (float(len(results))/float(a))
                    print "[saveNewItems] %s/%s for %s videos, ADDING: %s" % (a, len(commonWords), len(results), word)
                except Exception as e:
                    print "[saveNewItems] creating item error: %s " % e
                    pass
                if self.param is None:
                    results = Video.objects.filter(Q(title__icontains = word) | Q(name__icontains=word)).distinct()
                else:
                    #results = Event.objects.filter(name__icontains = word).distinct()
                    results = Event.objects.filter(Q(videos__title__icontains=word) | Q(name__icontains=word))
                occurrences = results.count()
                print "[saveNewItems] creating item: %s for %s events" % (word, occurrences)
                for result in results:
                    text = ""
                    if self.param is None:
                        text = result.title.lower()
                    else:
                        text = result.name.lower()
                    if word in text:
                        try:
                            category = result.event_set.all()[0].trunk.all()[0].category
                            root = result.event_set.all()[0].trunk.all()[0].root
                            print "[saveNewItems] Updating items by VIDEO TITLE..."
                        except Exception as e:
                            #nda
                            print e
                            #sys.exit(2)
                            category = result.trunk.all()[0].category
                            root = result.trunk.all()[0].root
                            #category = result.event_set.all()[0].trunk.all()[0].category
                            #   root = self.root
                        print "[saveNewItems] Creating %s for %s events" % (word, occurrences)
                        try:
                            item = {"name": word, "root": self.root, "category": category, "ner": "person", "occurrences": occurrences}
                        except Exception as e:
                            print "[saveNewItems] Item error: %s" % e
                        save = add.SaveEventsInDatabase(root = self.root, itemInput = item)
                        item = save.addItem()
                        print "[saveNewItems] Adding item by event...%s" % item
                        if item is not False and self.root in root:
                            try:
                                i = Event.objects.get(id = result.event_set.all()[0].id).items.add(item)
                                #i.save()
                                allItems.append(i)
                            except Exception as e:
                                i = result.items.add(item)
                                #result.save
                                allItems.append(i)
                                #print "[saveNewItems] Adding item by event...%s" % i
                        #else:
                        #    print "[saveNewItems] item = %s " % item
                a = a + 1
        except Exception as e:
            print "[saveNewItems] Error: %s" % e
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = "Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            print "[saveNewItems] ERROR: %s" % error
            #logging.error(error)
            sys.exit(2)
        totalSavedItems = len(allItems)
        return totalSavedItems

    def _selectDbTable(self, table):
        """Selects which db table to pull records from. Either trunk(root+category), item, event, video

        :param table:
            Name of table to retrieve records from, string.

        Return list with Django db objects from specific table.
        """
        try:
            from dinx_app.models import Item, Video, Event, Trunk
            if "item" in table:
                results = Item.objects.filter(event__trunk__root = self.root).distinct()
            if "video" in table:
                results = Video.objects.filter(event__trunk__root = self.root).distinct()
            if "trunk" in table:
                results = Trunk.objects.filter(root = self.root).distinct()
            if "event" in table:
                results = Event.objects.filter(trunk__root = self.root).distinct()
            return results
        except Exception as e:
            print "[selectDbTable] %s couldnt select results: %s" % e

    def _saveItemInDb(self,itemToSave):
        """Create SaveInDb instance and save unique word as item in event by video

        :param itemToSave:
            Dict with item name, category, ner, root to be saved.

        Return boolean with:

            * **True** -- Item was correctly saved in db, otherwise there is error.
            * **False** -- Error in saving item.

        """
        try:
            #for item save in db expects a dictionary
            #item = (name = n, ner = item['ner'], category = item['category'], root = self.custom_root, defaults={'occurrences': 1})
            saveindb = add.saveInDB(root = self.root, item = itemToSave)
            item = saveindb.addItem()
            event = result.event_set.all()[0]
            items = event.items.all()
            items.add(item)
            event.save()
            print "[saveItemInDb] Saved: %s for %s" % (item, event.name)
            return True
        except Exception as e:
            print "[saveItemInDb] Error: %s" % e
            return False

    def _addToDatabaseByItemCount(self):
        """Add more videos based on number of occurrences
           get items with high occurrence
           search for more based on category
        """
        from dinx_app.models import Item
        from generateVideos import GetVideos
        items = Item.most_frequent.all()
        for item in items:
            events = Event.objects.filter(items_name = item.name).distinct()
            category = item.category
            for event in events:
                gv = GetVideos(event = event, cat = category)
                all_videos_in_event = gv.runQuery()
                for video in all_videos_in_event:
                    improve = saveEventsInDatabase(video = video)
                    added = improve.saveEvents()


    def _generateItemsFromNer(self, results):
        """Associate person and location to video by event name from JSON input

        :param results:
            List of Django event objects to be matched with JSON filename to extract PERSON and LOCATION from JSON
        """
        try:
            data = "%s/%s"% (DATALOCATION,self.root)
            ner = "%s/%s" % (NEROUTPUTFOLDER, self.root)
            gp = DINXGenerateItemFromNer.GeneratePeople(results = results, root = self.root, matchingFolder = data, nerOutputFolder = ner, nerParserLocation = NERPARSERLOCATION)
            created, i = gp.generator(results)
            print "[generateItemsFromNer] Created %s items for %s files" % (created, i)
        except Exception as e:
            print "[generateItemsFromNer] Error: %s" % e

    def _checkOccurrances(self, results):
        """Count number of item mentions in database.

        :param results:
            List of Django event objects containing item name to be counted within database

        Return boolean with:

                * **True** -- Item has had occurrences and this number has been correctly saved within db.
                * **False** -- Item has not been saved and there has been an error.
        """
        outlist = []
        print "[checkOccurrances] Checking occurrences ..."
        totalResults = results.count()
        a = 0
        try:
            for item in results:
                occurrencesModifier = 0
                if item.name not in outlist and item.occurrences == 1:
                    a = a + 1
                    outlist.append(item.name)
                    occurrences = Video.objects.filter(event__trunk__root=self.root).filter(event__items__name=item.name).count()
                    ocInDescription = Video.objects.filter(event__trunk__root=self.root).filter(description__icontains=item.name).count()
                    if ocInDescription > 2:
                        occurrencesModifier = 10
                    else:
                        occurrencesModifier = 0
                    ocInTitle = Video.objects.filter(event__trunk__root=self.root).filter(title__icontains=item.name).count()
                    if ocInTitle > 2:
                        occurrencesModifier = occurrencesModifier + 10
                    item.occurrences = occurrences + ocInDescription + ocInTitle + occurrencesModifier
                    item.save()
                    print "[checkOccurrances] %s/%s Found %s occurrences for %s" % (a, totalResults, item.occurrences, item.name)
            print "[checkOccurrances] Occurrences saved for %s items" % (a)
            return True
        except Exception as e:
            print "[checkOccurrances] Error in check occurrences %s" % e
            return False

    def _checkRemovedVideos(self,results):
        """Check which videos are available.

        :param results:
            List with Django DB Video objects to check image url if video was removed or not.

        Return int with total number of removed videos.
        """
        #from dinx_app.models import Video, Event
        f = 0
        try:
            available = True
            for result in results:
                try:
                    rem = remove.DeleteFromDatabase(image = result.thumbnail)
                    available = rem.removeUnavailableByImageUrlResponse()
                except Exception as e:
                    available = True                    
                try:
                    if available is False:
                        print "[prePaginate] Removed: %s URL: %s" % (result.title, result.url)
                        f = f + 1
                except Exception as e:
                    pass
        except Exception as e:
            print "[prePaginate] Error in removing unavailable before pagination: %s" %e

        return f


    def _removeByWord(self, param = None):
        """Remove videos by a specific word.
        
        :param param: 
            String with word to delete from Video, Event, Item DB tables.
            
        Return int with total number of removed videos.
        """
        if param is None:
            param = self.param
        rem = remove.DeleteFromDatabase(item_to_delete = param)
        removedVideos = rem.removeByWord(root = self.root)
        print "[removeByWord] Removed %s videos" % removedVideos
        return removedVideos

    def _removeRoot(self, root = None):
        """Remove videos by a root.

        :param root:
            String with root to delete from all DB tables.

        Return int with total number of removed videos.
        """
        if root is None:
            root = self.root
        rem = remove.DeleteFromDatabase(item_to_delete = root)
        removedVideos = rem.remove()
        print "[removeRoot] Removed %s videos" % removedVideos
        return removedVideos

    def getMovieInformation(self, event, **kwargs):
        """Get movie information from IMDB.
        
        :param event:
            Django DB object containing event information to be retrieved.
        """
        from imdb import IMDb
        try:
                event.name = event.name.lstrip().rstrip().replace('film','').replace('(','').replace(')','').replace(str(event.date.year),'').replace('720','').replace('HD','').replace('720p','').replace('full','').replace('movie','').replace('dubbed','').replace('subs','').replace('1080','').replace('1080p','').replace(' p ','').replace('p ','').replace('film','').replace('completo','').replace('film completo','')
                outlist = []
                for char in event.name:
                    if char.isdigit():
                        outlist.append(char)
                if len(outlist) >= 4:
                    outlist = ''.join(outlist)
                    event.name = event.name.replace(outlist, '').lstrip().rstrip()
                parameters = {'q': event.name, 'json':1, 'nr':1, 'tt':'on'}
                parameters.update(kwargs)
                client = requests.session()
                url = r'http://www.imdb.com/xml/find?'
                r = client.get(url,params=parameters)
                j = json.loads(r.text)
                try:
                    title = j['title_exact'][0]['title']
                    id = j['title_exact'][0]['id'].replace('tt','')
                except Exception as e:
                    try:
                        title = j['title_approx'][0]['title']
                        id = j['title_approx'][0]['id'].replace('tt','')
                    except Exception as e:
                        print '[getMovieInformation] Error in fetching movie: %s' % e
                try:
                    print "[getMovieInformation] Title found: %s" % title
                except:
                    pass
                ia = IMDb()
                movieInfo = ia.get_movie(id)
                try:
                    try:
                        genres = movieInfo['genres']
                    except:
                        genres = movieInfo['genres']
                    print "[getMovieInformation] saving genres: %s for %s" % (genres, event.name)
                    for genre in genres:
                        item = {'name':genre, 'ner':'GENRE', 'category':'movies', 'root':self.root}
        	        event = self._createItemsForEvent(event, item)
                except Exception as e:
                    print "[getMovieInformation] Error in saving genres: %s for %s" % (e, event.name)                    
                try:
                    try:
                        language = movieInfo['language'][0]
                    except:
                        language = movieInfo['language'][0]
                    item = {'name':language, 'ner':'LANGUAGE', 'category':'movies', 'root':self.root}
	            event = self._createItemsForEvent(event, item)
                except:
                    pass
                try:
                    country = movieInfo['country'][0]                    
                    try:
                        countriesList = self._getCountriesFromString(country)
                    except Exception as e:
                        print "[getMovieInformation] Error in getting country list %s" % e
                        countriesList = country
                    for place in countriesList:
                        item = {'name':place, 'ner':'LOCATION', 'category':'movies', 'root':self.root}
                        event = self._createItemsForEvent(event, item)
                        print "[getMovieInformation] saving country: %s for %s" % (countriesList, event.name)
                except Exception as e:
                    print "[getMovieInformation] Error in saving country: %s for %s" % (e, event.name)
                    pass
                try:
                    actors = movieInfo['cast']
                    try:
                        directors = movieInfo['directors']
                    except:
                        try:
                            directors = movieInfo['director']
                        except:
                            directors = []
                    try:
                        actors = actors.split(',')
                        directors = directors.split(',')
                    except:
                        pass
	            actors = actors + directors
        	    for actor in actors:
                        item = {'name':actor['name'], 'ner':'person', 'category':'movies', 'root':self.root}
	                event = self._createItemsForEvent(event, item)                        
                except Exception as e:
		    print "[getMovieInformation] Error in saving: %s for %s" % (e, event.name)
        except Exception as e:
                print "[getMovieInformation] Error: %s, NOT FOUND %s" % (e, event.name)

    def countriesGetter(self, country = 'UKRomaniaGermanyFrance'):
        """Getter for getCountriesFromString"""
        cn = self._getCountriesFromString(country)
        return cn

    def _getCountriesFromString(self, country):
        """Get country names from input by checking for small word before capital word.

        :param country:
            Holds movie country information, string.

        Return list with country names.
        """
        countriesList = []        
        twoLetterCountries = ['USA', 'UK']
        for t in twoLetterCountries:
            if t in country:
                countriesList.append(t)
                country = country.replace(t,'')
        try:
            temp = 0
            country = country + 'X'
            for a in range(1,len(country)):
                if country[a-1].islower() and country[a].isupper():
                    cn = country[temp:a]
                    temp = a
                    countriesList.append(cn)
        except Exception as e:
            print "[getCountriesFromString] Error: %s " % (e)
        return countriesList

    def _createItemsForEvent(self, event, item):
        """Saves item with specific information to specific event.

        :param event:
            Django DB Event object containing event information to be matched with item.

        :param item:
            Django DB Item object containing item info to be associated with event and saved.

        Return event object with updated items.
        """
        save = add.SaveEventsInDatabase(root = self.root, itemInput = item)
	item = save.addItem()
        event.items.add(item)
        return event

    def _rename(self, table, renameConfig):
        """Rename word1 to word2 in renameConfig. Table can be: root, item, video.

        :param table:
            String with DB table name containing the item to be renamed.

        :param renameConfig:
            Dictionary containing in first value word1 to be renamed and in second value word2 to rename word1.

        Return int with total number of renamed videos or items.
        """
        from dinx_app.models import Item, Video, Event, Trunk
        word1 = renameConfig['word1']
        word2 = renameConfig['word2']
        if "root" in table:
            results = Trunk.objects.filter(root=word1)
        elif "item" in table:
            results = Item.objects.filter(root=word1)
        elif "video" in table:
            results = Video.objects.filter(root=word1)
        i = 0
        for result in results:
            result.root = word2
            result.save()
            i = i + 1
        return i

def main():
    item = sys.argv[1]
    try:
        root = sys.argv[2]
    except:
        pass
    try:
        getdata = Update(option = item, root = root)
    except:
        getdata = Update(option = item)
    try:
        param = sys.argv[3]
        getdata = Update(option = item, root = root, param = param)
    except:
        pass
    try:
        param = sys.argv[3]
        wordBinding = sys.argv[4]
        getdata = Update(option = item, root = root, param = param, wordBinding = wordBinding)
    except:
        pass
    getdata.updateDatabase()

if __name__ == "__main__":
    main()
