#!/usr/local/bin/python
# coding: utf-8
import math, operator
import sys
import os
#os.path.join(os.path.dirname(__file__), 'templates').replace('\\','/'),
#go up one dir



class DeleteFromDatabase:
    def __init__(self,  deleteVideo = True, image = None, item_to_delete = None):
        os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..'))
        from dinx_app.models import Video, Event
        from dinx_app.models import Item
        self.deleteVideo = deleteVideo
        self.image = image    
        self.item_to_delete = item_to_delete
        
    def remove(self, itemToDelete = None):
        """Remove specific instance or set of events from database

        :param itemToDelete:
            Name to delete from database, useful to delete multiple wrong videos with same word, string.
        """
        from dinx_app.models import Trunk, Event, Item, Video
        if itemToDelete is None:
            itemToDelete = self.item_to_delete
        Event.objects.filter(trunk__root__icontains = itemToDelete).delete()
        Video.objects.filter(event__trunk__root__icontains = itemToDelete).delete()
        Item.objects.filter(event__trunk__root__icontains = itemToDelete).delete()
        Trunk.objects.filter(root__icontains = itemToDelete).delete()
        Trunk.objects.filter(category__icontains = itemToDelete).delete()
        print "{DeleteFromDatabase}[remove] deleted: %s" % itemToDelete
        
    def removeByWord(self, root):
        """remove specific instance or set of events from database.

        :param root:
            Removes only from Event, Video and Item by word, string.

        Return int with total number of removed videos.
        """
        from dinx_app.models import Trunk, Event, Item, Video
        Event.objects.filter(trunk__root=root).filter(videos__title__icontains = self.item_to_delete).delete()
        res = Video.objects.filter(root=root).filter(title__icontains = self.item_to_delete)
        removedVideos = res.count()
        res.delete()
        Item.objects.filter(root=root).filter(name__icontains = self.item_to_delete).delete()
        #Trunk.objects.filter(root__icontains = self.item_to_delete).delete()
        #Trunk.objects.filter(category__icontains = self.item_to_delete).delete()
        try:
            print "{DeleteFromDatabase}[removeByWord] deleted: %s" % self.item_to_delete
        except:
            pass
        return removedVideos

    def removeUnavailableByImageUrlResponse(self, image = None):
        """Check if video exists by checking img and url

        :param image:
            Url of YouTube video thumbnail to check for video existence, string.

        Return boolean:

                * **False** -- Video doesn't exist anymore.
                * **True** -- Video still exists.
        """
        if image is None:
            image = self.image
        try:
            from dinx_app.models import Video, Event
            import requests
            conn = requests.session()
            r = conn.get(image)
            if r.status_code == 404:
                vid = Video.objects.get(thumbnail=image).delete()
                return False
            else:
                return True
        except Exception as e:
            print "{DeleteFromDatabase}[removeUnavailableByImageUrlResponse] Error in removing video: %s" % e

def main():
    item = sys.argv[1]
    getdata = DeleteFromDatabase(item_to_delete = item)
    matching = getdata.remove()
    #removed = getdata.update_database()
    #getdata.loadJSON()           

if __name__ == "__main__":
    main()

