/**
 * DINX FrontEnd file
 * Refer to www.dinx.tv
 *
 * @author code@dinx.tv
 * @date 25-Jan-2013 01:09:55
 *
 */


var dinxPlayer = {
    tag: document.createElement('script'),
    firstScriptTag: document.getElementsByTagName('script')[0],
    player3: {},
    nextVideo: {}, //video object with url, title, name, date etc
    isMute: false,
    seekTime: 5
}

dinxPlayer.init = function () {
    console.log("Loading youtube player...");
    this.tag.src = "//www.youtube.com/iframe_api";
    this.firstScriptTag.parentNode.insertBefore(this.tag, this.firstScriptTag);
};

function onYouTubeIframeAPIReady() {
    console.log("IFRAME ready");
    dinxPlayer.player3 = new YT.Player('playerElement', {
        height: '100%',
        width: '100%',
        videoId: 'PH54cp2ggFk',
	playerVars: {'controls': 0 },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
};

function onPlayerReady(event) {
    console.log("Player ready...");
    //event.target.playVideo();
    //this.player3.playVideo();
    dinxPlayer.loadVideo(dinxController.videos[dinxController.currentChannel.videoIndex].url);
    
};

function onPlayerStateChange(event) {
    //if (event.data == YT.PlayerState.PLAYING) {
    //    console.log("Player is playing...")
    //};
    if (event.data == YT.PlayerState.ENDED) {
        v = dinxController.updateCurrentChannel();
        console.log("playing nextvideo... " + dinxController.currentChannel.videoUrl);
        console.log("Video index in onStateChange " + v + " videos list size: " + dinxController.videos.length);
        dinxPlayer.loadVideo(dinxController.videos[v].url);
        dinxUI.showSearching();
    };
};

//video controls

dinxPlayer.pause = function () {
    if (this.player3) {
        this.player3.pauseVideo();
    };
};

dinxPlayer.play = function () {
    if (this.player3) {
        this.player3.playVideo();
    };
};

//Play channel in new selection

dinxPlayer.loadVideo = function (url) {
    if (dinxPlayer.player3) {
        dinxPlayer.player3.loadVideoByUrl(url);
    };
};

dinxPlayer.toggleMute = function () {
    if (this.player3) {
        if(this.isMute==false) {
            this.player3.mute();
            this.isMute=true;
        } else {
            this.player3.unMute();
            this.isMute = false;
        };
    };
};

dinxPlayer.seek = function (seekType) {
    if(this.player3)	{
        played = this.player3.getCurrentTime();
        this.player3.seekTo(played+(seekType * this.seekTime));
    };
};

dinxPlayer.focusEvent = function (url,current,c){
    if(player3) {
        player3.loadVideoByUrl(url);
        document.getElementById('event_box').innerHTML="<p>"+decodeURI(current)+"</p>";
        glob.i=c
    };
};

dinxPlayer.stopVideo = function () {
    player3.stopVideo();
    //play_channel_by_video('http://www.youtube.com/v/4Owfx3f2zYU?version=3&f=videos&app=youtube_gdata')
};