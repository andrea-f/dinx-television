/**
 * DINX FrontEnd file
 * Refer to www.dinx.tv
 * 
 * @author code@dinx.tv
 * @date 25-Jan-2013 01:09:55
 * 
 */

var dinxFeedsHandler = {
    "progress": false
};
dinxFeedsHandler.makeRequest = function (channel)
{
    if ((dinxController.requestCount <= 2)) {
        var channelToSend = {}, pag = {};
        channel['videoIndex'] = 0;
        channel['videoUrl'] = "";
        for (key in channel){
            if (channel[key] !== "" && channel[key] !== 0) {
                channelToSend[key] = channel[key];
            }
        }
        //console.log("before REQUEST ajaxInProgress is: " + dinxFeedsHandler.ajaxInProgress);
        dinxUI.toggleSelectInteraction(0);
        this.progress = true;
        dinxUI.showLoading(1);
        channelInJSON = JSON.stringify(channelToSend, null, 2);
        $.ajax({
            url: '/ajax/',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: channelInJSON,
            dataType: 'text',
            success: function(result) {
                dinxFeedsHandler.progress = false;
                var channelData = JSON.parse(result);
                showIndicator = false;
                dinxUI.showLoading(0);
                dinxController.handleResponse(channelData.info, channelData.viewBy, channelData.pagination);
            },
            error: function(xhr, status, error) {
                document.write(xhr.responseText);
            }
        });
    }
};

