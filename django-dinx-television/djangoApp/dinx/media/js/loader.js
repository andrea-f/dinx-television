
//info.all_events = new Object();
var info = new Object();
info.current = new Object()

info.videos = new Array();
info.events = new Array();
info.dates = new Array();
info.years = new Array();
info.items = new Array();

//Currently playing channel
info.search = new Array();
info.current.search_terms = new Array();

//currently playing event
info.current.event = new String();

//Currently playing URL
info.current.url = new String();

//Currently playing root
info.current.root = new String(); 

//Currently available dates in search terms or zapping
info.current.available_dates = new Array();
info.current.available_roots = new Array();
//Currently playing date
info.current.date = new String();

//Is a video playing?
info.playing = new Boolean();
info.playing = false;



var glob = new Object();
glob.m = false;
glob.next = "video";
glob.i = 0;
glob.root = '';
glob.options = new Array();
glob.event_num = 0;



/*Stop noise video, play channel first video*/
function stopVideo() {
        player3.stopVideo();
		
        play_channel_by_video('http://www.youtube.com/v/4Owfx3f2zYU?version=3&f=videos&app=youtube_gdata')
      }


//load input
function createArray(events)
{
 console.log("populating array");
ev = events;
p = 0;
    f = 0;
    //alert(info.videos.length);
    info.videos.length = 0;
    //alert(info.videos.length);
    info.items.length = 0;
    info.events.length = 0;
    glob.i = 0
    glob.next = 'video'
//alert(evsl.info.length);
    for(g=0;g<ev.info.length;g=g+1)
	{
        //alert(evsl.info[0].roots);
        //POPULATE VIDEO ARRAY
	for(y=0;y<ev.info[g].videos.length;y=y+1)
	{
		info.videos[p]= ev.info[g].videos[y]; 
		info.events[p]= ev.info[g].name;
		info.dates[p] = ev.info[g].date;
		p = p+1;
	}
//POPULATE ITEMS ARRAY
	for(e=0;e<ev.info[g].items.length;e=e+1)
	{
		info.items[f] = ev.info[g].items[e];
		f = f +1;
    }
}
}



//START search functionality


function choose_search(category, item){
	//Show available search options
	//save them in array
	var options = new Array();
	options[0] = 'date'
	options[1] = 'location'
	//options[4] = 'all'
	if ('sports' == category)
	{
		options[2] = 'team'
		options[3] = 'events'
	}
	else if ('history' == category)
	{
		options[2] = 'person'
		options[3] = 'events'
		
	}
	else if ('movies' == category)
	{
		options[2] = 'person'
		options[3] = 'title'
	}
	//item = encodeURI(item);
	glob.root = category;
    info.current.category = category;
	glob.options = options;
    
    
	channel(item); 
    
	//create timeline for later usage
	$('#timeline').append("<div class='thumb_event'></div>")
    $('thumb_event').hide();
    $('#timeline').hide()
    display_search_options(info.current.search_terms);
}

info.type = new String();


function show_selection(option)
{
	//item = decodeURI(item);
	if ('date' == option)
	{
		//show all dates in root
        //terms = decodeURI(terms);
        //alert(terms);

		test_load_data(info.current.search_terms, 'show_dates');
		//remove option from options list
		remove_option(glob.options,option);
		
	}
	else if ('team' == option || 'person' == option || 'location'==option)
	{
		//show all items in root
		remove_option(glob.options,option);
        info.type = option;
		test_load_data(info.current.search_terms ,'show_items')
		
		
	}

	
	else if ('title' == option || 'events' == option)
	{
		//show all persons and years by title
        test_load_data(info.current.search_terms ,'all')
	}
}



function test_load_data(terms,field)
{
    
    var myEvent = {terms: terms, field:field, type: info.type};
    //alert(myEvent.item);
    ev = JSON.stringify(myEvent, null, 2);
    
    console.log(terms,field)

		$.ajax({
		    url: '/ajax/',
		    type: 'POST',
		    contentType: 'application/json; charset=utf-8',
		    data: ev,
            onLoading: function(request){$('loading').show();},
            onComplete: function(request){$('loading').hide();},
		    dataType: 'text',
		    success: function(result) {
		var ev = JSON.parse(result);

		info.ev = ev;

        //test_createOptionList(item, ev, 'show_selection'); 

		if ('show_dates' == field)
		{
			info.current.available_dates = ev.info;
			console.log(info.current.available_dates.length);
			test_createOptionList(ev, 'show_selection')
		}
		else if ('show_items' == field)
		{
			test_createOptionList(ev, 'show_selection')
		}
 		else if ('show_roots' == field)
		{
			info.current.available_roots = ev.info;
			createRootsList();
		}
        else if ('show_events' == field || 'all' == field)
		{
			//test_createOptionList(ev, 'play');
            //alert(typeof ev.info);
            console.log("response items "+ev.info.length)
			if (info.playing == true){
			if (ev.info.length == 0)
			{
				var r = new Array();
				r.push(info.current.root)
				r.push(decodeURI(info.clicked_on))
				console.log(r);
				test_load_data(r, 'all')
			}}
			createArray(ev);
			if(info.playing == true)
			{
				console.log("updating_template");
                reloadTemplate();
			}
			load_template(ev);
		}
},

error: function(xhr, status, error) {

  //var err = eval("(" + xhr.responseText + ")");

  document.write(xhr.responseText);

}
});

}


info.type = new String();
function show_selection(option)
{
	//item = decodeURI(item);
	if ('date' == option)
	{
		//show all dates in root
        //terms = decodeURI(terms);
        //alert(terms);

		test_load_data(info.current.search_terms, 'show_dates');
		//remove option from options list
		remove_option(glob.options,option);
		
	}
	else if ('team' == option || 'person' == option || 'location'==option)
	{
		//show all items in root
		remove_option(glob.options,option);
        info.type = option;
		test_load_data(info.current.search_terms ,'show_items')
		
		
	}

	
	else if ('title' == option || 'events' == option)
	{
		//show all persons and years by title
        test_load_data(info.current.search_terms ,'all')
	}
}




function isNumeric(num){
    return !isNaN(num)
}



//load events from item in same year and root
info.zapping = new Array();
info.clicked_on = new String();
info.first_change = new Boolean();
info.first_change = true;
info.click_type = "none";


function update_events_by_item(item)
{
	var all = new Boolean();
	
	info.zapping.length = 0;
	info.watching = info.current.search_terms;
	
//	if (info.current.item == item
	info.clicked_on = item
	


//    if (info.current.search_terms.indexOf(item) !== -1){ info.current.search_terms.push(item);}
    //console.log(info.current.item);
    var num = isNumeric(item);
	//rem = new String();

	if (info.first_change == true)
	{
    changePlaying(); 
	info.first_change = false;
	}

    if (num == true){var rem = info.current.date}
	else {var rem = info.current.item}
    remove_option(info.current.search_terms,rem);
    channel(item);

    //set channel text 
    //createItemsList();
	//createYearsList();
    //console.log(info.current.available_roots)
    // IS CLICKED ON ITEM ROOT?
    if (info.current.available_roots.indexOf(decodeURI(item)) !== -1)
	{
		//info.zapping.push(item);
        console.log("is root "+decodeURI(item));
		info.zapping.push(decodeURI(info.current.date))
        remove_option(info.current.search_terms,info.current.root);  
        all = true;
        info.click_type = "root";
	}
	else
	{
	    info.zapping.push(info.current.root);

	}

	// IS CLICKED ON ITEM DATE?
	if (num == true)
	{
		//info.zapping.push(item)
        info.click_type = "date";        
		info.zapping.push(info.current.item);
        info.current.date = decodeURI(item);
    }
	else
	{    
		info.zapping.push(decodeURI(item));

	}

	if (info.click_type == "")
	{
		info.click_type = "item";
	}
    //console.log(info.current.category)
	if (info.current.category == "sports" )
	{

        if (info.zapping.indexOf(info.current.date) == -1){ 
		info.zapping.push(decodeURI(info.current.date));	
        all = false;}
        else {info.zapping.push(decodeURI(item));}	
		
	}
	else{			
		if (num == false){all = true;}
        else{info.zapping.push(decodeURI(item));}
    var newArr = remove_undefined(info.zapping)
	console.log("NEWaRR="+newArr);	
	if (newArr.length == 2)
		{
			all = true;
		}

	info.zapping = newArr;
	}
	if (all == false){ field = 'show_events';}	else {field = 'all'}
	console.log("info.zapping="+info.zapping);
		  

	
	test_load_data(info.zapping,field) ;
    //alert(info.playing);
	
}

function channel_and_load(item,field)
{
	channel(item);
    if (info.current.search_terms.length == 3)
	{
    	test_load_data(info.current.search_terms,'show_events') ;
	}
    else if ('show_selection' == field)
	{
		display_search_options()
        }
    else
	{
    	test_load_data(info.current.search_terms,field) ;
	}

}

function remove_undefined(arr)
{

		var newArr = new Array();
		for (var index in arr)
		{ 
	  
		  if( arr[index] ) { 
			newArr.push( arr[index] ); 
	  		} 
  
		}
	return newArr
}


function remove_option(arr,option)
{
	//remove option from option list
	option = decodeURI(option);
	while (arr.indexOf(option) !== -1) {
	  arr.splice(arr.indexOf(option), 1);
		}
	
	while (arr.indexOf(option) !== -1) {
	  delete arr[arr.indexOf(option)];
	}

}


function load_video(url, event_name, position, date)
{
	info.current.date = date;
		c = new Array();
		c.push(decodeURI(info.current.date));
		test_load_data(c, 'show_roots');
	if(info.playing == true)
	{
		focusEvent(url, decodeURI(event_name), position);
	    $('#event_box').find("p").attr("class","event_box_playing");
        //$('#event_box').css('top','20px').css('font-size','15pt');
		info.first_change = true;
		changePlaying();

		
        
	}
	else
	{
		show_video_area(url);

		//set currently playing event
        info.current.event = event_name;

	}
}

function play_channel_by_video(url)
{
	//alert(url);
	
	player3.loadVideoByUrl(info.current.url);
	player3.unMute();
	//createEventsList();
	show_controls();
	var control = 'thumbs_view';
	show_other_videos(control);
}



