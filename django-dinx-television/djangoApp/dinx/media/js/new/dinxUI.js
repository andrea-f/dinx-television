/**
 * DINX FrontEnd file
 * Refer to www.dinx.tv
 * 
 * @author code@dinx.tv
 * @date 25-Jan-2013 01:09:55
 * 
 */
var dinxUI = {
    categoriesElement: $('<div class=categoriesElement></div>'),
    rootsElement: $('<div class=rootsElement></div>'),
    optionsElement: $('<div class=viewByElement></div>'),
    datesElement: $('<div class=datesElement></div>'),
    videosElement: $('<div class=videosElement></div>'),
    itemsElement: $('<div class=itemsElement></div>'),
    searchingElement: $('<div class= searchingElement></div>'),
    loadingElement: $('<div class = loadingElement></div>'),
    controlsElement: $('<div class = controlsElement></div>'),
    channelControlsElement: $('<div class = channelControlsElement></div>'),
    paginationElement: $('<div class = paginationElement></div>'),
    itemsNavigation: $(''),
    aside: $('aside'),
    playerAndControlsElement: $('.playerAndControls'),
    isFullScreen: false,
    isTimelineActive: false    
};

dinxUI.uiTypes = {
    roots: "roots",
    dates: "dates",
    items: "items",
    videos: "videos",
    categories: "categories",
    options: "options",
    playerAndControls: "playerAndControls"
};


dinxUI.uiLists = {
    roots: "<select class = rootsSelectElement></select>",
    dates: "<select class = datesSelectElement></select>",
    items: "<select name = itemsSelectElement class = itemsSelectElement></select>",
    videos: "<select class = videosSelectElement ></select>",
    categories: "<select class = categoriesSelectElement ></select>",
    options: "<select class = optionsSelectElement ></select>"
};

dinxUI.controls = [
    "Play", "Pause", "RW", "FF", "Full Screen", "Mute", "Stop", "Next Video"
];

dinxUI.channelControls = [
    "Toggle Timeline"
]

dinxUI.init = function () {
    $('header').append(this.categoriesElement, this.searchingElement);
    //document.body.style.backgroundImage = "";    
};
/**
 * Shows and toggles visibility for received elementType
 * @param {String} elementType name of UI element to set and toggle
 * @param {Number} isVisible 0 ? 1 element visibility
 **/
dinxUI.showUIbyType = function (isVisible, elementType) {
    if (this.uiTypes[elementType]) {
        dinxUI.setUIElement(this.uiTypes[elementType]);
        dinxUI.toggleUIElements(isVisible, this.uiTypes[elementType]);
    } else {
        console.log("UI type " + elementType + " doesnt exist");
    }
};

dinxUI.clearBackground = function () {
    try {
        $('body').removeAttr('style').css('background-image','', 'important').css('background-color','#ffffff','important');
        console.log('{dinxUI}[init] set');
        throw "no attribute";
    } catch (err) {
        console.log('{dinxUI}[init] Error' + err);
    }
}

dinxUI.setAsideElements = function () {
    $('aside').append(this.rootsElement, this.datesElement, this.itemsElement, this.optionsElement);
    $('article').append(this.loadingElement, this.videosElement, this.paginationElement)
    this.paginationElement.hide();
    //console.log("Setting aside elements");
}

/**
 * Sets the HTML of the received component name
 * @param {Number} isVisible 0 ? 1 component visibility
 * @param {Object} componentName HTML <div> element
 **/
dinxUI.setUIElement = function (isVisible, componentName) {
    var componentItem =  {};
    if (dinxUI[componentName + "Element"]) {
        componentItem = dinxUI[componentName + "Element"];
        componentItem.html(dinxUI.uiLists[componentName]);
        switch (componentName) {
            case dinxUI.uiTypes.videos:
                $('.imgClass').addClass('imageBigThumbsView');
                break;
        };
    } else {
        console.log("Component " + componentName + " does not live in this DOM");
    };
    dinxUI.toggleUIElements(isVisible, componentName);
};

/*
 * This function shows the pagination next previous for videos
 */
dinxUI.showPagination = function (pag) {
    var pages = "", t = 0, last = "";
    //total pages max 10 - pag.p = total pages - pag.n = next page - pag.v = total videos
    if (pag.v > dinxController.currentChannel.videosPerPage) {
        this.paginationElement.show();
    }
    if (pag.p > 10) {
        t = 10;
        last = "...<a href = # onclick = 'dinxController.navigatePages("+ pag.p +")'>" + pag.p + "</a> ";
    } else {
        t = pag.p;
        last = "";
    }
    for (f = 1; f <= t; f = f + 1) {
        if (f === dinxController.currentChannel.videoPage) {
            pages = pages + "<a href = # onclick = 'dinxController.navigatePages("+ f +")'><b>" + f + "</b></a> ";
        } else {
            pages = pages + " <a href = # onclick = 'dinxController.navigatePages("+ f +")'>" + f + "</a> ";
        }
    }
    pages = pages + last;
    this.paginationElement.html("<a href = # onclick = 'dinxController.navigatePages(-1)'>Previous page</a> "+ pages +"- <a href = # onclick = 'dinxController.navigatePages(" +pag.n+ ")'>Next page</a> - Current: "+dinxController.currentChannel.videoPage);
}


/*
 * Hides or shows the element component provided
 * @param {Number} isVisible is 0 hide, 1 show.
 * @param {Object} component is a html element <div>
 */
dinxUI.toggleUIElements = function (isVisible, component) {
    if (dinxUI[component + "Element"]) {
        if (isVisible === 0) {
            dinxUI[component + "Element"].hide();
        } else {
            dinxUI[component + "Element"].show();
        }
    } else {
       console.log("Component " + component + " doesn't exist");
    }
};

/**
 * Generate list of available dates
 * Sets Object <ul> element containing date information
 **/

dinxUI.listGeneratorTypes = {
    "dates": function(m) {return (dinxController.dates[m])},
    "roots": function(m) {
        dinxController.updateYearsList(m);
        //alert("hhhhhh")
        //  && (jQuery.inArray(dinxController.roots[m].name, dinxController.uniqueRoots) === -1))
        if (((dinxController.currentChannel.category === dinxController.roots[m].category) && (dinxController.currentChannel.date === 0)) || (dinxController.currentChannel.date ===  dinxController.roots[m].date)) {
            //dinxController.log("[dinxUI.listGeneratorTypes] " + dinxController.currentChannel.date + " ss "+ dinxController.roots[m].date);
            //dinxController.uniqueRoots.push(dinxController.roots[m].name);
            return (dinxController.roots[m].name);
        };
    },
    "items": function (m) {
        item = dinxController.items[m];
        //return( item.name+"("+item.num+")" );
        return(item.name);
    },
    "videos": function (m) {
        var video = {};
        video = dinxController.videos[m];
        if (dinxController.GUIDEMODE) {
            return ("<a href=# onclick = 'dinxController.setListAction.videos(" + m + ")'><figure class = videoImage><img onerror = 'dinxUI.hideUnavailable()' src=" + video.image + " /><figcaption class=videosElementFigcation>"+video.name.substring(0,20)+"</figcaption></figure></a>");
        };
    },
    "options": function (m) {
        return (dinxController.options[m]);
    }
};

////////
dinxUI.generateList = function (typeOfList) {
    var listElement = {}, itemElement = {};
    if (dinxUI.uiTypes.videos === typeOfList) {
        this.videosElement.css('overflow','scroll');
        listElement = document.createElement('ul');
    } else {
        listElement = document.createElement('select');
        if (this.uiLists[typeOfList]) {
            listElement.className = typeOfList + "SelectElement";
            console.log("listElement class name: " + listElement.className);
        }
        listElement.setAttribute('onclick', "dinxController.selectHandler.action(this,\"" + typeOfList + "\")");
        listElement.setAttribute('onblur', "dinxController.selectHandler.blur()");
        listLen = dinxController[typeOfList].length;
        $(listElement).attr('size','7');
        
    }
    
    //dinxUI.generateOptionsList();
    //console.log(typeOfList);
    //dinxController.updateYearsList();
    for (m=0;m<dinxController[typeOfList].length;m=m+1) {
        if (dinxController.setListAction[typeOfList]) {
            if (dinxUI.uiTypes.videos === typeOfList) {
                itemElement = document.createElement('li');
            } else {
                itemElement = document.createElement('option');
                itemElement.setAttribute('value', m);
            };
        }

        if (this.listGeneratorTypes[typeOfList]) {
            itemElement.innerHTML = this.listGeneratorTypes[typeOfList](m);
        };
        if(itemElement.innerHTML !== 'undefined') {
            listElement.insertBefore(itemElement, listElement.firstChild);
        }
    };
    if (this.uiLists[typeOfList]) {
        this.uiLists[typeOfList] = listElement;
    }
};
/**
 * Show available search options
 * @return Array search options
 **/
dinxUI.generateOptionsList = function() {
    //save them in array
    var options = new Array();
    options[0] = 'date';
    options[1] = 'all';
    options[2] = 'item';

    options = dinxController.removeOption(options, dinxController.currentChannel.viewBy);
    dinxController.options = options;
};


dinxUI.showControls = function () {
    var i = "";
    $('.playerAndControls').append(this.controlsElement);
    for (var t = 0; t < this.controls.length; t = t+1) {
        i = i + "<a href=# onClick = dinxController.processControlsInput(\""+ encodeURI(this.controls[t]) +"\")> " + this.controls[t] + " </a>";
    };
    this.controlsElement.html(i);
    
};

dinxUI.showChannelControls = function () {
 $('aside').append(this.channelControlsElement);
 i = "";
 for (var t = 0; t < this.channelControls.length; t = t+1) {
        i = i + "<a href=# onClick = dinxController.processControlsInput(\""+ encodeURI(this.channelControls[t]) +"\")> " + this.channelControls[t] + " </a>";
    };
    this.channelControlsElement.html(i);
}

dinxUI.showPlayer = function () {
    //this.playerElement.detach().prependTo('article');
    this.toggleTimeline();
    this.showControls();
    this.showChannelControls();
    $('#playerElement').css('width', '60%').css('height', '65%');
    //.addClass('playerElementOn');

};

dinxUI.toggleTimeline = function () {
    //this.videosElement.detach().prependTo('footer');
    if (this.isTimelineActive === false) {
        this.videosElement.removeClass('videosElement');
        this.videosElement.addClass('timeline');
        this.isTimelineActive = true;
        $(".playerAndControls").show();        
    } else {
        this.videosElement.removeClass('timeline');
        $(".playerAndControls").hide();
        this.videosElement.addClass('videosElement');
        this.isTimelineActive = false;
    }
};

dinxUI.showSearching = function () {
    //console.log()
    var r = dinxController.currentChannel.root, c = dinxController.currentChannel.category, s = "", i = "", g = "", name = "", totalVideos = 0, tv = "";
    if (r !== "") {
        g = "<b>" + c + "</b> -> <b>" + r + "</b>";
    } else {
        g = c;
    };
    if (dinxController.currentChannel.date !== 0) {
        d = dinxController.currentChannel.date;
        s = g + " -> " + "<b>" + d + "</b>";
    } else {
        d = "";
        s = g;
    };
    if (dinxController.currentChannel.item !== "") {
        i = dinxController.currentChannel.item;
    }
    if (dinxController.currentChannel.eventName !== "") {
        name = dinxController.currentChannel.eventName;
        if (i !== "") {
            i = "<b>" + i + "</b> -> " + name;
        } else {
            i = name;
        };
    };
    totalVideos = dinxController.currentChannel.totalVideos;
    if (typeof totalVideos !== "undefined" && totalVideos !== 0) {
        tv = "<b>" + totalVideos + "</b> videos in ";
    };
    this.searchingElement.html(tv + s + " -> " + "<b>"+ i + "</b>");
}


/**
 * This function shows loading indicator duing ajax processing
 * @param {Number} dataIsLoading either 0 ? 1 to show loading indicator
 **/
dinxUI.showLoading = function (dataIsLoading) {
    var loadingImage = "<figure><img src=http://www.dinx.tv/media/images/ajax-loader.gif /><figcaption>Data is loading...</figcaption></figure>", msg = "";
    this.loadingElement.html(loadingImage);
    if (dataIsLoading === 1) {
        msg = "SHOWING indicator...";
        this.toggleSelectInteraction(0);
        this.loadingElement.show();
        this.videosElement.hide()
    } else {
        this.loadingElement.hide();
        msg = "hiding indicator....";
        this.videosElement.show()
        this.toggleSelectInteraction(1);
    };    
    //console.log(msg);
};

dinxUI.toggleSelectInteraction = function (editSelect) {
    var selectOption = false;
    //console.log("Disabling selects: " + selectOption);
    try {
        if (editSelect === 0) {
            selectOption = true;
        }
        $('.rootsSelectElement').prop('disabled', selectOption);
        $('.datesSelectElement').prop('disabled', selectOption);
        $('.itemsSelectElement').prop('disabled', selectOption);
        throw "prop not defined";
    } catch (err) {
        if (editSelect === 0) {
            selectOption = "disabled";
        }
        $('.itemsSelectElement').attr('disabled', selectOption);
        $('.rootsSelectElement').attr('disabled', selectOption);
        $('.datesSelectElement').attr('disabled', selectOption);
    }
    
}

dinxUI.toggleFullScreen = function () {
    if (this.isFullScreen === false) {
        $('iframe').addClass('fullScreenViewPlayer');
        $('.playerAndControls').css('top','0').css('left','0').css('width','100%').css('height','100%');
        $('.controlsElement').css('padding-left','0px').css('bottom','2%');
        $('.timeline').hide();
        this.isFullScreen = true;
    } else {
        $('iframe').removeClass('fullScreenViewPlayer');
        $('.playerAndControls').css('top','auto').css('left','auto').css('width','auto').css('height','auto');
        $('.controlsElement').css('padding-left','70px').css('bottom','5%');
        this.isFullScreen = false;
        $('.timeline').show();
    }
};

dinxUI.setFullScreen = function () {
    // Get the element that we want to take into fullscreen mode
    var element = document.getElementsByTagName("article")[0];
    // These function will not exist in the browsers that don't support fullscreen mode yet,
    // so we'll have to check to see if they're available before calling them.
    if (element.mozRequestFullScreen) {
      // This is how to go into fullscren mode in Firefox
      // Note the "moz" prefix, which is short for Mozilla.
      element.mozRequestFullScreen();
    } else if (element.webkitRequestFullScreen) {
      // This is how to go into fullscreen mode in Chrome and Safari
      // Both of those browsers are based on the Webkit project, hence the same prefix.
      element.webkitRequestFullScreen();
   }
   document.addEventListener("fullscreenchange", function () {
       dinxUI.toggleFullScreen();
   }, false);

   document.addEventListener("mozfullscreenchange", function () {
       dinxUI.toggleFullScreen();
   }, false);

   document.addEventListener("webkitfullscreenchange", function () {
       dinxUI.toggleFullScreen();
   }, false);
  };

dinxUI.showItemsNavigation = function () {
    //$('.itemsElement').append(this.itemsNavigation);

    $('.itemsElement').append("<br><a href=# onclick =dinxController.navigatePages(-1,'"+dinxController.viewBy.ITEM+"');><<</a> <a href=# onclick =dinxController.navigatePages(1,'"+dinxController.viewBy.ITEM+"');>>></a>");
    //this.itemsNavigation.html("<br><a href=# onclick ='dinxController.navigatePages(-1,"+moreItems.viewBy+");'><<</a> <a href=# onclick ='dinxController.navigatePages(1,"+moreItems.viewBy+");'>></a>");
    console.log("[showItemsNavigation] END");
}