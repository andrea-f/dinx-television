/**
 * DINX Television JWPlayer embedding
 * Refer to www.dinx.tv
 *
 * @author andrea@dinx.tv
 * @date 20-Jun-2013 01:09:55
 *
 *
 *<script src="{{ HOMEPATH }}media/jwplayer/jwplayer.js" ></script>
        <script>jwplayer.key="fLuJGN+g36H564yS2q04S6QMKiXJJyijX0WDaQ==";</script>
        
 */



dinxJWPlayer = {
    "videoUrl": "url of video",
    "isReady": false,
    "tag": document.createElement('script'),
    "firstScriptTag": document.getElementsByTagName('script')[0],
    "videosList": [], //mimic dinxController behaviour
    "videoPlayer": {}
}

dinxJWPlayer.current = {
    "videoIndex": 0
}

dinxJWPlayer.init = function (firstVideoUrl) {
    jwplayer('JWPlayerId').setup({
        'file': firstVideoUrl,
        'allowscriptaccess':'always',
        'id': 'JWPlayerId',
        'height': '100px',
        'width': '100px'
    });
    
    jwplayer("JWPlayerId").onComplete(dinxJWPlayer.playChannel);
};

dinxJWPlayer.availableOptions = {
    "roots": 0,
    "items": 0,
    "dates": 0,
    "events": 0
}

dinxJWPlayer.loadVideoByUrl = function (url) {
    arr = dinxJWPlayer.videosList;
    dinxJWPlayer.log("arr length is: "+arr.length);
    for (f = 0; f<arr.length; f++) {
        video = arr[f];
        url1 = url.split('?')[0];
        url2 = video['url'].split('?')[0];        
        if (url1 === url2) {
            dinxJWPlayer.loadVideo(f);
        }
    }
    
}

dinxJWPlayer.updateVideosIndex = function () {
    arr = dinxJWPlayer.videosList;
    for (f=0; f<arr.length; f++) {
        arr[f]['videoId'] = f;
    }
    dinxJWPlayer.videosList = arr;
}

dinxJWPlayer.loadVideo = function (videoId) {
    //player = document.getElementById('JWPlayerId');
    //this.player.sendEvent('LOAD', url);
    //jwplayer("JWPlayerId").load(url);
    dinxJWPlayer.log("[loadVideo] Next Event id: " + videoId)
    if (jwplayer('JWPlayerId')){
        //jwplayer("JWPlayerId").play();
        dinxJWPlayer.current.videoIndex = videoId;
        dinxJWPlayer.updateCurrentVideo();
        url = dinxJWPlayer.videosList[videoId].url;
        //jwplayer("JWPlayerId").load(url);
        jwplayer("JWPlayerId").setup({'file': url})
    } else {
        dinxJWPlayer.log("[loadVideo] JWPlayer hasn't loaded.")
    }
    jwplayer("JWPlayerId").onComplete(dinxJWPlayer.playChannel);

}

dinxJWPlayer.addVideo = function (video) {
    if (dinxJWPlayer.videosList.length === 0) {
        vidId = 0;
    } else {
        vidId = dinxJWPlayer.videosList.length - 1;
    }
    video['id'] = vidId;
    dinxJWPlayer.videosList.push(video);
    //dinxJWPlayer.mapVideoIdToVideoUrl(video.url)
    //dinxJWPlayer.log("[addVideo] video: " + video.name);
    //dinxJWPlayer.log("[addVideo] video11: " + this.videosList[0].url);
}

dinxJWPlayer.playChannel = function () {
    //jwpplayer("JWPlayer").setup({file: })
    //dinxJWPlayer.log("[playChannel] FIRED");

    videoId = dinxJWPlayer.current.videoIndex + 1;
    dinxJWPlayer.current.videoIndex = videoId;
    //dinxJWPlayer.log("[playChannel] id "+videoId);
    //dinxJWPlayer.log("[playChannel] total videos "+ dinxJWPlayer.videosList.length);
    
    url = dinxJWPlayer.videosList[videoId].url;
    //dinxJWPlayer.log("[playChannel] url "+url);
    dinxJWPlayer.updateCurrentVideo()
    dinxJWPlayer.loadVideo(videoId);
    //dinxJWPlayer.log("[playChannel] Play: " + dinxJWPlayer.current.videoIndex);
    if (jwplayer("JWPlayerId")){
        //dinxJWPlayer.log("VideoId: " + url);
        //dinxJWPlayer.videoPlayer.setup({'file': url})
        jwplayer("JWPlayerId").play();        
    } else {
        dinxJWPlayer.log("[playChannel] JWPlayer NOT defined");
    }
    dinxJWPlayer.log("[playChannel] Run");
}

dinxJWPlayer.log = function(text) {
    try {
        console.log(text)
        throw "Console not supported";
    } catch (err) {
        //err
    }
}

dinxJWPlayer.updateCurrentVideo = function () {
    videoId = dinxJWPlayer.current.videoIndex
    dinxJWPlayer.log("[updateCurrentVideo] id: "+videoId);
    if (typeof dinxJWPlayer.videosList[videoId] !== "undefined") {
        name = dinxJWPlayer.videosList[videoId].name;
        $('.liveEvent').html("<a href=# onclick=dinxJWPlayer.toggleOption('events')>"+name+"</a>  <a id=nextVideo href=# onclick=dinxJWPlayer.playChannel()>next video</a>");
    } else {
        dinxJWPlayer.log("[updateCurrentVideo] video in array is underfined");
    }

}

dinxJWPlayer.toggleOption = function (opt) {
    dinxJWPlayer.log("[toggleOption] Received: " +opt);
    this.log("[toggleOption] "+opt+" is " + dinxJWPlayer.availableOptions[opt]);
    if (dinxJWPlayer.availableOptions[opt] !== undefined) {
        optId = opt + "Nav";
        optObj = document.getElementById(optId);
        if (this.availableOptions[opt] === 0) {
            this.availableOptions[opt] = 1;
            optObj.style.display="block";
            optAction = "SHOWING";
        } else {
            this.availableOptions[opt] = 0;
            optObj.style.display="none";
            optAction = "HIDING";
        }
        dinxJWPlayer.log("[toggleOption] " + optAction + " " + opt);

    }
}

dinxJWPlayer.createVideoLink = function (image) {
    listElement = document.getElementById('videosBox');
    listItem = document.createElement('li');
    listItem.className = "videoLink";
    listItem.innerHTML = "<a href=# onclick=dinxJWPlayer.loadVideo("+(dinxJWPlayer.videosList.length-1)+")><img src="+image+" /></a>"
    listElement.insertBefore(listItem, listElement.firstChild);
}

