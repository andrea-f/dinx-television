/**
 * DINX FrontEnd file
 * Refer to www.dinx.tv
 *
 * @author code@dinx.tv
 * @date 25-Jan-2013 01:09:55
 *
 */

var dinxYTPlayer = {
    tag: document.createElement('script'),
    firstScriptTag: document.getElementsByTagName('script')[0],
    player3: {},
    nextVideo: {}, //video object with url, title, name, date etc
    "firstVideo": "YouTube compliant video id",
    "videos": [], //list of dictionaries containing video information
    "loaded": false
}

dinxYTPlayer.init = function (videoId) {
    dinxYTPlayer.log("[dinxYTPlayer.init] Loading youtube player...");
    dinxYTPlayer.firstVideo = videoId;
    this.tag.src = "//www.youtube.com/iframe_api";
    this.firstScriptTag.parentNode.insertBefore(this.tag, this.firstScriptTag);
};

function onYouTubeIframeAPIReady() {
    url = dinxYTPlayer.youtubeParser(dinxYTPlayer.firstVideo);
    dinxYTPlayer.log("[dinxYTPlayer] IFRAME ready");
    dinxYTPlayer.player3 = new YT.Player('playerElement', {
        height: '100%',
        width: '100%',
        videoId: url,
	playerVars: {'controls': 1 },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
};

function onPlayerReady(event) {
    dinxYTPlayer.log("[onPlayerReady] Player ready...");
    //dinxVideosController.playChannel(dinxVideosController.current.videoIndex);
    dinxYTPlayer.loaded = true;
    if (dinxVideosController.playing === true) {
        dinxYTPlayer.loadVideo(decodeURI(dinxVideosController.videosList[dinxVideosController.current.videoIndex].url));
        dinxYTPlayer.log("[onPlayerReady] Playing video...");
    }

};

dinxYTPlayer.youtubeParser = function (url){
    var regExp = /.*(?:youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;
    var match = url.match(regExp);
    if (match&&match[1].length==11){
        return match[1];
    }else{
       dinxVideosController.log("Url incorrect");
    }
}

function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.ENDED) {
        dinxVideosController.playChannel();
    };
};

//Play channel in new selection

dinxYTPlayer.loadVideo = function (url) {
    
    if (dinxYTPlayer.player3) {
        if (dinxVideosController.playing === true) {
            try {
                dinxYTPlayer.player3.loadVideoByUrl(url);
            } catch (err) {
                dinxVideosController.showPlayer();
                if (dinxYTPlayer.loaded === true) {
                    dinxYTPlayer.player3.loadVideoByUrl(url);
                }

            }
        } else {
            dinxYTPlayer.player3.cueVideoByUrl(url);
        }
        
    };
};


dinxYTPlayer.focusEvent = function (url,current,c){
    if(player3) {
        player3.loadVideoByUrl(url);
        document.getElementById('event_box').innerHTML="<p>"+decodeURI(current)+"</p>";
        glob.i=c
    };
};

dinxYTPlayer.log = function (text) {
    try {
        console.log(text);
    } catch (err) {
        //For IE compatibility
    }
}