/**
 * DINX Television VideosController
 * Refer to www.dinx.tv
 *
 * @author andrea@dinx.tv
 * @date 20-Jun-2013 01:09:55
 *
 *
 *<script src="{{ HOMEPATH }}media/jwplayer/jwplayer.js" ></script>
        <script>jwplayer.key="fLuJGN+g36H564yS2q04S6QMKiXJJyijX0WDaQ==";</script>

 */

document.onload = function () {
    dinxVideosController.init();
    dinxVideosController.log("document has loaded")
}

/*
 * Holds refrence to parameters used in functioning
 */
dinxVideosController = {
    "videoUrl": "url of video",
    "isReady": false,
    "tag": document.createElement('script'),
    "firstScriptTag": document.getElementsByTagName('script')[0],
    "videosList": [], //mimic dinxController behaviour
    "videoPlayer": {},
    "isTimelineActive": false,
    "titleVisible": false,
    "playing": false,
    "rootsInCategory": [],
    "urlMod": "videos",
    "clickCount": 0,
    "current": {
        "videoIndex": 0,
        "root": "",
        "channel": "",
        "item": "",
        "date": 0
    },
    "homeDivElement": $('<div id=homeDiv class=homeDiv></div>'),
    "allUrls":[],
    "videosPerPage": 30
}

/*
 * jquery reference to select elements on the page, add here to add more options
 */
dinxVideosController.selectElement = {
    "roots": $('.rootsSelectElement'),
    "dates": $('.datesSelectElement'),
    "items": $('.rootsSelectElement'),
    "related": $('.related')

}

dinxVideosController.availableOptions = {
    "roots": 0,
    "items": 0,
    "dates": 0,
    "events": 0
}

/*
 * Holds reference to UI elements with JQuery
 */
dinxVideosController.divElement = {
    "roots": $('#roots'),
    "body": $('body'),
    "items": $('#items'),
    "dates": $('#dates'),
    "events": $('#events'),
    "videosElement": $('.videosDiv'),
    "videosBox": $('.videosBox'),
    "titleList": $('.titleList'),
    "searchingTitle": $('#searchingTitle'),
    "playerElement": $("#playerElement"),
    "timeline": $('.timeline'),
    "currentEvent": $('.currentEvent'),
    "channelControls": $('#channelControls'),
    "paginationElement": $('.paginationElement'),
    "searchForm": $('.searchForm')
}

dinxVideosController.init = function () {
    dinxVideosController.showNavigation();
    this.divElement.channelControls.css('display','none');
    this.generateTitleList();
};

/*
 * Show navigation element like roots, items etc...
 */

dinxVideosController.showNavigation = function () {
    divElement = dinxVideosController.divElement;
    for (key in divElement) {
        dinxVideosController.log("[showNavigation] Showing element: " + key);
        elem = divElement[key].value;
        if (this.divElement[key] !== this.divElement.events) {
            dinxVideosController.toggleDivVisibility(elem)
        }
    }    
}


/*
 * Load video in player by passing specific url
 */
dinxVideosController.loadVideoByUrl = function (url) {
    arr = dinxVideosController.videosList;
    dinxVideosController.log("[loadVideoByUrl] arr length is: "+arr.length);
    for (f = 0; f<arr.length; f++) {
        video = arr[f];
        url1 = url.split('?')[0];
        url2 = video['url'].split('?')[0];
        if (url1 === url2) {
            dinxVideosController.loadVideo(f);
        }
    }

}

/*
 * Update video index for playlist reference by one.
 */
dinxVideosController.updateVideosIndex = function () {
    arr = dinxVideosController.videosList;
    for (f=0; f<arr.length; f++) {
        arr[f]['videoId'] = f;
    }
    dinxVideosController.videosList = arr;
}


/*
 * Load video by video id also shows video player on secreen.
 */
dinxVideosController.loadVideo = function (videoId) {
    dinxVideosController.log("[loadVideo] Next Event id: " + videoId)
    dinxVideosController.showPlayer();
    if (dinxYTPlayer.player3){
        //jwplayer("JWPlayerId").play();
        dinxVideosController.current.videoIndex = videoId;
        
        dinxVideosController.updateCurrentChannel();
        url = dinxVideosController.videosList[videoId].url;
        //jwplayer("JWPlayerId").load(url);
        dinxYTPlayer.loadVideo(url);
    } else {
        dinxVideosController.log("[loadVideo] YTPlayer hasn't loaded.")
    }
    //jwplayer("JWPlayerId").onComplete(dinxVideosController.playChannel);

}


/*
 * Adds video to page and to array of videos
 * @param {Object} video contains videoId, image,url, title, duration...
 */
dinxVideosController.addVideo = function (video) {
    if (dinxVideosController.videosList.length === 0) {
        vidId = 0;
    } else {
        vidId = dinxVideosController.videosList.length - 1;
    }
    video['id'] = vidId;
    dinxVideosController.videosList.push(video);   
}


dinxVideosController.log = function(text) {
    try {
        console.log(text)
        throw "Console not supported";
    } catch (err) {
        //err
    }
}

/*
 * Function is called when playchannel is called
 * updates current event div
 */
dinxVideosController.updateCurrentChannel = function () {
    videoId = dinxVideosController.current.videoIndex;
    dinxVideosController.log("[updateCurrentVideo] id: "+videoId);
    if (typeof dinxVideosController.videosList[videoId] !== "undefined") {
        name = dinxVideosController.videosList[videoId].name;
        this.divElement.currentEvent.html("<a href=# onclick='dinxVideosController.toggleEvents()'>"+name+"</a> <a id=nextVideo href=# onclick=dinxVideosController.playChannel()>next video</a>");
    } else {
        dinxVideosController.log("[updateCurrentVideo] video in array is underfined");
    }

}

/*
 * Function adds href and image thumbnail with link to video to screen 
 */
dinxVideosController.createVideoLink = function (image) {
    listElement = document.getElementById('videosBox');
    listItem = document.createElement('li');
    listItem.className = "videoLink";
    listItem.innerHTML = "<a href=# onmouseover=dinxVideosController.showSearching("+(this.videosList.length-1)+") onmouseout=dinxVideosController.showSearching() onclick = 'dinxVideosController.playChannel("+(dinxVideosController.videosList.length-1)+")'><figure class = videoImage><img src=" + image + " /><figcaption class=videosElementFigcation>"+dinxVideosController.videosList[dinxVideosController.videosList.length-1].title.slice(0,15)+"</figcaption></figure></a>"
    listElement.insertBefore(listItem, listElement.firstChild);
}


/*
 * sets background image on page
 */
dinxVideosController.setBackgroundImage = function (disable) {
    if (typeof disable === "undefined") {
        dir = "http://"+window.location.hostname + "/media/images/backgrounds/";
        var imgCount = 15;
        var randomCount = Math.round(Math.random() * (imgCount - 1)) + 1;
        // array of images & file name
        var images = [];
        for (var i = 0; i < imgCount; i++) {
            images[i] = i+".jpg";
        }
         imgUrl = "url(" + dir + images[randomCount] + ")";
    } else {
        imgUrl = "None";
    }
    document.body.style.backgroundImage = imgUrl;
}
/*
 * Hide or display select elements
 * @param {Object} element is id/class reference to hide/display
 */
dinxVideosController.toggleDivVisibility = function (rootsDiv, element) {
    selectElement = element
    dinxVideosController.log("[toggleDivVisibility] "+ selectElement.attr('class') +" length: " + selectElement.length);
    if (selectElement.length > 0) {
        rootsDiv.css('display','block');
    }
}

/*
 * Set select attributes
 */
dinxVideosController.setSelectAttributes = function (element) {
    element
        .attr('size','7')
    //element
    //    .click('dinxVideosController.selectInput.action(this)')
    //element
    //    .attr('onblur', 'dinxVideosController.selectInput.blur()');
    //return element;
}

/*
 * Show roots in specific category
 */
dinxVideosController.showRootsIn = function (category) {
    dinxVideosController.setBackgroundImage('disable');
    dinxVideosController.showSearchBar(1);
    this.divElement.searchForm.css('opacity','0.0').hide();
    try {
        if (this.current.root === "") {
            dinxVideosController.setEmptyRoot(1);
        } else {
            dinxVideosController.setEmptyRoot(0);
        }
    } catch (err) {dinxVideosController.log( "[showRootsIn] " +err);}
    dinxVideosController.log( "[showRootsIn] Here " );
    //rootsDiv = dinxVideosController.divElement.roots;
    rootsDiv = $('#roots')
    rootsDiv
        .css('display','block')
        .find('select')
        .children()
        .remove()
        .end();    
    //rootsElement = dinxVideosController.selectElement.roots;
    rootsElement= $('.rootsSelectElement');
    var i = 0;
    arr = this.rootsInCategory;
    for (i; i < arr.length; i++) {
        t = "Input " + category + ": root " + arr[i].root;
        $.each( arr[i], function(i, n){
            dinxVideosController.log( "[showRootsIn] Name: " + i + ", Value: " + n );
        });
        
        if (category === arr[i].category) {
            rootEncoded = encodeURIComponent(arr[i].root);
            key = "http://"+window.location.hostname + "/trova/?root="+rootEncoded+"/";
            rootsElement
                .append($("<option></option>")
                .attr("value",key) //key has to be URL to request
                .text(arr[i].root));
        }
    }
}

/*
 * Handle select clicking 
 */
dinxVideosController.selectInput = {
        clickCount : 0,
        action : function(select) {
            dinxVideosController.clickCount++;
            window.location.href = select.options[select.selectedIndex].value;
        },
        blur : function() {// needed for proper behaviour
            if(clickCount%2 != 0) {
                dinxVideosController.clickCount--;
            }
        },
        scroll: function(select) {
            var scrollAmount = select.scrollTop;

        }
};

/*
 * Show player element, set timeline class to videos box
 */
dinxVideosController.showPlayer = function (){
    this.divElement.playerElement = $('#playerElement'), this.divElement.videosElement = $('.videosDiv');
    this.divElement.playerElement.css('width', '60%').css('height', '65%');
    this.divElement.playerElement.show();
    this.divElement.videosElement.attr('class','timeline')
};

/*
 * Plays specific video id, if no videoId is provided plays next one in list
 */
dinxVideosController.playChannel = function (videoId) {
    if (typeof videoId === "undefined") {
        dinxVideosController.current.videoIndex = dinxVideosController.current.videoIndex + 1;
        videoId = dinxVideosController.current.videoIndex;
    } else {
        dinxVideosController.current.videoIndex = videoId;
    }
    
    if (this.playing === false) {
        this.playing = true;
        $('nav').append('<a href="#" onclick="dinxVideosController.toggleTimeline()">Toggle timeline</a>');
        dinxVideosController.showPlayer();
    } else {
        //set current event        
        if (dinxVideosController.isTimelineActive === false) {
            dinxVideosController.toggleTimeline();
            //dinxUI.showChannelControls()
        }
    };
    dinxVideosController.updateCurrentChannel()
    dinxYTPlayer.loadVideo(decodeURI(dinxVideosController.videosList[dinxVideosController.current.videoIndex].url));
    dinxVideosController.showRelatedItems(videoId);
}

/*
 * Toggles timeline view or on off. moves videos thumbs, shows video player
 */
dinxVideosController.toggleTimeline = function () {
    if (this.isTimelineActive === false) {
        $('.videosDiv').attr('class','timeline')
        this.isTimelineActive = true;
        this.divElement.playerElement.show();        
    } else {
        $('.timeline').attr('class','videosDiv');
        this.divElement.playerElement.hide();
        this.isTimelineActive = false;
    }
};

dinxVideosController.generateTitleList = function () {
    var d = 0;
    titles = document.createElement('ul');
    titles.className = 'titleList';
    for (d; d < this.videosList.length; d++) {
        li = document.createElement('li');
        eventItem = "<a href=# onclick='dinxVideosController.playChannel("+d+")'>"+this.videosList[d].title+"</a>"
        li.innerHTML = eventItem;
        titles.insertBefore(eventItem, titles.firstChild);
    }
    titles.style.visibility='0.0';
    videosDiv = document.getElementById('videosDiv');
    videosDiv.insertBefore(titles, videosDiv.firstChild);

}

dinxVideosController.toggleEvents = function () {
    if (this.titleVisible === false) {
        this.titleVisible = true;
        this.divElement.videosBox.css('visibility','0.0');
        this.divElement.titleList.css('visibility','1.0');
    } else {
        this.titleVisible = false;
        this.divElement.videosBox.css('visibility','1.0');
        this.divElement.videosBox.css('visibility','0.0');
    }
}


/*
 * Show video title on mouse over thumbnail
 */
dinxVideosController.showSearching = function (videoId) {
    searchingTitle = document.getElementById('searchingTitle');
    if (typeof videoId !== 'undefined') {
        searchingTitle.innerHTML = dinxVideosController.videosList[videoId].title;
    } else {
        searchingTitle.innerHTML = ""
    }
}

/*
 * Sets home phrase in homepage
 */
dinxVideosController.setHomeDiv = function () {
    this.divElement.body.append(this.homeDivElement);
    var language = window.navigator.userLanguage || window.navigator.language, phrase = "<span class=spanQuestion id=spanQuestion>";
    if (language === "it") {
        phrase = phrase + "<h1><span class=asterisk id=asterisk>*Ogni riferimento a fatti realmente accaduti o a persone realmente esistenti <span class=redText>non</span> è da ritenersi puramente casuale.</span></h1>";
    } else {

        phrase = phrase + "<h1><span class=asterisk id=asterisk>*Any resemblance to real events or to real persons, living or dead, is <span class=redText>not</span> purely coincidental.</span></h1>";
    }
    phrase = phrase + "</span>";
    dinxVideosController.log(phrase);
    this.homeDivElement.html(phrase);
    
}

dinxVideosController.showSearchBar = function (active) {
    if (active === 1) {
        $('.searchBar').css('opacity','1.0');
        sb = document.getElementById('searchBar');
        sb.style.opacity = '1.0';
        this.log('[showSearchBar] showingSearchBar');
    } else {
        $('.searchBar').css('opacity','0.0');
    }
}

/*
 * Show information text before root has been chosen
 */
dinxVideosController.setEmptyRoot = function (active) {

    var empty = "<div class=noroot id=noroot>Click<b>-></b> on one of the words in the right menu</div>";
    if (active === 1) {
        this.divElement.body.append(empty);
    } else {
        try{
            this.divElement.body.detach($('.noroot'));
            
        } catch (err) {
            this.divElement.body.remove($('.noroot'));
        }
        $('#noroot').css('opacity','0.0');

    }
    this.log("[setEmptyRoot] hiding root");
}

dinxVideosController.goToPrevious = function (root) {
    window.history.go("http://www.dinx.tv/"+root);
}

dinxVideosController.showRelatedItems = function (videoId) {
    relatedItems = dinxVideosController.videosList[videoId].relatedItems;
    var f = 0, items = "";

    for (f; f < relatedItems.length; f++) {
        items = items + "<option value='/trova/?root="+dinxVideosController.current.root + "&q=" +relatedItems[f]+"'>"+relatedItems[f]+"</option>";
    }
    related = $('<select class="itemsSelectElement related" id="itemsSelectElement related" size=3 onclick="dinxVideosController.selectInput.action(this)" onblur="dinxVideosController.selectInput.blur()"></select>').html(items)
    
    this.selectElement.related.text("").html(related)
    //this.log("[showRelatedItems] adding related select...");
}

dinxVideosController.setCurrentChannel = function () {
    var c = this.current.rootName;
    //if (this.current.item !== "") {
    //    c = c +"/"+ this.current.item + "/";
    //}
    if (this.current.date !== 0) {
        c = c + "/" + this.current.date;
    }
    this.current.channel = c;
    this.log("[setCurrentChannel] Setting current channel");
}

dinxVideosController.setPaginationElement = function (totalVideos) {
    this.divElement.paginationElement = $('.paginationElement');
    elem = $('<div class=pages></div>');
    currentUrl = location.href;
    var page = 1,
    allPages = "";
    totalPages = totalVideos / this.videosPerPage;
    if (totalPages > 1) {
        for (page; page <= totalPages; page++) {
            if (page <= 15) {
                        //if (page === currentPage) {
                        //    p = " <b><a href='"+currentUrl+"&page="+page+"'>"+page+"</a></b> ";
                        //} else {
                        p = " <a href='"+currentUrl+"&page="+page+"'>"+page+"</a> "
                        //}
                        allPages = allPages + p;
                        //dinxVideosController.log("showing page "+page)
                    };
            }
            elem.html(allPages + "...<a href='"+currentUrl+"&page="+ totalPages+">"+ totalPages +"</a>");
            //pages = document.getElementById('pages')
            //pages.insertBefore(allPages, pages.firstChild);
            //$(".pages").append(" <a href='"+currentUrl+"/"+totalPages+"'>"+ totalPages +"</a> ");
            this.divElement.paginationElement.append(elem)
    }
}