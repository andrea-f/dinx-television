/**
 * DINX FrontEnd file
 * Refer to www.dinx.tv
 * 
 * @author code@dinx.tv
 * @date 25-Jan-2013 01:09:55
 * 
 */

 var dinxController = {
    roots : [],
    type : "",
    dates : [],
    videos : [],
    items : [],
    options: [],
    playing : false,
    GUIDEMODE : true,
    itemIsSet: false,
    searchFilter : 0,
    alphabeticalItemsReference: [], // holds letter and m video index information
    objectsArray: [],
    requestCount: 0,
    ajaxRequestIsActive: false,
    scrolling: false,
    preventDefaultUpdate: false,
    previousScrollAmount: 0,
    uniqueRoots: [],
    allRootsByYear: []
 };

 dinxController.typeOfLink = {
     CATEGORY : "category",
     ROOT : "root",
     DATE : "date",
     ITEM : "item",
     EVENT : "event",
     VIEWBY : "viewby"
 };

dinxController.itemTypes = {
    "PERSON": "person",
    "LOCATION": "location",
    "TEAM": "team",
    "ORGANIZATION": "organization",
    "MISC": "misc"
};

 dinxController.currentChannel = {
     name : "",
     date : 0,
     item: "",
     root : "",
     category : "",
     viewBy : "",
     videoIndex : 0,
     videoUrl : "",
     eventName : "",
     numberOfVideos: 0,
     videosPerPage: 15,
     videoPage: 1,
     itemPage: 1,
     totalVideos: 0,
     totalItems: 0
     
 };

 dinxController.viewBy = {
     "DATE" : "date",
     "VIDEO" : "video",
     "ROOT": "root",
     "ITEM": "item",
     "ALL": "all"
 };


dinxController.uiListHandlers = {
    "dates": "dates",
    "roots": "roots",
    "items": "items",
    "videos": "videos",
    "options": "options",
    "pagination": "pagination"
}

 dinxController.CATEGORIES = {
     SPORT: "sports",
     HISTORY: "history",
     FILM: "movies"
 };

 dinxController.controls = {
    "Play": function () {dinxPlayer.play();},
    "Pause": function () {dinxPlayer.pause();},
    "Stop": function () {dinxPlayer.stop();},
    "RW": function () {dinxPlayer.seek(-1);},
    "FF": function () {dinxPlayer.seek(1)},
    "Mute": function () {dinxPlayer.toggleMute();},
    "Next Video": function () {dinxController.playNextVideo();},
    "Full Screen": function () {dinxUI.setFullScreen();},
    "Toggle Timeline": function () {dinxUI.toggleTimeline();}
}


dinxController.setListAction = {
    "roots": function(m) {dinxController.createChannel(encodeURI(dinxController.roots[m].name) , encodeURI(dinxController.typeOfLink.ROOT));},
    "items": function(m) {dinxController.createChannel(encodeURI(dinxController.items[m].name) , dinxController.items[m].type);},
    "dates": function(m) {dinxController.createChannel(dinxController.dates[m] , dinxController.typeOfLink.DATE );},
    "videos": function(m) {dinxController.playChannel(m, encodeURI(dinxController.videos[m].url), dinxController.videos[m].date, encodeURI(dinxController.videos[m].name));},
    "options": function (m) {dinxController.createChannel(dinxController.options[m], dinxController.typeOfLink.VIEWBY);}
}


dinxController.init = function () {
    dinxUI.init();
    dinxPlayer.init();
    
    
};

dinxController.selectHandler = {
        clickCount : 0,
        action : function(select,typeOfList) {
            dinxController.selectHandler.clickCount++;
            m = select.options[select.selectedIndex].value;
            if (dinxController.setListAction[typeOfList]) {
                dinxController.setListAction[typeOfList](m);
            } 
        },
        blur : function() {// needed for proper behaviour
            if(dinxController.selectHandler.clickCount%2 != 0)
            {
                dinxController.selectHandler.clickCount--;
            }
        },
        scroll: function(select) {
            var scrollAmount = select.scrollTop;

        }
};

dinxController.setCategory = function (category) {
    this.currentChannel.category = category;    
};

dinxController.setRoot = function (root) {
    this.currentChannel.root = root;    
};

dinxController.setDate = function (date) {
    this.currentChannel.date = date;
};

dinxController.setItem = function (name, type) {
    name = decodeURI(name);
    if (this.itemTypes[type]) {
        if ((type === this.itemTypes.ORGANIZATION) || (type === this.itemTypes.MISC)) {
            if (this.currentChannel.category === this.CATEGORIES.SPORT) {
                this.currentChannel.itemType = this.itemTypes.TEAM;
            }
        } else {
            this.currentChannel.itemType = type.toLowerCase();            
        }
        this.currentChannel.item = name;
    }
}

/**
 * From default view start path by identifying name of object clicked on and type
 * @param {String} name item is entity clicked on by user
 * @param {String} typeOfItem what type of item is being requested
 */
dinxController.createChannel = function (name, typeOfItem) {
    var currentType = "";
    name = decodeURI(name);
    typeOfItem = decodeURI(typeOfItem);
    console.log(name,typeOfItem);
    if (this.itemTypes[typeOfItem.toUpperCase()]) {
        currentType = typeOfItem.toUpperCase();
        typeOfItem = this.typeOfLink.ITEM;
    }
    this.preventDefaultUpdate = false;
    this.currentChannel.videoPage = 1;
    this.currentChannel.itemPage = 1;
    this.requestCount = 0;
    switch (typeOfItem) {
        case this.typeOfLink.CATEGORY:
            dinxUI.clearBackground();
            dinxUI.setAsideElements();
            this.clearItems();
            this.currentChannel.root = "";
            this.setDate(0);
            this.setCategory(name);
            dinxUI.generateList(this.uiListHandlers.roots);
            dinxUI.setUIElement(1, this.uiListHandlers.roots);
            dinxUI.setUIElement(0, this.uiListHandlers.items);
            dinxUI.setUIElement(0, this.uiListHandlers.dates);
            break;
        case this.typeOfLink.ROOT:
            this.setRoot(name);
            //this.setDate(0);
            dinxUI.setUIElement(1, this.uiListHandlers.items);
            dinxUI.setUIElement(1, this.uiListHandlers.dates);           
            break;        
        case this.typeOfLink.DATE:
            this.setDate(name);
            //this.currentChannel.item = "";
            this.setCurrentChannelView(typeOfItem);
            dinxUI.generateList(this.uiListHandlers.roots);
            dinxUI.setUIElement(1, this.uiListHandlers.roots);
            break;
        case this.typeOfLink.ITEM:
            this.setItem(encodeURI(name),currentType);
            break;
    };
    if (typeOfItem !== this.typeOfLink.CATEGORY) {
        this.updateObjectsLists(typeOfItem);        
    }
    if (typeOfItem !== this.typeOfLink.VIEWBY) {
        this.addToSearching(name);
    };
    console.log("[createChannel] received " + typeOfItem);
};


dinxController.splitItemsAlphabetically = function () {
    for (var m = 0; m < dinxController.items.length; m = m + 1) {
        firstLetter = dinxController.items[m].name.substring(0,1);
        try {
            var previousFirstLetter = dinxController.items[m+1].name.substring(0,1);
            throw "No item[m+1]";
        } catch (err) {
            console.log(err);
            fistLetter = previousFirstLetter;
        };
        if (firstLetter !== previousFirstLetter) {
        //hold pointers of firstLetter and m value of change to a different alphabet letter
        //array of objects [{"letter":firstLetter, "m": m }]
            dinxController.alphabeticalItemsReference.push({"letter":firstLetter, "m": m});
        };
    };
}

/**
 * This function changes the data view based on location, person, date...
 * @param {Object} option describes data organization parameter
 */
dinxController.setCurrentChannelView = function (option) {
    if (this.viewBy[option]) {
        this.currentChannel.viewBy = option;
    }
    msg = "showing " + this.currentChannel.root + " by " + option;
    console.log(msg);
    //dinxFeedsHandler.makeAjaxRequest(this.currentChannel, this.currentView);        
}

dinxController.clearItems = function () {
    this.currentChannel.item = "";
    this.currentChannel.viewBy = "";
}

dinxController.removeOption = function (arr,option) {
	//remove option from option list
    option = decodeURI(option);
    while (arr.indexOf(option) !== -1) {
        arr.splice(arr.indexOf(option), 1);
    };
    while (arr.indexOf(option) !== -1) {
        delete arr[arr.indexOf(option)];
    };
    return(arr);
};

/*
 * This function handles the server response
 * @param {String} responseViewBy type of response from server: all, dates, items
 * @param {Object} pagData holds information about total videos, total pages, next page contained in response
 * @param {Object} response is payload
 */

dinxController.handleResponse = function (response, responseViewBy, pagData) {
    console.log("[handleResponse] viewby: "+responseViewBy);
    pag = {};
    try {
        if ((typeof pagData.totalvideos !== 'undefined') && (pagData.totalvideos > dinxController.currentChannel.videosPerPage)) {
            pag.v = pagData.totalvideos;
            pag.p =  pagData.totalpages;
            pag.n =  pagData.nextpage;
            this.setPaginationData(pag, responseViewBy);
            //console.log(pag.v, pag.p, pag.n);
        } else {
            //situation where there are less than 15 videdos
            dinxUI.setUIElement(0, this.uiListHandlers.pagination);
            this.currentChannel.totalVideos = pagData.totalvideos;
            dinxUI.showSearching();
        }
     } catch(err) {
        //console.log("[dinxController.handleResponse] Error in pagination: " + err);
     }
    switch (responseViewBy) {
        case this.viewBy.DATE:
            this.dates = response;
            dinxUI.generateList(this.uiListHandlers.dates);
            dinxUI.setUIElement(1, this.uiListHandlers.dates);
            break;
        case this.viewBy.ITEM:
            this.items = response;
            dinxUI.generateList(this.uiListHandlers.items);
            dinxUI.setUIElement(1, this.uiListHandlers.items);
            if (this.items.length > 20) {
                 //this.splitItemsAlphabetically();
                 dinxUI.showItemsNavigation();
            }
            break;
        case this.viewBy.ALL:
            this.videos = response;
            this.currentChannel.numberOfVideos = this.videos.length;
            console.log("[handleResponse] Displaying"+ this.currentChannel.numberOfVideos +" videos..");
            dinxUI.generateList(this.uiListHandlers.videos);
            dinxUI.setUIElement(1, this.uiListHandlers.videos);
            break;
        case this.viewBy.ROOT:
            dinxUI.generateList(this.uiListHandlers.roots);
            dinxUI.setUIElement(1, this.uiListHandlers.roots);
            break;
    }
    if (responseViewBy !== this.viewBy.ALL && this.preventDefaultUpdate === false) {
        this.updateObjectsLists(responseViewBy);
    }
    dinxUI.generateList(this.uiListHandlers.options);
    dinxUI.setUIElement(0, this.uiListHandlers.options);    
};

dinxController.playChannel = function (m, url, date, name) {
    if (this.currentChannel.date === 0 || this.currentChannel.date !== date) {
        this.setDate(date);
    };
    this.currentChannel.eventName = decodeURI(name);
    this.currentChannel.videoIndex = m;
    dinxController.addToSearching();
    console.log("[playChannel] Playing video with url " + decodeURI(url));//this.currentChannel.videoUrl = url;
    if (!this.playing) {
        this.playing = true;
        dinxUI.showPlayer();
    } else {
        dinxPlayer.loadVideo(decodeURI(url));
        if (dinxUI.isTimelineActive === false) {
            dinxUI.toggleTimeline();
            //dinxUI.showChannelControls()
        }
    };
    
};

dinxController.addToSearching = function () {
    this.searchFilter = this.searchFilter + 1;
    dinxUI.showSearching();
};

dinxController.updateCurrentChannel = function () {
    var v = this.currentChannel.videoIndex + 1;
    this.currentChannel.videoIndex = v;
    this.currentChannel.videoUrl = this.videos[v].url;
    this.currentChannel.eventName = this.videos[v].name;
    return (v);
};

//Play next video in channel button action
dinxController.playNextVideo = function () {
    v = this.updateCurrentChannel();
    dinxPlayer.loadVideo(this.videos[v].url);
    dinxUI.showSearching();
};

dinxController.processControlsInput = function (controlType) {
    controlType = decodeURI(controlType);
    if (this.controls[controlType]) {
        console.log("[processControlsInput] Control received: " + controlType);
        this.controls[controlType]();
    };
};

/*
 *  This function updates items, dates, roots based on what is selected either date, item or root
 */
dinxController.updateObjectsLists = function (typeOfItem) {
    var c = {};
    c = this.currentChannel;
    console.log("[updateObjectsLists] item: " + this.currentChannel.item);
    switch(typeOfItem) {
        case this.viewBy.ITEM:
            if (this.requestCount === 0) {
                //c.date = 0;
                c.viewBy = this.viewBy.ROOT;
                
            } else if (this.requestCount === 1) {
                c.viewBy = this.viewBy.DATE;
            } else  if (this.currentChannel.item !== "") {
                this.preventDefaultUpdate = true;
            }
            break;
        case this.viewBy.DATE:
            if (this.requestCount === 0) {
                c.viewBy = this.viewBy.ITEM;                
            } else if (this.requestCount === 1) {
                c.viewBy = this.viewBy.ROOT;
                
            }
            break;
        case this.viewBy.ROOT:
            if (this.requestCount === 0) {
                //this.clearItems();
                //c.date = 0;
                c.item = "";
                c.viewBy = this.viewBy.ITEM;
            } else if (this.requestCount === 1) {
                c.viewBy = this.viewBy.DATE;
            }
            break;
    }    
    if (this.requestCount < 2) {
        dinxFeedsHandler.makeRequest(c);
    } else if (this.requestCount === 2 && this.preventDefaultUpdate === false) {
        for (key in c) {
             console.log("[updateObjectsLists] c." +key+ ": " + c[key]);
        }        
        c.viewBy = this.viewBy.ALL;
        dinxFeedsHandler.makeRequest(c);
    }
    if (this.requestCount === 1 && (this.currentChannel.date !== 0 || this.currentChannel.item !== "") && this.preventDefaultUpdate === false) {
        this.setCurrentChannelView(this.viewBy.ALL);
        dinxFeedsHandler.makeRequest(this.currentChannel);
    }
    console.log("[updateObjectsLists] Current channel: "+typeOfItem+ " Requesting viewBy: " + c.viewBy + " Request count: " + this.requestCount);
    this.requestCount = this.requestCount + 1;    
};

dinxController.navigatePages = function (pageNav, viewBy) {
    var v = "", c = {}, nextItem = "", moreItems = {};
    if (typeof viewBy === 'undefined'){
        v = this.viewBy.VIDEO;
        nextItem = pageNav;
        //prevents default behaviour of asking for more queries
        this.preventDefaultUpdate = false;
    } else {
        v = this.viewBy.ITEM;
        nextItem = dinxController.currentChannel[v+'Page'] + pageNav;
        this.preventDefaultUpdate = true;
    }
    if (pageNav === -1) {
        dinxController.currentChannel[v+'Page'] = dinxController.currentChannel.videoPage - 1;
    } else {
        dinxController.currentChannel[v+'Page'] = nextItem;
    }
    console.log("[navigatePages] Navigation recevied: " + pageNav + " viewBy: " + viewBy + " v: " + v);
    if (this.requestCount > 0) {
        this.requestCount = 2;
    }
    c = this.currentChannel;
    c.numberOfVideos = 0;
    c.pageItems = 0;
    if (viewBy === dinxController.viewBy.ITEM){
        moreItems = dinxController.currentChannel;
        moreItems.item = "";
        moreItems.viewBy = dinxController.viewBy.ITEM;
        if (dinxController.currentChannel.category === dinxController.CATEGORIES.SPORT) {
            moreItems.itemType = dinxController.itemTypes.TEAM;
        } else {
            moreItems.itemType = dinxController.itemTypes.PERSON;
        }
        console.log('requestCount: ' + dinxController.requestCount + " progress: " + dinxFeedsHandler.progress);
        //dinxController.navigatePages(1,moreItems.viewBy);
         console.log(moreItems.viewBy);
        dinxFeedsHandler.makeRequest(moreItems);
    } else {
        dinxFeedsHandler.makeRequest(this.currentChannel);
    }
}

dinxController.setPaginationData = function (pag, viewBy) {
    console.log("[dinxController.setPaginationData]"+ viewBy + " pages: "+ pag.p + " videos: " + pag.v + " next " + pag.n);
    if (viewBy === this.viewBy.ALL) {
        v = this.viewBy.VIDEO;
        this.currentChannel.totalVideos = pag.v;
        dinxUI.showPagination(pag);
    }
    if (pag.p === (pag.n - 1)) {
        // Last page of results
        this.currentChannel[v + 'Page'] = pag.p;
    } else {
        //Not last page of results
        this.currentChannel[v + 'Page'] = (pag.n);
    }
    //console.log("[dinxController.setPaginationData] Total videos: " + this.currentChannel.totalVideos);
    dinxUI.showSearching();
    
    
}

/*
 * This function matches the current date with all the roots for that date and adds it to the roots list
 */
dinxController.updateYearsList = function (m) {
    var years = dinxController.allRootsByYear, roots = [];
    if (this.currentChannel.date !== 0) {
        for (c = 0; c < years.length; c++) {            
            if (dinxController.currentChannel.date === years[c].date) {
                if (jQuery.inArray(dinxController.roots[m].name, years[c].roots) !== -1) {
                    dinxController.roots[m].date = dinxController.currentChannel.date;
                    //alert("setting "+dinxController.roots[m].date+" for "+dinxController.roots[m].name);
                }
            }

        }
    }
}

dinxController.log = function (text) {
    try {
        console.log(text);
    } catch (err) {};
}