# Django settings for dinx project.
import os
#import logging
import socket

LOGGING_CONFIG = None

if socket.gethostname() == 'legionovainvicta':
    DEBUG = TEMPLATE_DEBUG = True
else:
    DEBUG = TEMPLATE_DEBUG = False

TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)



#from django.conf import settings
#from django.db.backends import BaseDatabaseWrapper
#from django.db.backends.util import CursorWrapper

#if DEBUG:
#    BaseDatabaseWrapper.make_debug_cursor = lambda self, cursor: CursorWrapper(cursor, self)

#if DEBUG:
#EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
#EMAIL_FILE_PATH = '/usr/local/src/emails/'
CACHE_BACKEND = 'memcached://127.0.0.1:11211/'

#CACHES = {
#    'default': {
#        #'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
#        #'LOCATION': '127.0.0.1:11211',    }
#        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
#        'LOCATION': 'dinx_cache'}
#}

#logging.basicConfig(
#    level = logging.DEBUG,
#    format = '%(asctime)s %(levelname)s %(message)s',
#    filename = '/django_log.log',
#    filemode = 'a'
#)

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.solr_backend.SolrEngine',
        'URL': 'http://127.0.0.1:8983/solr'
    },
}

MANAGERS = ADMINS
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_HOST = 'localhost'
EMAIL_PORT = 25
#EMAIL_HOST_USER = 'information'
#EMAIL_HOST_PASSWORD = 'mainagioia55'
EMAIL_USE_TLS = False
DEFAULT_FROM_EMAIL = 'information@dinx.tv'


AUTH_PROFILE_MODULE = 'dinx_app.UserProfile'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'django_dinx_user',                      # Or path to database file if using sqlite3.
        'USER': 'django_user',                      # Not used with sqlite3.
        'PASSWORD': 'dinx',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    },
    'videoDBUsers': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'django_dinx',                      # Or path to database file if using sqlite3.
        'USER': 'django_user',                      # Not used with sqlite3.
        'PASSWORD': 'dinx',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

#try:
#    from django.db import connections
#except:
#    pass
#try:
#    DATABASE_ROUTERS = ['dinx_app.videoDB.videoDBUsers',]
#except:
DATABASE_ROUTERS = ['dinx.dinx_app.videoDB.videoDBUsers',]

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/"

#STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
 #   os.path.join(os.path.dirname(__file__), 'static').replace('\\','/'),

#)

#CURRENT_PATH = os.getcwd()
#MEDIA_ROOT = os.path.join(CURRENT_PATH, 'static')
def rel(*x):
    return os.path.join(os.path.abspath(os.path.dirname(__file__)), *x)

from os import path
#MEDIA_ROOT = rel('media')
MEDIA_ROOT = path.join(path.abspath(path.dirname(__file__)), 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/media/'

if socket.gethostname() == 'legionovainvicta':
    UP = 'http://localhost/'

else:
    UP = 'http://www.dinx.tv/'




# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/s-media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'e0u71w+%4a!+qate&@6q=49^^nj5y)duot^%4x+uve()tauu-c'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
)

ROOT_URLCONF = 'dinx.urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(os.path.dirname(__file__), 'templates').replace('\\','/'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'haystack',
    'django.contrib.sessions',
    'django.contrib.sites',
    #'django.contrib.messages',
    'dinx.dinx_app',
    'registration',
    'django.contrib.humanize',
    # Uncomment the next line to enable the admin:
     'django.contrib.admin',
     
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
)

ACCOUNT_ACTIVATION_DAYS = 7

LOGIN_REDIRECT_URL = '/trova/?root=Serie A'
