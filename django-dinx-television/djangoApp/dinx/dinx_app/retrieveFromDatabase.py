#!/usr/local/bin/python
# coding: utf-8

from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.db.models import Count,Sum, Q
from django.core.paginator import Paginator
import sys,os
os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', '..'))
from models import *

from django.core.exceptions import ObjectDoesNotExist

class SearchInDB:
    """This class retrieves results from database based on url input"""
    def __init__(self, key = None):
        self.key = key
        self.timeout = 5000        
    
    def getInputType(self,keyword):
        """Understand what type of keyword has been inserted and returning relevant data.

        :param keyword:
            Type of keyword passed. Can be root, item, category, date. String.

        Return if keyword is root:
            dict with 'root' and 'category' keys.
        Return if keyword is item:
            return root category
        if keyword is category
            return all roots
        if keyword is date
            return all dates
        """
        itemType = {}
        try:
            try:
                e = Trunk.objects.get(root=keyword)
            except:
                try:
                    e = Trunk.objects.filter(root=keyword).distinct()[0]
                except:
                    pass
            itemType['type'] = "root"
            itemType['category'] = e.category
        except Exception as r:
            try: #with keyword = category
                e = Trunk.objects.get(category=keyword)
                itemType['type'] = "category"
            except ObjectDoesNotExist:
                try: #with keyword = item
                    e = Item.objects.filter(Q(name__icontains=keyword) | Q(event_name__icontains=keyword)).distinct().annotate(num_submissions=Count('event__trunk__root')).order_by('-num_submissions')[0]
                    itemType['category'] = e.category
                    itemType['root'] = e.root
                    itemType['type'] = 'item'
                    try:
                        itemType['ner'] = e.ner
                    except:
                        itemType['ner'] = "misc"
                except Exception as e:
                    print("the entry doesn't exist.")
                    try: # if neither of previous 2 video fetcheshas any objects
                        e = Event.objects.filter( Q(videos__description__icontains=keyword) | Q(videos__title__icontains=keyword) | Q(name__icontains=keyword)).annotate(num_submissions=Count('trunk__root')).order_by('-num_submissions').distinct()[0]
                        sys.stderr.write("\n[getInputType] Result from event name: %s\n" % e.trunk.all()[0].category)
                        sys.stderr.flush()
                        itemType['category'] = e.trunk.all()[0].category
                        itemType['root'] = e.trunk.all()[0].root
                        itemType['type'] = 'item'
                        itemType['ner'] = "misc"
                    except ObjectDoesNotExist:
                        print("the entry doesn't exist.")
                        return None
        sys.stderr.write("\n[getInputType] itemType: %s\n" % itemType)
        sys.stderr.flush()
        return itemType
    
    def getDataByWordInYear(self, key = self.key):
        """Select objects from database by item and date
        :param key:
            Has root, date, item, videosPerPage, page keys, dict.

        Return dict with:
        
            * **results** -- Django Video DB objects.
            * **totalpages** -- Total pages of video results after pagination, int.
            * **nextpage** -- Next page number, int.
            * **totalvideos** -- Total videos in search result, int.
        """
        print "[getDataByWordInYear] searchKey: %s" % key
        paginatedResults = {}
        try:
            results = Video.objects.filter(event__trunk__root__icontains=key['root'])
            try:
                results = results.filter(event__date__year=key['date'])
            except Exception as e:
                print "[getDataByWordInYear] Selecting results only by root..."
                pass
            key['item'] = key['item'].rstrip().lstrip()
            try:              
                results = results.filter(event__items__name__icontains=key['item'])
                res = results.count()
                if res == 0:
                    try:
                        results = results.filter(event__name__icontains=key['item'])
                        print "[getDataByWordInYear] Getting videos by team..."
                    except Exception as e:
                        pass
            except Exception as e:
                pass
            try:
                if results.count() == 0:
                    results.filter(event__date__year=key['date'])
            except:
                pass
            try:
                res2 = results.count()
                if res2 == 0:
                    try:
                        results = Video.objects.filter(title__icontains=key['item']).filter(event__trunk__root__icontains=key['root']).filter(event__date__year=key['date']).distinct()
                    except:
                        if results.count() == 0:
                            results = Video.objects.filter(title__icontains=key['item']).filter(event__trunk__root__icontains=key['root']).distinct()
                    print "[getDataByWordInYear] Searching in video titles..."
                    #print "last res: %s" % results.count()
            except Exception as e:
                pass
            results = results.distinct()
        except Exception as e:
            print "[getDataByWordInYear] Error in ROOT query %s " % e
        events = []
        try:
            try:
                itemsPerPage = int(key['videosPerPage'])
            except Exception as e:
                itemsPerPage = 30
                pass            
            numberOfResults = results.count()
            #print "[getDataByWordInYear] number of results: %s" % numberOfResults
            if numberOfResults > itemsPerPage:
                try:
                    page = self.key['page']
                    paginationConfig = {"results": results, "itemsPerPage": itemsPerPage, "page":page}
                except Exception as e:
                    paginationConfig = {"results": results, "itemsPerPage": itemsPerPage}
                    print "[getDataByWordInYear] error: %s" % e
                paginatedResults = self._paginateResults(paginationConfig)
                results = paginatedResults['results']
                print "[getDataByWordInYear] number of results after pagination: %s" % results.count()
            else:
                paginatedResults['totalpages'] = 1
                paginatedResults['nextpage'] = 2
                paginatedResults['totalvideos'] = numberOfResults
            outlistEv = []
            j = []
            for result in results:
                try:
                    try:
                        i = result.event_set.all()[0].items.all()
                        y = result.event_set.all()[0].date

                    except Exception as e:
                        print "[getDataByWordInYear] not working... %s" % e
                    roots = result.event_set.all()[0].trunk.all()
                    outlist = []
                    for s in roots:
                        if s.root in outlist:
                            pass
                        else:
                            outlist.append(s.root)
                    n = result.event_set.all()[0].name.replace('\'','')
                    o = [n, result, y,i]
                    j.append(o)
                    if n not in outlistEv:
                        outlistEv.append(n)
                except Exception as e:
                    pass
            for eventName in outlistEv:
                v = []
                for t in j:
                    if (eventName in t[0]):
                        v.append(t[1])
                        i = t[3]
                        f = t[2]
                e = {'name':eventName,'videos':v, 'items':i, 'roots':outlist, 'date': f}
                #print outlist
                if (key['root'] in outlist):
                     events.append(e)
            self.events = events
            print "[getDataByWordInYear] Retrieved events: %s" % len(events)
            paginatedResults['results'] = events
            return paginatedResults
        except Exception as e:
            print "[getDataByWordInYear] Error: %s" % e
    
    def getAvailableRoots(self, key=self.key):
        """Select available dates from database by item root.

        :param key:
            Has root, date, item, videosPerPage, page keys, dict.
        
        Return dictionary with:
            * **roots** -- Roots matching either date or item.
            * **categories** -- Categories matching either date or item.
        """
        roots = []
        outlist = []
        categories = []
        print "\n[getAvailableRoots] key: %s " % key
        try:
            results = Trunk.objects.filter(date__year=key['date']).distinct()
        except:
            results = Trunk.objects.filter(event__items__name__icontains=key['item']).distinct()

        for result in results:
            if result.root not in outlist:
                outlist.append(result.root)
                roots.append(result.root)
                cat = result.category
                if cat not in categories:
                    categories.append(cat)
                #print self.key['year'],result.root
        roots = sorted(roots, reverse = True)
        rootsMatched = {'roots':roots, 'categories':categories}
        return rootsMatched

    def getYearsByRootOrItem(self, key=self.key):
        """Select all available years for a item or root

        :param key:
            Has root, date, item, videosPerPage, page keys, dict.

        Return dictionary with:

            * **results** -- Django Video DB objects.
            * **totalpages** -- Total pages of video results after pagination, int.
            * **nextpage** -- Next page number, int.
            * **totalvideos** -- Total videos in search result, int.
        """
        availableYears = []
        pag = {}
        try:
            try:
                results = Event.objects.filter(trunk__root = key['root'])
                try:
                    results = results.filter(items__name__icontains = key['item'])
                except Exception as e:
                    pass
                try:
                    results = results.filter(items__name__icontains = key['location'])
                except Exception as e:
                    pass
                try:
                    results = results.filter(items__name__icontains = key['person'])
                except Exception as e:
                    pass
                try:
                    if results.count() == 0:
                        results = Event.objects.filter(Q(videos__title__icontains = key['item']) | Q(name__icontains=key['item']))
                        sys.stderr.write("[getYearsByRootOrItem] Looking in event name or video title: %s for %s" % (results.count(), key['item']))
                        sys.stderr.flush()
                except Exception as e:
                    sys.stderr.write("[getYearsByRootOrItem] Error in getting dates from title or event name: %s" % e)
                    sys.stderr.flush()
                    pass
                results = results.distinct()
            except Exception as e:
                print "Error in getting dates from DB %s" % e
                
            for result in results:
                year = int(str(result.date)[:4])
                if (year not in availableYears) and (year < 2014) and (year > 1899):
                    #print result.date
                    availableYears.append(int(str(result.date)[:4]))
        except Exception as e:
            print "[getYearsByRootOrItem] error: %s" % e
        pag['results'] = sorted(availableYears)
        return pag

    def getItemsByRootOrDate(self, key=self.key):
        """Select all items and the date for which these items are present from database by root

        :param key:
            Has root, date, item, videosPerPage, page keys, dict.

        Return dictionary with:

            * **results** -- Django Video DB objects.
            * **totalpages** -- Total pages of video results after pagination, int.
            * **nextpage** -- Next page number, int.
            * **totalvideos** -- Total videos in search result, int.
        """
        events = []
        pag = {}
        try:
            results = Item.objects.filter(root=key['root']).filter(event__date__year=int(key['date'])).filter(Q(name__icontains='USA') | Q(name__icontains='war') | Q(name__regex = r'.{4}.*')).distinct()
            print "[getItemsByRootOrDate] fetching items by date...."
        except:
            results = Item.objects.filter(root=key['root']).filter(Q(name__icontains='USA') | Q(name__icontains='war') | Q(name__regex = r'.{4}.*')).distinct()
            pass
        g = results.count()
        print "[getItemsByRootOrDate] root %s" % g
        mostFrequent = False
        try:
            itemsPerPage = searchKey['itemsPerPage']            
        except Exception as e:
            itemsPerPage = 100
            pass
        if g > itemsPerPage:
            paginationConfig = {}
            mostFrequent = True
            try:
                paginationConfig['page'] = key['page']
            except:
                paginationConfig['page'] = 1
            paginationConfig['itemsPerPage'] = itemsPerPage
            paginationConfig['results'] = results.order_by('-occurrences')
            pag = self._paginateResults(paginationConfig)
            results = pag['results']

        items = []
        print "[getItemsByRootOrDate] analysing results...."
        for result in results:
                #item = result
               # print "[getItemsByRootOrDate] result2....%s" % result.name
            try:
                if result.name not in items:
                    items.append(result.name)
                    num = result.occurrences
                    print "[getItemsByRootOrDate] result....%s %s" % (result.name,result.ner)
                    try:
                        itemInfo = {'name':result.name, 'type':result.ner, 'num': num, 'date':key['date'], "occ": result.occurrences}
                    except Exception as e:
                        itemInfo = {'name':result.name, 'type':result.ner, 'num': num, "occ": result.occurrences}
                    if "person" not in result.ner:
                        events.append(itemInfo)
                    else:
                        if mostFrequent is True:
                            if num > 4:
                                events.append(itemInfo)
                        else:
                            events.append(itemInfo)
            except Exception as e:
                print "[getItemsByRootOrDate] Error in retrieving related items %s" % e
        try:

            print "[getItemsByRootOrDate] Returning %s items" % len(events)
            events = sorted(events,key=lambda k:k['name'])
            pag['results'] = events
            return pag
        except Exception as e:
            print "[getItemsByRootOrDate] Error in returning itmes %s" % e

    def getAllRoots(self, filter = None, key = self.key):
        """Select all available roots in all years

        :param key:
            Has root, date, item, videosPerPage, page, category keys, dict.


        Return dictionary with:

            * **results** -- List with roots.

        Or based on key request type:

            * **roots** -- List with roots and years.
            * **categories** -- List of categories and roots.
        """
        year_num = 1900
        rootsInYear = []
        categories = []
        outlist = []
        pag = {}
        while year_num <= 2014:
            roots = []
            try:
               results = Trunk.objects.filter(date__year = key['date']).distinct()
               year_num = 2013
            except:
                try:
                    if filter is None:
                        results = Trunk.objects.filter(category = key['category']).distinct()
                    else:
                        results = Trunk.objects.filter(date__year=year_num).distinct()
                except Exception as e:
                    results = Trunk.objects.filter(date__year=year_num).distinct()
            for result in results:
                #print "[retrieveFromDatabase] %s - %s" % (result.date.year, result.root)
                if result.root not in outlist:
                    #print result.category
                    outlist.append(result.root)
                    categories.append({'root':result.root, 'category':result.category, 'date':result.date.year})
                if (year_num == result.date.year):
                        #print "[retrieveFromDatabase] %s %s" % (result.date.year, result.root)
                        roots.append(result.root)
                        roots = sorted(roots, reverse = True)
            if len(roots)>0:
                rootsInYear.append({'date':year_num, 'roots':roots})
            year_num = year_num + 1
        rootsCategoryDict = {'roots':rootsInYear, 'categories':categories}
        if filter is not None:
            #filter is when called from xhr function
            pag['results'] = sorted(roots)
            return pag
        else:
            return rootsCategoryDict

    def _paginateResults(self, paginationConfig):
        """This function paginates results where results.count > 100.

        :param paginationConfig:
            {"results": results, "itemsPerPage": itemsPerPage, "page":page}
            {"results": results, "itemsPerPage": itemsPerPage, "page":page, "isFirstPage": True}

        Return dictionary with:
            {"results": results, "totalpages": p.num_pages, "totalvideos": p.count, "nextpage": nextPage}
        """
        itemsPerPage = int(paginationConfig['itemsPerPage'])
        results = paginationConfig['results']
        try:
            pg = int(paginationConfig['page'])
        except Exception as e:
            pg = 1

        p = Paginator(results, itemsPerPage)
        try:
            pageObject = p.page(pg)
        except Exception as e:
            #get last page
            pageObject = p.page(pg)
        results = pageObject.object_list
        nextPage = pageObject.next_page_number()
        paginatedResults = {"results": results, "totalpages": p.num_pages, "totalvideos": p.count, "nextpage": nextPage}
        return paginatedResults

    def getAllActiveRoots(self):
        """Select all available roots in all years.

        Return dictionary with:
            {"results": roots, "totalpages": p.num_pages, "totalvideos": p.count, "nextpage": nextPage}
        """
        rootInformation = []
        outlist = []
        pag = {}
        try:
            results = Trunk.objects.all().distinct()
            for result in results:
                if result.root not in outlist:
                    outlist.append(result.root)
                    rootInformation.append({'root':result.root, 'category':result.category})
            roots = sorted(outlist, reverse = True)
                #filter is when called from xhr function
            pag['results'] = sorted(roots)
        except Exception as e:
            print "[getAllActiveRoots] Error in getting all active roots: %s" % e
        return pag
                 
    