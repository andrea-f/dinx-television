# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render_to_response
import os
import sys
from django.views.decorators.csrf import csrf_exempt
l = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'db_operations'))
sys.path.append(l)
from retrieveFromDatabase import SearchInDB
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
import socket
from django.template import Library
from random import choice
import traceback
from haystack.views import basic_search
from django.core.paginator import Paginator, InvalidPage
from django.http import Http404
from haystack.forms import ModelSearchForm, FacetedSearchForm
from haystack.query import EmptySearchQuerySet
from django.conf import settings
#TIMEOUT = 2592000
TIMEOUT = 2591918
RESULTS_PER_PAGE = getattr(settings, 'HAYSTACK_SEARCH_RESULTS_PER_PAGE', 20)
from haystack.query import SearchQuerySet
from haystack.views import FacetedSearchView
from haystack.inputs import AutoQuery, Exact, Clean

def _hostNameContext():
    """Check hostname context to set either localhost or domain in links.

    Return string with either localhost or dinx.tv
    """
    if socket.gethostname() == 'legionovainvicta':
        UNIVERSALPATH = 'http://localhost/'
        
    else:
        UNIVERSALPATH = 'http://www.dinx.tv/'
    return UNIVERSALPATH

def _homePathContext():
    """Check to see if other things need to be removed from home path.

    Return string with localhost normalized for Django dev server.
    """
    UNIVERSALPATH = _hostNameContext()
    if 'localhost' in UNIVERSALPATH:
        HOMEPATH = '%s:8000/' % UNIVERSALPATH[:-1]
    else:
        HOMEPATH = UNIVERSALPATH
    return HOMEPATH


#@login_required
def watchHome(request):
    """Display list of available topics if no year has been input.

    :param request:
        HTTP request passed from url.

    Return HTML response with watch_view template.
    """
    #UNIVERSALPATH = _hostNameContext()
    #HOMEPATH = _homePathContext()
    home = 'Dinx Television - Simple access to quality video content'
    channelInfo = _apiEngine(channelRequested = "", viewBy = "root", videosPerPage = 30)
    roots1 = channelInfo['results']
    print "channelinfo: %s" % channelInfo
    for root1 in roots1:
        print "root: %s" % root1
    return render_to_response('watch_view/watch_data.html', locals(), RequestContext(request))    

def ajaxHome(request):
    #t = get_template('home.html')
    """Display list of available topics if no year has been input.
    This handles authenticated AJAX calls.

    :param request:
        HTTP request passed from url.

    Returns AJAX template.
    """
    import json
    UNIVERSALPATH = _hostNameContext()
    HOMEPATH = _homePathContext()
    print "universal path: %s" % UNIVERSALPATH
    home = 'Dinx Television - Simple access to quality video content'
    if request.user.is_authenticated():        
        retrieve = SearchInDB()
        #list of dictionaries with year and associated roots for that year
        roots_category_dict = retrieve.getAllRoots()
        #print roots_category_dict['categories']        
        rootsInYears = []
        outlist = []
        categories = []
        unique_root = []
        #v is a list of dictionaries of <root> and <category>        
        v = roots_category_dict['categories']
        #r is list of {date:date, roots:[roots]}
        r = roots_category_dict['roots']
        for d in v:
            if d['category'] not in categories:
                categories.append(d['category'])
            if d['root'] not in unique_root:
                outlist.append(d)
                outlist = sorted(outlist, reverse = True)
                unique_root.append(d['root'])
                unique_root = sorted(unique_root, reverse = True)
    return render_to_response('videos_ajax_view/home_page.html', locals(), RequestContext(request))


def _getQuestionAndRoot():
    """Get random question with root

    Return string with question and string with root.
    """
    questions = [
        "What is your favorite Serie A team?*"#,
        #"Who is one of your favorite actors?*",
        #"Who do you admire as an historical figure form the XX century?*",
        #"Who is one of your favorite cartoon characers?*",
        #"What is one of your favorite videogames?*"
    ]
    q = choice(questions)
    if q == questions[0]:
        root = "Serie A"
    elif q == questions[1]:
        root = "Film ITA"
    elif q == questions[2]:
        root = "World History"
    elif q == questions[3]:
        root = "Cartoons"
    elif q == questions[4]:
        root = "Videogames"
    return q, root

def videosHome(request):
    """Display list of available topics if no year has been input.
    This handles NOT AJAX and NOT authenticated requests.

    :param request:
        HTTP request passed from url.

    Return if internal request:

        * **outlist** -- List with all unique roots.
        * **categories** -- List with categories.
    Else return:
        HTML response object.
    """
    import json
    outlist = []
    categories = []
    roots = []
    rootsInYears = []
    home = 'Dinx Television - Simple access to quality video content'
    isHome = True
    showSearch = 1
    retrieve = SearchInDB()
    #list of dictionaries with year and associated roots for that year
    roots_category_dict = retrieve.getAllRoots()    
    q, r = _getQuestionAndRoot()   
    #v is a list of dictionaries of <root> and <category>
    v = roots_category_dict['categories']
    #r = roots_category_dict['roots']
    for d in v:
        if d['category'] not in categories:
            categories.append(d['category'])
        if d['root'] not in roots:
            outlist.append(d)
            outlist = sorted(outlist, reverse = True)
            roots.append(d['root'])
    if "internal" in request:
        return outlist,categories
    else:
        return render_to_response('videos_view/videos_data.html', locals(), RequestContext(request))




def about(request):
    """DINX project information.

    :param request:
        HTTP request passed from url.

    Return HTML response object for about page.
    """
    #read files from folder
    UNIVERSALPATH = _hostNameContext()
    aboutPage = True
    print "[about]UNIVERSALPATH: %s" % UNIVERSALPATH
    return render_to_response('about.html', locals(), RequestContext(request))

def _addToCache(currentChannel, events):
    """Add to cache by channel and events.

    :param currentChannel:
        Holds date, category, item, root, videosPerPage regarding current channel being handled, dict.

    :param events:
        Holds events list with information to associate to channels. Information can be date, video info, roots, items.

    Return events list added to cache.

    """
    try:
        s = cache.get('cachedChannels')
        s.append(currentChannel)
        cache.set('cachedChannels',  s, TIMEOUT)
        cache.add(currentChannel,  events, TIMEOUT)
        sys.stderr.write("[addToCache] Caching events for: %s events\n" % len(cache.get(currentChannel)))
        sys.stderr.flush()
    except Exception as e:
        sys.stderr.write("[addToCache] Error in caching events for: %s \nBecause: %s" % (cache.get(currentChannel, e)))
        sys.stderr.flush()
    return events

def _readFromCache(currentChannel):
    """Checks if an element is present in cache, if it is, return events

    :param currentChannel:
        Holds date, category, item, root, videosPerPage regarding current channel being handled, dict.

    Return if channel is found channel information and events associated with channel.
    Else return False.

    """
    print "[readFromCache] currentChannel: %s" % currentChannel
    try:
        cachedEvents = cache.get(currentChannel, 'channelNotFound')
        try:
            sys.stderr.write("[readFromCache] cachedEvents: %s \n" % len(cachedEvents))
            sys.stderr.flush()
        except Exception as e:
            #sys.stderr.write("[readFromCache] Error in cachedEvents: %s , cachedEvents: %s\n" % (e,cachedEvents))
            #sys.stderr.flush()
            pass
    except Exception as e:
        print "[readFromCache] Error in retrieving from cache %s" % e
    if "channelNotFound" not in cachedEvents:
        #print "[readFromCache] %s present in cache" % currentChannel
        sys.stderr.write("[readFromCache] %s present in cache \n" % currentChannel)
        sys.stderr.flush()
        return cachedEvents
    else:
        s = cache.get('cachedChannels', "noCachedChannels")
        sys.stderr.write("[readFromCache] %s NOT present in cache \n" % currentChannel)
        sys.stderr.flush()
        if "noCachedChannels" in str(s):
            cachedChannels = []
            cache.set('cachedChannels',  cachedChannels, TIMEOUT)
            sys.stderr.write("[readFromCache] Setting cached channels" )
            sys.stderr.flush()
        return False

def _checkWord(keyword):
    """Check the inputted word if its a root or item name.

    :param keyword:
        Keyword typed by the user, string.

    Return type of item matching keyword.

    """
    retrieve = SearchInDB()
    try:
        itemType = retrieve.getInputType(keyword)
        sys.stderr.write("[checkWord] itemType %s" % itemType)
        sys.stderr.flush()
    except Exception as e:
        print "[checkWord] error in getting keyword %s" % e

        sys.stderr.write("[checkWord] error in getting keyword %s" % e)
        sys.stderr.flush()
        exc_type, exc_value, exc_traceback = sys.exc_info()
        error = "Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
        sys.stderr.write(error)
        sys.stderr.flush()
    return itemType

def cd(request):
    """Serve crossdomain.xml"""
    r = render_to_response('crossdomain.xml', {'Content-Type':'text/xml'}, RequestContext(request))    
    return r

#@login_required
def api(request, channelRequested):
    """Check if two keys in url belong to date item root category, retrieve data from database accordingly

    :param request:
        HTTP request passed from url.

    :param channelRequested:
        Url separated by slashes of channel to retrieve in the form 'serie_a/lazio/1974', string.
        
    """
    import json
    print "[API] channel Requested: %s" % channelRequested
    jsonFormat = False
    if "json" in channelRequested:
        jsonFormat = True
        channelRequested = channelRequested.replace('json','')
    channelInfo = _apiEngine(channelRequested = channelRequested)
    channel = channelInfo['channel']
    results = channelInfo['results']
    try:
        eventsSerialized = _serialize(results)
    except Exception as e:
        eventsSerialized = results
        "[API] Error in events jsonification: %s" % e
    if jsonFormat is True:
        json_events =  json.dumps(eventsSerialized)
        return HttpResponse( json_events , mimetype="application/json")
    else:
        return render_to_response('api.html', locals(), RequestContext(request))


def _paginateConfig(channelInfo):
    """Set pagination configuration for results

    :param channelInfo:
        All channel information like date etc..., dict.

    Return dict with pagination information only.
    
    """
    channelPagInfo = {}
    try:
        channelPagInfo['totalPages'] = channelInfo['totalPages']
    except:
        pass
    try:
        channelPagInfo['totalVideos'] = channelInfo['totalVideos']
    except:
        pass
    try:
        channelPagInfo['nextPage'] = channelInfo['nextPage']
    except:
        pass
    return channelPagInfo


#@csrf_exempt
#@login_required
def _watchVideos(channelRequested = "dinx.tv/videos/1945/World%20History/"):
    """Channel view of data api.

    :param channelRequested:
        Url separated by slashes of channel to retrieve in the form 'serie_a/lazio/1974', string.

    Return dictionary with:
        * **results** -- Django DB object or list retrieved from database.
        * **channel** -- Channel containing channel name.
        * **roots** -- if 'roots' in option of available views on data, specific type results, list.
        * **dates** -- if 'dates' in option of available views on data, specific type results, list.
        * **items** -- if 'item' in option of available views on data, specific type results, list of Item objects.
        * **liveRoot** -- current root.
        * **liveCategory** -- current category.
        * **liveItem** -- current item name.


    """
    inputURL = channelRequested.split('/')
    channelData = {}
    print "[watchVideos] channel Requested: %s" % channelRequested
    try:
        channelInfo = _apiEngine(channelRequested = channelRequested, videosPerPage = 30)
        channelData['results'] = channelInfo['results']
        channelData['channel'] = channelInfo['channel']
        availableViews = ["date", "all", "item", "root"]
        videosPag = _paginateConfig(channelInfo)
        try:
            for key, value in videosPag.iteritems():
                channelData[key] = value
        except:
            print "[watchVideos] Error in paginating results..."
        try:
            for opt in availableViews:
                channelInfo = _apiEngine(channelRequested = channelRequested, viewBy = opt, videosPerPage = 30)
                if "root" in opt:
                    channelData['roots']= channelInfo['results']
                elif "date" in opt:
                    channelData['dates'] = channelInfo['results']
                elif "item" in opt:
                    channelData['items'] = channelInfo['results']
        except Exception as e:
            print "[videos] Error in fetching %s: %s" % (opt.upper(), e)

        channelData['liveRoot'] = channelData['channel']['root']
        try:
            channelData['liveCategory'] = channel['category']
        except:
            channelData['liveCategory'] = "items"
            pass
        try:
            channelData['liveItem'] = channelInfo['channel']['item']
        except:
            #itemSpecificName = history: people, movies: characters, sports: teams
            itemSpecificName = _getSpecificItemName(channelData['liveCategory'])
            channelData['liveItem'] = "+%s" % itemSpecificName
            pass
        try:
            channelData['liveDate'] = channelInfo['channel']['date']
        except:
            channelData['liveDate'] = "+date"
            pass
        #liveChannel = %s %s %s" % (liveRoot,liveItem,liveDate)
        results = channelData['results']
        try:
            channelData['videoUrlWithOptions'] = results[0]['videos'][0].url
            channelData['videoUrl'] = channelData['videoUrlWithOptions'].split('?')
            channelData['firstVideoUrl'] = channelData['videoUrl'][0]
            channelData['firstVideoName'] = channelData['results'][0]['name']
            #print "VDEO URL: %s NAME: %s" % (firstVideoUrl, firstVideoName)
        except Exception as e:
            sys.stderr.write("[apiEngine] Error in setting videourl: %s" % e)
            sys.stderr.flush()
            pass
    except Exception as e:
        sys.stderr.write("[watchVideos]Error:  \n %s" % (e))
        sys.stderr.flush()
        exc_type, exc_value, exc_traceback = sys.exc_info()
        error = "Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
        sys.stderr.write(error)
        sys.stderr.flush()
        #sys.exit(2)
    return channelData

@login_required
def watch(request, channelRequested):
    """For TV devices.
    Expose channelData to template:

    paginatedResults = {
         "results": results,
         "totalpages": p.num_pages,
         "totalvideos": p.count,
         "nextpage": nextPage
    }

    :param channelRequested:
        Url separated by slashes of channel to retrieve in the form 'serie_a/lazio/1974', string.

    :param request:
        HTTP request passed from url.

    Return HTML response optimized for TV viewing.
    
    """
    URLROOT = "watch"
    channelData = _watchVideos(channelRequested = channelRequested)
    try:
        totalPages = channelData['totalPages']
    except:
        pass
    try:
        totalVideos = channelData['totalVideos']
    except:
        pass
    try:
        nextPage = int(channelData['nextPage'])        
    except:
        pass
    try:
        currentPage = nextPage - 1
    except:
        currentPage = 1
    results = channelData['results']
    channel = channelData['channel']
    roots = channelData['roots']
    dates = channelData['dates']
    items = channelData['items']
    liveRoot = channelData['liveRoot']
    liveCategory = channelData['liveCategory']
    liveDate = channelData['liveDate']
    liveItem = channelData['liveItem']
    videoUrl = channelData['videoUrl']
    firstVideoUrl = channelData['firstVideoUrl']
    firstVideoName = channelData['firstVideoName']
    showNavigation = True
    response = render_to_response('watch_view/watch_data.html', locals(), RequestContext(request))
    return response

register = Library()
@register.filter
def videos(request, channelRequested):
    """For PCs devices.
    Expose channelData to template:
    paginatedResults = {
         "results": results,
         "totalpages": p.num_pages,
         "totalvideos": p.count,
         "nextpage": nextPage
    }

    :param channelRequested:
        Url separated by slashes of channel to retrieve in the form 'serie_a/lazio/1974', string.

    :param request:
        HTTP request passed from url.
        
    """
    URLROOT = "videos"
    channelData = _watchVideos(channelRequested = channelRequested)    
    try:
        currentRoot = request.POST.get('currentRoot')
        currentRoot = "%s/%s" % (URLROOT, currentRoot)
    except:
        pass
    try:
        totalPages = channelData['totalPages']
        print "\n[videos] Total pages: %s" % totalPages
    except:
        pass
    try:
        totalVideos = channelData['totalVideos']
        print "\n[videos] Total videos: %s" % totalVideos
    except:
        pass
    try:
        nextPage = int(channelData['nextPage'])
        print "\n[videos] next page: %s" % nextPage
    except:
        pass
    try:
        currentPage = nextPage - 1
    except:
        currentPage = 1
    try:
        results = channelData['results']
        resPage = True
        query = channelRequested
        latestResults = results#.order_by('-created_at')
        updatedAt = latestResults[0]['videos'][len(latestResults[0]['videos']) - 1].created_at
        channel = channelData['channel']
        roots = channelData['roots']
        dates = channelData['dates']
        items = channelData['items']
        liveRoot = channelData['liveRoot']
        liveCategory = channel['category']
        liveDate = channelData['liveDate']
        liveItem = channelData['liveItem']
        people = []
        locations = []
        misc = []
        genres = []
        languages = []
        for item in items:
            #Type contains ner information
            if 'person' in item['type'].lower():
                people.append(item)
            elif ('location' in item['type'].lower()) and ('sports' not in liveCategory):
                locations.append(item)
            elif ('misc' in item['type'].lower()) and ('sports' not in liveCategory):
                misc.append(item)
            elif 'genre' in item['type'].lower():
                genres.append(item)
            elif 'language' in item['type'].lower():
                languages.append(item)
            else:
                misc.append(item)
            sys.stderr.write("\n[videos] ITEM NAME: %s TYPE %s\n" % (item['name'],item['type']))
            sys.stderr.flush()        
        u = "%s/" % liveRoot
        p = ""
        c = ""
        if "+" not in liveItem:
            i = "%s/" % (liveItem)
            p = p + i
        if "+" not in liveDate:
            d = "%s/" % (liveDate)
            c = c + d
        u = u + p + c
        currentChannelUrl = u
        videoUrl = channelData['videoUrl']
        firstVideoUrl = channelData['firstVideoUrl']
        firstVideoName = channelData['firstVideoName']
        showNavigation = True
        outlist,categories = videosHome("internal")
    except Exception as e:
        err = "No results found."
        sys.stderr.write("\n[videos] Error in fetching results: %s\n" % (e))
        sys.stderr.flush()
    response = render_to_response('videos_view/videos_data.html', locals(), RequestContext(request))
    return response

def _getSpecificItemName(liveCategory):
    """Set specific name based on root. EVENTUALLY picked up from database

    :param liveCategory:
        User chosen category, string.

    Return string with specific name based on category.
    
    """
    specific = "items"
    if "sports" in liveCategory:
        specific = "teams"
    elif "movies" in liveCategory:
        specific = "characters"
    elif "history" in liveCategory:
        specific = "people"
    return specific
    

def _checkOptionInUrl(channelRequested = "/Serie A/lazio/1974/?viewby=item"):
    """Check that inserted option in url exists.

    :param channelRequested:
        Contains user requested url in the form of slash separated options. Not used with Solr, string.

    Return dictionary with:

        * **viewBy** -- Url option to view videos by date, location, person, string.
        * **videosPerPage** -- Number of videos to return per page, int.
        * **itemPage** -- Number of page related to items, int.
        * **channelRequested** -- Pages requested slash separated with requested option removed, string.
    """
    availableViews = ["date", "all", "item", "root"]
    optionDict = {}
    try:
        options = channelRequested.split('?')[1]
        if "&" in options:
            options = options.split('&')
        for option in options:
            if "view=" in option:
                vk = option.split('=')[1]
                if vk in availableViews:
                    optionDict['viewBy'] = vk
            elif "videosperpage=" in option:
                vp = option.split('=')[1]
                if vp.isdigit() and (vp < 150):
                    optionDict['videosPerPage'] = vp
            elif "itempage=" in option:
                ip = option.split('=')[1]
                if ip.isdigit() and (ip < 40):
                    optionDict['itemPage'] = ip
    except Exception as e:
        print "[apiEngine] Error in parsing url options: %s" % e
        pass
    try:
        optionInfo = "?%s" % options
        optionDict['channelRequested'] = channelReqiested.replace(optionInfo,'')
    except:
        pass
    return optionDict

def _apiEngine(channelRequested = "lazio/1974", viewBy = "all", videosPerPage = 15):
    """Converts input text in video channel.

    Calls _createKeys and _getEventsFromDbOrCache to fetch results from either db or cache.

    Format of retrieved results from DB:
    paginatedResults = {
        "results": results,
        "totalpages": p.num_pages,
        "totalvideos": p.count,
        "nextpage": nextPage
    }

    :param channelRequested:
        Contains user requested url in the form of slash separated options, string.

    :param viewBy:
        Url option to view videos by date, location, person, string.

    :param videosPerPage:
        Number of videos to return per page, int.

    Return dictionary with:

        * **results** -- Django DB object or list retrieved from database.
        * **channel** -- Channel information containing: category, item, root, viewBy, videoPage, videosPerPage, itemsPerPage, itemPage.

    If viewBy is 'all' or 'item', add to returned dictionary:
        * **totalPages** -- Total pages in results.
        * **totalVideos** -- Total videos returned from results.
        * **nextPage** -- Next page in results set.
        
    """
    channel = {}    
    channelArray = channelRequested.split('/')
    sys.stderr.write("[apiEngine] channelArray: %s \n channelRequested: %s" % (channelArray, channelRequested.replace('%','%%')))
    sys.stderr.flush()
    for input in channelArray:
        if input.isdigit():
            print "[API] Date input: %s" % input
            if (len(input)==4) and (int(input) > 1900) and (int(input) < 2014):
                channel['date'] = input
                try:
                    itemType['type'] = 'date'
                except:
                    hasDate = True
            else:
                videoPage = input
        else:
            try:
                itemType = _checkWord(input)
                print "[apiEngine] itemType: \n %s" % itemType
            except Exception as e:
                itemType = None
                print "[apiEngine] Error in checking type of input: %s" % e
                sys.stderr.write("[apiEngine] Error in checking type of input: %s" % e)
                sys.stderr.flush()
            sys.stderr.write("\n[apiEngine] itemType : %s \n Input: %s\n" % (itemType, input))
            sys.stderr.flush()
            if itemType is not None:
                if "root" in itemType['type']:
                    root = input
                    category = itemType['category']
                    rootCat = category
                elif "item" in itemType['type']:
                    item = input
                    try:
                        if len(root) == 0:
                            root = itemType['root']
                    except:
                        pass
                    try:
                        category = rootCat
                    except:
                        category = itemType['category']
                elif "category" in itemType['type']:
                    category = input                    
        try:
            print "[apiEngine] %s : %s \n" % (itemType['type'], input)
            sys.stderr.write("\n[apiEngine] %s : %s \n" % (itemType['type'], input))
            sys.stderr.flush()
        except:
            pass
    #channel for API
    try:
        channel['root'] = root
    except:
        try:
            channel['root'] = itemType['root']
        except Exception as e:
            sys.stderr.write("\n[apiEngine] Error in assigning root: %s \n" % (e))
            sys.stderr.flush()
    try:
        channel['category'] = category
    except:
        pass
    try:
        channel['item'] = item
    except:
        pass
    try:
        channel["viewBy"] =  viewBy
    except Exception as e:
        print "[apiEngine] Error in setting viewBy: %s" % e
    try:
        channel["videosPerPage"] = videosPerPage
    except:
        channel["videosPerPage"] = 15
    try:
        channel["videoPage"] = videoPage
    except:
        channel["videoPage"] = 1
    try:
        channel["itemPage"] = itemPage
    except:
        channel["itemPage"] = 1
    print "[API] Channel from input:\n %s" % channel
    try:
        searchKeyInfo = _createKeys(channel)
        events = _getEventsFromDbOrCache(searchKeyInfo)
        results = events['results']           
        print "[API] RESULTS: \n %s" % results
    except Exception as e:
        print "[API] Error in retrieving channel: %s" % e
    try:
        channelInfo = {
            "results": results,
            "channel": channel
        }
    except Exception as e:
        print "[apiEngine] Error in forming channelInfo: %s" % e
    if ("all" in viewBy) or ("item" in viewBy):
        try:
            channelInfo['totalPages'] = events['totalpages']
        except Exception as e:
            sys.stderr.write("[apiEngine] Error in total pages : %s \n" % e)
            sys.stderr.flush()
            pass
        try:
            channelInfo['totalVideos'] = events['totalvideos']
        except:
            pass
        try:
            channelInfo['nextPage'] = events['nextpage']
        except:
            pass        
    return channelInfo

def _serialize(events):
    """Serialize returned data into JSON.

    :param events:
        Set of returned results from DB. Either list or dictionary.

    Return dictionary with JSON encoded events.
    
    """
    try:
        import json
        jsonEvents = []
        outlist = []
        for event in events:
            vids = []
            try:
                for v in event['videos']:
                   jsonVideo = {"name":event['name'], "title": v.title, "image":v.thumbnail, "root":event['roots'][0], "url":v.url, "date":str(event['date'])[:4]}
                   jsonEvents.append(jsonVideo);                   
            except Exception as e:
                try:
                    for i in event['items']:
                        if i.name not in outlist:
                            itms.append(i.name)
                            outlist.append(i.name)
                        else:
                            pass
                    jsonEvent = {"name":event['name'], "videos":vids, "items":itms, "roots":event['roots'], "images":thumbnails, "date":str(event['date'])[:4]}
                    jsonEvents.append(jsonEvent)
                except Exception as e:
                    pass
                    #passing dates
                    jsonEvents = events
        
        ev = {"info":jsonEvents}
        return ev
    except Exception as e:
        print "[serialize] Error in serialization of events: %s" % e

@csrf_exempt
def xhr(request):
    """AJAX request handler. Processes request if it is AJAX and user is authenticated.

    :param request:
        HTTP request passed from url.

    Return HTTP response with JSON mime type containing serialized results from DB.
    
    """
    if request.user.is_authenticated():
        import json
        if request.is_ajax():
            if request.method == 'POST':
                eventsSerialized = {}
                paginationInfo = {}
                sys.stderr.write('[xhr] Raw Data: %s \n' % request.raw_post_data)
                sys.stderr.flush()
                channel = json.loads(request.raw_post_data)
                #Generate search key information
                searchKeyInfo = _createKeys(channel)
                events = _getEventsFromDbOrCache(searchKeyInfo)
                #print type(events)
                try:
                    eventsSerialized = _serialize(events['results'])
                    try:
                        for key, value in events.iteritems():
                            if "results" not in key:
                                #eventsSerialized["pagination"] = events.pop('results')
                                paginationInfo[key] = value
                        eventsSerialized["pagination"] = paginationInfo
                    except Exception as e:
                        eventsSerialized["pagination"] = {}
                        
                except Exception as e:
                    sys.stderr.write("\n[xhr] Error in converting to JSON %s" % e)
                    sys.stderr.flush()
                eventsSerialized["viewBy"] = searchKeyInfo['viewKey']
                json_events =  json.dumps(eventsSerialized)
                return HttpResponse(json_events , mimetype="application/json")

def _getEventsFromDbOrCache(searchKeyInfo):
    """Either get events from cache or retrieve from database.

    :param searchKeyInfo:
        Contains currentChannel(dict), searchKey(dict), viewKey(string), page(int) and videosPerPage(int) requested by user, dictionary.

    Return dictionary with:
        * **results** -- Set of information returned from DB. Either Django DB object or list.
        * **totalPages** -- Total pages in results after pagination, int.
        * **totalVideos** -- Total videos after search, int.
        * **nextPage** -- Next page in results set, int.

    """
    currentChannel = searchKeyInfo['currentChannel']
    events = []
    eventsPag = {}
    try:
        eventsPag = _readFromCache(currentChannel)
    except Exception as e:
        sys.stderr.write("[getEventsFromDbOrCache] Error in reading from CACHE: %s \n" % e)
        sys.stderr.flush()                    
    try:
        if eventsPag is False:
            try:
                eventsPag = _modelInteraction(searchKeyInfo)
                try:
                    eventsT = eventsPag['results']                    
                except Exception as e:
                    try:
                        eventsPag['results'] = eventsPag
                        print "[getEventsFromDbOrCache] error paginationInfo: %s, type: %s" % (e, paginationInfo)
                    except:
                        pass
                
            except Exception as e:
                sys.stderr.write("[getEventsFromDbOrCache] Error in modelInteraction %s \n" % e)
                sys.stderr.flush()
            try:
                events = _addToCache(currentChannel, eventsPag)
                sys.stderr.write("[getEventsFromDbOrCache] Retrieved events after adding to cache: %s \n" % currentChannel)
                sys.stderr.flush()
            except Exception as e:
                sys.stderr.write("[getEventsFromDbOrCache] Error in ADDING to cache: %s \n" % e)
                sys.stderr.flush()
        else:
            sys.stderr.write("[getEventsFromDbOrCache] %s is CACHED with %s events\n" % (currentChannel, len(events)))
            sys.stderr.flush()            
    except Exception as e:
        sys.stderr.write("[getEventsFromDbOrCache] Error in querying the cache %s for %s\n" % (e, currentChannel))
        sys.stderr.flush()
    return eventsPag
    


def _createKeys(channel):
    """Create database and cache search keys.

    :param channel:
        Holds search information specified by user in the format:
        'category': 'movies',
        'itemPage': 1,
        'viewBy': 'all',
        'videosPerPage': 15,
        'videoPage': 1,
        'numberOfVideos': 14,
        'date': '1971',
        'root': 'Cartoons'
        dict.

    Return dictionary with:
        * **currentChannel** -- Information about current channel excluding numerical values, except for date and event name, string.
        * **searchKey** -- Information with what the user requsted, dict.
        * **viewKey** -- How to view the returned data either: 'date', 'all' (videos), 'item', 'root', string.
        * **page** -- Current page of video results.
        * **videosPerPage** -- total number of videos on each page.
        
    """
    channelParametersList = []
    currentChannel = ""
    searchKey = {}
    viewKey = channel['viewBy']    
    sys.stderr.write("\n[createKeys] Channel: \n %s" % channel)
    sys.stderr.flush()
    try:
        if "item" in viewKey:
            if channel['itemPage'] != 0:
                page = str(channel['itemPage'])
            else:
                page = "1"
        else:
            if (channel['videoPage'] == 0):
                page = "1"
            else:
                page = str(channel['videoPage'])
        videosPerPage = str(channel['videosPerPage'])
    except Exception as e:
        page = "1"
        videosPerPage = "15"
        pass
        print "[createKeys] error in page number: %s" % e
    try:        
        searchKey['item'] = channel['item']
        if ('person' not in channel['itemType'] and ('sports' in channel['category'])):
            channel['itemType'] = 'team'
    except Exception as e:
        try:
            channel.pop('itemType')
        except:
            pass
        pass
    for key, value in sorted(channel.iteritems(), reverse=True):
        if value and (value != 0) and ("viewBy" not in key) and ("eventName" not in key) and ("videoPage" not in key) and ("itemPage" not in key) and ("videosPerPage" not in key)and ("numberOfVideos" not in key):
            searchKey[key] = value
    for key in searchKey.keys():
        if ('pageVideos' in key) or ('pageItems' in key) or ('totalVideos' in key) or (len(value)==0):
            searchKey.pop(key)
    sys.stderr.write("[createKeys] searchKey: %s \n" % searchKey)
    sys.stderr.flush()
    for key, value in searchKey.iteritems():
        channelParametersList.append(value)
    try:        
        channelParametersList = sorted(channelParametersList, reverse=True)
        channelParametersList.append(viewKey)
        channelParametersList.append(page)
        currentChannel = '_'.join(str(v) for v in channelParametersList)
        currentChannel = currentChannel.replace(' ', '-')
    except Exception as e:
        print "[createKeys] Error in creating current channel: %s" % e
    searchKeyInfo = {'currentChannel':currentChannel, "searchKey": searchKey, "viewKey": viewKey, "page": page, "videosPerPage": videosPerPage}
    return searchKeyInfo


def _modelInteraction(searchKeyInfo):
    """Handles data retrieval from database.

    :param searchKeyInfo:
        Holds what information to retrieve from database, including metadata, dict.
        Channel information has the following format:
        searchKey:
        {
            'date': '1941',
            'category': 'movies',
            'root': 'Cartoons',
            'page': '1',
            'videosPerPage': '15'
        }
    

    Return dictionary for items, events:
        * **results** -- Django DB object of retrieved videos matching search, dict.
        * **totalpages** Total number of pages in result set, int.
        * **totalvideos** -- Total number of videos, int.
        * **nextpage** -- Next page in results set, int.
    
    Return list with information for dates and roots.
    """
    viewKey =  searchKeyInfo['viewKey'] # What type of information to get. date, all, items, root
    searchKey =  searchKeyInfo['searchKey'] # {} of search parameters
    searchKey['page'] = searchKeyInfo['page'] #Page number to retrieve
    searchKey['videosPerPage'] = searchKeyInfo['videosPerPage'] # Number of videos per returned page
    sys.stderr.write("[modelInteraction] searchKey: %s" % searchKey)
    sys.stderr.flush()
    retrieve = SearchInDB(key = searchKey)
    if ('date' in viewKey):
        sys.stderr.write("[modelInteraction] date is in viewKey getting getYearsByRootOrItem")
        sys.stderr.flush()
        try:
            events = retrieve.getYearsByRootOrItem()
            sys.stderr.write("[modelInteraction] dates: %s" % events)
            sys.stderr.flush()
        except Exception as e:
            sys.stderr.write("[modelInteraction] Error in retrieving by date %s" % e)
            sys.stderr.flush()
    elif ('all' in viewKey):
        try:
            events = retrieve.getDataByWordInYear()
            sys.stderr.write("[modelInteraction] Retrieving by getDataByWordInYear")
            sys.stderr.flush()
            if len(events) == 0:
                    r = searchKey['root']
                    try:
                        d = searchKey['date']
                    except Exception as e:
                        pass
                    searchKey = {}
                    searchKey =  searchKeyInfo['searchKey']
                    searchKey['root'] = r
                    try:
                        searchKey['date'] = d
                    except Exception as e:
                        pass
                    searchKey['page'] = page
                    retrieve = SearchInDB(key = searchKey)
                    events = retrieve.getDataByWordInYear()                                    
        except Exception as e:
                sys.stderr.write("[modelInteraction] Error in searching by all %s" % e)
                sys.stderr.flush()
    elif ('item' in viewKey):
        try:
            events = retrieve.getItemsByRootOrDate()
        except Exception as e:
            sys.stderr.write("[modelInteraction] error in retrieving item %s" % e)
            sys.stderr.flush()
            pass
    elif ('root' in viewKey):
        try:
            roots = 'roots';
            if searchKey['date'] != 0:
                events = retrieve.getAvailableRoots()                
        except:
            try:
                events = retrieve.getAllActiveRoots()
                sys.stderr.write("[modelInteraction] Getting all roots..." )
                sys.stderr.flush()

                roots = 'results'
            except Exception as e:
                sys.stderr.write("[modelInteraction] error in retrieving roots %s" % e)
                sys.stderr.flush()
                pass
        events = {'results': events[roots]}        
    return events

def customSearch(request):
    """Generates a Haystack response with context of search

    :param request:
        HTTP request passed from url.

    Return HTML response with requested data both from Solr and Dinx API.
    Dinx results are a dictionary with:
        * **people** -- Names of people retrieved from search, list.
        * **locations** -- Locations retrieved from search, list.
        * **misc** -- Not people or locations retrieved from search, list.
        * **genres** -- Genres retrieved from search, only applies to Movies, list.
        * **updatedAt** -- Timestamp of last added video to database, date field.
        * **resultsInt** -- Complete set of returned results from Dinx API, each result is a Django db object, list.
        * **currentUrl** -- User current viewing channel.

    """
    channelRequested = ""
    currentUrl = '/trova/?'
    try:
        opts = ['root','date','location','misc','genre','q','page']
        for opt in opts:
            try:
                if request.GET[opt]:
                    r = request.GET.get(opt)
                    r = r.strip()
                    if r is not None:
                        channelRequested = u"%s/%s" % (channelRequested,r)
                        if "page" not in opt:
                            currentUrl = u'%s%s=%s&' % (currentUrl, opt, request.GET.get(opt))
            except:
                pass
        channelRequested = channelRequested.rstrip('/')
        sys.stderr.write("[customSearch] channelRequested: %s" % channelRequested)
        sys.stderr.flush()
        channelData = _watchVideos(channelRequested = channelRequested)
        try:
            channelData['category'] = channelData['channel']['category']
        except:
            pass
        channelData['resultsInt'] = channelData['results']
        sys.stderr.write("[customSearch] resultsInt, no solr: %s" % len(channelData['resultsInt']))
        sys.stderr.flush()
        people = []
        locations = []
        misc = []
        genres = []
        languages = []
        try:
            for item in channelData['items']:
                #Type contains ner information
                sys.stderr.write("\n[customSearch] %s: %s" % (item, item['type']))
                sys.stderr.flush()
                if 'person' in item['type'].lower():
                    people.append(item)
                elif ('location' in item['type'].lower()) and ('sports' not in channelData['category']):
                    locations.append(item)
                elif ('misc' in item['type'].lower()) and ('sports' not in channelData['category']):
                    misc.append(item)
                elif 'genre' in item['type'].lower():
                    genres.append(item)                    
                elif 'language' in item['type'].lower():
                    languages.append(item)
                else:
                    misc.append(item)
        except:
            pass
        channelData['people'] = people
        channelData['locations'] = locations
        channelData['misc'] = misc
        channelData['genres'] = genres
        try:
            channelData['updatedAt'] = channelData['results'][0]['videos'][len(channelData['results'][0]['videos']) - 1].created_at
        except:
            pass
        channelData['currentUrl'] = currentUrl
    except Exception as e:
        channelData = {}
        sys.stderr.write("[customSearch] Error: %s" % e )
        exc_type, exc_value, exc_traceback = sys.exc_info()
        error = "[customSearchQuery] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
        sys.stderr.write(error)
        sys.stderr.flush()
    return customSearchQuery(request, extra_context=channelData, resultsInt = channelData['resultsInt'])

def faq(request):
    """Returns faq document.

    :param request:
        HTTP request passed from url.

    Return HTML response of FAQ page.

    """
    aboutPage = True
    faqPage = True
    return render_to_response('faq.html', locals(), RequestContext(request))

def customSearchQuery(request, resultsInt = None, template='search/search.html', load_all=True, form_class=ModelSearchForm, searchqueryset=None, context_class=RequestContext, extra_context=None, results_per_page=None):
    """
    A more traditional view that also demonstrate an alternative
    way to use Haystack.

    Useful as an example of for basing heavily custom views off of.

    Also has the benefit of thread-safety, which the ``SearchView`` class may
    not be.

    :param template:
        ``search/search.html`` html page with information from search, string.
        
    :param context_class:
        * form
          An instance of the ``form_class``. (default: ``ModelSearchForm``)
        * page
          The current page of search results.
        * paginator
          A paginator instance for the results.
        * query
          The query received by the form.

    :param resultsInt:
        Complete set of returned results from Dinx API, each result is a Django db object, list.

    :param load_all:
        Haystack and Solr specific way to do a broad search, boolean.

    :param form_class:
        Type of form class to use with Haystack, default is ModelSearchForm, class.

    :param searchqueryset:
        Search parameters to pass to Haystack for Solr searching, Queryset.

    :param extra_context:
        Extra information to associate to Solr search, dict.

    :param results_per_page:
        Total number of results on each page from Solr, int.

    Return HTML response template with results from search and dictionary with:
        * **form** -- form used in the search, class.
        * **page** -- Number of page in results, int.
        * **paginator** -- Contains information about resutls pagination, dict.
        * **query** -- User requested keyword for videos, string.
        * **suggestion** -- Suggested related search NOT USED, list.
        * **outlist** -- All unique roots, list.
        * **categories** -- All unique cateogries, list.

    """
    query = ""
    item = ""
    results = EmptySearchQuerySet()
    #sqs = SearchQuerySet().facet('author')
    #sqs = SearchQuerySet().filter(date=date)
    try:
        sqs =  SearchQuerySet()#.auto_query(self.cleaned_data['q'])
        if request.GET.get('root'):
            sqs = sqs.filter(root=request.GET['root'])
            query = "%s %s" % (query, request.GET['root'])
        if request.GET.get('location'):
            item = request.GET.get('location')
            itemType = 'location'
        if request.GET.get('genre'):
            item = request.GET.get('genre')
            itemType = 'genre'
        if len(item)>0:
            sqs = sqs.filter(description=item)
            sqs = sqs.filter(title=item)
            sqs = sqs.filter(items=item)
            #sqs = sqs.filter(ner=itemType)
            query = "%s %s" % (query, item)
        if request.GET.get('q'):
            #sqs = sqs.filter(content=AutoQuery(request.GET['q']))
            sqs = sqs.auto_query(request.GET.get('q'))
            query = "%s %s" % (query, request.GET['q'])
        if request.GET.get('date'):
            sqs = sqs.filter(description=request.GET['date'])
            sqs = sqs.filter(title=request.GET['date'])
            #sqs = sqs.filter(date=Exact(request.GET['date']))
            sqs = sqs.filter(date=request.GET['date'])
            query = "%s %s" % (query, request.GET['date'])
            #sqs = SearchQuerySet().facet('date')
            #FacetedSearchView(form_class=FacetedSearchForm, searchqueryset=sqs)
        searchqueryset = sqs
        form = form_class(request.GET, searchqueryset=searchqueryset, load_all=load_all)
        results = sqs.load_all()
        resList = []
        results1 = results
        try:
            if resultsInt is not None:
                totalVideos = len(resultsInt) + len(results)
                sys.stderr.write("\n[customSearchQuery] totalVideos: %s" % totalVideos)
                sys.stderr.flush()
        except Exception as e:
            pass
    except Exception as e:
        sys.stderr.write("[customSearchQuery] Error: %s" % e )
        sys.stderr.flush()
        exc_type, exc_value, exc_traceback = sys.exc_info()
        error = "[customSearchQuery] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
        sys.stderr.write(error)
        sys.stderr.flush()
        #sys.exit(2)
        results = []
        pass
    #res = FacetedSearchView(form_class=FacetedSearchForm, searchqueryset=sqs, name='haystack_search')
    paginator = Paginator(results, results_per_page or RESULTS_PER_PAGE)
    try:
        paginatorNoSolr = Paginator(resultsInt, results_per_page or RESULTS_PER_PAGE)
    except Exception as e:
        sys.stderr.write("[customSearchQuery] Error in paginatinating no Solr results: %s\n" % e )
        sys.stderr.flush()
    currentPage = 1
    try:
        if request.GET.get('page'):
            currentPage = int(request.GET['page'])
    except:
        pass
    try:
        page = paginator.page(currentPage)
    except InvalidPage:
        try:
            page = paginatorNoSolr.page(currentPage)
        except InvalidPage:
            #raise Http404("No such page of results!")
            page = {}            
    #outlist is unique list of roots by category,
    #categories is unique list of categories
    try:
        outlist,categories = videosHome("internal")
    except:
        pass
    context = {
        'form': form,
        'page': page,
        'paginator': paginator,
        'query': query,
        'suggestion': None,
        'outlist': outlist,
        'categories': categories
    }
    if results1.query.backend.include_spelling:
        context['suggestion'] = form.get_suggestion()
    if extra_context:
        context.update(extra_context)
    return render_to_response(template, context, context_instance=context_class(request))

