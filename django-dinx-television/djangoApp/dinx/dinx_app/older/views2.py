# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render_to_response
import os,sys,random
from django.views.decorators.csrf import csrf_exempt

#lib_path = os.path.abspath('../')
#sys.path.append(lib_path)
from retrieveFromDatabase import SearchInDB
from django.template import RequestContext
from django.contrib.auth.decorators import login_required


def hello(request):
    return HttpResponse("Hello world")

@login_required
def home(request):
    #t = get_template('home.html')
    """Display list of available topics if no year has been input"""
    
    if request.user.is_authenticated():
        home = 'Dinx Television'
        retrieve = SearchInDB()
        #list of dictionaries with year and associated roots for that year
        roots_category_dict = retrieve.getAllRoots()
        #print roots_category_dict['categories']
        rootsInYears = []
        outlist = []
        categories = []
        unique_root = []
        #v is a list of dictionaries of <root> and <category>
        v = roots_category_dict['categories']
        r = roots_category_dict['roots']
        for d in v:
            
            if d['category'] not in categories:
                categories.append(d['category'])
            if d['root'] not in unique_root:
                outlist.append(d)
                unique_root.append(d['root'])
        #foo = ['a', 'b', 'c', 'd', 'e']
        #print random.choice(foo)


        #for a in roots_category_dict['roots']:
        #    for b in a['roots']:
        #        if b not in outlist:
        #            print b
        #            if b in 
        #                c = {'name':b, 'category':d}
        #                outlist.append(c)
        #    rootsInYears.append(a)            
        #for a in rootsInYears:
        #    print a
        return render_to_response('all_content.html', locals(), RequestContext(request))

@login_required
def defaultYear(request,year):
    """Returns a list of events name by year"""
    if request.user.is_authenticated():
        import json
        key = {}
        key['year'] = year

        retrieve = SearchInDB(key = key)
        #event_names_by_year = retrieve.getDataByYear()
        #events = retrieve.getDataByYear() 
        available_dates = retrieve.getAvailableDates()
        roots = retrieve.getAvailableRoots()
        #init == 1
        #jsonEvents = serialize(events) 
    
        #jsonEvents = json.dumps(jsonEvents)
        return render_to_response('events_by_year.html',locals(), RequestContext(request))

@login_required
def item_in_root_in_year(request, year, item, root):
    """get events by year item and root """
    if request.user.is_authenticated():
        key = {}
        import json 
        key['year'] = year
        key['word'] = item
        key['root'] = root
        try:
            retrieve = SearchInDB(key = key)
            events = retrieve.getEventRelatedToItem()
        except Exception as e:
            print e
        print events

@login_required
def yearItem(request, year, item):
    """Search by a year and an item - can be root or not"""
    if request.user.is_authenticated():
        import json
        key = {}
        key['year'] = year
        key['word'] = item
        print year,item
        try:
            retrieve = SearchInDB(key = key)
            #roots = retrieve.getAvailableRoots()
            events = retrieve.getDataByWordInYear()  
            roots = retrieve.getAvailableRoots()  
            available_dates = retrieve.getAvailableDates()   

        except Exception as e:
            print "Error in yearItem %s" % e
            return render_to_response(e,locals(), RequestContext(request))
     
        outlist = [] 
        for event in events:
            items = []
            for i in event['items']:
                if i.name not in outlist:
                    json_items = i.name
                    items.append(json_items)
                    outlist.append(i.name) 
                else:
                    pass
    

       
        jsonEvents = serialize(events) 
        jsonEvents = json.dumps(jsonEvents)
        return render_to_response('events_by_year.html',locals(), RequestContext(request))

def serialize(events):
    """Serialize returned data into json"""
    try:
        import json
        json_events = []
        outlist = []
        outlist1 = []
        for event in events:
            vids = []
            try:
                for v in event['videos']:
                    json_video = v.url
                    vids.append(json_video)
                itms = []
        
                for i in event['items']:
                    if i.name not in outlist:
                        json_items = i.name
                        itms.append(json_items)
                        outlist.append(i.name) 
                    else:
                        pass
                json_event = {"name":event['name'], "videos":vids, "items":itms, "roots":event['roots']}
                json_events.append(json_event)
            except:
                pass
                json_events = events    
        ev = {"info":json_events}
        return ev 
    except Exception as e:
        print "Error in serialization of events"

@csrf_exempt
def xhr(request):
    """AJAX request handler"""
    if request.user.is_authenticated():
        import json
        if request.is_ajax():
        
            if request.method == 'POST':
                print 'Raw Data: %s' % request.raw_post_data
                transformed_data = json.loads(request.raw_post_data)
                #SHOULD COME here considering the key inserted self.availableDates()
                #print transformed_data['year']
                k = {}
                field = transformed_data['field']
                k['year'] = transformed_data['date']
                k['word'] = transformed_data['item']
                retrieve = SearchInDB(key = k) 
                if ('item' in field):
                    try:
                        if k['word'] and k['year']:
                            events = retrieve.getEventRelatedToItem()
                        else:
                            events = retrieve.getDataByYear() 
                            print str(events)[:1000]                
                    except Exception as e:
                        print e    
                elif ('year' in field):
                    try:
                        if transformed_data['item']:
                            k['root_item'] = transformed_data['item']
                            events = retrieve.getYearsByRoot()

                    except Exception as e:
                        print "Error in getYearsByRoot %s " % e 
                        pass
                    
                    if k['year'] != 0:
                        try:
                            print k['word'] 
                            events = retrieve.getDataByWordInYear()
                        except Exception as e:
                            print "error in processing keyword %s " % e
                            try:
                                events = retrieve.getDataByYear() 
                            except Exception as e:
                                print e
                            pass
                print "here"
                try:
                    events = serialize(events)
                    print events
                except Exception as e:    
                    print e
     
                return HttpResponse( json.dumps( events ), mimetype="application/json")

@login_required
def availableDates(self):
    """Select available dates from database by item root"""
    if request.user.is_authenticated():
        retrieve = SearchInDB()
        available_dates = retrieve.getAvailableDates()
        return available_dates
