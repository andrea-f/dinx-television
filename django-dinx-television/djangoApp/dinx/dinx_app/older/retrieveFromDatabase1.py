#!/usr/local/bin/python
# coding: utf-8

from django.http import HttpResponse
from django.shortcuts import render_to_response
from models import *

class SearchInDB:
    """This class retrieves results from database based on url input"""
    def __init__(self, key = None):
        self.key = key
        
    
    def getDataByYear(self):
        """Select objects from database"""
        results = Event.objects.filter(date__year=self.key['year']).distinct()	
        events = []
        
        for result in results:
            v = result.videos.all()
            i = result.items.all()
            r = result.path.all()
            
            outlist = []
            for s in r:
                if s.root in outlist:
                    pass
                else:
                    outlist.append(s.root)
            try:
                if self.key['word']: 
                    for i in outlist:
                        if self.key['word'] in i:
                            e = {'name':result.name,'videos':v, 'items':i, 'roots':outlist}
                            events.append(e)
            except Exception as e:
                j = {'name':result.name,'videos':v, 'items':i, 'roots':outlist}
                events.append(j)
                print e
            #print i.all()
            #for f in v:
            #    print f.url
        self.events = events
        return events
    
    def getDataByWordInYear(self):
        """Select objects from database by item and date"""
        try: 
            results = Event.objects.filter(date__year=self.key['year']).filter(path__root__icontains=self.key['word']).distinct()
            res = results.count()
            if res == 0:
                results = Event.objects.filter(date__year=self.key['year']).filter(name__icontains=self.key['word']).distinct()
            else:
                pass     
            
        except Exception as e:
            print "Error in ROOT query %s " % e
            try:
                results = Event.objects.filter(date__year=self.key['year']).filter(name__icontains=self.key['word']).distinct()
            except Exception as e:
                print "Error in running query %s " % e
        events = []
        for result in results:
            v = result.videos.all()
            i = result.items.all()
            r = result.path.all()
            outlist = []
            for s in r:
                if s.root in outlist:
                    pass
                else:
                    outlist.append(s.root)
                    print s.root
                    print result.name
            result.name = result.name.replace('\'','')
            e = {'name':result.name,'videos':v, 'items':i, 'roots':outlist}
            if (self.key['word'] in result.name) or (self.key['word'] in outlist[0]):
                events.append(e)
                #print i.all()
            #for f in v:
            #    print f.url
        self.events = events
        return events
    
    def getAvailableRoots(self):
        """Select available dates from database by item root"""
        roots = []
        outlist = []
        results = RootsYear.objects.filter(date__year=self.key['year']).distinct()
        for result in results:
            if result.root not in outlist:
                outlist.append(result.root)
                roots.append(result.root)
                print self.key['year'],result.root
        return roots
            
    
    def getAvailableDates(self):
        """Return available dates in DB by subject ex: serie A"""
        year_num = 1900
        available_years = []
        while year_num<=2012:
            try:
                yearlist = Event.objects.filter(date__year=year_num)[0]
            except IndexError:
                print "year not present"
            else:
                only_year=str(yearlist.date)
                only_year=only_year[:4]
                available_years.append(only_year)
            year_num=year_num+1
        return available_years
        
    def getAllRoots(self):
        """Select all available roots in all years"""
        year_num = 1900
        rootsInYear = []
        categories = []	
        while year_num <= 2012:
            outlist = []
            roots = []
            
            try:
                results = RootsYear.objects.filter(date__year=year_num).distinct()
                for result in results:
                    if result.root not in outlist:
                        outlist.append(result.root)
                        roots.append(result.root)
                        print year_num,result.root
                    categories.append({'root':result.root, 'category':result.category})
                if len(roots)>0:
                    rootsInYear.append({'year':year_num, 'roots':roots})    
            except Exception as e:
                print e
            year_num = year_num + 1
        
        roots_category_dict = {'roots':rootsInYear, 'categories':categories}
    
        return roots_category_dict

    def getRelatedToDate(self):
        """Return available related fields for date: Serie B, Football in England"""
