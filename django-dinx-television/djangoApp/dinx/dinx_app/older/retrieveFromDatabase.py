#!/usr/local/bin/python
# coding: utf-8

from django.http import HttpResponse
from django.shortcuts import render_to_response
from models import *

class SearchInDB:
    """This class retrieves results from database based on url input"""
    def __init__(self, key = None):
        self.key = key
        
    
    def getEventsByYear(self):
        """Select objects from database"""
        results = Event.objects.filter(date__year=self.key['year']).distinct()	
        events = []
        
        for result in results:
            v = result.videos.all()
            i = result.items.all()
            r = result.path.all()
            
            outlist = []
            for s in r:
                if s.root in outlist:
                    pass
                else:
                    outlist.append(s.root)
            try:
                if self.key['word']: 
                    for i in outlist:
                        if self.key['word'] in i:
                            e = {'name':result.name,'videos':v, 'items':i, 'roots':outlist}
                            events.append(e)
            except Exception as e:
                j = {'name':result.name,'videos':v, 'items':i, 'roots':outlist}
                events.append(j)
                print e
            #print i.all()
            #for f in v:
            #    print f.url
        self.events = events
        return events
    
    def getDataByWordInYear(self):
        """Select objects from database by item and date"""
        try: 
            results = Event.objects.filter(date__year=self.key['year']).filter(path__root__icontains=self.key['word']).distinct()
            res = results.count()
            if res == 0:
                results = Event.objects.filter(date__year=self.key['year']).filter(item__name__icontains=self.key['word']).distinct()
            else:
                pass     
            
        except Exception as e:
            print "Error in ROOT query %s " % e
            try:
                results = Event.objects.filter(date__year=self.key['year']).filter(name__icontains=self.key['word']).distinct()
            except Exception as e:
                results = Event.objects.filter(path__name__icontains=self.key['word']).distinct()
                print "Error in running query %s " % e
        events = []
        for result in results:
            v = result.videos.all()
            i = result.items.all()
            roots = result.path.all()
            outlist = []
            for s in roots:
                if s.root in outlist:
                    pass
                else:
                    outlist.append(s.root)
                    print s.root
                    print result.name
            result.name = result.name.replace('\'','')
            e = {'name':result.name,'videos':v, 'items':i, 'roots':outlist}
            if (self.key['word'] in result.name) or (self.key['word'] in outlist[0]):
                events.append(e)
                #print i.all()
            #for f in v:
            #    print f.url
        self.events = events
        return events
    
    def getAvailableRoots(self):
        """Select available dates from database by item root"""
        roots = []
        outlist = []
        results = RootsYear.objects.filter(date__year=self.key['year']).distinct()
        for result in results:
            if result.root not in outlist:
                outlist.append(result.root)
                roots.append(result.root)
                print self.key['year'],result.root
        return roots
            
    
    def getAvailableDates(self):
        """Return available dates in DB by subject ex: serie A"""
        year_num = 1900
        available_years = []
        while year_num<=2012:
            try:
                yearlist = Event.objects.filter(date__year=year_num)[0]
            except IndexError:
                print "year not present"
            else:
                only_year=str(yearlist.date)
                only_year=only_year[:4]
                available_years.append(only_year)
            year_num=year_num+1
        return available_years

    def getYearsByRootOrItem(self):
        """Select all available years for a item or root"""
        #self.key['root_item']
        av_years = []
        try:
            try:
                results = RootsYear.objects.filter(root = self.key['word'])
                if results.count() == 0:
                    results = Event.objects.filter(path__root = self.key['root']).filter(items__name = self.key['word']).distinct()

            except Exception as e:
                print e 
#            specific = result.path.all()
#            for s in specific:
                
            for result in results:
                if int(str(result.date)[:4]) not in av_years:
                   
                    av_years.append(int(str(result.date)[:4]))
        except Exception as e:
            print e

        return sorted(av_years)
    
    def getAllRoots(self):
        """Select all available roots in all years"""
        year_num = 1900
        rootsInYear = []
        categories = []	
        while year_num <= 2012:
            outlist = []
            roots = []
            
            try:
                results = RootsYear.objects.filter(date__year=year_num).distinct()
                for result in results:
                    if result.root not in outlist:
                        
                        outlist.append(result.root)
                        roots.append(result.root)
                        print year_num,result.root
                    categories.append({'root':result.root, 'category':result.category})
                if len(roots)>0:
                    rootsInYear.append({'year':year_num, 'roots':roots})    
            except Exception as e:
                print e
            year_num = year_num + 1
        
        roots_category_dict = {'roots':rootsInYear, 'categories':categories}
    
        return roots_category_dict

    def getEventRelatedToItem(self):
        """Return available related fields for item"""
        """movies->person or location->video"""
        print self.key
        try:
            try:
                if self.key['year'] and self.key['root']: 
                    results = Item.objects.filter(event__path__root=self.key['root']).filter(event__name__icontains=self.key['word']).filter(event__date__year=self.key['year']).distinct()  
            except Exception as e:
                pass

            if self.key['year'] and self.key['word']: 
                results = Event.objects.filter(items__name__icontains=self.key['word']).filter(date__year=self.key['year']).distinct()
  
            elif self.key['root'] and self.key['word']:
                results = Event.objects.filter(path__root=self.key['root']).filter(items__name__icontains=self.key['word']).distinct()                 
            else:
                results = Item.objects.filter(event__name__icontains=self.key['word']).distinct()  
            #print results  
            
        except Exception as e:
            print "Error in getRelatedToItem query %s " % e

        events = []
        outlist = []
        for result in results:
            if result.name not in outlist:
                v = result.videos.all()
                i = result.items.all()
                r = result.path.all()
                thumbs = []
                for vi in v:
                    thumbs.append(vi.thumbnail)
                outlist1 = []
                for s in r:
                    if s.root in outlist1:
                        pass
                    else:
                        outlist1.append(s.root)

                event = {'name': result.name,'videos': v,'items':i, 'roots':outlist1}
                events.append(event)
                outlist.append(result.name)

        

        return events


    def getItemsByRootOrDate(self):
        """Select all items and the date for which these items are present from database by root"""
        try:
            if self.key['year']:
                results = Item.objects.filter(event__path__root__contains=self.key['word']).filter(event__date__year=self.key['year']).distinct()
            #genre = root
            #root = RootsYear.objects.get(root__icontains = self.key['word'])
                scope = True
            
        except Exception as e:
            results = Item.objects.filter(event__path__root__icontains=self.key['word']).distinct()
            #print "Error in running query %s " % e
            scope = False
            pass
        events = []
        dates = []
        items = []
        for result in results:
            
            #i = result.items.all()
                item = result
            #for item in i:
                if item.name not in items:
                    #print item.name
                    items.append(item.name)
                    #if str(result.date)[:4] not in dates:
                    #    dates.append(str(result.date)[:4])
                    #, 'date':dates
                    num = 1
                    item_and_dates = {'name':item.name, 'ner':item.ner, 'num': num}
                    events.append(item_and_dates)
                else:
                    for event in events:
                        if item.name in event['name']:
                            event['num'] = event['num'] + 1
                        
        events = sorted(events,key=lambda k:k['name'])        
        high_events = []
        for event in events:
            if int(event['num']) > 21:
                high_events.append(event)
            
        if  'history' in self.key['word']:
            scope = True
        self.events = high_events
        if scope is True: 
            print events
            return events
        else:
            if len(high_events) == 0:
                return events
            else:
                return high_events

