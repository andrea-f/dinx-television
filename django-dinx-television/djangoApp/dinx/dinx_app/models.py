from django.contrib.auth.models import User
from django.db import models
from videoDB import *
# Create your models here.

class Created(models.Model):
    created_at = models.DateTimeField(auto_now_add = True, db_index=True)
    updated_at = models.DateTimeField(auto_now = True)
    class Meta:
        abstract = True

class RedditManager(models.Manager):
    """Returns Person from items"""

    def get_query_set(self):
        return super(RedditManager, self).get_query_set().filter(title__icontains = '(').filter(title__icontains = ')').filter(title__icontains = '19')

    def __unicode__(self):
        return self.name

class Video(Created):
    url = models.URLField(db_index=True, unique = True)
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    thumbnail = models.CharField(max_length=150)
    duration = models.IntegerField(db_index=True)
    viewcount = models.IntegerField()
    keywords = models.TextField(blank=True, null=True)
    score = models.IntegerField(null=True)
    root = models.CharField(max_length=150)

    def __unicode__(self):
        return self.title
    def __unicode__(self):
        return self.description

    class Meta:
        ordering = ('viewcount','title',)

class PersonManager(models.Manager):
    """Returns Person from items"""
    
    def get_query_set(self):
        return super(PersonManager, self).get_query_set().filter(ner='PERSON')

    def __unicode__(self):
        return self.name

class LocationManager(models.Manager):
    """Returns Locations from items"""
    def get_query_set(self):
        return super(LocationManager, self).get_query_set().filter(ner='LOCATION')

    def __unicode__(self):
        return self.name

class MiscManager(models.Manager):
    """Returns Locations from items"""
    def get_query_set(self):
        return super(MiscManager, self).get_query_set().filter(ner='ORG').filter(ner='MISC').filter(ner='LOCATION')

    def __unicode__(self):
        return self.name



class MostFrequent(models.Manager):
    """Returns Locations from items"""
    def get_query_set(self):
        return super(MostFrequent, self).get_query_set().filter(occurrences=2)

    def __unicode__(self):
        return self.name



class Item(Created):
    name = models.CharField(max_length=200,db_index=True)
    ner = models.CharField(max_length=200,db_index=True)
    category = models.CharField(max_length=200,db_index=True)
    #genre = models.CharField(max_length=200,null=True,blank=True)
    root = models.CharField(max_length=150)
    occurrences = models.PositiveIntegerField(db_index = True)
    objects = models.Manager()
    people = PersonManager()
    locations = LocationManager()
    misc = MiscManager()
    most_frequent = MostFrequent()


    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('name','occurrences',)


class Trunk(models.Model):
    helper_words = models.CharField(max_length=200, null=True,blank=True)
    root = models.CharField(max_length=200, db_index= True)
    date = models.DateField(db_index=True) 
    category = models.CharField(max_length=150)
    subroot = models.CharField(max_length=200, null=True,blank=True)
    def __unicode__(self):
        return self.root

    class Meta:
        ordering = ('root',)

class Event(Created):
    name = models.CharField(max_length=200,db_index=True)
    trunk = models.ManyToManyField(Trunk)
    date = models.DateField(db_index=True)
    videos = models.ManyToManyField(Video)
    items = models.ManyToManyField(Item)
    #helper_words = models.ManyToManyField()
    occurrences = models.PositiveIntegerField(db_index = True, null=True,blank=True)
    normalized = models.DateField(null = True, blank = True)
    objects = models.Manager()
    reddit = RedditManager()
    def __unicode__(self):
        return self.name
   
    class Meta:
        #ordering = ('name','date','occurrences',)
        ordering = ('name','occurrences',)
    

class Channel(models.Model):
    url = models.URLField(db_index = True)
    date = models.DateField(db_index=True)
    item = models.CharField(max_length=200)
    root = models.ForeignKey(Trunk)
    resutltnum = models.PositiveIntegerField(db_index=True)
    event = models.CharField(max_length=200)
    views = models.PositiveIntegerField(db_index=True)
    category = models.CharField(max_length=200)

    class Meta:
        ordering = ('views','-date',)

    def __unicode__(self):
        return self.name

class UserWatcgHistory(Channel):
    username = models.CharField(max_length=200)
    channel = models.ManyToManyField(Channel, related_name='user_watched_channel')
    watchedAt = models.DateField(db_index=True)
    watchedVideoId = models.PositiveIntegerField(db_index=True)

    class Meta:
        ordering = ('-watchedAt',)

    def __unicode__(self):
        return self.name