from django.contrib import admin

try:
    from dinx_app.models import Item,Video,Event,Trunk
except:
    from models import Item,Video,Event,Trunk

admin.site.register(Item)
admin.site.register(Video)
admin.site.register(Event)
admin.site.register(Trunk)
