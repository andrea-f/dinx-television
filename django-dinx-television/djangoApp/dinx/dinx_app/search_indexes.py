import datetime
from haystack import indexes
from models import Video, Item


class VideoIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title', faceted=True)
    root = indexes.CharField(model_attr='root', faceted=True)
    url = indexes.CharField(model_attr='url', faceted=True)
    description = indexes.CharField(model_attr='description', faceted=True)
    thumbnail = indexes.CharField(model_attr='thumbnail', faceted=True)
    duration = indexes.IntegerField(model_attr='duration', faceted=True)
    items = indexes.MultiValueField(faceted=True)
    name = indexes.MultiValueField(faceted=True)
    date = indexes.MultiValueField(faceted=True)
    

    def prepare_date(self, obj):
        """ Prepare the attributes for any file attachments on the
         current page as specified in the M2M relationship.

        :param obj:
            Video object from database to retrieve date from.

        Return int with date.
        """
        #attributes = obj.trunk.all()
        #date = obj.event_set.all()[0].date
        date = 0
        try:
            date = obj.event_set.all()[0].date
            print date
        except:
            print "no date"
        return date

    def prepare_items(self, obj):
        """ Prepare the attributes for any file attachments on the
        current page as specified in the M2M relationship.

        :param obj:
            Video object from database to retrieve date from.

        Return list with item names.
        """
        #attributes = obj.trunk.all()
        items = []
        try:
            it = obj.event_set.all()[0].items.all()
            for item in it:
                items.append(item.name)
        except:
            pass
        return items

    def prepare_name(self, obj):
        """ Prepare the attributes for any file attachments on the
        current page as specified in the M2M relationship.

        :param obj:
            Video object from database to retrieve date from.

        Return string with name.
        """
        try:
            name = obj.event_set.all()[0].name
        except:
            try:
                name = obj.title
                
            except:
                name = "Noname"
        return name

    def get_model(self):
        return Video

    def index_queryset(self):
        return self.get_model().objects.all()

class ItemIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    itemName = indexes.CharField(model_attr='name', faceted=True)
    ner = indexes.CharField(model_attr='ner', faceted=True)
    itemRoot = indexes.CharField(model_attr='root', faceted=True)
    occurrences = indexes.IntegerField(model_attr='occurrences', faceted=True)

    def get_model(self):
        return Item

    def index_queryset(self):
        return self.get_model().objects.all()


#class EventIndex(indexes.SearchIndex, indexes.Indexable):
#    text = indexes.CharField(document=True, use_template=True)
#    name= indexes.CharField(model_attr='name', faceted=True)
#    trunk = indexes.MultiValueField()
#    date = indexes.DateField(model_attr='date', faceted=True)
#    videos = indexes.MultiValueField()
#    items = indexes.MultiValueField()
#    #helper_words = models.ManyToManyField()
#    occurrences = indexes.PositiveIntegerField(model_attr='items', faceted=True)#

    #def prepare_trunk(self, obj):
     #   """ Prepare the attributes for any file attachments on the
     #    current page as specified in the M2M relationship. """
     #   #attributes = obj.trunk.all()
     #   return [str(v.root) for v in obj.trunk.all()]

    #def prepare_videos(self, obj):
    #    """ Prepare the attributes for any file attachments on the
    #     current page as specified in the M2M relationship. """
        #attributes = obj.trunk.all()
   #     for v in obj.videos.all():

    #    return vids

    #def get_model(self):
    #    return Event

   # def index_queryset(self):
    #    return self.get_model().objects.all()