=====
DINX
=====

DINX is an automatic channel generator. Create playlists of related videos, and update database. Provides TV user experience with VOD content.
Released by DINX LTD under BSD license. Dinx provides a simple access to quality video content. www.dinx.tv

Quick start
-----------

1. Add "dinx" to your INSTALLED_APPS setting like this::

      INSTALLED_APPS = (
          ...
          'dinx',
      )

2. Include the dinx URLconf in your project urls.py like this::

      url(r'^dinx/', include('dinx.urls')),

3. Run `python manage.py syncdb` to create the dinx models.

4. Start the development server and visit http://127.0.0.1:8000/admin/
   to create a user (you'll need the Admin app enabled).

5. Visit http://127.0.0.1:8000/ to participate in dinx.
